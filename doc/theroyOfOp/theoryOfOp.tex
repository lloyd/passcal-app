\documentclass{rev0Rep}
\usepackage{url}
\usepackage{amssymb}
\usepackage[section]{placeins}
\usepackage[singlelinecheck=false]{caption}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{longtable}
\usepackage{pdflscape}

\hypersetup{
  colorlinks   = true,    % Colours links instead of ugly boxes
  urlcolor     = blue,    % Colour for external hyperlinks
  linkcolor    = blue,    % Colour of internal links
  citecolor    = red      % Colour of citations
}
\begin{document}

\frontmatter

\reportnum{Theory of Operation}

\program{PASSCAL Field Application 1.X}

\title{Theory of Operation}

\subtitle{Phase 1 Version~2.0}

\date{September 2015}

\author{Matt Briggs}

\affiliation{rev0 Technologies\\
1415 El Segundo NE\\
Albuquerque, NM, 87113}

\coverart{iPodwPasscalLogo}

\reporttype{Report}

% \distribution{Distribution authorized to U.S. Government Agencies
% only; Test and Evaluation; November 2005.  Other requests should be
% referred to U.S. Army Engineer Research and Development Center}

%\additionalinfo{Supersedes ERDC/CREL AF-01-23}

\begin{abstract}

The theory of operation is a living document which serves at a high level to
explain the general operations and some of the inner workings of the
application.  Some specific topics covered by this document are:

\begin{itemize}
\item Traceability of data fields to source documentation
\item Input and output formats and connection information
\item High-level GUI flow
\item General distribution strategy
\end{itemize}

\end{abstract}

%\disclaimer{Some other disclaimer}

%\contractnum{Work Unit 33143}

%\monitoredby{U.S. Army Engineer Research and Development Center\\
%  3909 Halls Ferry Road, Vicksburg, MS 39180-6199}

\preparedfor{IRIS PASSCAL Instrument Center\\
New Mexico Tech\\
100 East Road, Socorro, NM 87801}

\maketitle

\tableofcontents

\listoffiguresandtables

\mainmatter

\chapter{Application Architecture}
\section {Overview}
The application's primary focus is to aid in the service / install / removal of
seismic stations.  The application will allow the collection of service
information to be easier, more extensive, and more accurate.  The application
will connect to and communicate with current and future field equipment using a
hand held device and be able to store the collected information. 

At this point of application development the application is limited to
communicating with a single model of DAS, the RefTek 130.  Communication is
facilitated by a WIFI bridge dongle, model number 130-WIFI.  In addition, to
limiting the DAS the application is limited to the iOS platform with specific
emphasis placed on iPad mini screen size.

From this WIFI interface the software will connect to the DAS and based off
user input will capture the majority of information through a sequence of
queries.  In later stages of the project the application may be extended to
control the parameters of the station but this feature is omitted from the
current design.

The queried information from both the DAS and the user is reviewed and 
persistently stored on the device until it is later exported.
These exported files may then be downloaded to user's laptop via lightning cable
in the case of newer generation iPads.

\begin {figure}[!ht]
\centering
    \includegraphics[width=\textwidth]{img/passcalAppDesign}
    \caption {Application Dataflow}
    \label {dataflow}
\end {figure}

\section {Hardware Connection}
\label{sec:hwConnection}
The hardware connection for this phase is focused on the 130-WIFI interface.
This interface bridges TCP packets to the serial interface on RefTek 130.  In
this interface the application acts as the client connecting to the 130-WiFi,
acting as a server, on port 2000.  The IP address of the 130-Wifi is always at
the base of the class-c subnet.  This has been tested on a handful of dongles
and this relationship has held all for all dongles tested (see Table~\ref{dongleTests}).
Once connected the interface follows the same binary format described in
\cite{130CmdRef} which one would expect to use with a serial connection.  The
TCP connection is done utilizing the NSStream\cite{iosNetworking} interface
available in iOS.

\begin{table*}[ht]
    % increase table row spacing, adjust to taste
    \renewcommand{\arraystretch}{1.3}
    \resizebox{\linewidth}{!}{
    \begin{tabular}{|c||c||c|}
    \hline
    Dongle Serial Number & Client IP Address & Dongle IP Address\\
    \hline
    0001 & 192.168.1.13 & 192.168.1.1\\
    \hline
    0009 & 192.168.4.11 & 192.168.4.1\\
    \hline
    0034 & 192.168.7.11 & 192.168.7.1\\
    \hline
    0075 & 192.168.7.11 & 192.168.7.1\\
    \hline
    0083 & 192.168.4.13 & 192.168.4.1\\
    \hline
    0139 & 192.168.2.11 & 192.168.2.1\\
    \hline
    0154 & 192.168.4.11 & 192.168.4.1\\
    \hline
    \end{tabular}}
    \caption{130-WiFi Dongle Test Summary}
    \label{dongleTests}
\end{table*}

The connection module is responsible for generating a set of query messages and
then parsing these messages into data objects.  These objects then can be reviewed via review GUI and based off a preloaded configuration file are checked in real-time.  At the same time these objects are passed to
the data store where they are then displayed back to the user via the review
GUI.  These interfaces follow a subscriber / publisher pattern to allow future
expansion and promote modularity.

\section {Model Data}
\label{sec:modelData}
The data models as shown in Figure \ref{dataflow} are stored in a common data
store.  The main model for storing data is made up of a handful of entities.
The primary interface to the data is the Entry entity which through
relationships links to the other entities.  These entities are depicted in
Figure~\ref{coreDataEntities}.  Each one of these entities
is under version control.  For more details on how version control is
managed see Chapter~\ref{sec:versionCtrl}.

\begin {figure}[!ht]
\centering
    \includegraphics[width=\textwidth]{img/coreDataEntities}
    \caption {Core Data Entities}
    \label {coreDataEntities}
\end {figure}

The currentEntry model has universal resource identifiers
(URIs) to entries within the model.  This is how the App keeps track
of GUI logic like which entry is the current collection entry and which 
entry is the current review entry.  The data in both these stores is
stored as quickly as possible to minimize possible data loss for any reason
(battery failure, application closure, etc).

The primary input for a single entry is a set of data objects derived from messages received via the hardware
connection (see section \ref{sec:hwConnection}).  These data objects are translated into data store entities.  In addition to input from the hardware connection there is input via user fillable fields.  This input is translated from input boxes directly into data entities.  These entities are used to update the entry in core data.

The data stored in core data entities can be accessed as individually or 
as one large entry via a dictionary key value pair representation.  
This dictionary of key value pairs is also how the data is provided
to review GUI interfaces and export functions.  This conversion helps decouple the model data from the view logic.  These keys are verbosely
listed in Appendix~\ref{dataKeysAppendix}.  These keys are further used
in both configuration files and import file interfaces.

Once the data is in the data store.  This data can be queried by the
review/edit GUI or export GUI by date, station name, and experiment name.

\chapter{Graphical User Interface (GUI)}
The graphical interface for this application is divided into 3 main flows.
Each flow does share some functionality with others but keeping them separate
allows for maximum user efficiency at the cost of some code reuse.  Each flow
is programmed using Apple's storyboard interface \cite{storyboardTutorial} for
master-detail view applications.  This interface will define and generate
skeleton code for view logic such as segues, buttons, and input boxes.  Given
the requirements outlined for this application native iOS GUI elements can be
utilized and no additional graphics work need be done.

\section {Collection GUI}
\label{sec:collectionGui}

The collection GUI starts by ensuring that a DAS can be reached by triggering
the hardware connection module to establish a TCP connection.  Once this
connection is established then the user upon activating the collect button will
trigger the application to send a series of queries which will gather all
automated data from the DAS in rapid succession.  The user then enters in the
few manual entries.  The user can then move to review or export flows.

\section {Review GUI}
\label{sec:reviewGui}

The review/edit flow allows the user to review and edit a single entry at a
time.  User is allowed to edit fields based off configuration file.

\section {Export GUI}
\label{sec:exportGui}

The export GUI allows the user to export a single station entry from the all entries
data store then pick a single format.  The user is also 
presented with an option to pick a custom filename or use the default.  This
data is then exported as a file where a USB connected laptop can retrieve the
data for further processing or archiving outside of the mobile application.  In addition, from the single entry export option it is possible to export the full data store as a single file.  For more information on either importing or
exporting see chapter~\ref{sec:importExport}

\section {Import GUI}
\label{sec:importGui}

The import GUI allows the user to import either the full data store or dasFieldsComplete.  For more information on either importing or
exporting see chapter~\ref{sec:importExport} 

\chapter{Data Revision Control}
\label{sec:versionCtrl}
Revision control is of chief concern for App design as any data loss
is unacceptable.  To meet this goal a strong commit philosophy
is used for GUI design so that is apparent to the user when their
changes are added to the data store and popup with a revision 
number is displayed.

Internally not only are commits handled atomically but all versions of
the entries are stored indefinitely.  This functionality is achieved
via two concepts; first is the nodeID.  This nodeID is generated everytime
a change is made to an entity.  This allows each change to have a unique
ID across both revisions and across devices.  Conceptually, one can view
an entry as simply a set of pointers to several unique nodeID entities.

The second concept which allows version management is the treeID which
provides a grouping within a set of nodeID entities.  This treeID is
inherited by all descendants of the original nodeIDs.  In fact the only
time new treeIDs are generated is when a user initiates a new collect via
the collect button.  In all other instances a treeID is copied from parent
and associated with a new nodeID.

As with most interactions with the datastore the Entry entity plays
a key role in operations.  As such whenever a change is made to a
referenced entity the Entry is also changed.  In this way simply
listing the entries of a given treeID gives a complete picture of
all revisions.  This property is exposed via revision history
functionality.  Here one can select one or more versions to be shown
via review GUI.  This allows one to revert to any revision committed.

\chapter{Import / Export Functionality}
\label{sec:importExport}
\section{Import}
Importing of files of both complete entry XML and full datastore.
Place the XML files to import into the import directory.  These
files are then listed in the import table.  A selected entries
file format is automatically detected based off the root tag of
the XML document.

Importing takes place by scanning an import file and adding any
new entities.  Each entity has a unique identifier called a nodeID.
This nodeID is assigned at creation and is guaranteed to be unique
even across devices.

After, all entities are placed in core data then relationships are
annotated between the Entry and its referenced entities.  The new review
entries list should now include any visible from imported list.  Note,
if both an entity existed on current device and the export device.  The
current devices version takes precedent.  This should only effect
changes to values such as isHidden as any data changes would result in
a new entity being created see Chapter~\ref{sec:versionCtrl} for more
details.  

\section{Export}
\subsection {Station XML - Not implemented}
The Station XML format is meant to be a subset of the full FDSN StationXML 
Schema\cite{FDSNSchema}.  This schema is optimized so that it is
possible for the application to generate it and require no extra inputs
from the user.

\begin {figure}[!ht]
\centering
    \includegraphics[width=\textwidth]{stationXML}
    \caption {station XML Diagram}
    \label {stationXMLDiagram}
\end {figure}

\subsection {DAS Fields Complete XML}
The DAS Fields Complete XML is intended to be a flat XML schema where all 
fields of a single entry are exported so that other
software can extract any parameter required.

\subsection {Full DataStore XML}
This format is an XML export of the entire devices data store.  This
file is intended to be used to backup, transfer, and import data
between devices.

\chapter{Configuration File}
\section{Overview}
The primary purpose of the configuration file is to allow flexible rules to be applied to data keys.  In addition, several display options are configurable
through this same interface.  This file is intended to be preloaded on to a iOS device
such that when the App is first launched the file is read and parsed.

Each key can have multiple rules applied to it.  In this case all rules must be
complied to for a field to be considered as correct.  A complete listing of keys
can be found in Appendix~\ref{dataKeysAppendix}.  A skeleton of a configuration file
can be found in Appendix~\ref{skeltonConfig}.

The configuration file should be named configRules.xml and placed in the
root directory of the App via iTunes file sharing.

\section{Simple Rules}
Simple rules have a rule key listed below and either 
have a very simple parameter or in the case of notEmptyRule
no parameters.
\subsection {Not Empty Rule}
\begin{table*}[ht]
\renewcommand{\arraystretch}{1.3}
\centering
\begin{tabular}{|c||c||c|}
\hline
Key & Parameters & Description\\
\hline
notEmptyRule & None & Check that a string field is not empty\\
\hline
\end{tabular}
\end{table*}

Note that whitespace does not count as content for a field.

\section{Regular Expression Rules}
\begin{table*}[ht]
\renewcommand{\arraystretch}{1.3}
\centering
\begin{tabular}{|c||c||c|}
\hline
Key & Parameters & Description\\
\hline
regexRule & None & Check that a string follows pattern\\
\hline
\end{tabular}
\end{table*}
The regexRule tags are embedded in the KeyConfiguration structure and placed within their respective keys.  The pattern string follows the a format found in NSRegularExpressions.  The format is fully documented in reference \cite{appleRegularExpression}. 

\section{Simplified Rule Language}
The simplified rule language supports basic comparisons and lets one perform
intervalue comparisons.  Several examples can be found in 
the following example files:

\begin{enumerate}
\item dasFieldsExample.xml
\end{enumerate}

\subsection{Identifiers}
Identifiers in this context may refer to one of two things first
if can correspond to the key itself with the keyword "self".  It
can also mean a reference to a different key as listed in
Appendix~\ref{dataKeysAppendix}.  Note that each identifier must 
be prefixed by a '\$'.

\subsection{Literal}
Literals for rules currently only refer to numbers such as integers
and floats.

\subsection{Operators}
Operators can fall into two categories; first, expression operators
 '<', '<=', '>', '>=', '==', and '!='.  Note when these operators
 are used in the xml file the '<' and '>' than must be escaped via
 '\&lt;' and '\&gt'.  The second kind of
operators are compound operators which combine expressions.  The
only two supported so far are 'and' and 'or'.  In addition,
expressions with compound operators may be grouped via parenthesizes.

\section{Display Rules}
\subsection{Display key}
\begin{table*}[ht]
\renewcommand{\arraystretch}{1.3}
\centering
\begin{tabular}{|c||c||c|}
\hline
Key & Parameters & Description\\
\hline
displayKey & String to be displayed & Specify display string for review GUI\\
\hline
\end{tabular}
\end{table*}

The display key tags are embedded in the KeyConfiguration structure and placed within their respective keys.  These keys allow for the displayed key to be different then the key used in data store.  Note the data store key can be found in Appendix~\ref{dataKeysAppendix}.

\subsection{Editable}
\begin{table*}[ht]
\renewcommand{\arraystretch}{1.3}
\centering
\begin{tabular}{|c||c||c|}
\hline
Key & Parameters & Description\\
\hline
Editable & None & If applied then a field is editable via Review GUI\\
\hline
\end{tabular}
\end{table*}
The editable tags are embedded in the KeyConfiguration structure and placed within their respective keys.  If the tag is present then the field is editable via the
review interface and indicated by a ">" at the right side of the cell.  By
default all fields are considered not editable.

\subsection{Exclude from Master}
\begin{table*}[ht]
\renewcommand{\arraystretch}{1.3}
\centering
\begin{tabular}{|c||c||c|}
\hline
Key & Parameters & Description\\
\hline
excludeFromMaster & None & If applied then a rules associated with this key \\
 & & do not effect master status\\
\hline
\end{tabular}
\end{table*}
The excludeMaster tags are embedded in the KeyConfiguration structure
 and placed with their respective keys.  If this key is present the correctness
 of a given field, as determined by its rules, will not effect master status. 

\subsection{Value Display format}
\begin{table*}[ht]
\renewcommand{\arraystretch}{1.3}
\centering
\begin{tabular}{|c||c||c|}
\hline
Key & Parameters & Description\\
\hline
valueDisplayFormat & format string & Specifies how a value is displayed in review GUI\\
\hline
\end{tabular}
\end{table*}

The display format tags are embedded in the KeyConfiguration structure and placed within their respective keys.  The format string follows the a format similar to C format strings.  The format is fully documented in reference \cite{appleStringFormat}. 

\section{Review Key Lists}
The review list keys allow the configuration of what keys are
displayed when a user clicks on predefined categories in the
review GUI. 

Under the key "ReviewStatusLists" the following keys should be listed:

\begin{enumerate}
\item stationFields
\item statusFields
\item gpsFields
\item sensorFields
\item versionFields
\item userFields
\end{enumerate}

Under each of these categories keys lists of the field keys which 
should be displayed in the review GUI should be listed newline
delimited text.

\chapter{Distribution}
Distribution in the current context of this document will discuss all of
the options which are available before submitting to the App Store.  In
general all of these options rely on an Apple Developer account supplied
by Rev0 Technologies.

\section {TestFlight}

TestFlight recently released with iOS 8 which designed to make beta
testing easier as a tester simply registers with the developer using an email
address and a tester can receive application and updates on their device for
testing.  Although, not originally intended for iOS 6.0 the web portal from
Safari is rumored to work for older devices.

This option is the best for integration and rapid development as it is easy
for the developer to make updates and the users to automatically download
updates.  The disadvantage of this option is the App must be submitted to
Apple for preliminary review and a stable Internet connection is required
for both the developer and tester.

\section {Distribution via Ad Hoc Provisioning}

Until the recent TestFlight option distribution was done via ad hoc
provisioning\cite{betaTestingIOS}.  Thus this method should be compatible with
iOS 6.0.  The advantage of this option is that there is no need to get an Apple
review and completely off-line application loads are possible.  The disadvantage is
the developer must register all test devices before compilation and the tester
has some extra steps before an application can be loaded.

\chapter{Development Tools}

\section{mogenerator}
mogenerator is a tool to generate much of the glue logic required to interface
with core data storage.  This tool is combined with RHManagedObject library
(included in source tree) to greatly simplify dealing with data objects.

\subsection{Install}
To install mogenerator simply download version from
http://rentzsch.github.io/mogenerator/ and install.  This should add
mogenerator to terminal path.  Then add an "Aggregate" target to project (note
should already be done in source tree).  Select new target and under Build
Phases add "New Run Script Build Phase".  Leave shell as /bin/sh and add
following:

\begin{lstlisting}[breaklines=true]
mogenerator -m "passcalApp_1_X/passcalApp_1_X.xcdatamodeld" -O "passcalApp_1_X/model" --template-var arc=true --base-class RHManagedObject
\end{lstlisting}  

Note for tool to work correctly ensure class attribute within core data tools
class method matches name of data object.

\subsection{Usage}
To use mogenerator simple select the build target and build.  This should
update "\_entity" files with new fields or create new "\_entity"
and "entity".

\bibliographystyle{IEEEtran}
\bibliography{theoryOfOp}

\appendix

\chapter{Version History}
\begin{table*}[ht]
% increase table row spacing, adjust to taste
\renewcommand{\arraystretch}{1.3}
\centering
\begin{tabular}{|c||c||c|}
\hline
Date & Version & Description\\
\hline
2015/09/29 & 2.0 & Release for various phases at end of September\\
\hline
2015/09/27 & 1.3 & Cleaned up documentation in prep for integration\\
\hline
2015/09/25 & 1.2 & All around update with major changes to import,
export, \\
 &  &  and revision control.\\
\hline
2015/09/21 & 1.1 & First cut at changes to cover phases A, 2, and versioning.\\
\hline
2015/07/31 & 1.0 & Final version for phase 1\\
\hline
2015/07/22 & 0.3 & Updated GUI screens and details based off design actuals\\
\hline
2015/07/16 & 0.2 & Updated field tables\\
\hline
2015/06/25 & 0.1 & Initial release for preliminary design review\\
\hline
\end{tabular}
\caption{Version History}
\label{versionHistory}
\end{table*}

\chapter{Skeleton Configuration}
\label{skeltonConfig}
\lstinputlisting[numbers=left,title=\lstname]{code/skeletonConfigRules.xml}

\begin{landscape}
\chapter{Data Keys Table}
\label{dataKeysAppendix}
\begin{longtable}[h]{lllll}
\hline \hline
\input{tables/Master_Fields_List.csv.tex}
\caption{Master Key Table}
\label{masterFieldTable}
\end{longtable}
\end{landscape}


\end{document}
