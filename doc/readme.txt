=============
== Read Me ==
=============

The directories each correspond to a latex document.  To compile said documents
it is recommended to use a TexLive distribution or use an online tool such as
texshare to compile these documents.

