#!/bin/bash

#inFile=$1 # Too Simple

for inFile in $@;
do
    outFile=${inFile}.csv
    cat $inFile | \
    sed -e '/.*\\hline.*/d' | \
    sed -e 's/[ ]*&[ ]*/,/g' | \
    sed -e 's/[ ]*\\\\//' |\
    sed -e 's/\\textgreater/>/g' \
    > $outFile

    echo "Created ${outFile}"
done
