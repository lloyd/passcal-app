#!/bin/bash

xmlFile='../theroyOfOp/code/dasFieldsExample.xml'
outputFile='../theroyOfOp/code/fieldKeys.txt'

entryFields=$(cat $xmlFile | sed -e 's/<\([^ >]*\)>.*/\1/' \
		| sort | grep -v "\." | grep -v "dasFieldsComplete$")
subFields=$(cat $xmlFile | sed -e 's/<\([^ >]*\)>.*/\1/' | sort | grep "\.")

echo "$entryFields" > $outputFile
echo "$subFields" >> $outputFile

echo "Done."
