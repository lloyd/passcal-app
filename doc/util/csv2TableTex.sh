#!/bin/bash

#inFile=$1 # Too Simple

# 1st sed change > to tex espace
# 2nd sed add hline between all lines
# 3rd sed change , to & for tex

for inFile in $@;
do
    outFile=${inFile}.tex
    cat $inFile | \
    sed -e 's/$//' | \
    sed -e 's/>/\\textgreater/g' | \
    sed -e 's/^.*$/& \\\\\n\\hline/' | \
    sed -e 's/,/ \& /g' \
    > $outFile

    echo "Created ${outFile}"
done
