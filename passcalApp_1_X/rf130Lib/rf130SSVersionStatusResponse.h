//
//  rf130SSVersionStatusResponse.h
//
// A concrete implementation of RF130 response

#import "response.h"

@interface rf130SSVersionStatusResponse : response

@property NSDate          *dasTime;
@property NSString        *cpuVersion;
@property NSInteger       boardCount;

@property NSArray *boardInfos;

@property BOOL validCrc;

- (id) initWithHeaderPayload : (rf130Header *) header : (uint8_t*) payloadBytes : (uint16_t) len;

@end

@interface rf130SSVersionStatusBoardInfo : NSObject

@property NSString *boardNumber;
@property NSString *boardRevision;
@property NSString *boardAcronym;
@property NSString *boardSerialNumber;
@property NSString *fpgaBoardNumber;
@property NSString *fpgaMinBrdRev;
@property NSString *fpgaVersion;

@end
