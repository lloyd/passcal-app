//
//  rf130SSSensorInfoStatus.h
//
// A concrete implementation of RF130 response

#import "response.h"

@interface rf130SSSensorInfoStatus : response

@property NSDate          *dasTime;
@property NSString        *manufacturer;
@property NSInteger       sensorNumber;
@property NSString        *model;
@property NSString        *serialNumber;
@property NSInteger       numComponents;

@property NSArray         *components;

@property BOOL validCrc;

- (id) initWithHeaderPayload : (rf130Header *) header : (uint8_t*) payloadBytes : (uint16_t) len;

@end

@interface rf130SSSensorInfoStatusComponentInfo : NSObject

@property NSString        *componentNumber;
@property NSString        *orientation;
@property NSString        *measurementUnits;
@property NSDecimalNumber *voltPerUnit;

@end