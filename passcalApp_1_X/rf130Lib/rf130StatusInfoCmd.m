//
//  rf130StatusInfoCmd.m
//  passcalApp_1_X
//
//  Created by matt on 7/3/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "rf130StatusInfoCmd.h"
#import "utils.h"

const int statusInfoCmdSize = 36;
const NSString *statusInfoCode = @"SS";

@implementation rf130StatusInfoCmd

- (id) initWithStatusType : (NSString *) statusType {
    if (!(self = [super init]))
        return nil;
    
    Byte dataBytes[statusInfoCmdSize];
    const NSString *cmdLength = @"0026";
    
    dataBytes[0]  = cmdAttnByte;
    dataBytes[1]  = 0x00; // Reserved
    
    // Copy 4 bytes of unitID
    [allUnitId getBytes:&(dataBytes[2]) maxLength:4 usedLength:NULL encoding:NSUTF8StringEncoding options:0 range:NSMakeRange(0, 4) remainingRange:NULL];
    
    // Copy 4 bytes of cmdLength
    [cmdLength getBytes:&(dataBytes[6]) maxLength:4 usedLength:NULL encoding:NSUTF8StringEncoding options:0 range:NSMakeRange(0, 4) remainingRange:NULL];
    
    // Copy 2 bytes of id
    [statusInfoCode getBytes:&(dataBytes[10]) maxLength:2 usedLength:NULL encoding:NSUTF8StringEncoding options:0 range:NSMakeRange(0, 2) remainingRange:NULL];
    
    // Copy 2 bytes of statusType
    [statusType getBytes:&(dataBytes[12]) maxLength:2 usedLength:NULL encoding:NSUTF8StringEncoding options:0 range:NSMakeRange(0, 2) remainingRange:NULL];
    
    // Copy 14 bytes of statusParameters
    // TODO At some point make this configurable
    NSString *statusParameters = @"              "; // 14 spcace <SP> chars
    [statusParameters getBytes:&(dataBytes[14]) maxLength:14 usedLength:NULL encoding:NSUTF8StringEncoding options:0 range:NSMakeRange(0, 15) remainingRange:NULL];
    
    // Copy 2 bytes of id
    [statusInfoCode getBytes:&(dataBytes[28]) maxLength:2 usedLength:NULL encoding:NSUTF8StringEncoding options:0 range:NSMakeRange(0, 2) remainingRange:NULL];
    
    NSString *ccrcStr = [utils calcCRCStr:&(dataBytes[2]) : sizeof(dataBytes)-(2+6)];
    
    // Copy 4 bytes of ccrc
    [ccrcStr getBytes:&(dataBytes[30]) maxLength:4 usedLength:NULL encoding:NSUTF8StringEncoding options:0 range:NSMakeRange(0, 4) remainingRange:NULL];
    
    dataBytes[34] = 0x0D; // <CR>
    dataBytes[35] = 0x0A; // <LF>
    
    // copy again with ccrc data
    data = [NSData dataWithBytes:dataBytes length:sizeof(dataBytes)];
    
    return self;
}

@end
