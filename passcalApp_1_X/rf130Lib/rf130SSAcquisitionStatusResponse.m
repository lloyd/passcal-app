//
//  rf130SSAcquisitionStatusResponse.m
//  passcalApp_1_X
//
//  Created by matt on 7/7/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "rf130SSAcquisitionStatusResponse.h"
#import "utils.h"

const int rf130SsAqPayloadSize = (68-12-2); // payload is defined as everything but header (12 bytes) and subCmd

@implementation rf130SSAcquisitionStatusResponse

- (id) initWithHeaderPayload : (rf130Header *) header :
(uint8_t*) payloadBytes : (uint16_t) len {
    if (!(self = [super init]))
        return nil;
    
    const int prefixSize = rf130HeaderSize+2; // header + subCmd
    
    if (len < rf130SsAqPayloadSize) // Not enough bytes
    {
        return nil;
    }
    
    self.header = header;
    
    NSString *asciiStr;
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[14-prefixSize]) length:18-1 // remove space
                                      encoding:NSUTF8StringEncoding];
    self.dasTime = [utils getRf130DateFromString:asciiStr];
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[32-prefixSize]) length:1
                                      encoding:NSUTF8StringEncoding];
    self.acquisitionRequested = [asciiStr boolValue];
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[33-prefixSize]) length:1
                                      encoding:NSUTF8StringEncoding];
    self.acquisitionActive = [asciiStr boolValue];
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[34-prefixSize]) length:6
                                      encoding:NSUTF8StringEncoding];
    self.eventCount = [asciiStr integerValue];
    
    // Weird it is 2 bytes
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[40-prefixSize]) length:2
                                      encoding:NSUTF8StringEncoding];
    self.eventInProgress = [asciiStr boolValue];
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[42-prefixSize]) length:6
                                      encoding:NSUTF8StringEncoding];
    self.ramTotal = [asciiStr integerValue];
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[48-prefixSize]) length:6
                                      encoding:NSUTF8StringEncoding];
    self.ramUsed = [asciiStr integerValue];
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[54-prefixSize]) length:6
                                      encoding:NSUTF8StringEncoding];
    self.ramAvailable = [asciiStr integerValue];
    
    return self;
}

@end
