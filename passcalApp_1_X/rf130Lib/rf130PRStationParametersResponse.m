//
//  rf130PRStationParametersResponse.m
//  passcalApp_1_X
//
//  Created by field on 7/15/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "rf130PRStationParametersResponse.h"

const int rf130PrPsMinPayloadSize = (154+2-12-2); // payload is defined as everything but header (12 bytes) and subCmd
                                                  // but includes record number

@implementation rf130PRStationParametersResponse

- (id) initWithHeaderPayload : (rf130Header *) header :
(uint8_t*) payloadBytes : (uint16_t) len {
    if (!(self = [super init]))
        return nil;
    
    const int prefixSize = rf130HeaderSize-2; // just header since documentation does not show
                                            // either subCmd or record number
    
    if (len < rf130PrPsMinPayloadSize) // Not enough bytes
    {
        return nil;
    }
    
    self.header = header;
    
    NSString *asciiStr;
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[12-prefixSize]) length:2
                                      encoding:NSUTF8StringEncoding];
    self.experimentNumber = [asciiStr integerValue];
    self.experimentName = [[NSString alloc] initWithBytes:&(payloadBytes[14-prefixSize]) length:24
                                                 encoding:NSUTF8StringEncoding];
    self.experimentComment = [[NSString alloc] initWithBytes:&(payloadBytes[38-prefixSize]) length:40
                                                 encoding:NSUTF8StringEncoding];
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[78-prefixSize]) length:4
                                      encoding:NSUTF8StringEncoding];
    self.stationNumber = [asciiStr integerValue];
    self.stationName = [[NSString alloc] initWithBytes:&(payloadBytes[82-prefixSize]) length:24
                                                 encoding:NSUTF8StringEncoding];
    self.stationComment = [[NSString alloc] initWithBytes:&(payloadBytes[106-prefixSize]) length:40
                                                    encoding:NSUTF8StringEncoding];
    return self;
}

@end
