//
//  rf130IdResponse.h
//
// A concrete implementation of RF130 response

#import <Foundation/Foundation.h>
#import "response.h"

@interface rf130IdResponse : response

- (id) initWithHeaderPayload : (rf130Header *) header : (uint8_t*) payloadBytes : (uint16_t) len;

@property NSString* cpuVersion;
@property BOOL validCrc;

@end
