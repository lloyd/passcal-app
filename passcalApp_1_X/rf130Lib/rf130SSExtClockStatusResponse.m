//
//  rf130SSExtClockStatusResponse.m
//  passcalApp_1_X
//
//  Created by matt on 7/7/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "rf130SSExtClockStatusResponse.h"
#import "utils.h"

const int rf130SsXcPayloadSize = (94-12-2); // payload is defined as everything but header (12 bytes) and subCmd

@implementation rf130SSExtClockStatusResponse

- (id) initWithHeaderPayload : (rf130Header *) header :
(uint8_t*) payloadBytes : (uint16_t) len {
    if (!(self = [super init]))
        return nil;
    
    const int prefixSize = rf130HeaderSize+2; // header + subCmd
    
    if (len < rf130SsXcPayloadSize) // Not enough bytes
    {
        return nil;
    }
    
    self.header = header;
    
    NSString *asciiStr;
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[14-prefixSize]) length:18-1 // remove space
                                      encoding:NSUTF8StringEncoding];
    self.dasTime = [utils getRf130DateFromString:asciiStr];
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[32-prefixSize]) length:8
                                           encoding:NSUTF8StringEncoding];
    long minsSinceLastLock = [rf130SSExtClockStatusResponse lastLock2mins:asciiStr];
    self.lastLock = minsSinceLastLock;
     
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[40-prefixSize]) length:11
                                      encoding:NSUTF8StringEncoding];
    self.lastLockPhase = [rf130SSExtClockStatusResponse convert2DecimalUSecs:asciiStr];
    
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[51-prefixSize]) length:1
                                      encoding:NSUTF8StringEncoding];
    self.lockStatus = [asciiStr boolValue];
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[52-prefixSize]) length:2
                                      encoding:NSUTF8StringEncoding];
    self.satellitesTracked = [asciiStr integerValue];
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[54-prefixSize]) length:12
                                      encoding:NSUTF8StringEncoding];
    self.latitude = [utils convertDeminalMin2DecimalDegrees:asciiStr];
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[66-prefixSize]) length:12
                                      encoding:NSUTF8StringEncoding];
    self.longitude = [utils convertDeminalMin2DecimalDegrees:asciiStr];
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[78-prefixSize]) length:6
                                      encoding:NSUTF8StringEncoding];
    self.altitude = [asciiStr integerValue];
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[84-prefixSize]) length:1
                                      encoding:NSUTF8StringEncoding];
    self.gpsAwake = [asciiStr boolValue];
    
    self.gpsMode = [[NSString alloc] initWithBytes:&(payloadBytes[85-prefixSize]) length:1
                                           encoding:NSUTF8StringEncoding];
    return self;
}

+ (NSDecimalNumber*) convert2DecimalUSecs : (NSString*) inFormat {
    NSString *asciiStr;
    char signChar;
    int sign;
    int sec;
    int msec;
    int usec;
    float decimalSec;
    
    signChar = [inFormat characterAtIndex:0];
    if (signChar == '-')
        sign = -1;
    else
        sign = 1;
    
    asciiStr = [inFormat substringWithRange:NSMakeRange(1, 2)];
    sec = [asciiStr intValue];
    
    asciiStr = [inFormat substringWithRange:NSMakeRange(4, 3)];
    msec = [asciiStr intValue];
    
    asciiStr = [inFormat substringWithRange:NSMakeRange(8, 3)];
    usec = [asciiStr intValue];
    
    decimalSec = sign*((1e6)*sec+(1.0e3)*msec+usec);
    
    return [[NSDecimalNumber alloc] initWithFloat:decimalSec];
}

+ (long) lastLock2mins : (NSString*) inFormat {
    NSString *asciiStr;
    int day;
    int hour;
    int min;
    
    asciiStr = [inFormat substringWithRange:NSMakeRange(0, 2)];
    day = [asciiStr intValue];
    
    asciiStr = [inFormat substringWithRange:NSMakeRange(3, 2)];
    hour = [asciiStr intValue];
    
    asciiStr = [inFormat substringWithRange:NSMakeRange(6, 2)];
    min = [asciiStr intValue];

    return min+60*hour+24*60*day;
}

@end
