//
//  rf130CmdFactory.m
//  passcalApp_1_X
//
//  Created by matt on 6/30/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "rf130CmdFactory.h"
#import "rf130SimpleCmd.h"
#import "rf130StatusInfoCmd.h"
#import "rf130ParameterRequestCmd.h"

@implementation rf130CmdFactory

+ (cmd *) createCmd : (NSString *) id {
  
    return [self createCmd:id : nil];  // Send a nil subCmd
}

+ (cmd *) createCmd : (NSString *) id : (NSString*) subCmd {
    
    if([id isEqualToString:@"ID"]) {
        return [[rf130SimpleCmd alloc] initWithId:id];
    }
    else if([id isEqualToString:@"SS"]) {
        if (subCmd == nil) // requires subCmd
            return nil;
        
        return [[rf130StatusInfoCmd alloc] initWithStatusType: subCmd];
    }
    else if([id isEqualToString:@"PR"]) {
        if (subCmd == nil) // requires subCmd
            return nil;
        
        return [[rf130ParameterRequestCmd alloc] initWithStatusType:subCmd];
    }
    
    return nil;
}

+ (cmd *) createCmd : (NSString *) id : (NSString *) subCmd : (NSInteger) param {
    if([id isEqualToString:@"PR"]) {
        if (subCmd == nil) // requires subCmd
            return nil;
        
        return [[rf130ParameterRequestCmd alloc] initWithStatusTypeRecordNum:subCmd:param];
    }
    return nil;
}

@end
