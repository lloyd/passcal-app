//
//  rf130SimpleCmd.m
//  passcalApp_1_X
//
//  Created by matt on 6/30/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "rf130SimpleCmd.h"
#import "utils.h"

const int simpleCmdSize = 20;
//const NSString *allUnitId = @"0000";

@implementation rf130SimpleCmd

- (id) initWithId : (NSString *) idStr {
    if (!(self = [super init]))
        return nil;
    
    Byte dataBytes[simpleCmdSize];
    const NSString *cmdLength = @"0010";
    
    dataBytes[0]  = cmdAttnByte;
    dataBytes[1]  = 0x00; // Reserved
    dataBytes[18] = 0x0D; // <CR>
    dataBytes[19] = 0x0A; // <LF>
    
    // Copy 4 bytes of unitID
    [allUnitId getBytes:&(dataBytes[2]) maxLength:4 usedLength:NULL encoding:NSUTF8StringEncoding options:0 range:NSMakeRange(0, 4) remainingRange:NULL];
    
    // Copy 4 bytes of cmdLength
    [cmdLength getBytes:&(dataBytes[6]) maxLength:4 usedLength:NULL encoding:NSUTF8StringEncoding options:0 range:NSMakeRange(0, 4) remainingRange:NULL];
    
    // Copy 2 bytes of id
    [idStr getBytes:&(dataBytes[10]) maxLength:2 usedLength:NULL encoding:NSUTF8StringEncoding options:0 range:NSMakeRange(0, 2) remainingRange:NULL];
    
    // Copy 2 bytes of id
    [idStr getBytes:&(dataBytes[12]) maxLength:2 usedLength:NULL encoding:NSUTF8StringEncoding options:0 range:NSMakeRange(0, 2) remainingRange:NULL];
    
    // copy to data member
    //data = [NSData dataWithBytes:dataBytes length:sizeof(dataBytes)];
    
    NSString *ccrcStr = [utils calcCRCStr:&(dataBytes[2]) : sizeof(dataBytes)-(2+6)];
    
    // Copy 4 bytes of ccrc
    [ccrcStr getBytes:&(dataBytes[14]) maxLength:4 usedLength:NULL encoding:NSUTF8StringEncoding options:0 range:NSMakeRange(0, 4) remainingRange:NULL];
    
    // copy again with ccrc data
    data = [NSData dataWithBytes:dataBytes length:sizeof(dataBytes)];
    
    return self;
}

@end
