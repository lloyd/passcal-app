//
//  rf130PRSensorAutoReCenter.h
//
// This class will be implemented in the future for Auto ReCenter
// TODO

#import "response.h"

@interface rf130PRSensorAutoReCenter : response

@property NSInteger sensor;
@property BOOL enable;
@property NSDecimalNumber *levelVertical;
@property NSInteger cycleInterval;
@property NSDecimalNumber *levelHorizontal;
@property NSInteger attempts;
@property NSInteger attemptInterval;

@property BOOL validCrc;

- (id) initWithHeaderPayload : (rf130Header *) header : (uint8_t*) payloadBytes : (uint16_t) len;

@end
