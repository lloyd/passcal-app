//
//  rf130SSExtClockStatusResponse.h
//
// A concrete implementation of RF130 response

#import "response.h"

@interface rf130SSExtClockStatusResponse : response

@property NSDate          *dasTime;
@property NSInteger        lastLock; // in mins
@property NSDecimalNumber *lastLockPhase; // in decimal uSecs
@property BOOL            lockStatus;
@property NSInteger       satellitesTracked;
@property NSDecimalNumber *latitude;
@property NSDecimalNumber *longitude;
@property NSInteger       altitude;
@property BOOL            gpsAwake;
@property NSString        *gpsMode;

@property BOOL validCrc;

- (id) initWithHeaderPayload : (rf130Header *) header : (uint8_t*) payloadBytes : (uint16_t) len;
+ (NSDecimalNumber*) convert2DecimalUSecs : (NSString*) inFormat;
+ (long) lastLock2mins : (NSString*) inFormat;

@end
