//
//  rf130SSDiskStatusResponse.m
//  passcalApp_1_X
//
//  Created by matt on 7/6/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "rf130SSDiskStatusResponse.h"
#import "utils.h"

// Seems like should be 80 rather than 78
const int rf130SsDkPayloadSize;

@implementation rf130SSDiskStatusResponse

- (BOOL) isValidCRC {
    
    return true;
}

- (id) initWithHeaderPayload : (rf130Header *) header :
(uint8_t*) payloadBytes : (uint16_t) len {
    if (!(self = [super init]))
        return nil;
    
    const int prefixSize = rf130HeaderSize+2; // header + subCmd
    
    if (len < rf130SsDkPayloadSize) // Not enough bytes
    {
        return nil;
    }
    
    self.header = header;
    
    NSString *asciiStr;
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[14-prefixSize]) length:18-1 // remove space
                                      encoding:NSUTF8StringEncoding];
    self.dasTime = [utils getRf130DateFromString:asciiStr];
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[32-prefixSize]) length:6
                                                     encoding:NSUTF8StringEncoding];
    self.disk1Total = [asciiStr integerValue];
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[38-prefixSize]) length:6
                                                     encoding:NSUTF8StringEncoding];
    self.disk1Used = [asciiStr integerValue];
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[44-prefixSize]) length:6
                                                     encoding:NSUTF8StringEncoding];
    self.disk1Avail = [asciiStr integerValue];
    
    //// Disk 2 ////
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[50-prefixSize]) length:6
                                                     encoding:NSUTF8StringEncoding];
    self.disk2Total = [asciiStr integerValue];
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[56-prefixSize]) length:6
                                                    encoding:NSUTF8StringEncoding];
    self.disk2Used = [asciiStr integerValue];
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[62-prefixSize]) length:6
                                                     encoding:NSUTF8StringEncoding];
    self.disk2Avail = [asciiStr integerValue];
    
    ////
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[68-prefixSize]) length:1
                                      encoding:NSUTF8StringEncoding];
    self.currentDisk = [asciiStr intValue];
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[69-prefixSize]) length:1
                                      encoding:NSUTF8StringEncoding];
    self.wrapEnabled = [asciiStr boolValue];
    
    // Note converting a hex string
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[70-prefixSize]) length:2
                                      encoding:NSUTF8StringEncoding];
    self.wrapCount = strtol([asciiStr cStringUsingEncoding:NSUTF8StringEncoding], NULL, 16);
    
    return self;
}

@end
