//
//  rf130SSParameterStatusResponse.h
//
// A concrete implementation of RF130 response

#import "response.h"

@interface rf130SSParameterStatusResponse : response

@property NSDate          *dasTime;
@property NSInteger       maxChannels;
@property NSInteger       maxDatastreams;
@property NSInteger       maxNetworkPorts;

@property NSString        *activeChannels;
@property NSString        *activeDatastreams;

@property BOOL validCrc;

- (id) initWithHeaderPayload : (rf130Header *) header : (uint8_t*) payloadBytes : (uint16_t) len;

@end
