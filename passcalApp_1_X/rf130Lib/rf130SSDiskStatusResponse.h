//
//  rf130SSDiskStatusResponse.h
//
// A concrete implementation of RF130 response

#import "response.h"

@interface rf130SSDiskStatusResponse : response

@property NSDate    *dasTime;
@property NSInteger disk1Total;
@property NSInteger disk1Used;
@property NSInteger disk1Avail;

@property NSInteger disk2Total;
@property NSInteger disk2Used;
@property NSInteger disk2Avail;

@property NSInteger currentDisk;
@property BOOL      wrapEnabled;
@property NSInteger wrapCount;

@property BOOL validCrc;

- (id) initWithHeaderPayload : (rf130Header *) header : (uint8_t*) payloadBytes : (uint16_t) len;

@end
