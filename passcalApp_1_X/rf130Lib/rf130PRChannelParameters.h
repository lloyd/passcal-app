//
//  rf130PRChannelParameters.h
//
// A concrete implementation of RF130 response

#import "response.h"

@interface rf130PRChannelParameters : response

@property NSInteger  chNum;
@property NSString  *chName;
@property NSString  *azimuth;
@property NSString  *incline;
@property NSString  *locationX;
@property NSString  *locationY;
@property NSString  *locationZ;
@property NSString  *unitsXY;
@property NSString  *unitsZ;
@property NSInteger  gain;
@property NSString  *sensorModel;
@property NSString  *sensorSerialNum;
@property NSString  *chComment;

@property BOOL validCrc;

- (id) initWithHeaderPayload : (rf130Header *) header : (uint8_t*) payloadBytes : (uint16_t) len;

@end
