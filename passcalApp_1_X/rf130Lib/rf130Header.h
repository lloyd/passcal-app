//
//  rf130Header.h
//  passcalApp_1_X
//
// This class is used by both cmd and responses to build or parse a
// RF130Header respectively.

#import <Foundation/Foundation.h>

extern int const rf130HeaderSize;
extern Byte const cmdAttn;
extern Byte const responseAttn;
extern Byte const resvDefault;

@interface rf130Header : NSObject

@property uint8_t attn;
@property uint8_t resv;
@property uint16_t unitID;
@property uint16_t len;
@property NSString* cmdCode;

- (NSData*) getData;
- (id) initWithBytes : (uint8_t *) inBytes : (uint16_t) len;
- (id) initWithCmdCode : (NSString*) cmdCode : (uint16_t) msgLen;

@end
