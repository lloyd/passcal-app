//
//  rf130SSSensorInfoStatus.m
//  passcalApp_1_X
//
//  Created by field on 7/14/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "rf130SSSensorInfoStatus.h"
#import "utils.h"

const int rf130SsSiMinPayloadSize = (100-12-2); // payload is defined as everything but header (12 bytes) and subCmd

@implementation rf130SSSensorInfoStatusComponentInfo
// Intentially empty
@end

@implementation rf130SSSensorInfoStatus

- (id) initWithHeaderPayload : (rf130Header *) header :
(uint8_t*) payloadBytes : (uint16_t) len {
    if (!(self = [super init]))
        return nil;
    
    const int prefixSize = rf130HeaderSize+2; // header + subCmd
    
    if (len < rf130SsSiMinPayloadSize) // Not enough bytes
    {
        return nil;
    }
    
    self.header = header;
    
    NSString *asciiStr;
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[14-prefixSize]) length:18-1 // remove space
                                      encoding:NSUTF8StringEncoding];
    self.dasTime = [utils getRf130DateFromString:asciiStr];
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[32-prefixSize]) length:1
                                      encoding:NSUTF8StringEncoding];
    self.sensorNumber = [asciiStr integerValue];
    
    self.manufacturer = [[NSString alloc] initWithBytes:&(payloadBytes[34-prefixSize]) length:16
                                             encoding:NSUTF8StringEncoding];
    
    self.model = [[NSString alloc] initWithBytes:&(payloadBytes[64-prefixSize]) length:16
                                               encoding:NSUTF8StringEncoding];
    
    self.serialNumber = [[NSString alloc] initWithBytes:&(payloadBytes[78-prefixSize]) length:16
                                               encoding:NSUTF8StringEncoding];
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[92-prefixSize]) length:2
                                      encoding:NSUTF8StringEncoding];
    self.numComponents = [asciiStr integerValue];
    
    const int compInfoOffset = 94-prefixSize;
    const int compInfoSize = 30; // Bytes
    
    NSMutableArray *compInfos = [[NSMutableArray alloc] init];
    for (int i = 0; i < self.numComponents;i++)
    {
        rf130SSSensorInfoStatusComponentInfo *cInfo = [[rf130SSSensorInfoStatusComponentInfo alloc] init];
        // Skip component number
        cInfo.orientation = [[NSString alloc] initWithBytes:&(payloadBytes[2+compInfoOffset+i*compInfoSize])
                                                     length:4 encoding:NSUTF8StringEncoding];
        cInfo.measurementUnits = [[NSString alloc] initWithBytes:&(payloadBytes[6+compInfoOffset+i*compInfoSize])
                                                       length:10 encoding:NSUTF8StringEncoding];
        asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[16-prefixSize]) length:14
                                          encoding:NSUTF8StringEncoding];
        cInfo.voltPerUnit = [[NSDecimalNumber alloc] initWithString: asciiStr];
        [compInfos addObject:cInfo];
    }
    self.components = [[NSArray alloc] initWithArray:compInfos];

    return self;
}

@end
