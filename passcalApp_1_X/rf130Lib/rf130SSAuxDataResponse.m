//
//  rf130SSAuxDataResponse.m
//  passcalApp_1_X
//
//  Created by field on 7/24/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "rf130SSAuxDataResponse.h"
#import "utils.h"

const int rf130SsAdMinPayloadSize = (40-12-2); // payload is defined as everything but header (12 bytes) and subCmd

@implementation rf130SSAuxDataResponse
- (id) initWithHeaderPayload : (rf130Header *) header :
             (uint8_t*) payloadBytes : (uint16_t) len {
    if (!(self = [super init]))
        return nil;
    
    const int prefixSize = rf130HeaderSize+2; // header + subCmd
    
    if (len < rf130SsAdMinPayloadSize) // Not enough bytes
    {
        return nil;
    }
    
    self.header = header;
    
    NSString *asciiStr;
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[14-prefixSize]) length:18-1 // remove space
                                      encoding:NSUTF8StringEncoding];
    self.dasTime = [utils getRf130DateFromString:asciiStr];
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[32-prefixSize]) length:1
                                      encoding:NSUTF8StringEncoding];
    self.sensorCount = [asciiStr integerValue];
    
    const int auxSensorOffset = 34-prefixSize;
    const int auxSensorInfoSize = 30; // Bytes
    
    NSMutableArray *auxInfos = [[NSMutableArray alloc] init];
    for (int i=0;i<self.sensorCount;i++)
    {
        rf130SSAuxSensorInfo *auxSensorInfo = [[rf130SSAuxSensorInfo alloc] init];
        asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[0+auxSensorOffset+i*auxSensorInfoSize]) length:1
                                          encoding:NSUTF8StringEncoding];
        auxSensorInfo.sensorNum = [asciiStr integerValue];
        asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[2+auxSensorOffset+i*auxSensorInfoSize]) length:6
                                          encoding:NSUTF8StringEncoding];
        auxSensorInfo.count = [asciiStr integerValue];
        asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[8+auxSensorOffset+i*auxSensorInfoSize]) length:6
                                          encoding:NSUTF8StringEncoding];
        auxSensorInfo.countLimit = [asciiStr integerValue];
        asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[14+auxSensorOffset+i*auxSensorInfoSize]) length:4
                                          encoding:NSUTF8StringEncoding];
        auxSensorInfo.level = [[NSDecimalNumber alloc] initWithString: asciiStr];
        asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[18+auxSensorOffset+i*auxSensorInfoSize]) length:4
                                          encoding:NSUTF8StringEncoding];
        auxSensorInfo.aux1Data = [[NSDecimalNumber alloc] initWithString: asciiStr];
        asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[22+auxSensorOffset+i*auxSensorInfoSize]) length:4
                                          encoding:NSUTF8StringEncoding];
        auxSensorInfo.aux2Data = [[NSDecimalNumber alloc] initWithString: asciiStr];
        asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[26+auxSensorOffset+i*auxSensorInfoSize]) length:4
                                          encoding:NSUTF8StringEncoding];
        auxSensorInfo.aux3Data = [[NSDecimalNumber alloc] initWithString: asciiStr];
        [auxInfos addObject:auxSensorInfo];
    }
    self.auxSensorInfos = [[NSArray alloc] initWithArray:auxInfos];
    
    return self;
}
@end

@implementation rf130SSAuxSensorInfo

@end
