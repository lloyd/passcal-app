//
//  rf130ResponseFactory.h
//
// This factory generates all supported responses.  Given a buffer and size this class
// will split messages, parse the header/msg, check the CRC, a create an appropriate concrete
// reponse class.

#import <Foundation/Foundation.h>
#import "response.h"

@interface rf130ResponseFactory : NSObject

// createResponse
// Returns a concrete message if valid message inside, nil if CRC failed, or NSNull
// if buffer runs out before the end of message found.
+ (response*) createResponse : (uint8_t*) inBuffer : (uint16_t) len;

@end
