//
//  rf130Header.m
//  passcalApp_1_X
//
//  Created by matt on 7/2/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "rf130Header.h"
#include "utils.h"

int const rf130HeaderSize = 12;
Byte const cmdAttn = 0x84;
Byte const responseAttn = 0x84;
Byte const resvDefault = 0x00;

@implementation rf130Header

- (NSData*) getData{
    Byte dataBytes[rf130HeaderSize];
    //Byte* dataBytes = ;
    
    dataBytes[0]  = self.attn;
    dataBytes[1]  = self.resv; // Reserved
    
    // Create 4 ASCII characters of unitID.
    // Note in base 16
    NSString *unitId = [NSString stringWithFormat:@"%04X", self.unitID];
    NSData *unitIdData = [unitId dataUsingEncoding:NSUTF8StringEncoding];
    memcpy(&(dataBytes[2]), [unitIdData bytes], 4);
    
    // Create 4 ASCII characters of length.
    // Note base 10 in ASCII
    NSString *len = [NSString stringWithFormat:@"%04d", self.len];
    NSData *lenData = [len dataUsingEncoding:NSUTF8StringEncoding];
    memcpy(&(dataBytes[6]), [lenData bytes], 4);
    
    // Copy 2 ASCII character command code to header
    NSData *cmdCodeData = [self.cmdCode dataUsingEncoding:NSUTF8StringEncoding];
    memcpy(&(dataBytes[10]), [cmdCodeData bytes], 2);
    
    NSData *rtnData= [NSData dataWithBytes:dataBytes length:sizeof(dataBytes)];
    return rtnData;
    
}- (id) initWithBytes : (uint8_t *) inBytes : (uint16_t) len{
    if (!(self = [super init]) || len < rf130HeaderSize)
        return nil;
    
    self.attn = inBytes[0];
    self.resv = inBytes[1]; // Reserved
    
    
    NSString *unitIDStr = [[NSString alloc] initWithBytes:&(inBytes[2]) length:4
                                                 encoding:NSUTF8StringEncoding];
    self.unitID = [utils stringToHex:unitIDStr];
    
    NSString *lenStr = [[NSString alloc] initWithBytes:&(inBytes[6]) length:4
                                              encoding:NSUTF8StringEncoding];
    self.len = [lenStr integerValue];
    self.cmdCode = [[NSString alloc] initWithBytes:&(inBytes[10]) length:2
                                          encoding:NSUTF8StringEncoding];
    return self;
}

- (id) initWithCmdCode : (NSString*) cmdCode : (uint16_t) msgLen {
    if (!(self = [super init]))
        return nil;
    
    self.attn = cmdAttn;
    self.resv = resvDefault; // Reserved
    self.unitID = 0; // Assume default
    self.len = msgLen;
    self.cmdCode = cmdCode;
    return self;
}
@end
