//
//  rf130PRAuxiliaryDataParamsResponse.h
//
// A concrete implementation of RF130 response

#import "response.h"

@interface rf130PRAuxiliaryDataParamsResponse : response

@property NSInteger marker;
@property NSString *channels;
@property NSInteger samplePeriod;
@property NSInteger dataFormat;
@property NSInteger recordLength;
@property NSString *recordingDst;

@property NSString *differentialCtrl;

@property BOOL validCrc;

- (id) initWithHeaderPayload : (rf130Header *) header : (uint8_t*) payloadBytes : (uint16_t) len;

@end
