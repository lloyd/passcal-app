//
//  rf130SSUnitStatusResponse.h
//
// A concrete implementation of RF130 response

#import "response.h"

@interface rf130SSUnitStatusResponse : response

@property NSDate          *dasTime;
@property NSDecimalNumber *inputPower;
@property NSDecimalNumber *backupPower;
@property NSDecimalNumber *temperature;
@property NSDecimalNumber *chargerPower;

@property BOOL validCrc;

- (id) initWithHeaderPayload : (rf130Header *) header : (uint8_t*) payloadBytes : (uint16_t) len;

@end
