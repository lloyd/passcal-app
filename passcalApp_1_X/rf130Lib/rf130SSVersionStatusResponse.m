//
//  rf130SSVersionStatusResponse.m
//  passcalApp_1_X
//
//  Created by matt on 7/7/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "rf130SSVersionStatusResponse.h"
#import "utils.h"

const int rf130SsVsMinPayloadSize = (50-12-2); // payload is defined as everything but header (12 bytes) and subCmd

@implementation rf130SSVersionStatusBoardInfo
// Intentially empty
@end

@implementation rf130SSVersionStatusResponse

- (id) initWithHeaderPayload : (rf130Header *) header :
(uint8_t*) payloadBytes : (uint16_t) len {
    if (!(self = [super init]))
        return nil;
    
    const int prefixSize = rf130HeaderSize+2; // header + subCmd
    
    if (len < rf130SsVsMinPayloadSize) // Not enough bytes
    {
        return nil;
    }
    
    self.header = header;
    
    NSString *asciiStr;
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[14-prefixSize]) length:18-1 // remove space
                                      encoding:NSUTF8StringEncoding];
    self.dasTime = [utils getRf130DateFromString:asciiStr];
    
    self.cpuVersion = [[NSString alloc] initWithBytes:&(payloadBytes[32-prefixSize]) length:16
                                          encoding:NSUTF8StringEncoding];
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[48-prefixSize]) length:2
                                      encoding:NSUTF8StringEncoding];
    self.boardCount = [asciiStr integerValue];
    
    const int boardInfoOffset = 50-prefixSize;
    const int boardInfoSize = 20; // Bytes
    
    NSMutableArray *boardInfos = [[NSMutableArray alloc] init];
    for (int i = 0; i < self.boardCount;i++)
    {
        rf130SSVersionStatusBoardInfo *bInfo = [[rf130SSVersionStatusBoardInfo alloc] init];
        bInfo.boardNumber = [[NSString alloc] initWithBytes:&(payloadBytes[0+boardInfoOffset+i*boardInfoSize])
                                                     length:4 encoding:NSUTF8StringEncoding];
        bInfo.boardRevision = [[NSString alloc] initWithBytes:&(payloadBytes[4+boardInfoOffset+i*boardInfoSize])
                                                     length:1 encoding:NSUTF8StringEncoding];
        bInfo.boardAcronym = [[NSString alloc] initWithBytes:&(payloadBytes[5+boardInfoOffset+i*boardInfoSize])
                                                     length:3 encoding:NSUTF8StringEncoding];
        bInfo.boardSerialNumber = [[NSString alloc] initWithBytes:&(payloadBytes[8+boardInfoOffset+i*boardInfoSize])
                                                     length:4 encoding:NSUTF8StringEncoding];
        bInfo.fpgaBoardNumber = [[NSString alloc] initWithBytes:&(payloadBytes[12+boardInfoOffset+i*boardInfoSize])
                                                     length:4 encoding:NSUTF8StringEncoding];
        bInfo.fpgaMinBrdRev = [[NSString alloc] initWithBytes:&(payloadBytes[16+boardInfoOffset+i*boardInfoSize])
                                                     length:1 encoding:NSUTF8StringEncoding];
        bInfo.fpgaVersion = [[NSString alloc] initWithBytes:&(payloadBytes[17+boardInfoOffset+i*boardInfoSize])
                                                     length:3 encoding:NSUTF8StringEncoding];
        [boardInfos addObject:bInfo];
    }
    self.boardInfos = [[NSArray alloc] initWithArray:boardInfos];
    
    return self;
}

@end
