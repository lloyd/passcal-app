//
//  rf130SSAuxDataResponse.h
//
// A concrete implementation of RF130 response

#import "response.h"

@interface rf130SSAuxDataResponse : response
@property NSDate    *dasTime;
@property NSInteger sensorCount;

@property NSArray *auxSensorInfos;

- (id) initWithHeaderPayload : (rf130Header *) header :
              (uint8_t*) payloadBytes : (uint16_t) len;

@property BOOL validCrc;

@end

@interface rf130SSAuxSensorInfo : NSObject
@property NSInteger sensorNum;
@property NSInteger count;
@property NSInteger countLimit;
@property NSDecimalNumber *level;
@property NSDecimalNumber *aux1Data;
@property NSDecimalNumber *aux2Data;
@property NSDecimalNumber *aux3Data;

@end