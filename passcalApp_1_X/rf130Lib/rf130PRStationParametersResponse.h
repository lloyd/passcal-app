//
//  rf130PRStationParametersResponse.h
//
// A concrete implementation of RF130 response

#import "response.h"

@interface rf130PRStationParametersResponse : response

@property NSInteger experimentNumber;
@property NSString  *experimentName;
@property NSString  *experimentComment;

@property NSInteger stationNumber;
@property NSString  *stationName;
@property NSString  *stationComment;

@property BOOL validCrc;

- (id) initWithHeaderPayload : (rf130Header *) header : (uint8_t*) payloadBytes : (uint16_t) len;

@end
