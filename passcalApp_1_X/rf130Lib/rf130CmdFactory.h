//
//  rf130CmdFactory.h
//
// This factory creates concrete commands given a RF130 ID, sub cmd, and param.  These
// commands follow the cmd interface so that the inner data is read-only except
// for child classes.

#import <Foundation/Foundation.h>
#import "cmd.h"

@interface rf130CmdFactory : NSObject

+ (cmd *) createCmd : (NSString *) id;
+ (cmd *) createCmd : (NSString *) id : (NSString *) subCmd;
+ (cmd *) createCmd : (NSString *) id : (NSString *) subCmd : (NSInteger) param;

@end
