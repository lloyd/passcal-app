//
//  rf130IdResponse.m
//  passcalApp_1_X
//
//  Created by matt on 7/3/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "rf130IdResponse.h"
#import "utils.h"

const int rf130IdPayloadSize = 16; // payload is defined as everything but header

@implementation rf130IdResponse

- (id) initWithHeaderPayload : (rf130Header *) header :
                               (uint8_t*) payloadBytes : (uint16_t) len {
    if (!(self = [super init]))
        return nil;
    
    if (len < rf130IdPayloadSize) // Not enough bytes
    {
        return nil;
    }
    
    self.header = header;
    
    self.cpuVersion = [[NSString alloc] initWithBytes:payloadBytes length:8
                                                 encoding:NSUTF8StringEncoding];
    return self;
}


@end
