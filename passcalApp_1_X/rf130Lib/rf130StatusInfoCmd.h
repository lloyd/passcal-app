//
//  rf130StatusInfoCmd.h
//
// Concrete class which implements cmd interface.

#import "cmd.h"

@interface rf130StatusInfoCmd : cmd

- (id) initWithStatusType : (NSString *) statusType;

@end
