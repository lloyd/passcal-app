//
//  rf130SSUnitStatusResponse.m
//  passcalApp_1_X
//
//  Created by matt on 7/7/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "rf130SSUnitStatusResponse.h"
#import "utils.h"

const int rf130SsDkPayloadSize = (58-12-2); // payload is defined as everything but header (12 bytes) and subCmd

@implementation rf130SSUnitStatusResponse

- (id) initWithHeaderPayload : (rf130Header *) header :
(uint8_t*) payloadBytes : (uint16_t) len {
    if (!(self = [super init]))
        return nil;
    
    const int prefixSize = rf130HeaderSize+2; // header + subCmd
    
    if (len < rf130SsDkPayloadSize) // Not enough bytes
    {
        return nil;
    }
    
    self.header = header;

    NSString *asciiStr;
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[14-prefixSize]) length:18-1 // remove space
                                      encoding:NSUTF8StringEncoding];
    self.dasTime = [utils getRf130DateFromString:asciiStr];
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[32-prefixSize]) length:4
                                      encoding:NSUTF8StringEncoding];
    self.inputPower = [[NSDecimalNumber alloc] initWithString: asciiStr];
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[36-prefixSize]) length:4
                                      encoding:NSUTF8StringEncoding];
    self.backupPower = [[NSDecimalNumber alloc] initWithString: asciiStr];
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[40-prefixSize]) length:6
                                      encoding:NSUTF8StringEncoding];
    self.temperature = [[NSDecimalNumber alloc] initWithString: asciiStr];
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[46-prefixSize]) length:4
                                      encoding:NSUTF8StringEncoding];
    self.chargerPower = [[NSDecimalNumber alloc] initWithString: asciiStr];
    
    return self;
}

@end
