//
//  rf130PRAuxiliaryDataParamsResponse.m
//  passcalApp_1_X
//
//  Created by field on 7/15/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "rf130PRAuxiliaryDataParamsResponse.h"

const int rf130PrPaMinPayloadSize = (72+2-12-2); // payload is defined as everything but header (12 bytes) and subCmd
                                                  // but includes record number

@implementation rf130PRAuxiliaryDataParamsResponse

- (id) initWithHeaderPayload : (rf130Header *) header :
(uint8_t*) payloadBytes : (uint16_t) len {
    if (!(self = [super init]))
        return nil;
    
    const int prefixSize = rf130HeaderSize; // just header since documentation does not show
    // either subCmd or record number
    
    if (len < rf130PrPaMinPayloadSize) // Not enough bytes
    {
        return nil;
    }
    
    self.header = header;
    
    NSString *asciiStr;
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[12-prefixSize]) length:2
                                      encoding:NSUTF8StringEncoding];
    self.marker = [asciiStr integerValue];
    self.channels = [[NSString alloc] initWithBytes:&(payloadBytes[14-prefixSize]) length:16
                                         encoding:NSUTF8StringEncoding];
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[30-prefixSize]) length:8
                                      encoding:NSUTF8StringEncoding];
    self.samplePeriod = [asciiStr integerValue];
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[38-prefixSize]) length:2
                                      encoding:NSUTF8StringEncoding];
    self.dataFormat = [asciiStr integerValue];
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[40-prefixSize]) length:8
                                      encoding:NSUTF8StringEncoding];
    self.recordLength = [asciiStr integerValue];
    self.recordingDst = [[NSString alloc] initWithBytes:&(payloadBytes[48-prefixSize]) length:4
                                           encoding:NSUTF8StringEncoding];
    self.differentialCtrl = [[NSString alloc] initWithBytes:&(payloadBytes[56-prefixSize]) length:16
                                           encoding:NSUTF8StringEncoding];    
    return self;
}

@end
