//
//  rf130SSAcquisitionStatusResponse.h
//
// A concrete implementation of RF130 response

#import "response.h"

@interface rf130SSAcquisitionStatusResponse : response

@property NSDate    *dasTime;
@property BOOL      acquisitionRequested;
@property BOOL      acquisitionActive;
@property NSInteger eventCount;
@property BOOL      eventInProgress;

@property NSInteger ramTotal;
@property NSInteger ramUsed;
@property NSInteger ramAvailable;

@property BOOL validCrc;

- (id) initWithHeaderPayload : (rf130Header *) header : (uint8_t*) payloadBytes : (uint16_t) len;

@end
