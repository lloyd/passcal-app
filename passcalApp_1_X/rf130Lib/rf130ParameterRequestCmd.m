//
//  rf130ParameterRequestCmd.m
//  passcalApp_1_X
//
//  Created by field on 7/14/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "rf130ParameterRequestCmd.h"
#import "utils.h"

const int parameterRequestCmdSize = 24;
const NSString *parameterRequestCode = @"PR";

@implementation rf130ParameterRequestCmd

- (id) initWithStatusTypeRecordNum : (NSString *) statusType : (NSInteger) recordNum {
    
    if (!(self = [super init]))
        return nil;
    
    Byte dataBytes[parameterRequestCmdSize];
    const NSString *cmdLength = @"0014"; //24 - 10
    
    dataBytes[0]  = cmdAttnByte;
    dataBytes[1]  = 0x00; // Reserved
    
    // Copy 4 bytes of unitID
    [allUnitId getBytes:&(dataBytes[2]) maxLength:4 usedLength:NULL encoding:NSUTF8StringEncoding options:0 range:NSMakeRange(0, 4) remainingRange:NULL];
    
    // Copy 4 bytes of cmdLength
    [cmdLength getBytes:&(dataBytes[6]) maxLength:4 usedLength:NULL encoding:NSUTF8StringEncoding options:0 range:NSMakeRange(0, 4) remainingRange:NULL];
    
    // Copy 2 bytes of id
    [parameterRequestCode getBytes:&(dataBytes[10]) maxLength:2 usedLength:NULL encoding:NSUTF8StringEncoding options:0 range:NSMakeRange(0, 2) remainingRange:NULL];
    
    // Copy 2 bytes of statusType
    [statusType getBytes:&(dataBytes[12]) maxLength:2 usedLength:NULL encoding:NSUTF8StringEncoding options:0 range:NSMakeRange(0, 2) remainingRange:NULL];
    
    // Copy 2 bytes of record number
    NSString *statusParameters = [NSString stringWithFormat:@"%02d", (int)recordNum];
    [statusParameters getBytes:&(dataBytes[14]) maxLength:2 usedLength:NULL encoding:NSUTF8StringEncoding options:0 range:NSMakeRange(0, 2) remainingRange:NULL];
    
    // Copy 2 bytes of id
    [parameterRequestCode getBytes:&(dataBytes[16]) maxLength:2 usedLength:NULL encoding:NSUTF8StringEncoding options:0 range:NSMakeRange(0, 2) remainingRange:NULL];
    
    NSString *ccrcStr = [utils calcCRCStr:&(dataBytes[2]) : sizeof(dataBytes)-(2+6)];
    
    // Copy 4 bytes of ccrc
    [ccrcStr getBytes:&(dataBytes[18]) maxLength:4 usedLength:NULL encoding:NSUTF8StringEncoding options:0 range:NSMakeRange(0, 4) remainingRange:NULL];
    
    dataBytes[22] = 0x0D; // <CR>
    dataBytes[23] = 0x0A; // <LF>
    
    // copy again with ccrc data
    data = [NSData dataWithBytes:dataBytes length:sizeof(dataBytes)];
    
    return self;
}

- (id) initWithStatusType : (NSString *) statusType {
    if (!(self = [super init]))
        return nil;
    
    Byte dataBytes[parameterRequestCmdSize];
    const NSString *cmdLength = @"0014"; //24 - 10
    
    dataBytes[0]  = cmdAttnByte;
    dataBytes[1]  = 0x00; // Reserved
    
    // Copy 4 bytes of unitID
    [allUnitId getBytes:&(dataBytes[2]) maxLength:4 usedLength:NULL encoding:NSUTF8StringEncoding options:0 range:NSMakeRange(0, 4) remainingRange:NULL];
    
    // Copy 4 bytes of cmdLength
    [cmdLength getBytes:&(dataBytes[6]) maxLength:4 usedLength:NULL encoding:NSUTF8StringEncoding options:0 range:NSMakeRange(0, 4) remainingRange:NULL];
    
    // Copy 2 bytes of id
    [parameterRequestCode getBytes:&(dataBytes[10]) maxLength:2 usedLength:NULL encoding:NSUTF8StringEncoding options:0 range:NSMakeRange(0, 2) remainingRange:NULL];
    
    // Copy 2 bytes of statusType
    [statusType getBytes:&(dataBytes[12]) maxLength:2 usedLength:NULL encoding:NSUTF8StringEncoding options:0 range:NSMakeRange(0, 2) remainingRange:NULL];
    
    // Copy 2 bytes of record number
    NSString *statusParameters = @"  ";
    [statusParameters getBytes:&(dataBytes[14]) maxLength:2 usedLength:NULL encoding:NSUTF8StringEncoding options:0 range:NSMakeRange(0, 2) remainingRange:NULL];
    
    // Copy 2 bytes of id
    [parameterRequestCode getBytes:&(dataBytes[16]) maxLength:2 usedLength:NULL encoding:NSUTF8StringEncoding options:0 range:NSMakeRange(0, 2) remainingRange:NULL];
    
    NSString *ccrcStr = [utils calcCRCStr:&(dataBytes[2]) : sizeof(dataBytes)-(2+6)];
    
    // Copy 4 bytes of ccrc
    [ccrcStr getBytes:&(dataBytes[18]) maxLength:4 usedLength:NULL encoding:NSUTF8StringEncoding options:0 range:NSMakeRange(0, 4) remainingRange:NULL];
    
    dataBytes[22] = 0x0D; // <CR>
    dataBytes[23] = 0x0A; // <LF>
    
    // copy again with ccrc data
    data = [NSData dataWithBytes:dataBytes length:sizeof(dataBytes)];
    
    return self;
}

- (id) initWithStatusTypeAllRec : (NSString *) statusType {
    if (!(self = [super init]))
        return nil;
    
    Byte dataBytes[parameterRequestCmdSize];
    const NSString *cmdLength = @"0014"; //24 - 10
    
    dataBytes[0]  = cmdAttnByte;
    dataBytes[1]  = 0x00; // Reserved
    
    // Copy 4 bytes of unitID
    [allUnitId getBytes:&(dataBytes[2]) maxLength:4 usedLength:NULL encoding:NSUTF8StringEncoding options:0 range:NSMakeRange(0, 4) remainingRange:NULL];
    
    // Copy 4 bytes of cmdLength
    [cmdLength getBytes:&(dataBytes[6]) maxLength:4 usedLength:NULL encoding:NSUTF8StringEncoding options:0 range:NSMakeRange(0, 4) remainingRange:NULL];
    
    // Copy 2 bytes of id
    [parameterRequestCode getBytes:&(dataBytes[10]) maxLength:2 usedLength:NULL encoding:NSUTF8StringEncoding options:0 range:NSMakeRange(0, 2) remainingRange:NULL];
    
    // Copy 2 bytes of statusType
    [statusType getBytes:&(dataBytes[12]) maxLength:2 usedLength:NULL encoding:NSUTF8StringEncoding options:0 range:NSMakeRange(0, 2) remainingRange:NULL];
    
    // Copy 2 bytes of record number
    NSString *statusParameters = @"**";
    [statusParameters getBytes:&(dataBytes[14]) maxLength:2 usedLength:NULL encoding:NSUTF8StringEncoding options:0 range:NSMakeRange(0, 2) remainingRange:NULL];
    
    // Copy 2 bytes of id
    [parameterRequestCode getBytes:&(dataBytes[16]) maxLength:2 usedLength:NULL encoding:NSUTF8StringEncoding options:0 range:NSMakeRange(0, 2) remainingRange:NULL];
    
    NSString *ccrcStr = [utils calcCRCStr:&(dataBytes[2]) : sizeof(dataBytes)-(2+6)];
    
    // Copy 4 bytes of ccrc
    [ccrcStr getBytes:&(dataBytes[18]) maxLength:4 usedLength:NULL encoding:NSUTF8StringEncoding options:0 range:NSMakeRange(0, 4) remainingRange:NULL];
    
    dataBytes[22] = 0x0D; // <CR>
    dataBytes[23] = 0x0A; // <LF>
    
    // copy again with ccrc data
    data = [NSData dataWithBytes:dataBytes length:sizeof(dataBytes)];
    
    return self;
}

@end
