//
//  rf130PRChannelParameters.m
//  passcalApp_1_X
//
//  Created by field on 7/30/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "rf130PRChannelParameters.h"

const int rf130PrPcMinPayloadSize = (158+2-12-2); // payload is defined as everything but header (12 bytes) and subCmd
                                                  // but includes record number

@implementation rf130PRChannelParameters

- (id) initWithHeaderPayload : (rf130Header *) header :
(uint8_t*) payloadBytes : (uint16_t) len {
    if (!(self = [super init]))
        return nil;
    
    const int prefixSize = rf130HeaderSize; // just header since documentation does not show
                                            // either subCmd or record number
    
    if (len < rf130PrPcMinPayloadSize) // Not enough bytes
    {
        return nil;
    }
    
    self.header = header;
    
    NSString *asciiStr;
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[12-prefixSize]) length:2
                                      encoding:NSUTF8StringEncoding];
    self.chNum = [asciiStr integerValue];
    self.chName = [[NSString alloc] initWithBytes:&(payloadBytes[14-prefixSize]) length:10
                                             encoding:NSUTF8StringEncoding];
    self.azimuth = [[NSString alloc] initWithBytes:&(payloadBytes[24-prefixSize]) length:10
                                               encoding:NSUTF8StringEncoding];
    self.incline = [[NSString alloc] initWithBytes:&(payloadBytes[34-prefixSize]) length:10
                                          encoding:NSUTF8StringEncoding];
    self.locationX = [[NSString alloc] initWithBytes:&(payloadBytes[44-prefixSize]) length:10
                                           encoding:NSUTF8StringEncoding];
    self.locationY = [[NSString alloc] initWithBytes:&(payloadBytes[54-prefixSize]) length:10
                                            encoding:NSUTF8StringEncoding];
    self.locationZ = [[NSString alloc] initWithBytes:&(payloadBytes[64-prefixSize]) length:10
                                            encoding:NSUTF8StringEncoding];
    self.unitsXY = [[NSString alloc] initWithBytes:&(payloadBytes[74-prefixSize]) length:4
                                            encoding:NSUTF8StringEncoding];
    self.unitsZ = [[NSString alloc] initWithBytes:&(payloadBytes[78-prefixSize]) length:4
                                            encoding:NSUTF8StringEncoding];
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[82-prefixSize]) length:4
                                      encoding:NSUTF8StringEncoding];
    self.gain = [asciiStr integerValue];
    self.sensorModel = [[NSString alloc] initWithBytes:&(payloadBytes[86-prefixSize]) length:12
                                             encoding:NSUTF8StringEncoding];
    self.sensorSerialNum = [[NSString alloc] initWithBytes:&(payloadBytes[98-prefixSize]) length:12
                                           encoding:NSUTF8StringEncoding];
    self.chComment = [[NSString alloc] initWithBytes:&(payloadBytes[110-prefixSize]) length:40
                                                  encoding:NSUTF8StringEncoding];
    
    return self;
}
@end
