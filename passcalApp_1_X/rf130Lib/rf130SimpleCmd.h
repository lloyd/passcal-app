//
//  rf130SimpleCmd.h
//
// Concrete class which implements cmd interface.

#import <Foundation/Foundation.h>
#import "cmd.h"

@interface rf130SimpleCmd : cmd

- (id) initWithId : (NSString *) idStr;


@end
