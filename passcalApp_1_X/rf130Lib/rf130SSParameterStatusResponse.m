//
//  rf130SSParameterStatusResponse.m
//  passcalApp_1_X
//
//  Created by field on 7/14/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "rf130SSParameterStatusResponse.h"
#import "utils.h"

const int rf130SsPrMinPayloadSize = (46-12-2); // payload is defined as everything but header (12 bytes) and subCmd

@implementation rf130SSParameterStatusResponse

- (id) initWithHeaderPayload : (rf130Header *) header :
(uint8_t*) payloadBytes : (uint16_t) len {
    if (!(self = [super init]))
        return nil;
    
    const int prefixSize = rf130HeaderSize+2; // header + subCmd
    
    if (len < rf130SsPrMinPayloadSize) // Not enough bytes
    {
        return nil;
    }
    
    self.header = header;
    
    NSString *asciiStr;
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[14-prefixSize]) length:18-1 // remove space
                                      encoding:NSUTF8StringEncoding];
    self.dasTime = [utils getRf130DateFromString:asciiStr];
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[32-prefixSize]) length:2
                                      encoding:NSUTF8StringEncoding];
    self.maxChannels = [asciiStr integerValue];
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[34-prefixSize]) length:1
                                      encoding:NSUTF8StringEncoding];
    self.maxDatastreams = [asciiStr integerValue];
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[35-prefixSize]) length:1
                                      encoding:NSUTF8StringEncoding];
    self.maxNetworkPorts = [asciiStr integerValue];
    
    self.activeChannels = [[NSString alloc] initWithBytes:&(payloadBytes[40-prefixSize]) length:self.maxChannels
                                             encoding:NSUTF8StringEncoding];
    self.activeDatastreams = [[NSString alloc] initWithBytes:&(payloadBytes[self.maxChannels+40-prefixSize])
                                                   length:self.maxDatastreams encoding:NSUTF8StringEncoding];
    return self;
}

@end
