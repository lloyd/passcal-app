//
//  rf130PRDataStreamParamsResponse.h
//
// A concrete implementation of RF130 response

#import "response.h"

@interface rf130PRDataStreamParamsResponse : response

@property NSInteger streamNumber;
@property NSString  *streamName;
@property NSString  *recordingDst;
@property NSString  *channels;
@property NSInteger sampleRate;
@property NSString  *dataFormat;
@property NSString  *trigType;

// TODO add full trigger descip

@property BOOL validCrc;

- (id) initWithHeaderPayload : (rf130Header *) header : (uint8_t*) payloadBytes : (uint16_t) len;

@end
