//
//  rf130PRSensorAutoReCenter.m
//  passcalApp_1_X
//
//  Created by field on 7/24/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "rf130PRSensorAutoReCenter.h"

const int rf130PRSensorAutoReCenterSize = (34-12-2); // payload is defined as everything but header (12 bytes) and subCmd
                                                     // but includes record number

@implementation rf130PRSensorAutoReCenter

- (id) initWithHeaderPayload : (rf130Header *) header :
(uint8_t*) payloadBytes : (uint16_t) len {
    if (!(self = [super init]))
        return nil;
    
    const int prefixSize = rf130HeaderSize;
    
    if (len < rf130PRSensorAutoReCenterSize) // Not enough bytes
    {
        return nil;
    }
    
    self.header = header;
    
    NSString *asciiStr;
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[12-prefixSize]) length:1
                                      encoding:NSUTF8StringEncoding];
    self.sensor = [asciiStr integerValue];
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[13-prefixSize]) length:1
                                      encoding:NSUTF8StringEncoding];
    if([asciiStr rangeOfString:@" "].location != NSNotFound) // false if space otherwise true
        self.enable = false;
    else
        self.enable = true;
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[14-prefixSize]) length:4
                                      encoding:NSUTF8StringEncoding];
    self.levelVertical = [[NSDecimalNumber alloc] initWithString: asciiStr];
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[18-prefixSize]) length:2
                                      encoding:NSUTF8StringEncoding];
    self.cycleInterval = [asciiStr integerValue];

    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[20-prefixSize]) length:4
                                      encoding:NSUTF8StringEncoding];
    self.levelHorizontal = [[NSDecimalNumber alloc] initWithString: asciiStr];
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[24-prefixSize]) length:2
                                      encoding:NSUTF8StringEncoding];
    self.attempts = [asciiStr integerValue];
    
    asciiStr = [[NSString alloc] initWithBytes:&(payloadBytes[26-prefixSize]) length:2
                                      encoding:NSUTF8StringEncoding];
    self.attemptInterval = [asciiStr integerValue];
    
    return self;
}

@end
