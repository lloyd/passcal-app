//
//  rf130ParameterRequestCmd.h
//
// Concrete class which implements cmd interface.

#import "cmd.h"

@interface rf130ParameterRequestCmd : cmd

- (id) initWithStatusType : (NSString *) statusType;
- (id) initWithStatusTypeRecordNum : (NSString *) statusType : (NSInteger) recordNum;
- (id) initWithStatusTypeAllRec : (NSString *) statusType;

@end
