//
//  rf130ResponseFactory.m
//  passcalApp_1_X
//
//  Created by matt on 7/2/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "rf130ResponseFactory.h"
#import "utils.h"

#import "rf130IdResponse.h"
#import "rf130SSDiskStatusResponse.h"
#import "rf130SSAcquisitionStatusResponse.h"
#import "rf130SSUnitStatusResponse.h"
#import "rf130SSExtClockStatusResponse.h"
#import "rf130SSVersionStatusResponse.h"
#import "rf130SSParameterStatusResponse.h"
#import "rf130SSSensorInfoStatus.h"
#import "rf130PRStationParametersResponse.h"
#import "rf130PRDataStreamParamsResponse.h"
#import "rf130SSAuxDataResponse.h"
#import "rf130PRSensorAutoReCenter.h"
#import "rf130PRChannelParameters.h"
#import "rf130PRAuxiliaryDataParamsResponse.h"

#import "CocoaLumberjack.h"
static const DDLogLevel ddLogLevel = DDLogLevelInfo;

const int rf130SubCmdSize = 2;

@interface rf130ResponseFactory()

+ (NSString*) createSubCmd : (uint8_t*) inBuffer : (uint16_t) len;
+ (uint16_t) findEndOfRf130Msg : (uint8_t*) inBuffer : (uint16_t) len;

@end

@implementation rf130ResponseFactory

+ (NSString*) createSubCmd : (uint8_t*) inBuffer : (uint16_t) len {
    if (len < rf130SubCmdSize)
        return nil;
    return [[NSString alloc] initWithBytes:inBuffer length:rf130SubCmdSize
                                encoding:NSUTF8StringEncoding];
}

+ (BOOL) doResponseCRCCheck : (uint8_t*) inBuffer : (uint16_t) len {
    NSString *checkCRC = [utils calcCRCStr:&(inBuffer[2]) : len-2-responseSuffix];
    NSString *payloadCRC = [[NSString alloc]
                            initWithBytes:&(inBuffer[len-responseSuffix])
                            length:4
                            encoding:NSUTF8StringEncoding];
    return [checkCRC isEqualToString:payloadCRC];
}

+ (uint16_t) findEndOfRf130Msg : (uint8_t*) inBuffer : (uint16_t) len {
    // Search for <cr><lf>
    const uint8_t firstChar = 0x0D; // <CR>
    const uint8_t secondChar = 0x0A; // <LF>
    
    uint16_t idx = rf130HeaderSize; // No valud message can be less than header size
    while (idx < (len-1)) // Checking two chars at a time
    {
        if (inBuffer[idx] == firstChar && inBuffer[idx+1] == secondChar)
            return idx+2; // char after <LF> i.e. msgLen
        else
            idx++;
    }
    
    return 0; // Msg end not found
    
}

+ (response*) createResponse : (uint8_t*) inBuffer : (uint16_t) len {
    
    if (len < rf130HeaderSize)
        return nil;
    // parse header
    rf130Header *header = [[rf130Header alloc] initWithBytes:inBuffer :rf130HeaderSize];
    
    uint16_t msgLen = [self findEndOfRf130Msg:inBuffer : len];
    if (msgLen == 0)
    {
        DDLogInfo(@"message not found!!!");
        return (response*)[NSNull null]; // Quick way to let caller no that there is no message in bytes
    }
    BOOL validCrc = [self doResponseCRCCheck : inBuffer : msgLen];
    if (!validCrc)
    {
        DDLogWarn(@"Bad CRC Found!!!");
        return nil;
    }
    
    if ([header.cmdCode isEqualToString:@"ID"])
    {
        rf130IdResponse *response = [[rf130IdResponse alloc] initWithHeaderPayload:
                                    header :
                                    (uint8_t *)&(inBuffer[rf130HeaderSize]) :
                                    msgLen-rf130HeaderSize];
        response.rawMessageSize = msgLen;
        response.validCrc = validCrc;
        return response;
    }

    if ([header.cmdCode isEqualToString:@"SS"])
    {
        NSString *subCmd = [self createSubCmd: (uint8_t *)&(inBuffer[rf130HeaderSize]) :msgLen-rf130HeaderSize];
        if ([subCmd isEqualToString:@"AD"])
        {
            rf130SSAuxDataResponse *response = [[rf130SSAuxDataResponse alloc] initWithHeaderPayload:
                                                   header:
                                                   (uint8_t *)&(inBuffer[rf130HeaderSize+rf130SubCmdSize]) :
                                                   msgLen-rf130HeaderSize-rf130SubCmdSize];
            response.rawMessageSize = msgLen;
            response.validCrc = validCrc;
            return response;
        }
        if ([subCmd isEqualToString:@"DK"])
        {
            rf130SSDiskStatusResponse *response = [[rf130SSDiskStatusResponse alloc] initWithHeaderPayload:
                                                                                                    header:
                                                   (uint8_t *)&(inBuffer[rf130HeaderSize+rf130SubCmdSize]) :
                                                   msgLen-rf130HeaderSize-rf130SubCmdSize];
            response.rawMessageSize = msgLen;
            response.validCrc = validCrc;
            return response;
        }
        if ([subCmd isEqualToString:@"AQ"])
        {
            rf130SSAcquisitionStatusResponse *response = [[rf130SSAcquisitionStatusResponse alloc] initWithHeaderPayload:
                                                    header:
                                                   (uint8_t *)&(inBuffer[rf130HeaderSize+rf130SubCmdSize]) :
                                                   msgLen-rf130HeaderSize-rf130SubCmdSize];
            response.rawMessageSize = msgLen;
            response.validCrc = validCrc;
            return response;
        }
        if ([subCmd isEqualToString:@"US"])
        {
            rf130SSUnitStatusResponse *response = [[rf130SSUnitStatusResponse alloc] initWithHeaderPayload:
                                                   header:
                                                   (uint8_t *)&(inBuffer[rf130HeaderSize+rf130SubCmdSize]) :
                                                   msgLen-rf130HeaderSize-rf130SubCmdSize];
            response.rawMessageSize = msgLen;
            response.validCrc = validCrc;
            return response;
        }
        if ([subCmd isEqualToString:@"VS"])
        {
            rf130SSVersionStatusResponse *response = [[rf130SSVersionStatusResponse alloc] initWithHeaderPayload:
                                                      header:
                                                      (uint8_t *)&(inBuffer[rf130HeaderSize+rf130SubCmdSize]) :
                                                      msgLen-rf130HeaderSize-rf130SubCmdSize];
            response.rawMessageSize = msgLen;
            response.validCrc = validCrc;
            return response;
        }
        if ([subCmd isEqualToString:@"XC"])
        {
            rf130SSExtClockStatusResponse *response = [[rf130SSExtClockStatusResponse alloc] initWithHeaderPayload:
                                                       header:
                                                       (uint8_t *)&(inBuffer[rf130HeaderSize+rf130SubCmdSize]) :
                                                       msgLen-rf130HeaderSize-rf130SubCmdSize];
            response.rawMessageSize = msgLen;
            response.validCrc = validCrc;
            return response;
        }
        
        if ([subCmd isEqualToString:@"PR"])
        {
            rf130SSParameterStatusResponse *response = [[rf130SSParameterStatusResponse alloc] initWithHeaderPayload:
                                                        header:
                                                       (uint8_t *)&(inBuffer[rf130HeaderSize+rf130SubCmdSize]) :
                                                       msgLen-rf130HeaderSize-rf130SubCmdSize];
            response.rawMessageSize = msgLen;
            response.validCrc = validCrc;
            return response;
        }
        if ([subCmd isEqualToString:@"SI"])
        {
            rf130SSSensorInfoStatus *response = [[rf130SSSensorInfoStatus alloc] initWithHeaderPayload:
                                                 header:
                                                 (uint8_t *)&(inBuffer[rf130HeaderSize+rf130SubCmdSize]) :
                                                 msgLen-rf130HeaderSize-rf130SubCmdSize];
            response.rawMessageSize = msgLen;
            response.validCrc = validCrc;
            return response;
        }
    }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    if ([header.cmdCode isEqualToString:@"PR"])
    {
        NSString *subCmd = [self createSubCmd: (uint8_t *)&(inBuffer[rf130HeaderSize]) :msgLen-rf130HeaderSize];
        if ([subCmd isEqualToString:@"PS"])
        {
            rf130PRStationParametersResponse *response = [[rf130PRStationParametersResponse alloc] initWithHeaderPayload:
                                                         header:
                                                         (uint8_t *)&(inBuffer[rf130HeaderSize+rf130SubCmdSize]) :
                                                         msgLen-rf130HeaderSize-rf130SubCmdSize];
            response.rawMessageSize = msgLen;
            response.validCrc = validCrc;
            return response;
        }
        if ([subCmd isEqualToString:@"PC"])
        {
            rf130PRChannelParameters *response = [[rf130PRChannelParameters alloc] initWithHeaderPayload:
                                                        header:
                                                        (uint8_t *)&(inBuffer[rf130HeaderSize+rf130SubCmdSize]) :
                                                        msgLen-rf130HeaderSize-rf130SubCmdSize];
            response.rawMessageSize = msgLen;
            response.validCrc = validCrc;
            return response;
        }
        if ([subCmd isEqualToString:@"PD"])
        {
            rf130PRDataStreamParamsResponse *response = [[rf130PRDataStreamParamsResponse alloc] initWithHeaderPayload:
                                                         header:
                                                         (uint8_t *)&(inBuffer[rf130HeaderSize+rf130SubCmdSize]) :
                                                         msgLen-rf130HeaderSize-rf130SubCmdSize];
            response.rawMessageSize = msgLen;
            response.validCrc = validCrc;
            return response;
        }
        if ([subCmd isEqualToString:@"PQ"])
        {
            rf130PRSensorAutoReCenter *response = [[rf130PRSensorAutoReCenter alloc] initWithHeaderPayload:
                                                         header:
                                                         (uint8_t *)&(inBuffer[rf130HeaderSize+rf130SubCmdSize]) :
                                                         msgLen-rf130HeaderSize-rf130SubCmdSize];
            response.rawMessageSize = msgLen;
            response.validCrc = validCrc;
            return response;
        }
        if ([subCmd isEqualToString:@"PA"]) // Not currently queried
        {
            rf130PRAuxiliaryDataParamsResponse *response = [[rf130PRAuxiliaryDataParamsResponse alloc] initWithHeaderPayload:
                                                   header:
                                                   (uint8_t *)&(inBuffer[rf130HeaderSize+rf130SubCmdSize]) :
                                                   msgLen-rf130HeaderSize-rf130SubCmdSize];
            response.rawMessageSize = msgLen;
            response.validCrc = validCrc;
            return response;
        }
    }

    return nil;
}

@end
