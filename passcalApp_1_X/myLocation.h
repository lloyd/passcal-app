//
//  myLocation.h
//  passcalApp_1_X
//
//  Created by field on 9/26/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreLocation;

@interface myLocation : NSObject<CLLocationManagerDelegate>

@property CLLocationManager *manager;
@property BOOL validGps;
@property float lat;
@property float lon;
@property float altitude;
@property float hAccuracy;
@property float vAccuracy;

-(id) init;

-(void) start;
-(void) stop;

@end
