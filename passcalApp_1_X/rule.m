//
//  rule.m
//  passcalApp_1_X
//
//  Created by field on 9/14/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "rule.h"

#import "CocoaLumberjack.h"
static const DDLogLevel ddLogLevel = DDLogLevelInfo;

@implementation simpleRule
-(id) init: (id) value {
    // TODO check if things can be compared
    
    return nil;
}

-(BOOL) evalRule : (NSString*) key : (NSDictionary *) deepDict{
    return false;
}

@end

@implementation simpleRuleWithRef

 -(id) initWithLeftLiteral: (floatLiteral*) leftOperand : (identifier*) rightOperand : (enum ruleExprOpsEnum) operation{
    if (!(self = [super init]))
        return nil;
    
    self.ruleLevel = error;
    self.ruleType = leftLiteralType;
    self.operation = operation;
    self.literal = leftOperand;
    self.ident1 = rightOperand;
     
    return self;
}

-(id) initWithRightLiteral: (identifier*) leftOperand : (floatLiteral*) rightOperand : (enum ruleExprOpsEnum) operation{
    if (!(self = [super init]))
        return nil;
    
    self.ruleLevel = error;
    self.ruleType = rightLiteralType;
    self.operation = operation;
    self.ident1 = leftOperand;
    self.literal = rightOperand;
    
    return self;
}

-(id) initWithTwoIdentifiers: (identifier*) leftOperand : (identifier*) rightOperand : (enum ruleExprOpsEnum) operation{
    if (!(self = [super init]))
        return nil;
    
    self.ruleLevel = error;
    self.ruleType = twoIdentifiersType;
    self.operation = operation;
    self.ident1 = leftOperand;
    self.ident2 = rightOperand;
    
    return self;
}

-(BOOL) evalRule : (NSString*) key : (NSDictionary *) deepDict {
    floatLiteral *leftOp;
    floatLiteral *rightOp;
    switch (self.ruleType) {
        case leftLiteralType:
            leftOp = self.literal;
            rightOp = [ruleHelper getLiteralFromIdentifer:self.ident1:key:deepDict];
            break;
        case rightLiteralType:
            leftOp = [ruleHelper getLiteralFromIdentifer:self.ident1:key:deepDict];
            rightOp = self.literal;
            break;
        case twoIdentifiersType:
            leftOp = [ruleHelper getLiteralFromIdentifer:self.ident1:key:deepDict];
            rightOp = [ruleHelper getLiteralFromIdentifer:self.ident2:key:deepDict];
            break;
        default:
            // TODO log error
            break;
    }
    
    // TODO handle if either left or right is nil
    
    return [ruleHelper evalExpr:leftOp:rightOp:self.operation];
}

@end

@implementation compoundRuleWithRefs
-(id) init {
    if (!(self = [super init]))
        return nil;
    self.ruleLevel = error;
    self.rules = [[NSMutableArray alloc] init];
    self.compOps = [[NSMutableArray alloc] init];
    return self;
}
-(void) addRule : (id<rule>) newRule {
    [self.rules addObject:newRule];
}
-(void) addCompOp : (enum ruleCompoundOpsEnum) newOp {
    NSValue *val = [NSValue value: &newOp withObjCType: @encode(enum ruleCompoundOpsEnum)];
    [self.compOps addObject:val];
}

-(BOOL) evalRule : (NSString*) key : (NSDictionary *) deepDict {
    BOOL rtnVal = true;
    id<rule> rule1, rule2;
    NSValue *op;
    enum ruleCompoundOpsEnum compOp;
    
    // Check that compound rule is setup properly (prevents index out of bounds error)
    if ([self.rules count] != ([self.compOps count]+1)){
        DDLogError(@"Error unbalenced rules\n");
        return false;
    }
    
    for (int opIdx=0;opIdx<[self.compOps count];opIdx++) {
        rule1 = self.rules[opIdx];
        rule2 = self.rules[opIdx+1]; // safe since above size condition tested
        
        // Convert stored op back to enum
        op = self.compOps[opIdx];
        [op getValue:&compOp];
        
        rtnVal = rtnVal && [ruleHelper evalComp:[rule1 evalRule:key :deepDict] :
                            [rule2 evalRule:key :deepDict] : compOp];
    }
    
    return rtnVal;
}

@end
@implementation notEmptyRule

-(id) init {
    if (!(self = [super init]))
        return nil;
    self.ruleLevel = error;
    
    return self;
}

-(BOOL) evalRule : (NSString*) key : (NSDictionary *) deepDict {
    if (deepDict[key]==nil) {
        DDLogError(@"Error key not found");
        return false;
    }
    if ([deepDict[key] isKindOfClass: [NSNull class]])
        return false;
    else if ([deepDict[key] isKindOfClass: [NSString class]]) {
        NSString *str = deepDict[key];
        if ([str rangeOfString:@"\\S+" options:NSRegularExpressionSearch].location == NSNotFound)
            return false;
    }
    return true;
}
@end

@implementation regexRule

-(id) initWithPattern: (NSString*) pattern {
    if (!(self = [super init]))
        return nil;
    self.ruleLevel = error;
    self.pattern = pattern;
    
    return self;
}

-(BOOL) evalRule : (NSString*) key : (NSDictionary *) deepDict {
    if (deepDict[key]==nil) {
        DDLogError(@"Error key not found");
        return false;
    }

    if ([deepDict[key] isKindOfClass: [NSString class]]) {
        NSString *str = deepDict[key];
        if ([str rangeOfString:self.pattern options:NSRegularExpressionSearch].location != NSNotFound)
            return true; // Pattern not found
        else
            return false; // Pattern not found
    }
    else if ([deepDict[key] isKindOfClass: [NSNull class]]) {
        return false; // Nothing set
    }
    else {
        DDLogError(@"RegExRules only valid for strings");
        return false;
    }
}
@end

@implementation ruleHelper

+(floatLiteral*) getLiteralFromIdentifer: (identifier*) ident : (NSString*) key : (NSDictionary *) deepDict{
    if ([ident isEqualToString:@"self"]) {
        return deepDict[key];
    }
    else {
        id rtn = deepDict[ident];
        if (!rtn) {
            DDLogError(@"Identifier: \"%@\" not found during rule eval", ident);
            return nil;
        }
        else if (![rtn isKindOfClass:[floatLiteral class]]) {
            DDLogError(@"Encountered unhandled type at for key: \"%@\" during rule eval", ident);
            return nil;
        }
        else
            return deepDict[ident];
    }
}

+(BOOL) evalExpr : (floatLiteral*) left : (floatLiteral *) right : (enum ruleExprOpsEnum) operation {
    // Bad input
    if (!left || !right)
        return false;
    
    if (![left isKindOfClass: [floatLiteral class]] ||
        ![right isKindOfClass: [floatLiteral class]])
          return false;
    
    NSComparisonResult result = [left compare:right];
    
    switch (operation) {
        case invalidExprOp:
            // TODO log error
            return false;
            break;
        case lessThan:
            return result == NSOrderedAscending;
            break;
        case greaterThan:
            return result == NSOrderedDescending;
            break;
        case equal:
            return result == NSOrderedSame;
            break;
        case notEqual:
            return result != NSOrderedSame;
            break;
        case lessThanEqual:
            return (result == NSOrderedAscending) || (result == NSOrderedSame);
            break;
        case greaterThanEqual:
            return (result == NSOrderedDescending) || (result == NSOrderedSame);
            break;
        default:
            // TODO log error
            break;
    }
}

+(BOOL) evalComp : (BOOL) left : (BOOL) right : (enum ruleCompoundOpsEnum) operation {
    switch (operation) {
        case invalidCompOp:
            // TODO log error
            return false;
            break;
        case compoundAnd:
            return left && right;
            break;
        case compoundOr:
            return left || right;
            break;
        default:
            // TODO log error
            break;
    }
    return false;
}

@end

