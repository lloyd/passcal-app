//
//  SimpleParser.h
//  passcalApp_1_X
//
//  Created by field on 9/18/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SimpleParser : NSObject

+ (NSDictionary *) ParseStringValues: (NSDictionary*) keyConfig : (NSString*) xmlKey;

+ (NSSet *) ParseEmptyValues: (NSDictionary*) keyConfig : (NSString*) xmlKey;

@end
