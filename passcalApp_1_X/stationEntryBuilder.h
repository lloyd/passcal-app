//
//  stationEntryBuilder.h
//
// This builder subscribes to the dasConnection and based off of response received
// will fill out different properties of stationEntry object.  When all messages
// have been seen by builder.  Builder automatically commits messages to persistant
// storage.

#import <Foundation/Foundation.h>
#import "subscriber.h"

extern int const totalRf130MsgCount; // Total number of messages before commit

@interface stationEntryBuilder : NSObject <subscriber>
// resetCurrentEntry
// resets builder state and clears current data
- (void) resetCurrentEntry;

// commitCurrentEntry
// commits current entry from any state.  Usually called internally.
- (void) commitCurrentEntry;

// resetMsgCount
// This method is called when GUI wants to clear status counter
- (void) resetMsgCount;

// getMsgCount
// This accesses message count which can be used to keep track of internal build
// progress.
- (int) getMsgCount;

// subscriber methods
// synthesized - (void) recvObject : (NSObject*) obj;

@end
