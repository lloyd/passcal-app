//
//  publisher.h
//
// Interface which all publisher should inherent.  Note this is
// an active publisher pattern.

#include "subscriber.h"

@protocol publisher

@property NSMutableArray* subscribers;

// regSubscriber
// Places subscriber in array
- (void) regSubscriber: (id<subscriber>) sub;

@end