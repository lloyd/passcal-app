//
//  rule.h
//  passcalApp_1_X
//
//  Created by field on 9/14/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//
// These classes and enumerations are meant to store the rules but the evaluation will need to be
// done during runtime and by passing current Entry to bew evaluated.

#import <Foundation/Foundation.h>

enum ruleExprOpsEnum {
    invalidExprOp,
    lessThan,
    greaterThan,
    equal,
    lessThanEqual,
    greaterThanEqual,
    notEqual
};

enum ruleCompoundOpsEnum {
    invalidCompOp,
    compoundAnd,
    compoundOr
};

enum ruleLevelEnum {
    error,
    warning
};

enum ruleTypeEnum {
    leftLiteralType,
    rightLiteralType,
    twoIdentifiersType
};

typedef NSString identifier;
typedef NSNumber floatLiteral;

@protocol rule

@property enum ruleLevelEnum ruleLevel;

-(BOOL) evalRule : (NSString*) key : (NSDictionary *) deepDict;

@end

@interface simpleRule : NSObject  <rule>

@property enum ruleLevelEnum ruleLevel;

-(id) init: (id) value;

-(BOOL) evalRule : (NSString*) key : (NSDictionary *) deepDict;

@end


@interface simpleRuleWithRef : NSObject  <rule>

@property enum ruleLevelEnum ruleLevel;
@property enum ruleTypeEnum ruleType;
@property enum ruleExprOpsEnum operation;
@property identifier *ident1;
@property identifier *ident2;
@property floatLiteral *literal;

-(id) initWithLeftLiteral: (floatLiteral*) leftOperand : (identifier*) rightOperand : (enum ruleExprOpsEnum) operation;
-(id) initWithRightLiteral: (identifier*) leftOperand : (floatLiteral*) rightOperand : (enum ruleExprOpsEnum) operation;
-(id) initWithTwoIdentifiers: (identifier*) leftOperand : (identifier*) rightOperand : (enum ruleExprOpsEnum) operation;

-(BOOL) evalRule : (NSString*) key : (NSDictionary *) deepDict;

@end

@interface compoundRuleWithRefs : NSObject  <rule>

@property enum ruleLevelEnum ruleLevel;
@property NSMutableArray *rules;
@property NSMutableArray *compOps; // There should always be one less ops than rules

-(id) init;

-(void) addRule : (id<rule>) newRule;
-(void) addCompOp : (enum ruleCompoundOpsEnum) newOp;

-(BOOL) evalRule : (NSString*) key : (NSDictionary *) deepDict;

@end

@interface notEmptyRule : NSObject  <rule>

@property enum ruleLevelEnum ruleLevel;

-(id) init;

-(BOOL) evalRule : (NSString*) key : (NSDictionary *) deepDict;

@end

@interface regexRule : NSObject  <rule>
@property enum ruleLevelEnum ruleLevel;
@property NSString *pattern;

-(id) initWithPattern: (NSString*) pattern;

-(BOOL) evalRule : (NSString*) key : (NSDictionary *) deepDict;
@end

@interface ruleHelper : NSObject

+(floatLiteral*) getLiteralFromIdentifer: (identifier*) ident : (NSString*) key : (NSDictionary *) deepDict;
+(BOOL) evalExpr : (floatLiteral*) left : (floatLiteral *) right : (enum ruleExprOpsEnum) operation;
+(BOOL) evalComp : (BOOL) left : (BOOL) right : (enum ruleCompoundOpsEnum) operation;

@end