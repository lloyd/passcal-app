//
//  SimpleParser.m
//  passcalApp_1_X
//
//  Created by field on 9/18/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "SimpleParser.h"

#import "CocoaLumberjack.h"
static const DDLogLevel ddLogLevel = DDLogLevelInfo;

@implementation SimpleParser

+ (NSDictionary *) ParseStringValues: (NSDictionary*) keyConfig : (NSString*) xmlKey {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    for (NSString *fieldKey in keyConfig) {
        if (![keyConfig[fieldKey] isKindOfClass:[NSDictionary class]])
        {
            DDLogError(@"Malformed key: \"%@\".  Possibly duplicate value", fieldKey);
            continue;
        }
        if (keyConfig[fieldKey][xmlKey]) // if the field key contains xmlKey
            dict[fieldKey] = keyConfig[fieldKey][xmlKey];
    }
    
    return [[NSDictionary alloc] initWithDictionary:dict];

}
+ (NSSet *) ParseEmptyValues: (NSDictionary*) keyConfig : (NSString*) xmlKey {
    NSMutableSet *set = [[NSMutableSet alloc] init];
    
    for (NSString *fieldKey in keyConfig) {
        if (![keyConfig[fieldKey] isKindOfClass:[NSDictionary class]])
        {
            DDLogError(@"Malformed key: \"%@\".  Possibly duplicate value", fieldKey);
            continue;
        }
        if (keyConfig[fieldKey][xmlKey]) // if the field key contains xmlKey
            [set addObject:fieldKey];
    }
    
    return [[NSSet alloc] initWithSet:set];
}

@end
