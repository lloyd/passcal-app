#import "_IosFields.h"
#import "RevControlHelper.h"

@interface IosFields : _IosFields<revControlFields>
+(id) initWithDictionary: (NSDictionary *) d;
+(BOOL) doesNodeIdExist: (NSString*) nodeId;

-(id) createNewRevision;
@end
