// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Entry.h instead.

#import <CoreData/CoreData.h>
#import "RHManagedObject.h"

extern const struct EntryAttributes {
	__unsafe_unretained NSString *entryTime;
	__unsafe_unretained NSString *isHidden;
	__unsafe_unretained NSString *nodeID;
	__unsafe_unretained NSString *revisionNum;
	__unsafe_unretained NSString *treeID;
	__unsafe_unretained NSString *updateTime;
} EntryAttributes;

extern const struct EntryRelationships {
	__unsafe_unretained NSString *iosFields;
	__unsafe_unretained NSString *rf130Station;
	__unsafe_unretained NSString *rf130Status;
	__unsafe_unretained NSString *rf130Version;
	__unsafe_unretained NSString *userFields;
} EntryRelationships;

@class IosFields;
@class Rf130StationFields;
@class Rf130StatusFields;
@class Rf130VersionFields;
@class UserFields;

@interface EntryID : NSManagedObjectID {}
@end

@interface _Entry : RHManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) EntryID* objectID;

@property (nonatomic, strong) NSDate* entryTime;

//- (BOOL)validateEntryTime:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isHidden;

@property (atomic) BOOL isHiddenValue;
- (BOOL)isHiddenValue;
- (void)setIsHiddenValue:(BOOL)value_;

//- (BOOL)validateIsHidden:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* nodeID;

//- (BOOL)validateNodeID:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* revisionNum;

@property (atomic) int32_t revisionNumValue;
- (int32_t)revisionNumValue;
- (void)setRevisionNumValue:(int32_t)value_;

//- (BOOL)validateRevisionNum:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* treeID;

//- (BOOL)validateTreeID:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* updateTime;

//- (BOOL)validateUpdateTime:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) IosFields *iosFields;

//- (BOOL)validateIosFields:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) Rf130StationFields *rf130Station;

//- (BOOL)validateRf130Station:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) Rf130StatusFields *rf130Status;

//- (BOOL)validateRf130Status:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) Rf130VersionFields *rf130Version;

//- (BOOL)validateRf130Version:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) UserFields *userFields;

//- (BOOL)validateUserFields:(id*)value_ error:(NSError**)error_;

@end

@interface _Entry (CoreDataGeneratedPrimitiveAccessors)

- (NSDate*)primitiveEntryTime;
- (void)setPrimitiveEntryTime:(NSDate*)value;

- (NSNumber*)primitiveIsHidden;
- (void)setPrimitiveIsHidden:(NSNumber*)value;

- (BOOL)primitiveIsHiddenValue;
- (void)setPrimitiveIsHiddenValue:(BOOL)value_;

- (NSString*)primitiveNodeID;
- (void)setPrimitiveNodeID:(NSString*)value;

- (NSNumber*)primitiveRevisionNum;
- (void)setPrimitiveRevisionNum:(NSNumber*)value;

- (int32_t)primitiveRevisionNumValue;
- (void)setPrimitiveRevisionNumValue:(int32_t)value_;

- (NSString*)primitiveTreeID;
- (void)setPrimitiveTreeID:(NSString*)value;

- (NSDate*)primitiveUpdateTime;
- (void)setPrimitiveUpdateTime:(NSDate*)value;

- (IosFields*)primitiveIosFields;
- (void)setPrimitiveIosFields:(IosFields*)value;

- (Rf130StationFields*)primitiveRf130Station;
- (void)setPrimitiveRf130Station:(Rf130StationFields*)value;

- (Rf130StatusFields*)primitiveRf130Status;
- (void)setPrimitiveRf130Status:(Rf130StatusFields*)value;

- (Rf130VersionFields*)primitiveRf130Version;
- (void)setPrimitiveRf130Version:(Rf130VersionFields*)value;

- (UserFields*)primitiveUserFields;
- (void)setPrimitiveUserFields:(UserFields*)value;

@end
