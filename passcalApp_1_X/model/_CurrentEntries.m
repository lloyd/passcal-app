// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CurrentEntries.m instead.

#import "_CurrentEntries.h"

const struct CurrentEntriesAttributes CurrentEntriesAttributes = {
	.collectUrlStr = @"collectUrlStr",
	.reviewUrlStr = @"reviewUrlStr",
};

@implementation CurrentEntriesID
@end

@implementation _CurrentEntries

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"CurrentEntries" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"CurrentEntries";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"CurrentEntries" inManagedObjectContext:moc_];
}

- (CurrentEntriesID*)objectID {
	return (CurrentEntriesID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic collectUrlStr;

@dynamic reviewUrlStr;

@end

