#import "_CurrentEntries.h"
#import "Entry.h"

@interface CurrentEntries : _CurrentEntries {}

@property (nonatomic) NSURL *reviewUrl;
@property (nonatomic) NSURL *collectUrl;

// TODO enforce singleton pattern better
+(id) getInstance;
+(void) updateCommitEntry: (Entry*) newEntry;
+(void) updateReviewEntry: (Entry*) newEntry;
+(void) updateReviewEntryIfMatchingTreeID: (Entry*) newEntry;

@end
