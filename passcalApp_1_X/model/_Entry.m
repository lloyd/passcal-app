// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Entry.m instead.

#import "_Entry.h"

const struct EntryAttributes EntryAttributes = {
	.entryTime = @"entryTime",
	.isHidden = @"isHidden",
	.nodeID = @"nodeID",
	.revisionNum = @"revisionNum",
	.treeID = @"treeID",
	.updateTime = @"updateTime",
};

const struct EntryRelationships EntryRelationships = {
	.iosFields = @"iosFields",
	.rf130Station = @"rf130Station",
	.rf130Status = @"rf130Status",
	.rf130Version = @"rf130Version",
	.userFields = @"userFields",
};

@implementation EntryID
@end

@implementation _Entry

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Entry" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Entry";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Entry" inManagedObjectContext:moc_];
}

- (EntryID*)objectID {
	return (EntryID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"isHiddenValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isHidden"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"revisionNumValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"revisionNum"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic entryTime;

@dynamic isHidden;

- (BOOL)isHiddenValue {
	NSNumber *result = [self isHidden];
	return [result boolValue];
}

- (void)setIsHiddenValue:(BOOL)value_ {
	[self setIsHidden:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsHiddenValue {
	NSNumber *result = [self primitiveIsHidden];
	return [result boolValue];
}

- (void)setPrimitiveIsHiddenValue:(BOOL)value_ {
	[self setPrimitiveIsHidden:[NSNumber numberWithBool:value_]];
}

@dynamic nodeID;

@dynamic revisionNum;

- (int32_t)revisionNumValue {
	NSNumber *result = [self revisionNum];
	return [result intValue];
}

- (void)setRevisionNumValue:(int32_t)value_ {
	[self setRevisionNum:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveRevisionNumValue {
	NSNumber *result = [self primitiveRevisionNum];
	return [result intValue];
}

- (void)setPrimitiveRevisionNumValue:(int32_t)value_ {
	[self setPrimitiveRevisionNum:[NSNumber numberWithInt:value_]];
}

@dynamic treeID;

@dynamic updateTime;

@dynamic iosFields;

@dynamic rf130Station;

@dynamic rf130Status;

@dynamic rf130Version;

@dynamic userFields;

@end

