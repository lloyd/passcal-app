// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UserFields.h instead.

#import <CoreData/CoreData.h>
#import "RHManagedObject.h"

extern const struct UserFieldsAttributes {
	__unsafe_unretained NSString *batteryVoltage;
	__unsafe_unretained NSString *clockSerialNumber;
	__unsafe_unretained NSString *departureTime;
	__unsafe_unretained NSString *enclosureSerialNumber;
	__unsafe_unretained NSString *experimentName;
	__unsafe_unretained NSString *fieldTeam;
	__unsafe_unretained NSString *flashDisk1SerialNum;
	__unsafe_unretained NSString *flashDisk2SerialNum;
	__unsafe_unretained NSString *gpsAltitude;
	__unsafe_unretained NSString *gpsLatitude;
	__unsafe_unretained NSString *gpsLongitude;
	__unsafe_unretained NSString *magneticDeclination;
	__unsafe_unretained NSString *nodeID;
	__unsafe_unretained NSString *powerBoxApv;
	__unsafe_unretained NSString *powerBoxSerialNumber;
	__unsafe_unretained NSString *powerBoxVp;
	__unsafe_unretained NSString *powerBoxVpn;
	__unsafe_unretained NSString *powerBoxVs;
	__unsafe_unretained NSString *revisionNum;
	__unsafe_unretained NSString *sensor1SerialNumber;
	__unsafe_unretained NSString *sensor2SerialNumber;
	__unsafe_unretained NSString *sensorOrientation;
	__unsafe_unretained NSString *siteContact;
	__unsafe_unretained NSString *solarPanelOrientation;
	__unsafe_unretained NSString *solarPanelSize;
	__unsafe_unretained NSString *solarPanelVoltage;
	__unsafe_unretained NSString *stationName;
	__unsafe_unretained NSString *telemetrySerialNumber;
	__unsafe_unretained NSString *treeID;
	__unsafe_unretained NSString *userNotes;
	__unsafe_unretained NSString *weatherNotes;
} UserFieldsAttributes;

extern const struct UserFieldsRelationships {
	__unsafe_unretained NSString *entry;
} UserFieldsRelationships;

@class Entry;

@interface UserFieldsID : NSManagedObjectID {}
@end

@interface _UserFields : RHManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) UserFieldsID* objectID;

@property (nonatomic, strong) NSNumber* batteryVoltage;

@property (atomic) float batteryVoltageValue;
- (float)batteryVoltageValue;
- (void)setBatteryVoltageValue:(float)value_;

//- (BOOL)validateBatteryVoltage:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* clockSerialNumber;

//- (BOOL)validateClockSerialNumber:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* departureTime;

//- (BOOL)validateDepartureTime:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* enclosureSerialNumber;

//- (BOOL)validateEnclosureSerialNumber:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* experimentName;

//- (BOOL)validateExperimentName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* fieldTeam;

//- (BOOL)validateFieldTeam:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* flashDisk1SerialNum;

//- (BOOL)validateFlashDisk1SerialNum:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* flashDisk2SerialNum;

//- (BOOL)validateFlashDisk2SerialNum:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* gpsAltitude;

@property (atomic) float gpsAltitudeValue;
- (float)gpsAltitudeValue;
- (void)setGpsAltitudeValue:(float)value_;

//- (BOOL)validateGpsAltitude:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* gpsLatitude;

@property (atomic) float gpsLatitudeValue;
- (float)gpsLatitudeValue;
- (void)setGpsLatitudeValue:(float)value_;

//- (BOOL)validateGpsLatitude:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* gpsLongitude;

@property (atomic) float gpsLongitudeValue;
- (float)gpsLongitudeValue;
- (void)setGpsLongitudeValue:(float)value_;

//- (BOOL)validateGpsLongitude:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* magneticDeclination;

@property (atomic) float magneticDeclinationValue;
- (float)magneticDeclinationValue;
- (void)setMagneticDeclinationValue:(float)value_;

//- (BOOL)validateMagneticDeclination:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* nodeID;

//- (BOOL)validateNodeID:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* powerBoxApv;

@property (atomic) float powerBoxApvValue;
- (float)powerBoxApvValue;
- (void)setPowerBoxApvValue:(float)value_;

//- (BOOL)validatePowerBoxApv:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* powerBoxSerialNumber;

//- (BOOL)validatePowerBoxSerialNumber:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* powerBoxVp;

@property (atomic) float powerBoxVpValue;
- (float)powerBoxVpValue;
- (void)setPowerBoxVpValue:(float)value_;

//- (BOOL)validatePowerBoxVp:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* powerBoxVpn;

@property (atomic) float powerBoxVpnValue;
- (float)powerBoxVpnValue;
- (void)setPowerBoxVpnValue:(float)value_;

//- (BOOL)validatePowerBoxVpn:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* powerBoxVs;

@property (atomic) float powerBoxVsValue;
- (float)powerBoxVsValue;
- (void)setPowerBoxVsValue:(float)value_;

//- (BOOL)validatePowerBoxVs:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* revisionNum;

@property (atomic) int32_t revisionNumValue;
- (int32_t)revisionNumValue;
- (void)setRevisionNumValue:(int32_t)value_;

//- (BOOL)validateRevisionNum:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* sensor1SerialNumber;

//- (BOOL)validateSensor1SerialNumber:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* sensor2SerialNumber;

//- (BOOL)validateSensor2SerialNumber:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensorOrientation;

@property (atomic) float sensorOrientationValue;
- (float)sensorOrientationValue;
- (void)setSensorOrientationValue:(float)value_;

//- (BOOL)validateSensorOrientation:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* siteContact;

//- (BOOL)validateSiteContact:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* solarPanelOrientation;

@property (atomic) float solarPanelOrientationValue;
- (float)solarPanelOrientationValue;
- (void)setSolarPanelOrientationValue:(float)value_;

//- (BOOL)validateSolarPanelOrientation:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* solarPanelSize;

//- (BOOL)validateSolarPanelSize:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* solarPanelVoltage;

@property (atomic) float solarPanelVoltageValue;
- (float)solarPanelVoltageValue;
- (void)setSolarPanelVoltageValue:(float)value_;

//- (BOOL)validateSolarPanelVoltage:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* stationName;

//- (BOOL)validateStationName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* telemetrySerialNumber;

//- (BOOL)validateTelemetrySerialNumber:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* treeID;

//- (BOOL)validateTreeID:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* userNotes;

//- (BOOL)validateUserNotes:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* weatherNotes;

//- (BOOL)validateWeatherNotes:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *entry;

- (NSMutableSet*)entrySet;

@end

@interface _UserFields (EntryCoreDataGeneratedAccessors)
- (void)addEntry:(NSSet*)value_;
- (void)removeEntry:(NSSet*)value_;
- (void)addEntryObject:(Entry*)value_;
- (void)removeEntryObject:(Entry*)value_;

@end

@interface _UserFields (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveBatteryVoltage;
- (void)setPrimitiveBatteryVoltage:(NSNumber*)value;

- (float)primitiveBatteryVoltageValue;
- (void)setPrimitiveBatteryVoltageValue:(float)value_;

- (NSString*)primitiveClockSerialNumber;
- (void)setPrimitiveClockSerialNumber:(NSString*)value;

- (NSDate*)primitiveDepartureTime;
- (void)setPrimitiveDepartureTime:(NSDate*)value;

- (NSString*)primitiveEnclosureSerialNumber;
- (void)setPrimitiveEnclosureSerialNumber:(NSString*)value;

- (NSString*)primitiveExperimentName;
- (void)setPrimitiveExperimentName:(NSString*)value;

- (NSString*)primitiveFieldTeam;
- (void)setPrimitiveFieldTeam:(NSString*)value;

- (NSString*)primitiveFlashDisk1SerialNum;
- (void)setPrimitiveFlashDisk1SerialNum:(NSString*)value;

- (NSString*)primitiveFlashDisk2SerialNum;
- (void)setPrimitiveFlashDisk2SerialNum:(NSString*)value;

- (NSNumber*)primitiveGpsAltitude;
- (void)setPrimitiveGpsAltitude:(NSNumber*)value;

- (float)primitiveGpsAltitudeValue;
- (void)setPrimitiveGpsAltitudeValue:(float)value_;

- (NSNumber*)primitiveGpsLatitude;
- (void)setPrimitiveGpsLatitude:(NSNumber*)value;

- (float)primitiveGpsLatitudeValue;
- (void)setPrimitiveGpsLatitudeValue:(float)value_;

- (NSNumber*)primitiveGpsLongitude;
- (void)setPrimitiveGpsLongitude:(NSNumber*)value;

- (float)primitiveGpsLongitudeValue;
- (void)setPrimitiveGpsLongitudeValue:(float)value_;

- (NSNumber*)primitiveMagneticDeclination;
- (void)setPrimitiveMagneticDeclination:(NSNumber*)value;

- (float)primitiveMagneticDeclinationValue;
- (void)setPrimitiveMagneticDeclinationValue:(float)value_;

- (NSString*)primitiveNodeID;
- (void)setPrimitiveNodeID:(NSString*)value;

- (NSNumber*)primitivePowerBoxApv;
- (void)setPrimitivePowerBoxApv:(NSNumber*)value;

- (float)primitivePowerBoxApvValue;
- (void)setPrimitivePowerBoxApvValue:(float)value_;

- (NSString*)primitivePowerBoxSerialNumber;
- (void)setPrimitivePowerBoxSerialNumber:(NSString*)value;

- (NSNumber*)primitivePowerBoxVp;
- (void)setPrimitivePowerBoxVp:(NSNumber*)value;

- (float)primitivePowerBoxVpValue;
- (void)setPrimitivePowerBoxVpValue:(float)value_;

- (NSNumber*)primitivePowerBoxVpn;
- (void)setPrimitivePowerBoxVpn:(NSNumber*)value;

- (float)primitivePowerBoxVpnValue;
- (void)setPrimitivePowerBoxVpnValue:(float)value_;

- (NSNumber*)primitivePowerBoxVs;
- (void)setPrimitivePowerBoxVs:(NSNumber*)value;

- (float)primitivePowerBoxVsValue;
- (void)setPrimitivePowerBoxVsValue:(float)value_;

- (NSNumber*)primitiveRevisionNum;
- (void)setPrimitiveRevisionNum:(NSNumber*)value;

- (int32_t)primitiveRevisionNumValue;
- (void)setPrimitiveRevisionNumValue:(int32_t)value_;

- (NSString*)primitiveSensor1SerialNumber;
- (void)setPrimitiveSensor1SerialNumber:(NSString*)value;

- (NSString*)primitiveSensor2SerialNumber;
- (void)setPrimitiveSensor2SerialNumber:(NSString*)value;

- (NSNumber*)primitiveSensorOrientation;
- (void)setPrimitiveSensorOrientation:(NSNumber*)value;

- (float)primitiveSensorOrientationValue;
- (void)setPrimitiveSensorOrientationValue:(float)value_;

- (NSString*)primitiveSiteContact;
- (void)setPrimitiveSiteContact:(NSString*)value;

- (NSNumber*)primitiveSolarPanelOrientation;
- (void)setPrimitiveSolarPanelOrientation:(NSNumber*)value;

- (float)primitiveSolarPanelOrientationValue;
- (void)setPrimitiveSolarPanelOrientationValue:(float)value_;

- (NSString*)primitiveSolarPanelSize;
- (void)setPrimitiveSolarPanelSize:(NSString*)value;

- (NSNumber*)primitiveSolarPanelVoltage;
- (void)setPrimitiveSolarPanelVoltage:(NSNumber*)value;

- (float)primitiveSolarPanelVoltageValue;
- (void)setPrimitiveSolarPanelVoltageValue:(float)value_;

- (NSString*)primitiveStationName;
- (void)setPrimitiveStationName:(NSString*)value;

- (NSString*)primitiveTelemetrySerialNumber;
- (void)setPrimitiveTelemetrySerialNumber:(NSString*)value;

- (NSString*)primitiveTreeID;
- (void)setPrimitiveTreeID:(NSString*)value;

- (NSString*)primitiveUserNotes;
- (void)setPrimitiveUserNotes:(NSString*)value;

- (NSString*)primitiveWeatherNotes;
- (void)setPrimitiveWeatherNotes:(NSString*)value;

- (NSMutableSet*)primitiveEntry;
- (void)setPrimitiveEntry:(NSMutableSet*)value;

@end
