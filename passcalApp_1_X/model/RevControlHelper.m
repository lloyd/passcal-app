//
//  RevControlHelper.m
//  passcalApp_1_X
//
//  Created by field on 8/23/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "RevControlHelper.h"
#import "utils.h"
#import "Entry.h"
#import "IosFields.h"
#import "Rf130StationFields.h"
#import "Rf130StatusFields.h"
#import "Rf130VersionFields.h"
#import "UserFields.h"

#import "CocoaLumberjack.h"
static const DDLogLevel ddLogLevel = DDLogLevelInfo;

@implementation RevControlHelper
+(id) createRevision: (id <revControlFields>) fields {
    id <revControlFields> newRev = [fields clone];
    newRev.revisionNumValue++;
    newRev.nodeID = [[NSProcessInfo processInfo] globallyUniqueString];
    
    return newRev;
}
+(void) commitAll {
    [IosFields commit];
    [Rf130StationFields commit];
    [Rf130StatusFields commit];
    [Rf130VersionFields commit];
    [UserFields commit];
    [Entry commit];
}

+(NSDictionary*) exportRelations: (id <revControlFields>) fields {
    NSArray *exportRelationFields = @[@"nodeID", @"treeID", @"revisionNum"];
    NSMutableDictionary *d = [[NSMutableDictionary alloc] init];
    
    for (NSString *relationName in [fields getRelationshipsByName]) {
        /*
        RHManagedObject *mObj = [fields valueForKey:relationName];
        if (mObj == nil)
            continue;
         */
        for (NSString *key in exportRelationFields) {
            //id value = [mObj valueForKeyPath:key];
            NSString* combinedKey = [NSString stringWithFormat:@"%@.%@", relationName, key];
            id value = [fields valueForKeyPath:combinedKey];
            
            if (value != nil) {
                [d setObject:value forKey:combinedKey];
            }
            else {
                [d setObject:[NSNull null] forKey:combinedKey];
            }
        }
    }
    
    return [NSDictionary dictionaryWithDictionary:d];
}
+(id) initWithDictionary: (Class) entityClass : (NSDictionary *) d {
    NSAttributeType attrType;
    id value = nil;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YY-MM-dd HH:mm:ss ZZZZ"];
    
    RHManagedObject *newEntry = [entityClass newEntityWithError:nil];
    
    for (NSString *key in d) {
        NSString *strValue = d[key];
        
        if (![strValue isKindOfClass: [NSString class]]) // Happens when a field was left blank
            continue;
        
        attrType = [entityClass attributeTypeWithKey:key error:nil];
        switch (attrType) {
            case NSStringAttributeType:
                value = strValue;
                break;
            case NSInteger16AttributeType:
            case NSInteger32AttributeType:
            case NSInteger64AttributeType:
                value = [[NSNumber alloc] initWithLong:[strValue integerValue]];
                break;
            case NSBooleanAttributeType:
                value = [[NSNumber alloc] initWithLong:[strValue boolValue]];
                break;
            case NSDecimalAttributeType:
            case NSDoubleAttributeType:
            case NSFloatAttributeType:
                value = [[NSNumber alloc] initWithDouble:[strValue doubleValue]];
                break;
            case NSDateAttributeType:
                value = [dateFormatter dateFromString:strValue];
                break;
            default:
                DDLogError(@"Unhandled type for key: %@", key);
                continue;
                //value = nil;
                break;
        }
        if (value == nil) {
            DDLogInfo(@"Key getting nil: %@", key);
        }
        
        [newEntry setValue:value forKey:key];
    }
    return newEntry;
}
+(BOOL) doesNodeIdExist: (Class) entityClass : (NSString*) nodeId {
    NSString *predStr;
    NSPredicate *predicate;
    NSUInteger number;
    
    predStr = [[NSString alloc] initWithFormat:@"nodeID = \"%@\"", nodeId];
    predicate = [NSPredicate predicateWithFormat:predStr];
    number = [entityClass countWithPredicate:predicate error:nil];
    
    return number > 0;
}
@end
