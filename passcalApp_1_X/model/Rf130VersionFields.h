#import "_Rf130VersionFields.h"
#import "RevControlHelper.h"

@interface Rf130VersionFields : _Rf130VersionFields<revControlFields>
+(id) initWithDictionary: (NSDictionary *) d;
+(BOOL) doesNodeIdExist: (NSString*) nodeId;

-(id) createNewRevision;
@end
