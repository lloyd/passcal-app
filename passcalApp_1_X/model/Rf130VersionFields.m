#import "Rf130VersionFields.h"

@interface Rf130VersionFields ()

// Private interface goes here.

@end

@implementation Rf130VersionFields

+(NSString *)modelName {
    return @"passcalApp_1_X";
}

+(id) initWithDictionary: (NSDictionary *) d {
    return [RevControlHelper initWithDictionary: [Rf130VersionFields class] : d];
}

+(BOOL) doesNodeIdExist: (NSString*) nodeId {
    return [RevControlHelper doesNodeIdExist:[Rf130VersionFields class] :nodeId];
}

-(id) createNewRevision {
    return [RevControlHelper createRevision:self];
}

@end
