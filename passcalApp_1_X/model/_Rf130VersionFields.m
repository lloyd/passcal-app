// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Rf130VersionFields.m instead.

#import "_Rf130VersionFields.h"

const struct Rf130VersionFieldsAttributes Rf130VersionFieldsAttributes = {
	.board0Acronym = @"board0Acronym",
	.board0FpgaNum = @"board0FpgaNum",
	.board0FpgaVersion = @"board0FpgaVersion",
	.board0Revision = @"board0Revision",
	.board0SerialNum = @"board0SerialNum",
	.board1Acronym = @"board1Acronym",
	.board1FpgaNum = @"board1FpgaNum",
	.board1FpgaVersion = @"board1FpgaVersion",
	.board1Revision = @"board1Revision",
	.board1SerialNum = @"board1SerialNum",
	.board2Acronym = @"board2Acronym",
	.board2FpgaNum = @"board2FpgaNum",
	.board2FpgaVersion = @"board2FpgaVersion",
	.board2Revision = @"board2Revision",
	.board2SerialNum = @"board2SerialNum",
	.board3Acronym = @"board3Acronym",
	.board3FpgaNum = @"board3FpgaNum",
	.board3FpgaVersion = @"board3FpgaVersion",
	.board3Revision = @"board3Revision",
	.board3SerialNum = @"board3SerialNum",
	.dasCpuVersion = @"dasCpuVersion",
	.nodeID = @"nodeID",
	.revisionNum = @"revisionNum",
	.sensorManufacturer = @"sensorManufacturer",
	.sensorModel = @"sensorModel",
	.sensorSerialNumber = @"sensorSerialNumber",
	.treeID = @"treeID",
};

const struct Rf130VersionFieldsRelationships Rf130VersionFieldsRelationships = {
	.entry = @"entry",
};

@implementation Rf130VersionFieldsID
@end

@implementation _Rf130VersionFields

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Rf130VersionFields" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Rf130VersionFields";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Rf130VersionFields" inManagedObjectContext:moc_];
}

- (Rf130VersionFieldsID*)objectID {
	return (Rf130VersionFieldsID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"revisionNumValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"revisionNum"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic board0Acronym;

@dynamic board0FpgaNum;

@dynamic board0FpgaVersion;

@dynamic board0Revision;

@dynamic board0SerialNum;

@dynamic board1Acronym;

@dynamic board1FpgaNum;

@dynamic board1FpgaVersion;

@dynamic board1Revision;

@dynamic board1SerialNum;

@dynamic board2Acronym;

@dynamic board2FpgaNum;

@dynamic board2FpgaVersion;

@dynamic board2Revision;

@dynamic board2SerialNum;

@dynamic board3Acronym;

@dynamic board3FpgaNum;

@dynamic board3FpgaVersion;

@dynamic board3Revision;

@dynamic board3SerialNum;

@dynamic dasCpuVersion;

@dynamic nodeID;

@dynamic revisionNum;

- (int32_t)revisionNumValue {
	NSNumber *result = [self revisionNum];
	return [result intValue];
}

- (void)setRevisionNumValue:(int32_t)value_ {
	[self setRevisionNum:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveRevisionNumValue {
	NSNumber *result = [self primitiveRevisionNum];
	return [result intValue];
}

- (void)setPrimitiveRevisionNumValue:(int32_t)value_ {
	[self setPrimitiveRevisionNum:[NSNumber numberWithInt:value_]];
}

@dynamic sensorManufacturer;

@dynamic sensorModel;

@dynamic sensorSerialNumber;

@dynamic treeID;

@dynamic entry;

- (NSMutableSet*)entrySet {
	[self willAccessValueForKey:@"entry"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"entry"];

	[self didAccessValueForKey:@"entry"];
	return result;
}

@end

