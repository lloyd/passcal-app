#import "Rf130StatusFields.h"

@interface Rf130StatusFields ()

// Private interface goes here.

@end

@implementation Rf130StatusFields

+(NSString *)modelName {
    return @"passcalApp_1_X";
}

+(id) initWithDictionary: (NSDictionary *) d {
    return [RevControlHelper initWithDictionary: [Rf130StatusFields class] : d];
}

+(BOOL) doesNodeIdExist: (NSString*) nodeId {
    return [RevControlHelper doesNodeIdExist:[Rf130StatusFields class] :nodeId];
}

-(id) createNewRevision {
    return [RevControlHelper createRevision:self];
}

@end
