// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IosFields.m instead.

#import "_IosFields.h"

const struct IosFieldsAttributes IosFieldsAttributes = {
	.appBuildNum = @"appBuildNum",
	.appVersion = @"appVersion",
	.iosDeviceModel = @"iosDeviceModel",
	.iosDeviceUdid = @"iosDeviceUdid",
	.nodeID = @"nodeID",
	.revisionNum = @"revisionNum",
	.treeID = @"treeID",
};

const struct IosFieldsRelationships IosFieldsRelationships = {
	.entry = @"entry",
};

@implementation IosFieldsID
@end

@implementation _IosFields

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"IosFields" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"IosFields";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"IosFields" inManagedObjectContext:moc_];
}

- (IosFieldsID*)objectID {
	return (IosFieldsID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"appBuildNumValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"appBuildNum"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"revisionNumValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"revisionNum"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic appBuildNum;

- (int32_t)appBuildNumValue {
	NSNumber *result = [self appBuildNum];
	return [result intValue];
}

- (void)setAppBuildNumValue:(int32_t)value_ {
	[self setAppBuildNum:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveAppBuildNumValue {
	NSNumber *result = [self primitiveAppBuildNum];
	return [result intValue];
}

- (void)setPrimitiveAppBuildNumValue:(int32_t)value_ {
	[self setPrimitiveAppBuildNum:[NSNumber numberWithInt:value_]];
}

@dynamic appVersion;

@dynamic iosDeviceModel;

@dynamic iosDeviceUdid;

@dynamic nodeID;

@dynamic revisionNum;

- (int32_t)revisionNumValue {
	NSNumber *result = [self revisionNum];
	return [result intValue];
}

- (void)setRevisionNumValue:(int32_t)value_ {
	[self setRevisionNum:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveRevisionNumValue {
	NSNumber *result = [self primitiveRevisionNum];
	return [result intValue];
}

- (void)setPrimitiveRevisionNumValue:(int32_t)value_ {
	[self setPrimitiveRevisionNum:[NSNumber numberWithInt:value_]];
}

@dynamic treeID;

@dynamic entry;

- (NSMutableSet*)entrySet {
	[self willAccessValueForKey:@"entry"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"entry"];

	[self didAccessValueForKey:@"entry"];
	return result;
}

@end

