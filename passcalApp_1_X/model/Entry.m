#import "Entry.h"
#import "IosFields.h"
#import "Rf130StationFields.h"
#import "Rf130StatusFields.h"
#import "Rf130VersionFields.h"
#import "UserFields.h"

#import "CocoaLumberjack.h"
static const DDLogLevel ddLogLevel = DDLogLevelInfo;

@interface Entry ()

-(void) applyChanges: (NSDictionary*) changes;

@end

@implementation Entry

+(NSString *)modelName {
    return @"passcalApp_1_X";
}

+(id) initWithDictionary: (NSDictionary *) d {
    return [RevControlHelper initWithDictionary: [Entry class] : d];
}

+(BOOL) doesNodeIdExist: (NSString*) nodeId {
    return [RevControlHelper doesNodeIdExist:[Entry class] :nodeId];
}

+(BOOL) updateRelations: (NSString*) targetNodeID : (NSDictionary *) relationsD {
    NSString *predStr;
    NSPredicate *predicate;
    Entry *targetEntry;
    
    predStr = [[NSString alloc] initWithFormat:@"nodeID = \"%@\"", targetNodeID];
    predicate = [NSPredicate predicateWithFormat:predStr];
    targetEntry = [Entry getWithPredicate:predicate error:nil];
    
    NSArray *keyParts;
    NSString *relationNodeID;
    for (NSString *key in relationsD) {
        keyParts = [key componentsSeparatedByString:@"."]; // relation.nodeId
        
        if (![@"nodeID" isEqualToString:keyParts[1]]) // Only work with nodeIDs for now
            continue;
        relationNodeID = relationsD[key];
        
        if ([[IosFields entityName] caseInsensitiveCompare:keyParts[0]] == NSOrderedSame) {
            if (targetEntry.iosFields != nil) // Don't overwrite it if it has a value
                continue;
            predStr = [[NSString alloc] initWithFormat:@"nodeID = \"%@\"", relationNodeID];
            predicate = [NSPredicate predicateWithFormat:predStr];
            targetEntry.iosFields = [IosFields getWithPredicate:predicate error:nil];
        }
        else if ([[Rf130StationFields entityName] rangeOfString:keyParts[0] options:NSCaseInsensitiveSearch].location != NSNotFound) {
            if (targetEntry.rf130Station != nil) // Don't overwrite it if it has a value
                continue;
            predStr = [[NSString alloc] initWithFormat:@"nodeID = \"%@\"", relationNodeID];
            predicate = [NSPredicate predicateWithFormat:predStr];
            targetEntry.rf130Station = [Rf130StationFields getWithPredicate:predicate error:nil];
        }
        else if ([[Rf130StatusFields entityName] rangeOfString:keyParts[0] options:NSCaseInsensitiveSearch].location != NSNotFound) {
            if (targetEntry.rf130Status != nil) // Don't overwrite it if it has a value
                continue;
            predStr = [[NSString alloc] initWithFormat:@"nodeID = \"%@\"", relationNodeID];
            predicate = [NSPredicate predicateWithFormat:predStr];
            targetEntry.rf130Status = [Rf130StatusFields getWithPredicate:predicate error:nil];
        }
        else if ([[Rf130VersionFields entityName] rangeOfString:keyParts[0] options:NSCaseInsensitiveSearch].location != NSNotFound) {
            if (targetEntry.rf130Version != nil) // Don't overwrite it if it has a value
                continue;
            predStr = [[NSString alloc] initWithFormat:@"nodeID = \"%@\"", relationNodeID];
            predicate = [NSPredicate predicateWithFormat:predStr];
            targetEntry.rf130Version = [Rf130VersionFields getWithPredicate:predicate error:nil];
        }
        else if ([[UserFields entityName] caseInsensitiveCompare:keyParts[0]] == NSOrderedSame) {
            if (targetEntry.userFields != nil) // Don't overwrite it if it has a value
                continue;
            predStr = [[NSString alloc] initWithFormat:@"nodeID = \"%@\"", relationNodeID];
            predicate = [NSPredicate predicateWithFormat:predStr];
            targetEntry.userFields = [UserFields getWithPredicate:predicate error:nil];
        }
        else {
            DDLogWarn(@"Unknown relation: %@", keyParts[0]);
        }
    }
    
    return true; // TODO error checking
}

-(id) createNewRevision {
    self.isHiddenValue = true;
    
    Entry *newRev = [self cloneWithRelationships];
    newRev.isHiddenValue = false;
    newRev.revisionNumValue++;
    newRev.nodeID = [[NSProcessInfo processInfo] globallyUniqueString];
    newRev.updateTime = [[NSDate alloc] init]; // Time in UTC
    // Should have treeID cloned
    
    return newRev;
}

-(id) createNewRevision: (NSDictionary*) changes {
    Entry *newRev = [self createNewRevision];
    [newRev applyChanges:changes];
    
    return newRev;
}

-(void) applyChanges: (NSDictionary*) changes {
    NSSet *attrKeys = [[NSSet alloc] initWithArray:[[self getAttributesByName] allKeys]];
    NSMutableSet *newRelations = [[NSMutableSet alloc] init];
    for (NSString *changeKey in changes)
    {
        if ([attrKeys containsObject:changeKey]) // if top level contains
        {
            [self setValue:changes[changeKey] forKey:changeKey];
            continue;
        }
        NSSet *combinedKeysSet;
        NSDictionary *relations = [self getRelationshipsByName];
        for (NSString *relationKey in relations)
        {
            id<revControlFields> relation = [self valueForKey:relationKey];
            NSMutableArray *combinedKeys = [[NSMutableArray alloc] init];
            NSArray *attrKeys = [[relation getAttributesByName] allKeys];
            for (NSString *attrKey in attrKeys) {
                NSString *combinedKey = [NSString stringWithFormat:@"%@.%@", relationKey, attrKey];
                [combinedKeys addObject:combinedKey];
            }
            
            combinedKeysSet = [[NSSet alloc] initWithArray:combinedKeys];
            if ([combinedKeysSet containsObject:changeKey]) // if relation contains key
            {
                if (![newRelations containsObject:relationKey]) // Create new version of relation
                {
                    [newRelations addObject:relationKey];
                    
                    id newRev = [relation createNewRevision];
                    relation = newRev;
                    [relations setValue:newRev forKey:relationKey]; // Update dict
                    [self setValue:newRev forKey:relationKey]; // Update Entry(self)
                }
                NSString *attrChangeKey = [self combinedKey2Attrkey:changeKey];
                [relations[relationKey] setValue:changes[changeKey] forKey:attrChangeKey];
                break; // break out of searching relations but not out of changes
            }
        }
        // TODO Add error handling for if it is an invalid key
        //NSLog(@"Key: %@ not found in deep entry!!!!", changeKey);
    }
}

-(NSString*) combinedKey2Attrkey: (NSString*) combinedKey
{
    const char sepChar = '.';
    int idx = 0;
    while (idx < [combinedKey length]) {
        if ([combinedKey characterAtIndex:idx] == sepChar)
            break;
        idx++;
    }
    
    return [combinedKey substringFromIndex:(idx+1)]; // Skip sepChar
}

@end
