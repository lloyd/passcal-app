#import "_Rf130StationFields.h"
#import "RevControlHelper.h"

@interface Rf130StationFields : _Rf130StationFields<revControlFields> {}
+(id) initWithDictionary: (NSDictionary *) d;
+(BOOL) doesNodeIdExist: (NSString*) nodeId;

-(id) createNewRevision;
@end
