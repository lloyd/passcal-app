// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CurrentEntries.h instead.

#import <CoreData/CoreData.h>
#import "RHManagedObject.h"

extern const struct CurrentEntriesAttributes {
	__unsafe_unretained NSString *collectUrlStr;
	__unsafe_unretained NSString *reviewUrlStr;
} CurrentEntriesAttributes;

@interface CurrentEntriesID : NSManagedObjectID {}
@end

@interface _CurrentEntries : RHManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) CurrentEntriesID* objectID;

@property (nonatomic, strong) NSString* collectUrlStr;

//- (BOOL)validateCollectUrlStr:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* reviewUrlStr;

//- (BOOL)validateReviewUrlStr:(id*)value_ error:(NSError**)error_;

@end

@interface _CurrentEntries (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveCollectUrlStr;
- (void)setPrimitiveCollectUrlStr:(NSString*)value;

- (NSString*)primitiveReviewUrlStr;
- (void)setPrimitiveReviewUrlStr:(NSString*)value;

@end
