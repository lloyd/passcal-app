// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to IosFields.h instead.

#import <CoreData/CoreData.h>
#import "RHManagedObject.h"

extern const struct IosFieldsAttributes {
	__unsafe_unretained NSString *appBuildNum;
	__unsafe_unretained NSString *appVersion;
	__unsafe_unretained NSString *iosDeviceModel;
	__unsafe_unretained NSString *iosDeviceUdid;
	__unsafe_unretained NSString *nodeID;
	__unsafe_unretained NSString *revisionNum;
	__unsafe_unretained NSString *treeID;
} IosFieldsAttributes;

extern const struct IosFieldsRelationships {
	__unsafe_unretained NSString *entry;
} IosFieldsRelationships;

@class Entry;

@interface IosFieldsID : NSManagedObjectID {}
@end

@interface _IosFields : RHManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) IosFieldsID* objectID;

@property (nonatomic, strong) NSNumber* appBuildNum;

@property (atomic) int32_t appBuildNumValue;
- (int32_t)appBuildNumValue;
- (void)setAppBuildNumValue:(int32_t)value_;

//- (BOOL)validateAppBuildNum:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* appVersion;

//- (BOOL)validateAppVersion:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* iosDeviceModel;

//- (BOOL)validateIosDeviceModel:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* iosDeviceUdid;

//- (BOOL)validateIosDeviceUdid:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* nodeID;

//- (BOOL)validateNodeID:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* revisionNum;

@property (atomic) int32_t revisionNumValue;
- (int32_t)revisionNumValue;
- (void)setRevisionNumValue:(int32_t)value_;

//- (BOOL)validateRevisionNum:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* treeID;

//- (BOOL)validateTreeID:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *entry;

- (NSMutableSet*)entrySet;

@end

@interface _IosFields (EntryCoreDataGeneratedAccessors)
- (void)addEntry:(NSSet*)value_;
- (void)removeEntry:(NSSet*)value_;
- (void)addEntryObject:(Entry*)value_;
- (void)removeEntryObject:(Entry*)value_;

@end

@interface _IosFields (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveAppBuildNum;
- (void)setPrimitiveAppBuildNum:(NSNumber*)value;

- (int32_t)primitiveAppBuildNumValue;
- (void)setPrimitiveAppBuildNumValue:(int32_t)value_;

- (NSString*)primitiveAppVersion;
- (void)setPrimitiveAppVersion:(NSString*)value;

- (NSString*)primitiveIosDeviceModel;
- (void)setPrimitiveIosDeviceModel:(NSString*)value;

- (NSString*)primitiveIosDeviceUdid;
- (void)setPrimitiveIosDeviceUdid:(NSString*)value;

- (NSString*)primitiveNodeID;
- (void)setPrimitiveNodeID:(NSString*)value;

- (NSNumber*)primitiveRevisionNum;
- (void)setPrimitiveRevisionNum:(NSNumber*)value;

- (int32_t)primitiveRevisionNumValue;
- (void)setPrimitiveRevisionNumValue:(int32_t)value_;

- (NSString*)primitiveTreeID;
- (void)setPrimitiveTreeID:(NSString*)value;

- (NSMutableSet*)primitiveEntry;
- (void)setPrimitiveEntry:(NSMutableSet*)value;

@end
