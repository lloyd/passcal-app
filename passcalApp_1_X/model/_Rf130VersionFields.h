// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Rf130VersionFields.h instead.

#import <CoreData/CoreData.h>
#import "RHManagedObject.h"

extern const struct Rf130VersionFieldsAttributes {
	__unsafe_unretained NSString *board0Acronym;
	__unsafe_unretained NSString *board0FpgaNum;
	__unsafe_unretained NSString *board0FpgaVersion;
	__unsafe_unretained NSString *board0Revision;
	__unsafe_unretained NSString *board0SerialNum;
	__unsafe_unretained NSString *board1Acronym;
	__unsafe_unretained NSString *board1FpgaNum;
	__unsafe_unretained NSString *board1FpgaVersion;
	__unsafe_unretained NSString *board1Revision;
	__unsafe_unretained NSString *board1SerialNum;
	__unsafe_unretained NSString *board2Acronym;
	__unsafe_unretained NSString *board2FpgaNum;
	__unsafe_unretained NSString *board2FpgaVersion;
	__unsafe_unretained NSString *board2Revision;
	__unsafe_unretained NSString *board2SerialNum;
	__unsafe_unretained NSString *board3Acronym;
	__unsafe_unretained NSString *board3FpgaNum;
	__unsafe_unretained NSString *board3FpgaVersion;
	__unsafe_unretained NSString *board3Revision;
	__unsafe_unretained NSString *board3SerialNum;
	__unsafe_unretained NSString *dasCpuVersion;
	__unsafe_unretained NSString *nodeID;
	__unsafe_unretained NSString *revisionNum;
	__unsafe_unretained NSString *sensorManufacturer;
	__unsafe_unretained NSString *sensorModel;
	__unsafe_unretained NSString *sensorSerialNumber;
	__unsafe_unretained NSString *treeID;
} Rf130VersionFieldsAttributes;

extern const struct Rf130VersionFieldsRelationships {
	__unsafe_unretained NSString *entry;
} Rf130VersionFieldsRelationships;

@class Entry;

@interface Rf130VersionFieldsID : NSManagedObjectID {}
@end

@interface _Rf130VersionFields : RHManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) Rf130VersionFieldsID* objectID;

@property (nonatomic, strong) NSString* board0Acronym;

//- (BOOL)validateBoard0Acronym:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* board0FpgaNum;

//- (BOOL)validateBoard0FpgaNum:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* board0FpgaVersion;

//- (BOOL)validateBoard0FpgaVersion:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* board0Revision;

//- (BOOL)validateBoard0Revision:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* board0SerialNum;

//- (BOOL)validateBoard0SerialNum:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* board1Acronym;

//- (BOOL)validateBoard1Acronym:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* board1FpgaNum;

//- (BOOL)validateBoard1FpgaNum:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* board1FpgaVersion;

//- (BOOL)validateBoard1FpgaVersion:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* board1Revision;

//- (BOOL)validateBoard1Revision:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* board1SerialNum;

//- (BOOL)validateBoard1SerialNum:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* board2Acronym;

//- (BOOL)validateBoard2Acronym:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* board2FpgaNum;

//- (BOOL)validateBoard2FpgaNum:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* board2FpgaVersion;

//- (BOOL)validateBoard2FpgaVersion:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* board2Revision;

//- (BOOL)validateBoard2Revision:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* board2SerialNum;

//- (BOOL)validateBoard2SerialNum:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* board3Acronym;

//- (BOOL)validateBoard3Acronym:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* board3FpgaNum;

//- (BOOL)validateBoard3FpgaNum:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* board3FpgaVersion;

//- (BOOL)validateBoard3FpgaVersion:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* board3Revision;

//- (BOOL)validateBoard3Revision:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* board3SerialNum;

//- (BOOL)validateBoard3SerialNum:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* dasCpuVersion;

//- (BOOL)validateDasCpuVersion:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* nodeID;

//- (BOOL)validateNodeID:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* revisionNum;

@property (atomic) int32_t revisionNumValue;
- (int32_t)revisionNumValue;
- (void)setRevisionNumValue:(int32_t)value_;

//- (BOOL)validateRevisionNum:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* sensorManufacturer;

//- (BOOL)validateSensorManufacturer:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* sensorModel;

//- (BOOL)validateSensorModel:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* sensorSerialNumber;

//- (BOOL)validateSensorSerialNumber:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* treeID;

//- (BOOL)validateTreeID:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *entry;

- (NSMutableSet*)entrySet;

@end

@interface _Rf130VersionFields (EntryCoreDataGeneratedAccessors)
- (void)addEntry:(NSSet*)value_;
- (void)removeEntry:(NSSet*)value_;
- (void)addEntryObject:(Entry*)value_;
- (void)removeEntryObject:(Entry*)value_;

@end

@interface _Rf130VersionFields (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveBoard0Acronym;
- (void)setPrimitiveBoard0Acronym:(NSString*)value;

- (NSString*)primitiveBoard0FpgaNum;
- (void)setPrimitiveBoard0FpgaNum:(NSString*)value;

- (NSString*)primitiveBoard0FpgaVersion;
- (void)setPrimitiveBoard0FpgaVersion:(NSString*)value;

- (NSString*)primitiveBoard0Revision;
- (void)setPrimitiveBoard0Revision:(NSString*)value;

- (NSString*)primitiveBoard0SerialNum;
- (void)setPrimitiveBoard0SerialNum:(NSString*)value;

- (NSString*)primitiveBoard1Acronym;
- (void)setPrimitiveBoard1Acronym:(NSString*)value;

- (NSString*)primitiveBoard1FpgaNum;
- (void)setPrimitiveBoard1FpgaNum:(NSString*)value;

- (NSString*)primitiveBoard1FpgaVersion;
- (void)setPrimitiveBoard1FpgaVersion:(NSString*)value;

- (NSString*)primitiveBoard1Revision;
- (void)setPrimitiveBoard1Revision:(NSString*)value;

- (NSString*)primitiveBoard1SerialNum;
- (void)setPrimitiveBoard1SerialNum:(NSString*)value;

- (NSString*)primitiveBoard2Acronym;
- (void)setPrimitiveBoard2Acronym:(NSString*)value;

- (NSString*)primitiveBoard2FpgaNum;
- (void)setPrimitiveBoard2FpgaNum:(NSString*)value;

- (NSString*)primitiveBoard2FpgaVersion;
- (void)setPrimitiveBoard2FpgaVersion:(NSString*)value;

- (NSString*)primitiveBoard2Revision;
- (void)setPrimitiveBoard2Revision:(NSString*)value;

- (NSString*)primitiveBoard2SerialNum;
- (void)setPrimitiveBoard2SerialNum:(NSString*)value;

- (NSString*)primitiveBoard3Acronym;
- (void)setPrimitiveBoard3Acronym:(NSString*)value;

- (NSString*)primitiveBoard3FpgaNum;
- (void)setPrimitiveBoard3FpgaNum:(NSString*)value;

- (NSString*)primitiveBoard3FpgaVersion;
- (void)setPrimitiveBoard3FpgaVersion:(NSString*)value;

- (NSString*)primitiveBoard3Revision;
- (void)setPrimitiveBoard3Revision:(NSString*)value;

- (NSString*)primitiveBoard3SerialNum;
- (void)setPrimitiveBoard3SerialNum:(NSString*)value;

- (NSString*)primitiveDasCpuVersion;
- (void)setPrimitiveDasCpuVersion:(NSString*)value;

- (NSString*)primitiveNodeID;
- (void)setPrimitiveNodeID:(NSString*)value;

- (NSNumber*)primitiveRevisionNum;
- (void)setPrimitiveRevisionNum:(NSNumber*)value;

- (int32_t)primitiveRevisionNumValue;
- (void)setPrimitiveRevisionNumValue:(int32_t)value_;

- (NSString*)primitiveSensorManufacturer;
- (void)setPrimitiveSensorManufacturer:(NSString*)value;

- (NSString*)primitiveSensorModel;
- (void)setPrimitiveSensorModel:(NSString*)value;

- (NSString*)primitiveSensorSerialNumber;
- (void)setPrimitiveSensorSerialNumber:(NSString*)value;

- (NSString*)primitiveTreeID;
- (void)setPrimitiveTreeID:(NSString*)value;

- (NSMutableSet*)primitiveEntry;
- (void)setPrimitiveEntry:(NSMutableSet*)value;

@end
