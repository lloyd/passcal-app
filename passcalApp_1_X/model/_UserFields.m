// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UserFields.m instead.

#import "_UserFields.h"

const struct UserFieldsAttributes UserFieldsAttributes = {
	.batteryVoltage = @"batteryVoltage",
	.clockSerialNumber = @"clockSerialNumber",
	.departureTime = @"departureTime",
	.enclosureSerialNumber = @"enclosureSerialNumber",
	.experimentName = @"experimentName",
	.fieldTeam = @"fieldTeam",
	.flashDisk1SerialNum = @"flashDisk1SerialNum",
	.flashDisk2SerialNum = @"flashDisk2SerialNum",
	.gpsAltitude = @"gpsAltitude",
	.gpsLatitude = @"gpsLatitude",
	.gpsLongitude = @"gpsLongitude",
	.magneticDeclination = @"magneticDeclination",
	.nodeID = @"nodeID",
	.powerBoxApv = @"powerBoxApv",
	.powerBoxSerialNumber = @"powerBoxSerialNumber",
	.powerBoxVp = @"powerBoxVp",
	.powerBoxVpn = @"powerBoxVpn",
	.powerBoxVs = @"powerBoxVs",
	.revisionNum = @"revisionNum",
	.sensor1SerialNumber = @"sensor1SerialNumber",
	.sensor2SerialNumber = @"sensor2SerialNumber",
	.sensorOrientation = @"sensorOrientation",
	.siteContact = @"siteContact",
	.solarPanelOrientation = @"solarPanelOrientation",
	.solarPanelSize = @"solarPanelSize",
	.solarPanelVoltage = @"solarPanelVoltage",
	.stationName = @"stationName",
	.telemetrySerialNumber = @"telemetrySerialNumber",
	.treeID = @"treeID",
	.userNotes = @"userNotes",
	.weatherNotes = @"weatherNotes",
};

const struct UserFieldsRelationships UserFieldsRelationships = {
	.entry = @"entry",
};

@implementation UserFieldsID
@end

@implementation _UserFields

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"UserFields" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"UserFields";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"UserFields" inManagedObjectContext:moc_];
}

- (UserFieldsID*)objectID {
	return (UserFieldsID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"batteryVoltageValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"batteryVoltage"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"gpsAltitudeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"gpsAltitude"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"gpsLatitudeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"gpsLatitude"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"gpsLongitudeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"gpsLongitude"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"magneticDeclinationValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"magneticDeclination"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"powerBoxApvValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"powerBoxApv"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"powerBoxVpValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"powerBoxVp"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"powerBoxVpnValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"powerBoxVpn"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"powerBoxVsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"powerBoxVs"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"revisionNumValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"revisionNum"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensorOrientationValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensorOrientation"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"solarPanelOrientationValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"solarPanelOrientation"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"solarPanelVoltageValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"solarPanelVoltage"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic batteryVoltage;

- (float)batteryVoltageValue {
	NSNumber *result = [self batteryVoltage];
	return [result floatValue];
}

- (void)setBatteryVoltageValue:(float)value_ {
	[self setBatteryVoltage:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveBatteryVoltageValue {
	NSNumber *result = [self primitiveBatteryVoltage];
	return [result floatValue];
}

- (void)setPrimitiveBatteryVoltageValue:(float)value_ {
	[self setPrimitiveBatteryVoltage:[NSNumber numberWithFloat:value_]];
}

@dynamic clockSerialNumber;

@dynamic departureTime;

@dynamic enclosureSerialNumber;

@dynamic experimentName;

@dynamic fieldTeam;

@dynamic flashDisk1SerialNum;

@dynamic flashDisk2SerialNum;

@dynamic gpsAltitude;

- (float)gpsAltitudeValue {
	NSNumber *result = [self gpsAltitude];
	return [result floatValue];
}

- (void)setGpsAltitudeValue:(float)value_ {
	[self setGpsAltitude:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveGpsAltitudeValue {
	NSNumber *result = [self primitiveGpsAltitude];
	return [result floatValue];
}

- (void)setPrimitiveGpsAltitudeValue:(float)value_ {
	[self setPrimitiveGpsAltitude:[NSNumber numberWithFloat:value_]];
}

@dynamic gpsLatitude;

- (float)gpsLatitudeValue {
	NSNumber *result = [self gpsLatitude];
	return [result floatValue];
}

- (void)setGpsLatitudeValue:(float)value_ {
	[self setGpsLatitude:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveGpsLatitudeValue {
	NSNumber *result = [self primitiveGpsLatitude];
	return [result floatValue];
}

- (void)setPrimitiveGpsLatitudeValue:(float)value_ {
	[self setPrimitiveGpsLatitude:[NSNumber numberWithFloat:value_]];
}

@dynamic gpsLongitude;

- (float)gpsLongitudeValue {
	NSNumber *result = [self gpsLongitude];
	return [result floatValue];
}

- (void)setGpsLongitudeValue:(float)value_ {
	[self setGpsLongitude:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveGpsLongitudeValue {
	NSNumber *result = [self primitiveGpsLongitude];
	return [result floatValue];
}

- (void)setPrimitiveGpsLongitudeValue:(float)value_ {
	[self setPrimitiveGpsLongitude:[NSNumber numberWithFloat:value_]];
}

@dynamic magneticDeclination;

- (float)magneticDeclinationValue {
	NSNumber *result = [self magneticDeclination];
	return [result floatValue];
}

- (void)setMagneticDeclinationValue:(float)value_ {
	[self setMagneticDeclination:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveMagneticDeclinationValue {
	NSNumber *result = [self primitiveMagneticDeclination];
	return [result floatValue];
}

- (void)setPrimitiveMagneticDeclinationValue:(float)value_ {
	[self setPrimitiveMagneticDeclination:[NSNumber numberWithFloat:value_]];
}

@dynamic nodeID;

@dynamic powerBoxApv;

- (float)powerBoxApvValue {
	NSNumber *result = [self powerBoxApv];
	return [result floatValue];
}

- (void)setPowerBoxApvValue:(float)value_ {
	[self setPowerBoxApv:[NSNumber numberWithFloat:value_]];
}

- (float)primitivePowerBoxApvValue {
	NSNumber *result = [self primitivePowerBoxApv];
	return [result floatValue];
}

- (void)setPrimitivePowerBoxApvValue:(float)value_ {
	[self setPrimitivePowerBoxApv:[NSNumber numberWithFloat:value_]];
}

@dynamic powerBoxSerialNumber;

@dynamic powerBoxVp;

- (float)powerBoxVpValue {
	NSNumber *result = [self powerBoxVp];
	return [result floatValue];
}

- (void)setPowerBoxVpValue:(float)value_ {
	[self setPowerBoxVp:[NSNumber numberWithFloat:value_]];
}

- (float)primitivePowerBoxVpValue {
	NSNumber *result = [self primitivePowerBoxVp];
	return [result floatValue];
}

- (void)setPrimitivePowerBoxVpValue:(float)value_ {
	[self setPrimitivePowerBoxVp:[NSNumber numberWithFloat:value_]];
}

@dynamic powerBoxVpn;

- (float)powerBoxVpnValue {
	NSNumber *result = [self powerBoxVpn];
	return [result floatValue];
}

- (void)setPowerBoxVpnValue:(float)value_ {
	[self setPowerBoxVpn:[NSNumber numberWithFloat:value_]];
}

- (float)primitivePowerBoxVpnValue {
	NSNumber *result = [self primitivePowerBoxVpn];
	return [result floatValue];
}

- (void)setPrimitivePowerBoxVpnValue:(float)value_ {
	[self setPrimitivePowerBoxVpn:[NSNumber numberWithFloat:value_]];
}

@dynamic powerBoxVs;

- (float)powerBoxVsValue {
	NSNumber *result = [self powerBoxVs];
	return [result floatValue];
}

- (void)setPowerBoxVsValue:(float)value_ {
	[self setPowerBoxVs:[NSNumber numberWithFloat:value_]];
}

- (float)primitivePowerBoxVsValue {
	NSNumber *result = [self primitivePowerBoxVs];
	return [result floatValue];
}

- (void)setPrimitivePowerBoxVsValue:(float)value_ {
	[self setPrimitivePowerBoxVs:[NSNumber numberWithFloat:value_]];
}

@dynamic revisionNum;

- (int32_t)revisionNumValue {
	NSNumber *result = [self revisionNum];
	return [result intValue];
}

- (void)setRevisionNumValue:(int32_t)value_ {
	[self setRevisionNum:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveRevisionNumValue {
	NSNumber *result = [self primitiveRevisionNum];
	return [result intValue];
}

- (void)setPrimitiveRevisionNumValue:(int32_t)value_ {
	[self setPrimitiveRevisionNum:[NSNumber numberWithInt:value_]];
}

@dynamic sensor1SerialNumber;

@dynamic sensor2SerialNumber;

@dynamic sensorOrientation;

- (float)sensorOrientationValue {
	NSNumber *result = [self sensorOrientation];
	return [result floatValue];
}

- (void)setSensorOrientationValue:(float)value_ {
	[self setSensorOrientation:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveSensorOrientationValue {
	NSNumber *result = [self primitiveSensorOrientation];
	return [result floatValue];
}

- (void)setPrimitiveSensorOrientationValue:(float)value_ {
	[self setPrimitiveSensorOrientation:[NSNumber numberWithFloat:value_]];
}

@dynamic siteContact;

@dynamic solarPanelOrientation;

- (float)solarPanelOrientationValue {
	NSNumber *result = [self solarPanelOrientation];
	return [result floatValue];
}

- (void)setSolarPanelOrientationValue:(float)value_ {
	[self setSolarPanelOrientation:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveSolarPanelOrientationValue {
	NSNumber *result = [self primitiveSolarPanelOrientation];
	return [result floatValue];
}

- (void)setPrimitiveSolarPanelOrientationValue:(float)value_ {
	[self setPrimitiveSolarPanelOrientation:[NSNumber numberWithFloat:value_]];
}

@dynamic solarPanelSize;

@dynamic solarPanelVoltage;

- (float)solarPanelVoltageValue {
	NSNumber *result = [self solarPanelVoltage];
	return [result floatValue];
}

- (void)setSolarPanelVoltageValue:(float)value_ {
	[self setSolarPanelVoltage:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveSolarPanelVoltageValue {
	NSNumber *result = [self primitiveSolarPanelVoltage];
	return [result floatValue];
}

- (void)setPrimitiveSolarPanelVoltageValue:(float)value_ {
	[self setPrimitiveSolarPanelVoltage:[NSNumber numberWithFloat:value_]];
}

@dynamic stationName;

@dynamic telemetrySerialNumber;

@dynamic treeID;

@dynamic userNotes;

@dynamic weatherNotes;

@dynamic entry;

- (NSMutableSet*)entrySet {
	[self willAccessValueForKey:@"entry"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"entry"];

	[self didAccessValueForKey:@"entry"];
	return result;
}

@end

