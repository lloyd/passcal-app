#import "_Entry.h"
#import "RevControlHelper.h"

@interface Entry : _Entry<revControlFields>
+(id) initWithDictionary: (NSDictionary *) d;
+(BOOL) doesNodeIdExist: (NSString*) nodeId;
+(BOOL) updateRelations: (NSString*) targetNodeID : (NSDictionary *) relationsD;

-(id) createNewRevision;
-(id) createNewRevision: (NSDictionary*) changes;
@end
