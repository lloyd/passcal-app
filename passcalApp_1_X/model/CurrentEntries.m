#import "CurrentEntries.h"
#import "RHManagedObjectContextManager.h"

@interface CurrentEntries ()

// Private interface goes here.

@end

@implementation CurrentEntries
@synthesize reviewUrl = _reviewUrl;
@synthesize collectUrl = _collectUrl;

// Make loose singleton within core data
+(id) getInstance {
    CurrentEntries *temp = [CurrentEntries newOrExistingEntityWithPredicate:[NSPredicate predicateWithFormat:@"1=1"]
                                                                      error:nil];
    return temp;
}

+(NSString *)modelName {
    return @"passcalApp_1_X";
}

-(void) setReviewUrl:(NSURL *)url {
    if (url == nil)
        self.reviewUrlStr = nil;
    else
        self.reviewUrlStr = [url absoluteString];
}
-(NSURL*) reviewUrl {
    if (self.reviewUrlStr == nil)
        return nil;
    return [[NSURL alloc] initWithString:self.reviewUrlStr];
}

-(void) setCollectUrl:(NSURL *)url {
    if (url == nil)
        self.collectUrlStr = nil;
    else
        self.collectUrlStr = [url absoluteString];
}
-(NSURL*) collectUrl {
    if (self.collectUrlStr == nil)
        return nil;
    return [[NSURL alloc] initWithString:self.collectUrlStr];
}

+(void) updateCommitEntry: (Entry*) newEntry{
    [Entry commit];  // Better to double commit than get a bad URL
    
    // There should only be a single currentEntry
    CurrentEntries *cEntry = [CurrentEntries getInstance];
    cEntry.collectUrl = newEntry.getUrl;
    [CurrentEntries commit];
}
+(void) updateReviewEntry: (Entry*) newEntry{
    [Entry commit];  // Better to double commit than get a bad URL
    
    // There should only be a single currentEntry
    CurrentEntries *cEntry = [CurrentEntries getInstance];
    cEntry.reviewUrl = newEntry.getUrl;
    [CurrentEntries commit];
}

+(void) updateReviewEntryIfMatchingTreeID: (Entry*) newEntry {
    CurrentEntries *cEntry = [CurrentEntries getInstance];
    Entry *currEntry = [Entry existingEntityWithUrl:cEntry.reviewUrl error:nil];
    if (currEntry == nil)
        return;
    
    if ([currEntry.treeID isEqualToString:newEntry.treeID])
    {
        [self updateReviewEntry:newEntry];
    }
}



@end
