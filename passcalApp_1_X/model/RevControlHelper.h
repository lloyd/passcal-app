//
//  RevControlHelper.h
//  passcalApp_1_X
//
//  Created by field on 8/23/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RHManagedObject.h"

@protocol revControlFields;

@interface RevControlHelper : NSObject
+(id) createRevision: (id <revControlFields>) fields;
+(void) commitAll;
+(NSDictionary*) exportRelations: (id <revControlFields>) fields;

+(id) initWithDictionary: (Class) entityClass : (NSDictionary *) d;
+(BOOL) doesNodeIdExist: (Class) entityClass : (NSString*) nodeId;
@end

@protocol revControlFields
// These properties should be taken care of mogenerator
@property (atomic) int32_t revisionNumValue;
@property (nonatomic, strong) NSString* nodeID; // Unique ID for revision
@property (nonatomic, strong) NSString* treeID; // Unique ID for set of revisions

-(NSDictionary*)getAttributesByName; // From RHManagedObject
-(NSDictionary*)getRelationshipsByName; // From RHManagedObject
-(id)clone; // From RHManagedObject
-(id)valueForKey:(NSString *)key; // KV Coding
-(id)valueForKeyPath:(NSString *)keyPath; // KV Coding

//+(NSUInteger)countWithPredicate:(NSPredicate *)predicate error:(NSError **)error; // From RHManagedObject

-(id)createNewRevision;
@end