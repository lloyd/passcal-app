#import "Rf130StationFields.h"

@interface Rf130StationFields ()

// Private interface goes here.

@end

@implementation Rf130StationFields

+(NSString *)modelName {
    return @"passcalApp_1_X";
}

+(id) initWithDictionary: (NSDictionary *) d {
    return [RevControlHelper initWithDictionary: [Rf130StationFields class] : d];
}

+(BOOL) doesNodeIdExist: (NSString*) nodeId {
    return [RevControlHelper doesNodeIdExist:[Rf130StationFields class] :nodeId];
}

-(id) createNewRevision {
    return [RevControlHelper createRevision:self];
}

@end
