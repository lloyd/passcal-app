#import "_UserFields.h"
#import "RevControlHelper.h"

@interface UserFields : _UserFields<revControlFields>
+(id) initWithDictionary: (NSDictionary *) d;
+(BOOL) doesNodeIdExist: (NSString*) nodeId;

-(id) createNewRevision;
@end
