#import "_Rf130StatusFields.h"
#import "RevControlHelper.h"

@interface Rf130StatusFields : _Rf130StatusFields<revControlFields>
+(id) initWithDictionary: (NSDictionary *) d;
+(BOOL) doesNodeIdExist: (NSString*) nodeId;

-(id) createNewRevision;
@end
