// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Rf130StatusFields.m instead.

#import "_Rf130StatusFields.h"

const struct Rf130StatusFieldsAttributes Rf130StatusFieldsAttributes = {
	.acquisitionActive = @"acquisitionActive",
	.acquisitionRequested = @"acquisitionRequested",
	.backupPower = @"backupPower",
	.channelDasRecording = @"channelDasRecording",
	.currentDisk = @"currentDisk",
	.dasDataStreams = @"dasDataStreams",
	.dasTemperature = @"dasTemperature",
	.dasTime = @"dasTime",
	.disk1PercentUsed = @"disk1PercentUsed",
	.disk1Total = @"disk1Total",
	.disk1Used = @"disk1Used",
	.disk2PercentUsed = @"disk2PercentUsed",
	.disk2Total = @"disk2Total",
	.disk2Used = @"disk2Used",
	.eventCount = @"eventCount",
	.gpsAltitude = @"gpsAltitude",
	.gpsLastLock = @"gpsLastLock",
	.gpsLastLockPhase = @"gpsLastLockPhase",
	.gpsLatitude = @"gpsLatitude",
	.gpsLocked = @"gpsLocked",
	.gpsLongitude = @"gpsLongitude",
	.gpsMode = @"gpsMode",
	.gpsOn = @"gpsOn",
	.gpsSatsTracked = @"gpsSatsTracked",
	.inputPower = @"inputPower",
	.nodeID = @"nodeID",
	.ramTotal = @"ramTotal",
	.ramUsed = @"ramUsed",
	.revisionNum = @"revisionNum",
	.treeID = @"treeID",
};

const struct Rf130StatusFieldsRelationships Rf130StatusFieldsRelationships = {
	.entry = @"entry",
};

@implementation Rf130StatusFieldsID
@end

@implementation _Rf130StatusFields

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Rf130StatusFields" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Rf130StatusFields";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Rf130StatusFields" inManagedObjectContext:moc_];
}

- (Rf130StatusFieldsID*)objectID {
	return (Rf130StatusFieldsID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"acquisitionActiveValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"acquisitionActive"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"acquisitionRequestedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"acquisitionRequested"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"backupPowerValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"backupPower"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"currentDiskValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"currentDisk"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"dasTemperatureValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"dasTemperature"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"disk1PercentUsedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"disk1PercentUsed"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"disk1TotalValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"disk1Total"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"disk1UsedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"disk1Used"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"disk2PercentUsedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"disk2PercentUsed"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"disk2TotalValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"disk2Total"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"disk2UsedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"disk2Used"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"eventCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"eventCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"gpsAltitudeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"gpsAltitude"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"gpsLastLockValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"gpsLastLock"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"gpsLastLockPhaseValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"gpsLastLockPhase"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"gpsLatitudeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"gpsLatitude"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"gpsLockedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"gpsLocked"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"gpsLongitudeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"gpsLongitude"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"gpsOnValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"gpsOn"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"gpsSatsTrackedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"gpsSatsTracked"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"inputPowerValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"inputPower"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"ramTotalValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"ramTotal"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"ramUsedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"ramUsed"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"revisionNumValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"revisionNum"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic acquisitionActive;

- (BOOL)acquisitionActiveValue {
	NSNumber *result = [self acquisitionActive];
	return [result boolValue];
}

- (void)setAcquisitionActiveValue:(BOOL)value_ {
	[self setAcquisitionActive:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveAcquisitionActiveValue {
	NSNumber *result = [self primitiveAcquisitionActive];
	return [result boolValue];
}

- (void)setPrimitiveAcquisitionActiveValue:(BOOL)value_ {
	[self setPrimitiveAcquisitionActive:[NSNumber numberWithBool:value_]];
}

@dynamic acquisitionRequested;

- (BOOL)acquisitionRequestedValue {
	NSNumber *result = [self acquisitionRequested];
	return [result boolValue];
}

- (void)setAcquisitionRequestedValue:(BOOL)value_ {
	[self setAcquisitionRequested:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveAcquisitionRequestedValue {
	NSNumber *result = [self primitiveAcquisitionRequested];
	return [result boolValue];
}

- (void)setPrimitiveAcquisitionRequestedValue:(BOOL)value_ {
	[self setPrimitiveAcquisitionRequested:[NSNumber numberWithBool:value_]];
}

@dynamic backupPower;

- (float)backupPowerValue {
	NSNumber *result = [self backupPower];
	return [result floatValue];
}

- (void)setBackupPowerValue:(float)value_ {
	[self setBackupPower:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveBackupPowerValue {
	NSNumber *result = [self primitiveBackupPower];
	return [result floatValue];
}

- (void)setPrimitiveBackupPowerValue:(float)value_ {
	[self setPrimitiveBackupPower:[NSNumber numberWithFloat:value_]];
}

@dynamic channelDasRecording;

@dynamic currentDisk;

- (int32_t)currentDiskValue {
	NSNumber *result = [self currentDisk];
	return [result intValue];
}

- (void)setCurrentDiskValue:(int32_t)value_ {
	[self setCurrentDisk:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCurrentDiskValue {
	NSNumber *result = [self primitiveCurrentDisk];
	return [result intValue];
}

- (void)setPrimitiveCurrentDiskValue:(int32_t)value_ {
	[self setPrimitiveCurrentDisk:[NSNumber numberWithInt:value_]];
}

@dynamic dasDataStreams;

@dynamic dasTemperature;

- (float)dasTemperatureValue {
	NSNumber *result = [self dasTemperature];
	return [result floatValue];
}

- (void)setDasTemperatureValue:(float)value_ {
	[self setDasTemperature:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveDasTemperatureValue {
	NSNumber *result = [self primitiveDasTemperature];
	return [result floatValue];
}

- (void)setPrimitiveDasTemperatureValue:(float)value_ {
	[self setPrimitiveDasTemperature:[NSNumber numberWithFloat:value_]];
}

@dynamic dasTime;

@dynamic disk1PercentUsed;

- (float)disk1PercentUsedValue {
	NSNumber *result = [self disk1PercentUsed];
	return [result floatValue];
}

- (void)setDisk1PercentUsedValue:(float)value_ {
	[self setDisk1PercentUsed:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveDisk1PercentUsedValue {
	NSNumber *result = [self primitiveDisk1PercentUsed];
	return [result floatValue];
}

- (void)setPrimitiveDisk1PercentUsedValue:(float)value_ {
	[self setPrimitiveDisk1PercentUsed:[NSNumber numberWithFloat:value_]];
}

@dynamic disk1Total;

- (int32_t)disk1TotalValue {
	NSNumber *result = [self disk1Total];
	return [result intValue];
}

- (void)setDisk1TotalValue:(int32_t)value_ {
	[self setDisk1Total:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveDisk1TotalValue {
	NSNumber *result = [self primitiveDisk1Total];
	return [result intValue];
}

- (void)setPrimitiveDisk1TotalValue:(int32_t)value_ {
	[self setPrimitiveDisk1Total:[NSNumber numberWithInt:value_]];
}

@dynamic disk1Used;

- (int32_t)disk1UsedValue {
	NSNumber *result = [self disk1Used];
	return [result intValue];
}

- (void)setDisk1UsedValue:(int32_t)value_ {
	[self setDisk1Used:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveDisk1UsedValue {
	NSNumber *result = [self primitiveDisk1Used];
	return [result intValue];
}

- (void)setPrimitiveDisk1UsedValue:(int32_t)value_ {
	[self setPrimitiveDisk1Used:[NSNumber numberWithInt:value_]];
}

@dynamic disk2PercentUsed;

- (float)disk2PercentUsedValue {
	NSNumber *result = [self disk2PercentUsed];
	return [result floatValue];
}

- (void)setDisk2PercentUsedValue:(float)value_ {
	[self setDisk2PercentUsed:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveDisk2PercentUsedValue {
	NSNumber *result = [self primitiveDisk2PercentUsed];
	return [result floatValue];
}

- (void)setPrimitiveDisk2PercentUsedValue:(float)value_ {
	[self setPrimitiveDisk2PercentUsed:[NSNumber numberWithFloat:value_]];
}

@dynamic disk2Total;

- (int32_t)disk2TotalValue {
	NSNumber *result = [self disk2Total];
	return [result intValue];
}

- (void)setDisk2TotalValue:(int32_t)value_ {
	[self setDisk2Total:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveDisk2TotalValue {
	NSNumber *result = [self primitiveDisk2Total];
	return [result intValue];
}

- (void)setPrimitiveDisk2TotalValue:(int32_t)value_ {
	[self setPrimitiveDisk2Total:[NSNumber numberWithInt:value_]];
}

@dynamic disk2Used;

- (int32_t)disk2UsedValue {
	NSNumber *result = [self disk2Used];
	return [result intValue];
}

- (void)setDisk2UsedValue:(int32_t)value_ {
	[self setDisk2Used:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveDisk2UsedValue {
	NSNumber *result = [self primitiveDisk2Used];
	return [result intValue];
}

- (void)setPrimitiveDisk2UsedValue:(int32_t)value_ {
	[self setPrimitiveDisk2Used:[NSNumber numberWithInt:value_]];
}

@dynamic eventCount;

- (int32_t)eventCountValue {
	NSNumber *result = [self eventCount];
	return [result intValue];
}

- (void)setEventCountValue:(int32_t)value_ {
	[self setEventCount:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveEventCountValue {
	NSNumber *result = [self primitiveEventCount];
	return [result intValue];
}

- (void)setPrimitiveEventCountValue:(int32_t)value_ {
	[self setPrimitiveEventCount:[NSNumber numberWithInt:value_]];
}

@dynamic gpsAltitude;

- (float)gpsAltitudeValue {
	NSNumber *result = [self gpsAltitude];
	return [result floatValue];
}

- (void)setGpsAltitudeValue:(float)value_ {
	[self setGpsAltitude:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveGpsAltitudeValue {
	NSNumber *result = [self primitiveGpsAltitude];
	return [result floatValue];
}

- (void)setPrimitiveGpsAltitudeValue:(float)value_ {
	[self setPrimitiveGpsAltitude:[NSNumber numberWithFloat:value_]];
}

@dynamic gpsLastLock;

- (int32_t)gpsLastLockValue {
	NSNumber *result = [self gpsLastLock];
	return [result intValue];
}

- (void)setGpsLastLockValue:(int32_t)value_ {
	[self setGpsLastLock:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveGpsLastLockValue {
	NSNumber *result = [self primitiveGpsLastLock];
	return [result intValue];
}

- (void)setPrimitiveGpsLastLockValue:(int32_t)value_ {
	[self setPrimitiveGpsLastLock:[NSNumber numberWithInt:value_]];
}

@dynamic gpsLastLockPhase;

- (int32_t)gpsLastLockPhaseValue {
	NSNumber *result = [self gpsLastLockPhase];
	return [result intValue];
}

- (void)setGpsLastLockPhaseValue:(int32_t)value_ {
	[self setGpsLastLockPhase:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveGpsLastLockPhaseValue {
	NSNumber *result = [self primitiveGpsLastLockPhase];
	return [result intValue];
}

- (void)setPrimitiveGpsLastLockPhaseValue:(int32_t)value_ {
	[self setPrimitiveGpsLastLockPhase:[NSNumber numberWithInt:value_]];
}

@dynamic gpsLatitude;

- (float)gpsLatitudeValue {
	NSNumber *result = [self gpsLatitude];
	return [result floatValue];
}

- (void)setGpsLatitudeValue:(float)value_ {
	[self setGpsLatitude:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveGpsLatitudeValue {
	NSNumber *result = [self primitiveGpsLatitude];
	return [result floatValue];
}

- (void)setPrimitiveGpsLatitudeValue:(float)value_ {
	[self setPrimitiveGpsLatitude:[NSNumber numberWithFloat:value_]];
}

@dynamic gpsLocked;

- (BOOL)gpsLockedValue {
	NSNumber *result = [self gpsLocked];
	return [result boolValue];
}

- (void)setGpsLockedValue:(BOOL)value_ {
	[self setGpsLocked:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveGpsLockedValue {
	NSNumber *result = [self primitiveGpsLocked];
	return [result boolValue];
}

- (void)setPrimitiveGpsLockedValue:(BOOL)value_ {
	[self setPrimitiveGpsLocked:[NSNumber numberWithBool:value_]];
}

@dynamic gpsLongitude;

- (float)gpsLongitudeValue {
	NSNumber *result = [self gpsLongitude];
	return [result floatValue];
}

- (void)setGpsLongitudeValue:(float)value_ {
	[self setGpsLongitude:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveGpsLongitudeValue {
	NSNumber *result = [self primitiveGpsLongitude];
	return [result floatValue];
}

- (void)setPrimitiveGpsLongitudeValue:(float)value_ {
	[self setPrimitiveGpsLongitude:[NSNumber numberWithFloat:value_]];
}

@dynamic gpsMode;

@dynamic gpsOn;

- (BOOL)gpsOnValue {
	NSNumber *result = [self gpsOn];
	return [result boolValue];
}

- (void)setGpsOnValue:(BOOL)value_ {
	[self setGpsOn:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveGpsOnValue {
	NSNumber *result = [self primitiveGpsOn];
	return [result boolValue];
}

- (void)setPrimitiveGpsOnValue:(BOOL)value_ {
	[self setPrimitiveGpsOn:[NSNumber numberWithBool:value_]];
}

@dynamic gpsSatsTracked;

- (int32_t)gpsSatsTrackedValue {
	NSNumber *result = [self gpsSatsTracked];
	return [result intValue];
}

- (void)setGpsSatsTrackedValue:(int32_t)value_ {
	[self setGpsSatsTracked:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveGpsSatsTrackedValue {
	NSNumber *result = [self primitiveGpsSatsTracked];
	return [result intValue];
}

- (void)setPrimitiveGpsSatsTrackedValue:(int32_t)value_ {
	[self setPrimitiveGpsSatsTracked:[NSNumber numberWithInt:value_]];
}

@dynamic inputPower;

- (float)inputPowerValue {
	NSNumber *result = [self inputPower];
	return [result floatValue];
}

- (void)setInputPowerValue:(float)value_ {
	[self setInputPower:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveInputPowerValue {
	NSNumber *result = [self primitiveInputPower];
	return [result floatValue];
}

- (void)setPrimitiveInputPowerValue:(float)value_ {
	[self setPrimitiveInputPower:[NSNumber numberWithFloat:value_]];
}

@dynamic nodeID;

@dynamic ramTotal;

- (int32_t)ramTotalValue {
	NSNumber *result = [self ramTotal];
	return [result intValue];
}

- (void)setRamTotalValue:(int32_t)value_ {
	[self setRamTotal:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveRamTotalValue {
	NSNumber *result = [self primitiveRamTotal];
	return [result intValue];
}

- (void)setPrimitiveRamTotalValue:(int32_t)value_ {
	[self setPrimitiveRamTotal:[NSNumber numberWithInt:value_]];
}

@dynamic ramUsed;

- (int32_t)ramUsedValue {
	NSNumber *result = [self ramUsed];
	return [result intValue];
}

- (void)setRamUsedValue:(int32_t)value_ {
	[self setRamUsed:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveRamUsedValue {
	NSNumber *result = [self primitiveRamUsed];
	return [result intValue];
}

- (void)setPrimitiveRamUsedValue:(int32_t)value_ {
	[self setPrimitiveRamUsed:[NSNumber numberWithInt:value_]];
}

@dynamic revisionNum;

- (int32_t)revisionNumValue {
	NSNumber *result = [self revisionNum];
	return [result intValue];
}

- (void)setRevisionNumValue:(int32_t)value_ {
	[self setRevisionNum:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveRevisionNumValue {
	NSNumber *result = [self primitiveRevisionNum];
	return [result intValue];
}

- (void)setPrimitiveRevisionNumValue:(int32_t)value_ {
	[self setPrimitiveRevisionNum:[NSNumber numberWithInt:value_]];
}

@dynamic treeID;

@dynamic entry;

- (NSMutableSet*)entrySet {
	[self willAccessValueForKey:@"entry"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"entry"];

	[self didAccessValueForKey:@"entry"];
	return result;
}

@end

