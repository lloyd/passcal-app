#import "IosFields.h"

@interface IosFields ()

// Private interface goes here.

@end

@implementation IosFields

+(NSString *)modelName {
    return @"passcalApp_1_X";
}

+(id) initWithDictionary: (NSDictionary *) d {
    return [RevControlHelper initWithDictionary: [IosFields class] : d];
}

+(BOOL) doesNodeIdExist: (NSString*) nodeId {
    return [RevControlHelper doesNodeIdExist:[IosFields class] :nodeId];
}

-(id) createNewRevision {
    return [RevControlHelper createRevision:self];
}
@end
