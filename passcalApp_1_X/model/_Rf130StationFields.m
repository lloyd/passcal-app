// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Rf130StationFields.m instead.

#import "_Rf130StationFields.h"

const struct Rf130StationFieldsAttributes Rf130StationFieldsAttributes = {
	.ch1Gain = @"ch1Gain",
	.ch1Name = @"ch1Name",
	.ch1SensorModel = @"ch1SensorModel",
	.ch1SensorSerialNumber = @"ch1SensorSerialNumber",
	.ch2Gain = @"ch2Gain",
	.ch2Name = @"ch2Name",
	.ch2SensorModel = @"ch2SensorModel",
	.ch2SensorSerialNumber = @"ch2SensorSerialNumber",
	.ch3Gain = @"ch3Gain",
	.ch3Name = @"ch3Name",
	.ch3SensorModel = @"ch3SensorModel",
	.ch3SensorSerialNumber = @"ch3SensorSerialNumber",
	.ch4Gain = @"ch4Gain",
	.ch4Name = @"ch4Name",
	.ch4SensorModel = @"ch4SensorModel",
	.ch4SensorSerialNumber = @"ch4SensorSerialNumber",
	.ch5Gain = @"ch5Gain",
	.ch5Name = @"ch5Name",
	.ch5SensorModel = @"ch5SensorModel",
	.ch5SensorSerialNumber = @"ch5SensorSerialNumber",
	.ch6Gain = @"ch6Gain",
	.ch6Name = @"ch6Name",
	.ch6SensorModel = @"ch6SensorModel",
	.ch6SensorSerialNumber = @"ch6SensorSerialNumber",
	.dasUnitId = @"dasUnitId",
	.experimentComment = @"experimentComment",
	.experimentName = @"experimentName",
	.experimentNumber = @"experimentNumber",
	.nodeID = @"nodeID",
	.revisionNum = @"revisionNum",
	.sensor1AutoReCenterAttemptInterval = @"sensor1AutoReCenterAttemptInterval",
	.sensor1AutoReCenterAttempts = @"sensor1AutoReCenterAttempts",
	.sensor1AutoReCenterCycleInterval = @"sensor1AutoReCenterCycleInterval",
	.sensor1AutoReCenterEnable = @"sensor1AutoReCenterEnable",
	.sensor1AutoReCenterLevelHorizontal = @"sensor1AutoReCenterLevelHorizontal",
	.sensor1AutoReCenterLevelVertical = @"sensor1AutoReCenterLevelVertical",
	.sensor1AuxData1 = @"sensor1AuxData1",
	.sensor1AuxData2 = @"sensor1AuxData2",
	.sensor1AuxData3 = @"sensor1AuxData3",
	.sensor2AutoReCenterAttemptInterval = @"sensor2AutoReCenterAttemptInterval",
	.sensor2AutoReCenterAttempts = @"sensor2AutoReCenterAttempts",
	.sensor2AutoReCenterCycleInterval = @"sensor2AutoReCenterCycleInterval",
	.sensor2AutoReCenterEnable = @"sensor2AutoReCenterEnable",
	.sensor2AutoReCenterLevelHorizontal = @"sensor2AutoReCenterLevelHorizontal",
	.sensor2AutoReCenterLevelVertical = @"sensor2AutoReCenterLevelVertical",
	.sensor2AuxData1 = @"sensor2AuxData1",
	.sensor2AuxData2 = @"sensor2AuxData2",
	.sensor2AuxData3 = @"sensor2AuxData3",
	.sensor3AutoReCenterAttemptInterval = @"sensor3AutoReCenterAttemptInterval",
	.sensor3AutoReCenterAttempts = @"sensor3AutoReCenterAttempts",
	.sensor3AutoReCenterCycleInterval = @"sensor3AutoReCenterCycleInterval",
	.sensor3AutoReCenterEnable = @"sensor3AutoReCenterEnable",
	.sensor3AutoReCenterLevelHorizontal = @"sensor3AutoReCenterLevelHorizontal",
	.sensor3AutoReCenterLevelVertical = @"sensor3AutoReCenterLevelVertical",
	.sensor3AuxData1 = @"sensor3AuxData1",
	.sensor3AuxData2 = @"sensor3AuxData2",
	.sensor3AuxData3 = @"sensor3AuxData3",
	.sensor4AutoReCenterAttemptInterval = @"sensor4AutoReCenterAttemptInterval",
	.sensor4AutoReCenterAttempts = @"sensor4AutoReCenterAttempts",
	.sensor4AutoReCenterCycleInterval = @"sensor4AutoReCenterCycleInterval",
	.sensor4AutoReCenterEnable = @"sensor4AutoReCenterEnable",
	.sensor4AutoReCenterLevelHorizontal = @"sensor4AutoReCenterLevelHorizontal",
	.sensor4AutoReCenterLevelVertical = @"sensor4AutoReCenterLevelVertical",
	.sensor4AuxData1 = @"sensor4AuxData1",
	.sensor4AuxData2 = @"sensor4AuxData2",
	.sensor4AuxData3 = @"sensor4AuxData3",
	.stationComment = @"stationComment",
	.stationName = @"stationName",
	.stationNumber = @"stationNumber",
	.stream1Channels = @"stream1Channels",
	.stream1SampleRate = @"stream1SampleRate",
	.stream2Channels = @"stream2Channels",
	.stream2SampleRate = @"stream2SampleRate",
	.stream3Channels = @"stream3Channels",
	.stream3SampleRate = @"stream3SampleRate",
	.stream4Channels = @"stream4Channels",
	.stream4SampleRate = @"stream4SampleRate",
	.stream5Channels = @"stream5Channels",
	.stream5SampleRate = @"stream5SampleRate",
	.stream6Channels = @"stream6Channels",
	.stream6SampleRate = @"stream6SampleRate",
	.stream7Channels = @"stream7Channels",
	.stream7SampleRate = @"stream7SampleRate",
	.stream8Channels = @"stream8Channels",
	.stream8SampleRate = @"stream8SampleRate",
	.treeID = @"treeID",
};

const struct Rf130StationFieldsRelationships Rf130StationFieldsRelationships = {
	.entry = @"entry",
};

@implementation Rf130StationFieldsID
@end

@implementation _Rf130StationFields

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Rf130StationFields" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Rf130StationFields";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Rf130StationFields" inManagedObjectContext:moc_];
}

- (Rf130StationFieldsID*)objectID {
	return (Rf130StationFieldsID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"ch1GainValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"ch1Gain"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"ch2GainValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"ch2Gain"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"ch3GainValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"ch3Gain"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"ch4GainValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"ch4Gain"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"ch5GainValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"ch5Gain"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"ch6GainValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"ch6Gain"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"dasUnitIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"dasUnitId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"experimentNumberValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"experimentNumber"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"revisionNumValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"revisionNum"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor1AutoReCenterAttemptIntervalValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor1AutoReCenterAttemptInterval"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor1AutoReCenterAttemptsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor1AutoReCenterAttempts"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor1AutoReCenterCycleIntervalValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor1AutoReCenterCycleInterval"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor1AutoReCenterEnableValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor1AutoReCenterEnable"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor1AutoReCenterLevelHorizontalValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor1AutoReCenterLevelHorizontal"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor1AutoReCenterLevelVerticalValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor1AutoReCenterLevelVertical"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor1AuxData1Value"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor1AuxData1"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor1AuxData2Value"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor1AuxData2"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor1AuxData3Value"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor1AuxData3"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor2AutoReCenterAttemptIntervalValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor2AutoReCenterAttemptInterval"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor2AutoReCenterAttemptsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor2AutoReCenterAttempts"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor2AutoReCenterCycleIntervalValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor2AutoReCenterCycleInterval"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor2AutoReCenterEnableValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor2AutoReCenterEnable"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor2AutoReCenterLevelHorizontalValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor2AutoReCenterLevelHorizontal"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor2AutoReCenterLevelVerticalValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor2AutoReCenterLevelVertical"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor2AuxData1Value"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor2AuxData1"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor2AuxData2Value"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor2AuxData2"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor2AuxData3Value"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor2AuxData3"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor3AutoReCenterAttemptIntervalValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor3AutoReCenterAttemptInterval"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor3AutoReCenterAttemptsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor3AutoReCenterAttempts"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor3AutoReCenterCycleIntervalValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor3AutoReCenterCycleInterval"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor3AutoReCenterEnableValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor3AutoReCenterEnable"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor3AutoReCenterLevelHorizontalValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor3AutoReCenterLevelHorizontal"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor3AutoReCenterLevelVerticalValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor3AutoReCenterLevelVertical"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor3AuxData1Value"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor3AuxData1"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor3AuxData2Value"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor3AuxData2"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor3AuxData3Value"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor3AuxData3"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor4AutoReCenterAttemptIntervalValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor4AutoReCenterAttemptInterval"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor4AutoReCenterAttemptsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor4AutoReCenterAttempts"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor4AutoReCenterCycleIntervalValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor4AutoReCenterCycleInterval"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor4AutoReCenterEnableValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor4AutoReCenterEnable"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor4AutoReCenterLevelHorizontalValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor4AutoReCenterLevelHorizontal"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor4AutoReCenterLevelVerticalValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor4AutoReCenterLevelVertical"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor4AuxData1Value"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor4AuxData1"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor4AuxData2Value"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor4AuxData2"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sensor4AuxData3Value"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sensor4AuxData3"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"stationNumberValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"stationNumber"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"stream1SampleRateValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"stream1SampleRate"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"stream2SampleRateValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"stream2SampleRate"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"stream3SampleRateValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"stream3SampleRate"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"stream4SampleRateValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"stream4SampleRate"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"stream5SampleRateValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"stream5SampleRate"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"stream6SampleRateValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"stream6SampleRate"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"stream7SampleRateValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"stream7SampleRate"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"stream8SampleRateValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"stream8SampleRate"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic ch1Gain;

- (float)ch1GainValue {
	NSNumber *result = [self ch1Gain];
	return [result floatValue];
}

- (void)setCh1GainValue:(float)value_ {
	[self setCh1Gain:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveCh1GainValue {
	NSNumber *result = [self primitiveCh1Gain];
	return [result floatValue];
}

- (void)setPrimitiveCh1GainValue:(float)value_ {
	[self setPrimitiveCh1Gain:[NSNumber numberWithFloat:value_]];
}

@dynamic ch1Name;

@dynamic ch1SensorModel;

@dynamic ch1SensorSerialNumber;

@dynamic ch2Gain;

- (float)ch2GainValue {
	NSNumber *result = [self ch2Gain];
	return [result floatValue];
}

- (void)setCh2GainValue:(float)value_ {
	[self setCh2Gain:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveCh2GainValue {
	NSNumber *result = [self primitiveCh2Gain];
	return [result floatValue];
}

- (void)setPrimitiveCh2GainValue:(float)value_ {
	[self setPrimitiveCh2Gain:[NSNumber numberWithFloat:value_]];
}

@dynamic ch2Name;

@dynamic ch2SensorModel;

@dynamic ch2SensorSerialNumber;

@dynamic ch3Gain;

- (float)ch3GainValue {
	NSNumber *result = [self ch3Gain];
	return [result floatValue];
}

- (void)setCh3GainValue:(float)value_ {
	[self setCh3Gain:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveCh3GainValue {
	NSNumber *result = [self primitiveCh3Gain];
	return [result floatValue];
}

- (void)setPrimitiveCh3GainValue:(float)value_ {
	[self setPrimitiveCh3Gain:[NSNumber numberWithFloat:value_]];
}

@dynamic ch3Name;

@dynamic ch3SensorModel;

@dynamic ch3SensorSerialNumber;

@dynamic ch4Gain;

- (float)ch4GainValue {
	NSNumber *result = [self ch4Gain];
	return [result floatValue];
}

- (void)setCh4GainValue:(float)value_ {
	[self setCh4Gain:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveCh4GainValue {
	NSNumber *result = [self primitiveCh4Gain];
	return [result floatValue];
}

- (void)setPrimitiveCh4GainValue:(float)value_ {
	[self setPrimitiveCh4Gain:[NSNumber numberWithFloat:value_]];
}

@dynamic ch4Name;

@dynamic ch4SensorModel;

@dynamic ch4SensorSerialNumber;

@dynamic ch5Gain;

- (float)ch5GainValue {
	NSNumber *result = [self ch5Gain];
	return [result floatValue];
}

- (void)setCh5GainValue:(float)value_ {
	[self setCh5Gain:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveCh5GainValue {
	NSNumber *result = [self primitiveCh5Gain];
	return [result floatValue];
}

- (void)setPrimitiveCh5GainValue:(float)value_ {
	[self setPrimitiveCh5Gain:[NSNumber numberWithFloat:value_]];
}

@dynamic ch5Name;

@dynamic ch5SensorModel;

@dynamic ch5SensorSerialNumber;

@dynamic ch6Gain;

- (float)ch6GainValue {
	NSNumber *result = [self ch6Gain];
	return [result floatValue];
}

- (void)setCh6GainValue:(float)value_ {
	[self setCh6Gain:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveCh6GainValue {
	NSNumber *result = [self primitiveCh6Gain];
	return [result floatValue];
}

- (void)setPrimitiveCh6GainValue:(float)value_ {
	[self setPrimitiveCh6Gain:[NSNumber numberWithFloat:value_]];
}

@dynamic ch6Name;

@dynamic ch6SensorModel;

@dynamic ch6SensorSerialNumber;

@dynamic dasUnitId;

- (int32_t)dasUnitIdValue {
	NSNumber *result = [self dasUnitId];
	return [result intValue];
}

- (void)setDasUnitIdValue:(int32_t)value_ {
	[self setDasUnitId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveDasUnitIdValue {
	NSNumber *result = [self primitiveDasUnitId];
	return [result intValue];
}

- (void)setPrimitiveDasUnitIdValue:(int32_t)value_ {
	[self setPrimitiveDasUnitId:[NSNumber numberWithInt:value_]];
}

@dynamic experimentComment;

@dynamic experimentName;

@dynamic experimentNumber;

- (int32_t)experimentNumberValue {
	NSNumber *result = [self experimentNumber];
	return [result intValue];
}

- (void)setExperimentNumberValue:(int32_t)value_ {
	[self setExperimentNumber:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveExperimentNumberValue {
	NSNumber *result = [self primitiveExperimentNumber];
	return [result intValue];
}

- (void)setPrimitiveExperimentNumberValue:(int32_t)value_ {
	[self setPrimitiveExperimentNumber:[NSNumber numberWithInt:value_]];
}

@dynamic nodeID;

@dynamic revisionNum;

- (int32_t)revisionNumValue {
	NSNumber *result = [self revisionNum];
	return [result intValue];
}

- (void)setRevisionNumValue:(int32_t)value_ {
	[self setRevisionNum:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveRevisionNumValue {
	NSNumber *result = [self primitiveRevisionNum];
	return [result intValue];
}

- (void)setPrimitiveRevisionNumValue:(int32_t)value_ {
	[self setPrimitiveRevisionNum:[NSNumber numberWithInt:value_]];
}

@dynamic sensor1AutoReCenterAttemptInterval;

- (int32_t)sensor1AutoReCenterAttemptIntervalValue {
	NSNumber *result = [self sensor1AutoReCenterAttemptInterval];
	return [result intValue];
}

- (void)setSensor1AutoReCenterAttemptIntervalValue:(int32_t)value_ {
	[self setSensor1AutoReCenterAttemptInterval:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveSensor1AutoReCenterAttemptIntervalValue {
	NSNumber *result = [self primitiveSensor1AutoReCenterAttemptInterval];
	return [result intValue];
}

- (void)setPrimitiveSensor1AutoReCenterAttemptIntervalValue:(int32_t)value_ {
	[self setPrimitiveSensor1AutoReCenterAttemptInterval:[NSNumber numberWithInt:value_]];
}

@dynamic sensor1AutoReCenterAttempts;

- (int32_t)sensor1AutoReCenterAttemptsValue {
	NSNumber *result = [self sensor1AutoReCenterAttempts];
	return [result intValue];
}

- (void)setSensor1AutoReCenterAttemptsValue:(int32_t)value_ {
	[self setSensor1AutoReCenterAttempts:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveSensor1AutoReCenterAttemptsValue {
	NSNumber *result = [self primitiveSensor1AutoReCenterAttempts];
	return [result intValue];
}

- (void)setPrimitiveSensor1AutoReCenterAttemptsValue:(int32_t)value_ {
	[self setPrimitiveSensor1AutoReCenterAttempts:[NSNumber numberWithInt:value_]];
}

@dynamic sensor1AutoReCenterCycleInterval;

- (int32_t)sensor1AutoReCenterCycleIntervalValue {
	NSNumber *result = [self sensor1AutoReCenterCycleInterval];
	return [result intValue];
}

- (void)setSensor1AutoReCenterCycleIntervalValue:(int32_t)value_ {
	[self setSensor1AutoReCenterCycleInterval:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveSensor1AutoReCenterCycleIntervalValue {
	NSNumber *result = [self primitiveSensor1AutoReCenterCycleInterval];
	return [result intValue];
}

- (void)setPrimitiveSensor1AutoReCenterCycleIntervalValue:(int32_t)value_ {
	[self setPrimitiveSensor1AutoReCenterCycleInterval:[NSNumber numberWithInt:value_]];
}

@dynamic sensor1AutoReCenterEnable;

- (BOOL)sensor1AutoReCenterEnableValue {
	NSNumber *result = [self sensor1AutoReCenterEnable];
	return [result boolValue];
}

- (void)setSensor1AutoReCenterEnableValue:(BOOL)value_ {
	[self setSensor1AutoReCenterEnable:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveSensor1AutoReCenterEnableValue {
	NSNumber *result = [self primitiveSensor1AutoReCenterEnable];
	return [result boolValue];
}

- (void)setPrimitiveSensor1AutoReCenterEnableValue:(BOOL)value_ {
	[self setPrimitiveSensor1AutoReCenterEnable:[NSNumber numberWithBool:value_]];
}

@dynamic sensor1AutoReCenterLevelHorizontal;

- (float)sensor1AutoReCenterLevelHorizontalValue {
	NSNumber *result = [self sensor1AutoReCenterLevelHorizontal];
	return [result floatValue];
}

- (void)setSensor1AutoReCenterLevelHorizontalValue:(float)value_ {
	[self setSensor1AutoReCenterLevelHorizontal:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveSensor1AutoReCenterLevelHorizontalValue {
	NSNumber *result = [self primitiveSensor1AutoReCenterLevelHorizontal];
	return [result floatValue];
}

- (void)setPrimitiveSensor1AutoReCenterLevelHorizontalValue:(float)value_ {
	[self setPrimitiveSensor1AutoReCenterLevelHorizontal:[NSNumber numberWithFloat:value_]];
}

@dynamic sensor1AutoReCenterLevelVertical;

- (float)sensor1AutoReCenterLevelVerticalValue {
	NSNumber *result = [self sensor1AutoReCenterLevelVertical];
	return [result floatValue];
}

- (void)setSensor1AutoReCenterLevelVerticalValue:(float)value_ {
	[self setSensor1AutoReCenterLevelVertical:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveSensor1AutoReCenterLevelVerticalValue {
	NSNumber *result = [self primitiveSensor1AutoReCenterLevelVertical];
	return [result floatValue];
}

- (void)setPrimitiveSensor1AutoReCenterLevelVerticalValue:(float)value_ {
	[self setPrimitiveSensor1AutoReCenterLevelVertical:[NSNumber numberWithFloat:value_]];
}

@dynamic sensor1AuxData1;

- (float)sensor1AuxData1Value {
	NSNumber *result = [self sensor1AuxData1];
	return [result floatValue];
}

- (void)setSensor1AuxData1Value:(float)value_ {
	[self setSensor1AuxData1:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveSensor1AuxData1Value {
	NSNumber *result = [self primitiveSensor1AuxData1];
	return [result floatValue];
}

- (void)setPrimitiveSensor1AuxData1Value:(float)value_ {
	[self setPrimitiveSensor1AuxData1:[NSNumber numberWithFloat:value_]];
}

@dynamic sensor1AuxData2;

- (float)sensor1AuxData2Value {
	NSNumber *result = [self sensor1AuxData2];
	return [result floatValue];
}

- (void)setSensor1AuxData2Value:(float)value_ {
	[self setSensor1AuxData2:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveSensor1AuxData2Value {
	NSNumber *result = [self primitiveSensor1AuxData2];
	return [result floatValue];
}

- (void)setPrimitiveSensor1AuxData2Value:(float)value_ {
	[self setPrimitiveSensor1AuxData2:[NSNumber numberWithFloat:value_]];
}

@dynamic sensor1AuxData3;

- (float)sensor1AuxData3Value {
	NSNumber *result = [self sensor1AuxData3];
	return [result floatValue];
}

- (void)setSensor1AuxData3Value:(float)value_ {
	[self setSensor1AuxData3:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveSensor1AuxData3Value {
	NSNumber *result = [self primitiveSensor1AuxData3];
	return [result floatValue];
}

- (void)setPrimitiveSensor1AuxData3Value:(float)value_ {
	[self setPrimitiveSensor1AuxData3:[NSNumber numberWithFloat:value_]];
}

@dynamic sensor2AutoReCenterAttemptInterval;

- (int32_t)sensor2AutoReCenterAttemptIntervalValue {
	NSNumber *result = [self sensor2AutoReCenterAttemptInterval];
	return [result intValue];
}

- (void)setSensor2AutoReCenterAttemptIntervalValue:(int32_t)value_ {
	[self setSensor2AutoReCenterAttemptInterval:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveSensor2AutoReCenterAttemptIntervalValue {
	NSNumber *result = [self primitiveSensor2AutoReCenterAttemptInterval];
	return [result intValue];
}

- (void)setPrimitiveSensor2AutoReCenterAttemptIntervalValue:(int32_t)value_ {
	[self setPrimitiveSensor2AutoReCenterAttemptInterval:[NSNumber numberWithInt:value_]];
}

@dynamic sensor2AutoReCenterAttempts;

- (int32_t)sensor2AutoReCenterAttemptsValue {
	NSNumber *result = [self sensor2AutoReCenterAttempts];
	return [result intValue];
}

- (void)setSensor2AutoReCenterAttemptsValue:(int32_t)value_ {
	[self setSensor2AutoReCenterAttempts:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveSensor2AutoReCenterAttemptsValue {
	NSNumber *result = [self primitiveSensor2AutoReCenterAttempts];
	return [result intValue];
}

- (void)setPrimitiveSensor2AutoReCenterAttemptsValue:(int32_t)value_ {
	[self setPrimitiveSensor2AutoReCenterAttempts:[NSNumber numberWithInt:value_]];
}

@dynamic sensor2AutoReCenterCycleInterval;

- (int32_t)sensor2AutoReCenterCycleIntervalValue {
	NSNumber *result = [self sensor2AutoReCenterCycleInterval];
	return [result intValue];
}

- (void)setSensor2AutoReCenterCycleIntervalValue:(int32_t)value_ {
	[self setSensor2AutoReCenterCycleInterval:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveSensor2AutoReCenterCycleIntervalValue {
	NSNumber *result = [self primitiveSensor2AutoReCenterCycleInterval];
	return [result intValue];
}

- (void)setPrimitiveSensor2AutoReCenterCycleIntervalValue:(int32_t)value_ {
	[self setPrimitiveSensor2AutoReCenterCycleInterval:[NSNumber numberWithInt:value_]];
}

@dynamic sensor2AutoReCenterEnable;

- (BOOL)sensor2AutoReCenterEnableValue {
	NSNumber *result = [self sensor2AutoReCenterEnable];
	return [result boolValue];
}

- (void)setSensor2AutoReCenterEnableValue:(BOOL)value_ {
	[self setSensor2AutoReCenterEnable:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveSensor2AutoReCenterEnableValue {
	NSNumber *result = [self primitiveSensor2AutoReCenterEnable];
	return [result boolValue];
}

- (void)setPrimitiveSensor2AutoReCenterEnableValue:(BOOL)value_ {
	[self setPrimitiveSensor2AutoReCenterEnable:[NSNumber numberWithBool:value_]];
}

@dynamic sensor2AutoReCenterLevelHorizontal;

- (float)sensor2AutoReCenterLevelHorizontalValue {
	NSNumber *result = [self sensor2AutoReCenterLevelHorizontal];
	return [result floatValue];
}

- (void)setSensor2AutoReCenterLevelHorizontalValue:(float)value_ {
	[self setSensor2AutoReCenterLevelHorizontal:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveSensor2AutoReCenterLevelHorizontalValue {
	NSNumber *result = [self primitiveSensor2AutoReCenterLevelHorizontal];
	return [result floatValue];
}

- (void)setPrimitiveSensor2AutoReCenterLevelHorizontalValue:(float)value_ {
	[self setPrimitiveSensor2AutoReCenterLevelHorizontal:[NSNumber numberWithFloat:value_]];
}

@dynamic sensor2AutoReCenterLevelVertical;

- (float)sensor2AutoReCenterLevelVerticalValue {
	NSNumber *result = [self sensor2AutoReCenterLevelVertical];
	return [result floatValue];
}

- (void)setSensor2AutoReCenterLevelVerticalValue:(float)value_ {
	[self setSensor2AutoReCenterLevelVertical:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveSensor2AutoReCenterLevelVerticalValue {
	NSNumber *result = [self primitiveSensor2AutoReCenterLevelVertical];
	return [result floatValue];
}

- (void)setPrimitiveSensor2AutoReCenterLevelVerticalValue:(float)value_ {
	[self setPrimitiveSensor2AutoReCenterLevelVertical:[NSNumber numberWithFloat:value_]];
}

@dynamic sensor2AuxData1;

- (float)sensor2AuxData1Value {
	NSNumber *result = [self sensor2AuxData1];
	return [result floatValue];
}

- (void)setSensor2AuxData1Value:(float)value_ {
	[self setSensor2AuxData1:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveSensor2AuxData1Value {
	NSNumber *result = [self primitiveSensor2AuxData1];
	return [result floatValue];
}

- (void)setPrimitiveSensor2AuxData1Value:(float)value_ {
	[self setPrimitiveSensor2AuxData1:[NSNumber numberWithFloat:value_]];
}

@dynamic sensor2AuxData2;

- (float)sensor2AuxData2Value {
	NSNumber *result = [self sensor2AuxData2];
	return [result floatValue];
}

- (void)setSensor2AuxData2Value:(float)value_ {
	[self setSensor2AuxData2:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveSensor2AuxData2Value {
	NSNumber *result = [self primitiveSensor2AuxData2];
	return [result floatValue];
}

- (void)setPrimitiveSensor2AuxData2Value:(float)value_ {
	[self setPrimitiveSensor2AuxData2:[NSNumber numberWithFloat:value_]];
}

@dynamic sensor2AuxData3;

- (float)sensor2AuxData3Value {
	NSNumber *result = [self sensor2AuxData3];
	return [result floatValue];
}

- (void)setSensor2AuxData3Value:(float)value_ {
	[self setSensor2AuxData3:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveSensor2AuxData3Value {
	NSNumber *result = [self primitiveSensor2AuxData3];
	return [result floatValue];
}

- (void)setPrimitiveSensor2AuxData3Value:(float)value_ {
	[self setPrimitiveSensor2AuxData3:[NSNumber numberWithFloat:value_]];
}

@dynamic sensor3AutoReCenterAttemptInterval;

- (int32_t)sensor3AutoReCenterAttemptIntervalValue {
	NSNumber *result = [self sensor3AutoReCenterAttemptInterval];
	return [result intValue];
}

- (void)setSensor3AutoReCenterAttemptIntervalValue:(int32_t)value_ {
	[self setSensor3AutoReCenterAttemptInterval:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveSensor3AutoReCenterAttemptIntervalValue {
	NSNumber *result = [self primitiveSensor3AutoReCenterAttemptInterval];
	return [result intValue];
}

- (void)setPrimitiveSensor3AutoReCenterAttemptIntervalValue:(int32_t)value_ {
	[self setPrimitiveSensor3AutoReCenterAttemptInterval:[NSNumber numberWithInt:value_]];
}

@dynamic sensor3AutoReCenterAttempts;

- (int32_t)sensor3AutoReCenterAttemptsValue {
	NSNumber *result = [self sensor3AutoReCenterAttempts];
	return [result intValue];
}

- (void)setSensor3AutoReCenterAttemptsValue:(int32_t)value_ {
	[self setSensor3AutoReCenterAttempts:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveSensor3AutoReCenterAttemptsValue {
	NSNumber *result = [self primitiveSensor3AutoReCenterAttempts];
	return [result intValue];
}

- (void)setPrimitiveSensor3AutoReCenterAttemptsValue:(int32_t)value_ {
	[self setPrimitiveSensor3AutoReCenterAttempts:[NSNumber numberWithInt:value_]];
}

@dynamic sensor3AutoReCenterCycleInterval;

- (int32_t)sensor3AutoReCenterCycleIntervalValue {
	NSNumber *result = [self sensor3AutoReCenterCycleInterval];
	return [result intValue];
}

- (void)setSensor3AutoReCenterCycleIntervalValue:(int32_t)value_ {
	[self setSensor3AutoReCenterCycleInterval:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveSensor3AutoReCenterCycleIntervalValue {
	NSNumber *result = [self primitiveSensor3AutoReCenterCycleInterval];
	return [result intValue];
}

- (void)setPrimitiveSensor3AutoReCenterCycleIntervalValue:(int32_t)value_ {
	[self setPrimitiveSensor3AutoReCenterCycleInterval:[NSNumber numberWithInt:value_]];
}

@dynamic sensor3AutoReCenterEnable;

- (BOOL)sensor3AutoReCenterEnableValue {
	NSNumber *result = [self sensor3AutoReCenterEnable];
	return [result boolValue];
}

- (void)setSensor3AutoReCenterEnableValue:(BOOL)value_ {
	[self setSensor3AutoReCenterEnable:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveSensor3AutoReCenterEnableValue {
	NSNumber *result = [self primitiveSensor3AutoReCenterEnable];
	return [result boolValue];
}

- (void)setPrimitiveSensor3AutoReCenterEnableValue:(BOOL)value_ {
	[self setPrimitiveSensor3AutoReCenterEnable:[NSNumber numberWithBool:value_]];
}

@dynamic sensor3AutoReCenterLevelHorizontal;

- (float)sensor3AutoReCenterLevelHorizontalValue {
	NSNumber *result = [self sensor3AutoReCenterLevelHorizontal];
	return [result floatValue];
}

- (void)setSensor3AutoReCenterLevelHorizontalValue:(float)value_ {
	[self setSensor3AutoReCenterLevelHorizontal:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveSensor3AutoReCenterLevelHorizontalValue {
	NSNumber *result = [self primitiveSensor3AutoReCenterLevelHorizontal];
	return [result floatValue];
}

- (void)setPrimitiveSensor3AutoReCenterLevelHorizontalValue:(float)value_ {
	[self setPrimitiveSensor3AutoReCenterLevelHorizontal:[NSNumber numberWithFloat:value_]];
}

@dynamic sensor3AutoReCenterLevelVertical;

- (float)sensor3AutoReCenterLevelVerticalValue {
	NSNumber *result = [self sensor3AutoReCenterLevelVertical];
	return [result floatValue];
}

- (void)setSensor3AutoReCenterLevelVerticalValue:(float)value_ {
	[self setSensor3AutoReCenterLevelVertical:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveSensor3AutoReCenterLevelVerticalValue {
	NSNumber *result = [self primitiveSensor3AutoReCenterLevelVertical];
	return [result floatValue];
}

- (void)setPrimitiveSensor3AutoReCenterLevelVerticalValue:(float)value_ {
	[self setPrimitiveSensor3AutoReCenterLevelVertical:[NSNumber numberWithFloat:value_]];
}

@dynamic sensor3AuxData1;

- (float)sensor3AuxData1Value {
	NSNumber *result = [self sensor3AuxData1];
	return [result floatValue];
}

- (void)setSensor3AuxData1Value:(float)value_ {
	[self setSensor3AuxData1:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveSensor3AuxData1Value {
	NSNumber *result = [self primitiveSensor3AuxData1];
	return [result floatValue];
}

- (void)setPrimitiveSensor3AuxData1Value:(float)value_ {
	[self setPrimitiveSensor3AuxData1:[NSNumber numberWithFloat:value_]];
}

@dynamic sensor3AuxData2;

- (float)sensor3AuxData2Value {
	NSNumber *result = [self sensor3AuxData2];
	return [result floatValue];
}

- (void)setSensor3AuxData2Value:(float)value_ {
	[self setSensor3AuxData2:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveSensor3AuxData2Value {
	NSNumber *result = [self primitiveSensor3AuxData2];
	return [result floatValue];
}

- (void)setPrimitiveSensor3AuxData2Value:(float)value_ {
	[self setPrimitiveSensor3AuxData2:[NSNumber numberWithFloat:value_]];
}

@dynamic sensor3AuxData3;

- (float)sensor3AuxData3Value {
	NSNumber *result = [self sensor3AuxData3];
	return [result floatValue];
}

- (void)setSensor3AuxData3Value:(float)value_ {
	[self setSensor3AuxData3:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveSensor3AuxData3Value {
	NSNumber *result = [self primitiveSensor3AuxData3];
	return [result floatValue];
}

- (void)setPrimitiveSensor3AuxData3Value:(float)value_ {
	[self setPrimitiveSensor3AuxData3:[NSNumber numberWithFloat:value_]];
}

@dynamic sensor4AutoReCenterAttemptInterval;

- (int32_t)sensor4AutoReCenterAttemptIntervalValue {
	NSNumber *result = [self sensor4AutoReCenterAttemptInterval];
	return [result intValue];
}

- (void)setSensor4AutoReCenterAttemptIntervalValue:(int32_t)value_ {
	[self setSensor4AutoReCenterAttemptInterval:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveSensor4AutoReCenterAttemptIntervalValue {
	NSNumber *result = [self primitiveSensor4AutoReCenterAttemptInterval];
	return [result intValue];
}

- (void)setPrimitiveSensor4AutoReCenterAttemptIntervalValue:(int32_t)value_ {
	[self setPrimitiveSensor4AutoReCenterAttemptInterval:[NSNumber numberWithInt:value_]];
}

@dynamic sensor4AutoReCenterAttempts;

- (int32_t)sensor4AutoReCenterAttemptsValue {
	NSNumber *result = [self sensor4AutoReCenterAttempts];
	return [result intValue];
}

- (void)setSensor4AutoReCenterAttemptsValue:(int32_t)value_ {
	[self setSensor4AutoReCenterAttempts:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveSensor4AutoReCenterAttemptsValue {
	NSNumber *result = [self primitiveSensor4AutoReCenterAttempts];
	return [result intValue];
}

- (void)setPrimitiveSensor4AutoReCenterAttemptsValue:(int32_t)value_ {
	[self setPrimitiveSensor4AutoReCenterAttempts:[NSNumber numberWithInt:value_]];
}

@dynamic sensor4AutoReCenterCycleInterval;

- (int32_t)sensor4AutoReCenterCycleIntervalValue {
	NSNumber *result = [self sensor4AutoReCenterCycleInterval];
	return [result intValue];
}

- (void)setSensor4AutoReCenterCycleIntervalValue:(int32_t)value_ {
	[self setSensor4AutoReCenterCycleInterval:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveSensor4AutoReCenterCycleIntervalValue {
	NSNumber *result = [self primitiveSensor4AutoReCenterCycleInterval];
	return [result intValue];
}

- (void)setPrimitiveSensor4AutoReCenterCycleIntervalValue:(int32_t)value_ {
	[self setPrimitiveSensor4AutoReCenterCycleInterval:[NSNumber numberWithInt:value_]];
}

@dynamic sensor4AutoReCenterEnable;

- (BOOL)sensor4AutoReCenterEnableValue {
	NSNumber *result = [self sensor4AutoReCenterEnable];
	return [result boolValue];
}

- (void)setSensor4AutoReCenterEnableValue:(BOOL)value_ {
	[self setSensor4AutoReCenterEnable:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveSensor4AutoReCenterEnableValue {
	NSNumber *result = [self primitiveSensor4AutoReCenterEnable];
	return [result boolValue];
}

- (void)setPrimitiveSensor4AutoReCenterEnableValue:(BOOL)value_ {
	[self setPrimitiveSensor4AutoReCenterEnable:[NSNumber numberWithBool:value_]];
}

@dynamic sensor4AutoReCenterLevelHorizontal;

- (float)sensor4AutoReCenterLevelHorizontalValue {
	NSNumber *result = [self sensor4AutoReCenterLevelHorizontal];
	return [result floatValue];
}

- (void)setSensor4AutoReCenterLevelHorizontalValue:(float)value_ {
	[self setSensor4AutoReCenterLevelHorizontal:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveSensor4AutoReCenterLevelHorizontalValue {
	NSNumber *result = [self primitiveSensor4AutoReCenterLevelHorizontal];
	return [result floatValue];
}

- (void)setPrimitiveSensor4AutoReCenterLevelHorizontalValue:(float)value_ {
	[self setPrimitiveSensor4AutoReCenterLevelHorizontal:[NSNumber numberWithFloat:value_]];
}

@dynamic sensor4AutoReCenterLevelVertical;

- (float)sensor4AutoReCenterLevelVerticalValue {
	NSNumber *result = [self sensor4AutoReCenterLevelVertical];
	return [result floatValue];
}

- (void)setSensor4AutoReCenterLevelVerticalValue:(float)value_ {
	[self setSensor4AutoReCenterLevelVertical:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveSensor4AutoReCenterLevelVerticalValue {
	NSNumber *result = [self primitiveSensor4AutoReCenterLevelVertical];
	return [result floatValue];
}

- (void)setPrimitiveSensor4AutoReCenterLevelVerticalValue:(float)value_ {
	[self setPrimitiveSensor4AutoReCenterLevelVertical:[NSNumber numberWithFloat:value_]];
}

@dynamic sensor4AuxData1;

- (float)sensor4AuxData1Value {
	NSNumber *result = [self sensor4AuxData1];
	return [result floatValue];
}

- (void)setSensor4AuxData1Value:(float)value_ {
	[self setSensor4AuxData1:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveSensor4AuxData1Value {
	NSNumber *result = [self primitiveSensor4AuxData1];
	return [result floatValue];
}

- (void)setPrimitiveSensor4AuxData1Value:(float)value_ {
	[self setPrimitiveSensor4AuxData1:[NSNumber numberWithFloat:value_]];
}

@dynamic sensor4AuxData2;

- (float)sensor4AuxData2Value {
	NSNumber *result = [self sensor4AuxData2];
	return [result floatValue];
}

- (void)setSensor4AuxData2Value:(float)value_ {
	[self setSensor4AuxData2:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveSensor4AuxData2Value {
	NSNumber *result = [self primitiveSensor4AuxData2];
	return [result floatValue];
}

- (void)setPrimitiveSensor4AuxData2Value:(float)value_ {
	[self setPrimitiveSensor4AuxData2:[NSNumber numberWithFloat:value_]];
}

@dynamic sensor4AuxData3;

- (float)sensor4AuxData3Value {
	NSNumber *result = [self sensor4AuxData3];
	return [result floatValue];
}

- (void)setSensor4AuxData3Value:(float)value_ {
	[self setSensor4AuxData3:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveSensor4AuxData3Value {
	NSNumber *result = [self primitiveSensor4AuxData3];
	return [result floatValue];
}

- (void)setPrimitiveSensor4AuxData3Value:(float)value_ {
	[self setPrimitiveSensor4AuxData3:[NSNumber numberWithFloat:value_]];
}

@dynamic stationComment;

@dynamic stationName;

@dynamic stationNumber;

- (int32_t)stationNumberValue {
	NSNumber *result = [self stationNumber];
	return [result intValue];
}

- (void)setStationNumberValue:(int32_t)value_ {
	[self setStationNumber:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveStationNumberValue {
	NSNumber *result = [self primitiveStationNumber];
	return [result intValue];
}

- (void)setPrimitiveStationNumberValue:(int32_t)value_ {
	[self setPrimitiveStationNumber:[NSNumber numberWithInt:value_]];
}

@dynamic stream1Channels;

@dynamic stream1SampleRate;

- (int32_t)stream1SampleRateValue {
	NSNumber *result = [self stream1SampleRate];
	return [result intValue];
}

- (void)setStream1SampleRateValue:(int32_t)value_ {
	[self setStream1SampleRate:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveStream1SampleRateValue {
	NSNumber *result = [self primitiveStream1SampleRate];
	return [result intValue];
}

- (void)setPrimitiveStream1SampleRateValue:(int32_t)value_ {
	[self setPrimitiveStream1SampleRate:[NSNumber numberWithInt:value_]];
}

@dynamic stream2Channels;

@dynamic stream2SampleRate;

- (int32_t)stream2SampleRateValue {
	NSNumber *result = [self stream2SampleRate];
	return [result intValue];
}

- (void)setStream2SampleRateValue:(int32_t)value_ {
	[self setStream2SampleRate:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveStream2SampleRateValue {
	NSNumber *result = [self primitiveStream2SampleRate];
	return [result intValue];
}

- (void)setPrimitiveStream2SampleRateValue:(int32_t)value_ {
	[self setPrimitiveStream2SampleRate:[NSNumber numberWithInt:value_]];
}

@dynamic stream3Channels;

@dynamic stream3SampleRate;

- (int32_t)stream3SampleRateValue {
	NSNumber *result = [self stream3SampleRate];
	return [result intValue];
}

- (void)setStream3SampleRateValue:(int32_t)value_ {
	[self setStream3SampleRate:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveStream3SampleRateValue {
	NSNumber *result = [self primitiveStream3SampleRate];
	return [result intValue];
}

- (void)setPrimitiveStream3SampleRateValue:(int32_t)value_ {
	[self setPrimitiveStream3SampleRate:[NSNumber numberWithInt:value_]];
}

@dynamic stream4Channels;

@dynamic stream4SampleRate;

- (int32_t)stream4SampleRateValue {
	NSNumber *result = [self stream4SampleRate];
	return [result intValue];
}

- (void)setStream4SampleRateValue:(int32_t)value_ {
	[self setStream4SampleRate:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveStream4SampleRateValue {
	NSNumber *result = [self primitiveStream4SampleRate];
	return [result intValue];
}

- (void)setPrimitiveStream4SampleRateValue:(int32_t)value_ {
	[self setPrimitiveStream4SampleRate:[NSNumber numberWithInt:value_]];
}

@dynamic stream5Channels;

@dynamic stream5SampleRate;

- (int32_t)stream5SampleRateValue {
	NSNumber *result = [self stream5SampleRate];
	return [result intValue];
}

- (void)setStream5SampleRateValue:(int32_t)value_ {
	[self setStream5SampleRate:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveStream5SampleRateValue {
	NSNumber *result = [self primitiveStream5SampleRate];
	return [result intValue];
}

- (void)setPrimitiveStream5SampleRateValue:(int32_t)value_ {
	[self setPrimitiveStream5SampleRate:[NSNumber numberWithInt:value_]];
}

@dynamic stream6Channels;

@dynamic stream6SampleRate;

- (int32_t)stream6SampleRateValue {
	NSNumber *result = [self stream6SampleRate];
	return [result intValue];
}

- (void)setStream6SampleRateValue:(int32_t)value_ {
	[self setStream6SampleRate:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveStream6SampleRateValue {
	NSNumber *result = [self primitiveStream6SampleRate];
	return [result intValue];
}

- (void)setPrimitiveStream6SampleRateValue:(int32_t)value_ {
	[self setPrimitiveStream6SampleRate:[NSNumber numberWithInt:value_]];
}

@dynamic stream7Channels;

@dynamic stream7SampleRate;

- (int32_t)stream7SampleRateValue {
	NSNumber *result = [self stream7SampleRate];
	return [result intValue];
}

- (void)setStream7SampleRateValue:(int32_t)value_ {
	[self setStream7SampleRate:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveStream7SampleRateValue {
	NSNumber *result = [self primitiveStream7SampleRate];
	return [result intValue];
}

- (void)setPrimitiveStream7SampleRateValue:(int32_t)value_ {
	[self setPrimitiveStream7SampleRate:[NSNumber numberWithInt:value_]];
}

@dynamic stream8Channels;

@dynamic stream8SampleRate;

- (int32_t)stream8SampleRateValue {
	NSNumber *result = [self stream8SampleRate];
	return [result intValue];
}

- (void)setStream8SampleRateValue:(int32_t)value_ {
	[self setStream8SampleRate:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveStream8SampleRateValue {
	NSNumber *result = [self primitiveStream8SampleRate];
	return [result intValue];
}

- (void)setPrimitiveStream8SampleRateValue:(int32_t)value_ {
	[self setPrimitiveStream8SampleRate:[NSNumber numberWithInt:value_]];
}

@dynamic treeID;

@dynamic entry;

- (NSMutableSet*)entrySet {
	[self willAccessValueForKey:@"entry"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"entry"];

	[self didAccessValueForKey:@"entry"];
	return result;
}

@end

