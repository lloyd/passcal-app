// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Rf130StatusFields.h instead.

#import <CoreData/CoreData.h>
#import "RHManagedObject.h"

extern const struct Rf130StatusFieldsAttributes {
	__unsafe_unretained NSString *acquisitionActive;
	__unsafe_unretained NSString *acquisitionRequested;
	__unsafe_unretained NSString *backupPower;
	__unsafe_unretained NSString *channelDasRecording;
	__unsafe_unretained NSString *currentDisk;
	__unsafe_unretained NSString *dasDataStreams;
	__unsafe_unretained NSString *dasTemperature;
	__unsafe_unretained NSString *dasTime;
	__unsafe_unretained NSString *disk1PercentUsed;
	__unsafe_unretained NSString *disk1Total;
	__unsafe_unretained NSString *disk1Used;
	__unsafe_unretained NSString *disk2PercentUsed;
	__unsafe_unretained NSString *disk2Total;
	__unsafe_unretained NSString *disk2Used;
	__unsafe_unretained NSString *eventCount;
	__unsafe_unretained NSString *gpsAltitude;
	__unsafe_unretained NSString *gpsLastLock;
	__unsafe_unretained NSString *gpsLastLockPhase;
	__unsafe_unretained NSString *gpsLatitude;
	__unsafe_unretained NSString *gpsLocked;
	__unsafe_unretained NSString *gpsLongitude;
	__unsafe_unretained NSString *gpsMode;
	__unsafe_unretained NSString *gpsOn;
	__unsafe_unretained NSString *gpsSatsTracked;
	__unsafe_unretained NSString *inputPower;
	__unsafe_unretained NSString *nodeID;
	__unsafe_unretained NSString *ramTotal;
	__unsafe_unretained NSString *ramUsed;
	__unsafe_unretained NSString *revisionNum;
	__unsafe_unretained NSString *treeID;
} Rf130StatusFieldsAttributes;

extern const struct Rf130StatusFieldsRelationships {
	__unsafe_unretained NSString *entry;
} Rf130StatusFieldsRelationships;

@class Entry;

@interface Rf130StatusFieldsID : NSManagedObjectID {}
@end

@interface _Rf130StatusFields : RHManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) Rf130StatusFieldsID* objectID;

@property (nonatomic, strong) NSNumber* acquisitionActive;

@property (atomic) BOOL acquisitionActiveValue;
- (BOOL)acquisitionActiveValue;
- (void)setAcquisitionActiveValue:(BOOL)value_;

//- (BOOL)validateAcquisitionActive:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* acquisitionRequested;

@property (atomic) BOOL acquisitionRequestedValue;
- (BOOL)acquisitionRequestedValue;
- (void)setAcquisitionRequestedValue:(BOOL)value_;

//- (BOOL)validateAcquisitionRequested:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* backupPower;

@property (atomic) float backupPowerValue;
- (float)backupPowerValue;
- (void)setBackupPowerValue:(float)value_;

//- (BOOL)validateBackupPower:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* channelDasRecording;

//- (BOOL)validateChannelDasRecording:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* currentDisk;

@property (atomic) int32_t currentDiskValue;
- (int32_t)currentDiskValue;
- (void)setCurrentDiskValue:(int32_t)value_;

//- (BOOL)validateCurrentDisk:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* dasDataStreams;

//- (BOOL)validateDasDataStreams:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* dasTemperature;

@property (atomic) float dasTemperatureValue;
- (float)dasTemperatureValue;
- (void)setDasTemperatureValue:(float)value_;

//- (BOOL)validateDasTemperature:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* dasTime;

//- (BOOL)validateDasTime:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* disk1PercentUsed;

@property (atomic) float disk1PercentUsedValue;
- (float)disk1PercentUsedValue;
- (void)setDisk1PercentUsedValue:(float)value_;

//- (BOOL)validateDisk1PercentUsed:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* disk1Total;

@property (atomic) int32_t disk1TotalValue;
- (int32_t)disk1TotalValue;
- (void)setDisk1TotalValue:(int32_t)value_;

//- (BOOL)validateDisk1Total:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* disk1Used;

@property (atomic) int32_t disk1UsedValue;
- (int32_t)disk1UsedValue;
- (void)setDisk1UsedValue:(int32_t)value_;

//- (BOOL)validateDisk1Used:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* disk2PercentUsed;

@property (atomic) float disk2PercentUsedValue;
- (float)disk2PercentUsedValue;
- (void)setDisk2PercentUsedValue:(float)value_;

//- (BOOL)validateDisk2PercentUsed:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* disk2Total;

@property (atomic) int32_t disk2TotalValue;
- (int32_t)disk2TotalValue;
- (void)setDisk2TotalValue:(int32_t)value_;

//- (BOOL)validateDisk2Total:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* disk2Used;

@property (atomic) int32_t disk2UsedValue;
- (int32_t)disk2UsedValue;
- (void)setDisk2UsedValue:(int32_t)value_;

//- (BOOL)validateDisk2Used:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* eventCount;

@property (atomic) int32_t eventCountValue;
- (int32_t)eventCountValue;
- (void)setEventCountValue:(int32_t)value_;

//- (BOOL)validateEventCount:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* gpsAltitude;

@property (atomic) float gpsAltitudeValue;
- (float)gpsAltitudeValue;
- (void)setGpsAltitudeValue:(float)value_;

//- (BOOL)validateGpsAltitude:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* gpsLastLock;

@property (atomic) int32_t gpsLastLockValue;
- (int32_t)gpsLastLockValue;
- (void)setGpsLastLockValue:(int32_t)value_;

//- (BOOL)validateGpsLastLock:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* gpsLastLockPhase;

@property (atomic) int32_t gpsLastLockPhaseValue;
- (int32_t)gpsLastLockPhaseValue;
- (void)setGpsLastLockPhaseValue:(int32_t)value_;

//- (BOOL)validateGpsLastLockPhase:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* gpsLatitude;

@property (atomic) float gpsLatitudeValue;
- (float)gpsLatitudeValue;
- (void)setGpsLatitudeValue:(float)value_;

//- (BOOL)validateGpsLatitude:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* gpsLocked;

@property (atomic) BOOL gpsLockedValue;
- (BOOL)gpsLockedValue;
- (void)setGpsLockedValue:(BOOL)value_;

//- (BOOL)validateGpsLocked:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* gpsLongitude;

@property (atomic) float gpsLongitudeValue;
- (float)gpsLongitudeValue;
- (void)setGpsLongitudeValue:(float)value_;

//- (BOOL)validateGpsLongitude:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* gpsMode;

//- (BOOL)validateGpsMode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* gpsOn;

@property (atomic) BOOL gpsOnValue;
- (BOOL)gpsOnValue;
- (void)setGpsOnValue:(BOOL)value_;

//- (BOOL)validateGpsOn:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* gpsSatsTracked;

@property (atomic) int32_t gpsSatsTrackedValue;
- (int32_t)gpsSatsTrackedValue;
- (void)setGpsSatsTrackedValue:(int32_t)value_;

//- (BOOL)validateGpsSatsTracked:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* inputPower;

@property (atomic) float inputPowerValue;
- (float)inputPowerValue;
- (void)setInputPowerValue:(float)value_;

//- (BOOL)validateInputPower:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* nodeID;

//- (BOOL)validateNodeID:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* ramTotal;

@property (atomic) int32_t ramTotalValue;
- (int32_t)ramTotalValue;
- (void)setRamTotalValue:(int32_t)value_;

//- (BOOL)validateRamTotal:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* ramUsed;

@property (atomic) int32_t ramUsedValue;
- (int32_t)ramUsedValue;
- (void)setRamUsedValue:(int32_t)value_;

//- (BOOL)validateRamUsed:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* revisionNum;

@property (atomic) int32_t revisionNumValue;
- (int32_t)revisionNumValue;
- (void)setRevisionNumValue:(int32_t)value_;

//- (BOOL)validateRevisionNum:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* treeID;

//- (BOOL)validateTreeID:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *entry;

- (NSMutableSet*)entrySet;

@end

@interface _Rf130StatusFields (EntryCoreDataGeneratedAccessors)
- (void)addEntry:(NSSet*)value_;
- (void)removeEntry:(NSSet*)value_;
- (void)addEntryObject:(Entry*)value_;
- (void)removeEntryObject:(Entry*)value_;

@end

@interface _Rf130StatusFields (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveAcquisitionActive;
- (void)setPrimitiveAcquisitionActive:(NSNumber*)value;

- (BOOL)primitiveAcquisitionActiveValue;
- (void)setPrimitiveAcquisitionActiveValue:(BOOL)value_;

- (NSNumber*)primitiveAcquisitionRequested;
- (void)setPrimitiveAcquisitionRequested:(NSNumber*)value;

- (BOOL)primitiveAcquisitionRequestedValue;
- (void)setPrimitiveAcquisitionRequestedValue:(BOOL)value_;

- (NSNumber*)primitiveBackupPower;
- (void)setPrimitiveBackupPower:(NSNumber*)value;

- (float)primitiveBackupPowerValue;
- (void)setPrimitiveBackupPowerValue:(float)value_;

- (NSString*)primitiveChannelDasRecording;
- (void)setPrimitiveChannelDasRecording:(NSString*)value;

- (NSNumber*)primitiveCurrentDisk;
- (void)setPrimitiveCurrentDisk:(NSNumber*)value;

- (int32_t)primitiveCurrentDiskValue;
- (void)setPrimitiveCurrentDiskValue:(int32_t)value_;

- (NSString*)primitiveDasDataStreams;
- (void)setPrimitiveDasDataStreams:(NSString*)value;

- (NSNumber*)primitiveDasTemperature;
- (void)setPrimitiveDasTemperature:(NSNumber*)value;

- (float)primitiveDasTemperatureValue;
- (void)setPrimitiveDasTemperatureValue:(float)value_;

- (NSDate*)primitiveDasTime;
- (void)setPrimitiveDasTime:(NSDate*)value;

- (NSNumber*)primitiveDisk1PercentUsed;
- (void)setPrimitiveDisk1PercentUsed:(NSNumber*)value;

- (float)primitiveDisk1PercentUsedValue;
- (void)setPrimitiveDisk1PercentUsedValue:(float)value_;

- (NSNumber*)primitiveDisk1Total;
- (void)setPrimitiveDisk1Total:(NSNumber*)value;

- (int32_t)primitiveDisk1TotalValue;
- (void)setPrimitiveDisk1TotalValue:(int32_t)value_;

- (NSNumber*)primitiveDisk1Used;
- (void)setPrimitiveDisk1Used:(NSNumber*)value;

- (int32_t)primitiveDisk1UsedValue;
- (void)setPrimitiveDisk1UsedValue:(int32_t)value_;

- (NSNumber*)primitiveDisk2PercentUsed;
- (void)setPrimitiveDisk2PercentUsed:(NSNumber*)value;

- (float)primitiveDisk2PercentUsedValue;
- (void)setPrimitiveDisk2PercentUsedValue:(float)value_;

- (NSNumber*)primitiveDisk2Total;
- (void)setPrimitiveDisk2Total:(NSNumber*)value;

- (int32_t)primitiveDisk2TotalValue;
- (void)setPrimitiveDisk2TotalValue:(int32_t)value_;

- (NSNumber*)primitiveDisk2Used;
- (void)setPrimitiveDisk2Used:(NSNumber*)value;

- (int32_t)primitiveDisk2UsedValue;
- (void)setPrimitiveDisk2UsedValue:(int32_t)value_;

- (NSNumber*)primitiveEventCount;
- (void)setPrimitiveEventCount:(NSNumber*)value;

- (int32_t)primitiveEventCountValue;
- (void)setPrimitiveEventCountValue:(int32_t)value_;

- (NSNumber*)primitiveGpsAltitude;
- (void)setPrimitiveGpsAltitude:(NSNumber*)value;

- (float)primitiveGpsAltitudeValue;
- (void)setPrimitiveGpsAltitudeValue:(float)value_;

- (NSNumber*)primitiveGpsLastLock;
- (void)setPrimitiveGpsLastLock:(NSNumber*)value;

- (int32_t)primitiveGpsLastLockValue;
- (void)setPrimitiveGpsLastLockValue:(int32_t)value_;

- (NSNumber*)primitiveGpsLastLockPhase;
- (void)setPrimitiveGpsLastLockPhase:(NSNumber*)value;

- (int32_t)primitiveGpsLastLockPhaseValue;
- (void)setPrimitiveGpsLastLockPhaseValue:(int32_t)value_;

- (NSNumber*)primitiveGpsLatitude;
- (void)setPrimitiveGpsLatitude:(NSNumber*)value;

- (float)primitiveGpsLatitudeValue;
- (void)setPrimitiveGpsLatitudeValue:(float)value_;

- (NSNumber*)primitiveGpsLocked;
- (void)setPrimitiveGpsLocked:(NSNumber*)value;

- (BOOL)primitiveGpsLockedValue;
- (void)setPrimitiveGpsLockedValue:(BOOL)value_;

- (NSNumber*)primitiveGpsLongitude;
- (void)setPrimitiveGpsLongitude:(NSNumber*)value;

- (float)primitiveGpsLongitudeValue;
- (void)setPrimitiveGpsLongitudeValue:(float)value_;

- (NSString*)primitiveGpsMode;
- (void)setPrimitiveGpsMode:(NSString*)value;

- (NSNumber*)primitiveGpsOn;
- (void)setPrimitiveGpsOn:(NSNumber*)value;

- (BOOL)primitiveGpsOnValue;
- (void)setPrimitiveGpsOnValue:(BOOL)value_;

- (NSNumber*)primitiveGpsSatsTracked;
- (void)setPrimitiveGpsSatsTracked:(NSNumber*)value;

- (int32_t)primitiveGpsSatsTrackedValue;
- (void)setPrimitiveGpsSatsTrackedValue:(int32_t)value_;

- (NSNumber*)primitiveInputPower;
- (void)setPrimitiveInputPower:(NSNumber*)value;

- (float)primitiveInputPowerValue;
- (void)setPrimitiveInputPowerValue:(float)value_;

- (NSString*)primitiveNodeID;
- (void)setPrimitiveNodeID:(NSString*)value;

- (NSNumber*)primitiveRamTotal;
- (void)setPrimitiveRamTotal:(NSNumber*)value;

- (int32_t)primitiveRamTotalValue;
- (void)setPrimitiveRamTotalValue:(int32_t)value_;

- (NSNumber*)primitiveRamUsed;
- (void)setPrimitiveRamUsed:(NSNumber*)value;

- (int32_t)primitiveRamUsedValue;
- (void)setPrimitiveRamUsedValue:(int32_t)value_;

- (NSNumber*)primitiveRevisionNum;
- (void)setPrimitiveRevisionNum:(NSNumber*)value;

- (int32_t)primitiveRevisionNumValue;
- (void)setPrimitiveRevisionNumValue:(int32_t)value_;

- (NSString*)primitiveTreeID;
- (void)setPrimitiveTreeID:(NSString*)value;

- (NSMutableSet*)primitiveEntry;
- (void)setPrimitiveEntry:(NSMutableSet*)value;

@end
