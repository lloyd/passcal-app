// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Rf130StationFields.h instead.

#import <CoreData/CoreData.h>
#import "RHManagedObject.h"

extern const struct Rf130StationFieldsAttributes {
	__unsafe_unretained NSString *ch1Gain;
	__unsafe_unretained NSString *ch1Name;
	__unsafe_unretained NSString *ch1SensorModel;
	__unsafe_unretained NSString *ch1SensorSerialNumber;
	__unsafe_unretained NSString *ch2Gain;
	__unsafe_unretained NSString *ch2Name;
	__unsafe_unretained NSString *ch2SensorModel;
	__unsafe_unretained NSString *ch2SensorSerialNumber;
	__unsafe_unretained NSString *ch3Gain;
	__unsafe_unretained NSString *ch3Name;
	__unsafe_unretained NSString *ch3SensorModel;
	__unsafe_unretained NSString *ch3SensorSerialNumber;
	__unsafe_unretained NSString *ch4Gain;
	__unsafe_unretained NSString *ch4Name;
	__unsafe_unretained NSString *ch4SensorModel;
	__unsafe_unretained NSString *ch4SensorSerialNumber;
	__unsafe_unretained NSString *ch5Gain;
	__unsafe_unretained NSString *ch5Name;
	__unsafe_unretained NSString *ch5SensorModel;
	__unsafe_unretained NSString *ch5SensorSerialNumber;
	__unsafe_unretained NSString *ch6Gain;
	__unsafe_unretained NSString *ch6Name;
	__unsafe_unretained NSString *ch6SensorModel;
	__unsafe_unretained NSString *ch6SensorSerialNumber;
	__unsafe_unretained NSString *dasUnitId;
	__unsafe_unretained NSString *experimentComment;
	__unsafe_unretained NSString *experimentName;
	__unsafe_unretained NSString *experimentNumber;
	__unsafe_unretained NSString *nodeID;
	__unsafe_unretained NSString *revisionNum;
	__unsafe_unretained NSString *sensor1AutoReCenterAttemptInterval;
	__unsafe_unretained NSString *sensor1AutoReCenterAttempts;
	__unsafe_unretained NSString *sensor1AutoReCenterCycleInterval;
	__unsafe_unretained NSString *sensor1AutoReCenterEnable;
	__unsafe_unretained NSString *sensor1AutoReCenterLevelHorizontal;
	__unsafe_unretained NSString *sensor1AutoReCenterLevelVertical;
	__unsafe_unretained NSString *sensor1AuxData1;
	__unsafe_unretained NSString *sensor1AuxData2;
	__unsafe_unretained NSString *sensor1AuxData3;
	__unsafe_unretained NSString *sensor2AutoReCenterAttemptInterval;
	__unsafe_unretained NSString *sensor2AutoReCenterAttempts;
	__unsafe_unretained NSString *sensor2AutoReCenterCycleInterval;
	__unsafe_unretained NSString *sensor2AutoReCenterEnable;
	__unsafe_unretained NSString *sensor2AutoReCenterLevelHorizontal;
	__unsafe_unretained NSString *sensor2AutoReCenterLevelVertical;
	__unsafe_unretained NSString *sensor2AuxData1;
	__unsafe_unretained NSString *sensor2AuxData2;
	__unsafe_unretained NSString *sensor2AuxData3;
	__unsafe_unretained NSString *sensor3AutoReCenterAttemptInterval;
	__unsafe_unretained NSString *sensor3AutoReCenterAttempts;
	__unsafe_unretained NSString *sensor3AutoReCenterCycleInterval;
	__unsafe_unretained NSString *sensor3AutoReCenterEnable;
	__unsafe_unretained NSString *sensor3AutoReCenterLevelHorizontal;
	__unsafe_unretained NSString *sensor3AutoReCenterLevelVertical;
	__unsafe_unretained NSString *sensor3AuxData1;
	__unsafe_unretained NSString *sensor3AuxData2;
	__unsafe_unretained NSString *sensor3AuxData3;
	__unsafe_unretained NSString *sensor4AutoReCenterAttemptInterval;
	__unsafe_unretained NSString *sensor4AutoReCenterAttempts;
	__unsafe_unretained NSString *sensor4AutoReCenterCycleInterval;
	__unsafe_unretained NSString *sensor4AutoReCenterEnable;
	__unsafe_unretained NSString *sensor4AutoReCenterLevelHorizontal;
	__unsafe_unretained NSString *sensor4AutoReCenterLevelVertical;
	__unsafe_unretained NSString *sensor4AuxData1;
	__unsafe_unretained NSString *sensor4AuxData2;
	__unsafe_unretained NSString *sensor4AuxData3;
	__unsafe_unretained NSString *stationComment;
	__unsafe_unretained NSString *stationName;
	__unsafe_unretained NSString *stationNumber;
	__unsafe_unretained NSString *stream1Channels;
	__unsafe_unretained NSString *stream1SampleRate;
	__unsafe_unretained NSString *stream2Channels;
	__unsafe_unretained NSString *stream2SampleRate;
	__unsafe_unretained NSString *stream3Channels;
	__unsafe_unretained NSString *stream3SampleRate;
	__unsafe_unretained NSString *stream4Channels;
	__unsafe_unretained NSString *stream4SampleRate;
	__unsafe_unretained NSString *stream5Channels;
	__unsafe_unretained NSString *stream5SampleRate;
	__unsafe_unretained NSString *stream6Channels;
	__unsafe_unretained NSString *stream6SampleRate;
	__unsafe_unretained NSString *stream7Channels;
	__unsafe_unretained NSString *stream7SampleRate;
	__unsafe_unretained NSString *stream8Channels;
	__unsafe_unretained NSString *stream8SampleRate;
	__unsafe_unretained NSString *treeID;
} Rf130StationFieldsAttributes;

extern const struct Rf130StationFieldsRelationships {
	__unsafe_unretained NSString *entry;
} Rf130StationFieldsRelationships;

@class Entry;

@interface Rf130StationFieldsID : NSManagedObjectID {}
@end

@interface _Rf130StationFields : RHManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) Rf130StationFieldsID* objectID;

@property (nonatomic, strong) NSNumber* ch1Gain;

@property (atomic) float ch1GainValue;
- (float)ch1GainValue;
- (void)setCh1GainValue:(float)value_;

//- (BOOL)validateCh1Gain:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* ch1Name;

//- (BOOL)validateCh1Name:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* ch1SensorModel;

//- (BOOL)validateCh1SensorModel:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* ch1SensorSerialNumber;

//- (BOOL)validateCh1SensorSerialNumber:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* ch2Gain;

@property (atomic) float ch2GainValue;
- (float)ch2GainValue;
- (void)setCh2GainValue:(float)value_;

//- (BOOL)validateCh2Gain:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* ch2Name;

//- (BOOL)validateCh2Name:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* ch2SensorModel;

//- (BOOL)validateCh2SensorModel:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* ch2SensorSerialNumber;

//- (BOOL)validateCh2SensorSerialNumber:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* ch3Gain;

@property (atomic) float ch3GainValue;
- (float)ch3GainValue;
- (void)setCh3GainValue:(float)value_;

//- (BOOL)validateCh3Gain:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* ch3Name;

//- (BOOL)validateCh3Name:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* ch3SensorModel;

//- (BOOL)validateCh3SensorModel:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* ch3SensorSerialNumber;

//- (BOOL)validateCh3SensorSerialNumber:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* ch4Gain;

@property (atomic) float ch4GainValue;
- (float)ch4GainValue;
- (void)setCh4GainValue:(float)value_;

//- (BOOL)validateCh4Gain:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* ch4Name;

//- (BOOL)validateCh4Name:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* ch4SensorModel;

//- (BOOL)validateCh4SensorModel:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* ch4SensorSerialNumber;

//- (BOOL)validateCh4SensorSerialNumber:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* ch5Gain;

@property (atomic) float ch5GainValue;
- (float)ch5GainValue;
- (void)setCh5GainValue:(float)value_;

//- (BOOL)validateCh5Gain:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* ch5Name;

//- (BOOL)validateCh5Name:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* ch5SensorModel;

//- (BOOL)validateCh5SensorModel:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* ch5SensorSerialNumber;

//- (BOOL)validateCh5SensorSerialNumber:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* ch6Gain;

@property (atomic) float ch6GainValue;
- (float)ch6GainValue;
- (void)setCh6GainValue:(float)value_;

//- (BOOL)validateCh6Gain:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* ch6Name;

//- (BOOL)validateCh6Name:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* ch6SensorModel;

//- (BOOL)validateCh6SensorModel:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* ch6SensorSerialNumber;

//- (BOOL)validateCh6SensorSerialNumber:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* dasUnitId;

@property (atomic) int32_t dasUnitIdValue;
- (int32_t)dasUnitIdValue;
- (void)setDasUnitIdValue:(int32_t)value_;

//- (BOOL)validateDasUnitId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* experimentComment;

//- (BOOL)validateExperimentComment:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* experimentName;

//- (BOOL)validateExperimentName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* experimentNumber;

@property (atomic) int32_t experimentNumberValue;
- (int32_t)experimentNumberValue;
- (void)setExperimentNumberValue:(int32_t)value_;

//- (BOOL)validateExperimentNumber:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* nodeID;

//- (BOOL)validateNodeID:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* revisionNum;

@property (atomic) int32_t revisionNumValue;
- (int32_t)revisionNumValue;
- (void)setRevisionNumValue:(int32_t)value_;

//- (BOOL)validateRevisionNum:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor1AutoReCenterAttemptInterval;

@property (atomic) int32_t sensor1AutoReCenterAttemptIntervalValue;
- (int32_t)sensor1AutoReCenterAttemptIntervalValue;
- (void)setSensor1AutoReCenterAttemptIntervalValue:(int32_t)value_;

//- (BOOL)validateSensor1AutoReCenterAttemptInterval:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor1AutoReCenterAttempts;

@property (atomic) int32_t sensor1AutoReCenterAttemptsValue;
- (int32_t)sensor1AutoReCenterAttemptsValue;
- (void)setSensor1AutoReCenterAttemptsValue:(int32_t)value_;

//- (BOOL)validateSensor1AutoReCenterAttempts:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor1AutoReCenterCycleInterval;

@property (atomic) int32_t sensor1AutoReCenterCycleIntervalValue;
- (int32_t)sensor1AutoReCenterCycleIntervalValue;
- (void)setSensor1AutoReCenterCycleIntervalValue:(int32_t)value_;

//- (BOOL)validateSensor1AutoReCenterCycleInterval:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor1AutoReCenterEnable;

@property (atomic) BOOL sensor1AutoReCenterEnableValue;
- (BOOL)sensor1AutoReCenterEnableValue;
- (void)setSensor1AutoReCenterEnableValue:(BOOL)value_;

//- (BOOL)validateSensor1AutoReCenterEnable:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor1AutoReCenterLevelHorizontal;

@property (atomic) float sensor1AutoReCenterLevelHorizontalValue;
- (float)sensor1AutoReCenterLevelHorizontalValue;
- (void)setSensor1AutoReCenterLevelHorizontalValue:(float)value_;

//- (BOOL)validateSensor1AutoReCenterLevelHorizontal:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor1AutoReCenterLevelVertical;

@property (atomic) float sensor1AutoReCenterLevelVerticalValue;
- (float)sensor1AutoReCenterLevelVerticalValue;
- (void)setSensor1AutoReCenterLevelVerticalValue:(float)value_;

//- (BOOL)validateSensor1AutoReCenterLevelVertical:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor1AuxData1;

@property (atomic) float sensor1AuxData1Value;
- (float)sensor1AuxData1Value;
- (void)setSensor1AuxData1Value:(float)value_;

//- (BOOL)validateSensor1AuxData1:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor1AuxData2;

@property (atomic) float sensor1AuxData2Value;
- (float)sensor1AuxData2Value;
- (void)setSensor1AuxData2Value:(float)value_;

//- (BOOL)validateSensor1AuxData2:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor1AuxData3;

@property (atomic) float sensor1AuxData3Value;
- (float)sensor1AuxData3Value;
- (void)setSensor1AuxData3Value:(float)value_;

//- (BOOL)validateSensor1AuxData3:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor2AutoReCenterAttemptInterval;

@property (atomic) int32_t sensor2AutoReCenterAttemptIntervalValue;
- (int32_t)sensor2AutoReCenterAttemptIntervalValue;
- (void)setSensor2AutoReCenterAttemptIntervalValue:(int32_t)value_;

//- (BOOL)validateSensor2AutoReCenterAttemptInterval:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor2AutoReCenterAttempts;

@property (atomic) int32_t sensor2AutoReCenterAttemptsValue;
- (int32_t)sensor2AutoReCenterAttemptsValue;
- (void)setSensor2AutoReCenterAttemptsValue:(int32_t)value_;

//- (BOOL)validateSensor2AutoReCenterAttempts:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor2AutoReCenterCycleInterval;

@property (atomic) int32_t sensor2AutoReCenterCycleIntervalValue;
- (int32_t)sensor2AutoReCenterCycleIntervalValue;
- (void)setSensor2AutoReCenterCycleIntervalValue:(int32_t)value_;

//- (BOOL)validateSensor2AutoReCenterCycleInterval:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor2AutoReCenterEnable;

@property (atomic) BOOL sensor2AutoReCenterEnableValue;
- (BOOL)sensor2AutoReCenterEnableValue;
- (void)setSensor2AutoReCenterEnableValue:(BOOL)value_;

//- (BOOL)validateSensor2AutoReCenterEnable:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor2AutoReCenterLevelHorizontal;

@property (atomic) float sensor2AutoReCenterLevelHorizontalValue;
- (float)sensor2AutoReCenterLevelHorizontalValue;
- (void)setSensor2AutoReCenterLevelHorizontalValue:(float)value_;

//- (BOOL)validateSensor2AutoReCenterLevelHorizontal:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor2AutoReCenterLevelVertical;

@property (atomic) float sensor2AutoReCenterLevelVerticalValue;
- (float)sensor2AutoReCenterLevelVerticalValue;
- (void)setSensor2AutoReCenterLevelVerticalValue:(float)value_;

//- (BOOL)validateSensor2AutoReCenterLevelVertical:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor2AuxData1;

@property (atomic) float sensor2AuxData1Value;
- (float)sensor2AuxData1Value;
- (void)setSensor2AuxData1Value:(float)value_;

//- (BOOL)validateSensor2AuxData1:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor2AuxData2;

@property (atomic) float sensor2AuxData2Value;
- (float)sensor2AuxData2Value;
- (void)setSensor2AuxData2Value:(float)value_;

//- (BOOL)validateSensor2AuxData2:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor2AuxData3;

@property (atomic) float sensor2AuxData3Value;
- (float)sensor2AuxData3Value;
- (void)setSensor2AuxData3Value:(float)value_;

//- (BOOL)validateSensor2AuxData3:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor3AutoReCenterAttemptInterval;

@property (atomic) int32_t sensor3AutoReCenterAttemptIntervalValue;
- (int32_t)sensor3AutoReCenterAttemptIntervalValue;
- (void)setSensor3AutoReCenterAttemptIntervalValue:(int32_t)value_;

//- (BOOL)validateSensor3AutoReCenterAttemptInterval:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor3AutoReCenterAttempts;

@property (atomic) int32_t sensor3AutoReCenterAttemptsValue;
- (int32_t)sensor3AutoReCenterAttemptsValue;
- (void)setSensor3AutoReCenterAttemptsValue:(int32_t)value_;

//- (BOOL)validateSensor3AutoReCenterAttempts:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor3AutoReCenterCycleInterval;

@property (atomic) int32_t sensor3AutoReCenterCycleIntervalValue;
- (int32_t)sensor3AutoReCenterCycleIntervalValue;
- (void)setSensor3AutoReCenterCycleIntervalValue:(int32_t)value_;

//- (BOOL)validateSensor3AutoReCenterCycleInterval:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor3AutoReCenterEnable;

@property (atomic) BOOL sensor3AutoReCenterEnableValue;
- (BOOL)sensor3AutoReCenterEnableValue;
- (void)setSensor3AutoReCenterEnableValue:(BOOL)value_;

//- (BOOL)validateSensor3AutoReCenterEnable:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor3AutoReCenterLevelHorizontal;

@property (atomic) float sensor3AutoReCenterLevelHorizontalValue;
- (float)sensor3AutoReCenterLevelHorizontalValue;
- (void)setSensor3AutoReCenterLevelHorizontalValue:(float)value_;

//- (BOOL)validateSensor3AutoReCenterLevelHorizontal:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor3AutoReCenterLevelVertical;

@property (atomic) float sensor3AutoReCenterLevelVerticalValue;
- (float)sensor3AutoReCenterLevelVerticalValue;
- (void)setSensor3AutoReCenterLevelVerticalValue:(float)value_;

//- (BOOL)validateSensor3AutoReCenterLevelVertical:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor3AuxData1;

@property (atomic) float sensor3AuxData1Value;
- (float)sensor3AuxData1Value;
- (void)setSensor3AuxData1Value:(float)value_;

//- (BOOL)validateSensor3AuxData1:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor3AuxData2;

@property (atomic) float sensor3AuxData2Value;
- (float)sensor3AuxData2Value;
- (void)setSensor3AuxData2Value:(float)value_;

//- (BOOL)validateSensor3AuxData2:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor3AuxData3;

@property (atomic) float sensor3AuxData3Value;
- (float)sensor3AuxData3Value;
- (void)setSensor3AuxData3Value:(float)value_;

//- (BOOL)validateSensor3AuxData3:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor4AutoReCenterAttemptInterval;

@property (atomic) int32_t sensor4AutoReCenterAttemptIntervalValue;
- (int32_t)sensor4AutoReCenterAttemptIntervalValue;
- (void)setSensor4AutoReCenterAttemptIntervalValue:(int32_t)value_;

//- (BOOL)validateSensor4AutoReCenterAttemptInterval:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor4AutoReCenterAttempts;

@property (atomic) int32_t sensor4AutoReCenterAttemptsValue;
- (int32_t)sensor4AutoReCenterAttemptsValue;
- (void)setSensor4AutoReCenterAttemptsValue:(int32_t)value_;

//- (BOOL)validateSensor4AutoReCenterAttempts:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor4AutoReCenterCycleInterval;

@property (atomic) int32_t sensor4AutoReCenterCycleIntervalValue;
- (int32_t)sensor4AutoReCenterCycleIntervalValue;
- (void)setSensor4AutoReCenterCycleIntervalValue:(int32_t)value_;

//- (BOOL)validateSensor4AutoReCenterCycleInterval:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor4AutoReCenterEnable;

@property (atomic) BOOL sensor4AutoReCenterEnableValue;
- (BOOL)sensor4AutoReCenterEnableValue;
- (void)setSensor4AutoReCenterEnableValue:(BOOL)value_;

//- (BOOL)validateSensor4AutoReCenterEnable:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor4AutoReCenterLevelHorizontal;

@property (atomic) float sensor4AutoReCenterLevelHorizontalValue;
- (float)sensor4AutoReCenterLevelHorizontalValue;
- (void)setSensor4AutoReCenterLevelHorizontalValue:(float)value_;

//- (BOOL)validateSensor4AutoReCenterLevelHorizontal:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor4AutoReCenterLevelVertical;

@property (atomic) float sensor4AutoReCenterLevelVerticalValue;
- (float)sensor4AutoReCenterLevelVerticalValue;
- (void)setSensor4AutoReCenterLevelVerticalValue:(float)value_;

//- (BOOL)validateSensor4AutoReCenterLevelVertical:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor4AuxData1;

@property (atomic) float sensor4AuxData1Value;
- (float)sensor4AuxData1Value;
- (void)setSensor4AuxData1Value:(float)value_;

//- (BOOL)validateSensor4AuxData1:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor4AuxData2;

@property (atomic) float sensor4AuxData2Value;
- (float)sensor4AuxData2Value;
- (void)setSensor4AuxData2Value:(float)value_;

//- (BOOL)validateSensor4AuxData2:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sensor4AuxData3;

@property (atomic) float sensor4AuxData3Value;
- (float)sensor4AuxData3Value;
- (void)setSensor4AuxData3Value:(float)value_;

//- (BOOL)validateSensor4AuxData3:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* stationComment;

//- (BOOL)validateStationComment:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* stationName;

//- (BOOL)validateStationName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* stationNumber;

@property (atomic) int32_t stationNumberValue;
- (int32_t)stationNumberValue;
- (void)setStationNumberValue:(int32_t)value_;

//- (BOOL)validateStationNumber:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* stream1Channels;

//- (BOOL)validateStream1Channels:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* stream1SampleRate;

@property (atomic) int32_t stream1SampleRateValue;
- (int32_t)stream1SampleRateValue;
- (void)setStream1SampleRateValue:(int32_t)value_;

//- (BOOL)validateStream1SampleRate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* stream2Channels;

//- (BOOL)validateStream2Channels:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* stream2SampleRate;

@property (atomic) int32_t stream2SampleRateValue;
- (int32_t)stream2SampleRateValue;
- (void)setStream2SampleRateValue:(int32_t)value_;

//- (BOOL)validateStream2SampleRate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* stream3Channels;

//- (BOOL)validateStream3Channels:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* stream3SampleRate;

@property (atomic) int32_t stream3SampleRateValue;
- (int32_t)stream3SampleRateValue;
- (void)setStream3SampleRateValue:(int32_t)value_;

//- (BOOL)validateStream3SampleRate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* stream4Channels;

//- (BOOL)validateStream4Channels:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* stream4SampleRate;

@property (atomic) int32_t stream4SampleRateValue;
- (int32_t)stream4SampleRateValue;
- (void)setStream4SampleRateValue:(int32_t)value_;

//- (BOOL)validateStream4SampleRate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* stream5Channels;

//- (BOOL)validateStream5Channels:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* stream5SampleRate;

@property (atomic) int32_t stream5SampleRateValue;
- (int32_t)stream5SampleRateValue;
- (void)setStream5SampleRateValue:(int32_t)value_;

//- (BOOL)validateStream5SampleRate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* stream6Channels;

//- (BOOL)validateStream6Channels:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* stream6SampleRate;

@property (atomic) int32_t stream6SampleRateValue;
- (int32_t)stream6SampleRateValue;
- (void)setStream6SampleRateValue:(int32_t)value_;

//- (BOOL)validateStream6SampleRate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* stream7Channels;

//- (BOOL)validateStream7Channels:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* stream7SampleRate;

@property (atomic) int32_t stream7SampleRateValue;
- (int32_t)stream7SampleRateValue;
- (void)setStream7SampleRateValue:(int32_t)value_;

//- (BOOL)validateStream7SampleRate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* stream8Channels;

//- (BOOL)validateStream8Channels:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* stream8SampleRate;

@property (atomic) int32_t stream8SampleRateValue;
- (int32_t)stream8SampleRateValue;
- (void)setStream8SampleRateValue:(int32_t)value_;

//- (BOOL)validateStream8SampleRate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* treeID;

//- (BOOL)validateTreeID:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *entry;

- (NSMutableSet*)entrySet;

@end

@interface _Rf130StationFields (EntryCoreDataGeneratedAccessors)
- (void)addEntry:(NSSet*)value_;
- (void)removeEntry:(NSSet*)value_;
- (void)addEntryObject:(Entry*)value_;
- (void)removeEntryObject:(Entry*)value_;

@end

@interface _Rf130StationFields (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveCh1Gain;
- (void)setPrimitiveCh1Gain:(NSNumber*)value;

- (float)primitiveCh1GainValue;
- (void)setPrimitiveCh1GainValue:(float)value_;

- (NSString*)primitiveCh1Name;
- (void)setPrimitiveCh1Name:(NSString*)value;

- (NSString*)primitiveCh1SensorModel;
- (void)setPrimitiveCh1SensorModel:(NSString*)value;

- (NSString*)primitiveCh1SensorSerialNumber;
- (void)setPrimitiveCh1SensorSerialNumber:(NSString*)value;

- (NSNumber*)primitiveCh2Gain;
- (void)setPrimitiveCh2Gain:(NSNumber*)value;

- (float)primitiveCh2GainValue;
- (void)setPrimitiveCh2GainValue:(float)value_;

- (NSString*)primitiveCh2Name;
- (void)setPrimitiveCh2Name:(NSString*)value;

- (NSString*)primitiveCh2SensorModel;
- (void)setPrimitiveCh2SensorModel:(NSString*)value;

- (NSString*)primitiveCh2SensorSerialNumber;
- (void)setPrimitiveCh2SensorSerialNumber:(NSString*)value;

- (NSNumber*)primitiveCh3Gain;
- (void)setPrimitiveCh3Gain:(NSNumber*)value;

- (float)primitiveCh3GainValue;
- (void)setPrimitiveCh3GainValue:(float)value_;

- (NSString*)primitiveCh3Name;
- (void)setPrimitiveCh3Name:(NSString*)value;

- (NSString*)primitiveCh3SensorModel;
- (void)setPrimitiveCh3SensorModel:(NSString*)value;

- (NSString*)primitiveCh3SensorSerialNumber;
- (void)setPrimitiveCh3SensorSerialNumber:(NSString*)value;

- (NSNumber*)primitiveCh4Gain;
- (void)setPrimitiveCh4Gain:(NSNumber*)value;

- (float)primitiveCh4GainValue;
- (void)setPrimitiveCh4GainValue:(float)value_;

- (NSString*)primitiveCh4Name;
- (void)setPrimitiveCh4Name:(NSString*)value;

- (NSString*)primitiveCh4SensorModel;
- (void)setPrimitiveCh4SensorModel:(NSString*)value;

- (NSString*)primitiveCh4SensorSerialNumber;
- (void)setPrimitiveCh4SensorSerialNumber:(NSString*)value;

- (NSNumber*)primitiveCh5Gain;
- (void)setPrimitiveCh5Gain:(NSNumber*)value;

- (float)primitiveCh5GainValue;
- (void)setPrimitiveCh5GainValue:(float)value_;

- (NSString*)primitiveCh5Name;
- (void)setPrimitiveCh5Name:(NSString*)value;

- (NSString*)primitiveCh5SensorModel;
- (void)setPrimitiveCh5SensorModel:(NSString*)value;

- (NSString*)primitiveCh5SensorSerialNumber;
- (void)setPrimitiveCh5SensorSerialNumber:(NSString*)value;

- (NSNumber*)primitiveCh6Gain;
- (void)setPrimitiveCh6Gain:(NSNumber*)value;

- (float)primitiveCh6GainValue;
- (void)setPrimitiveCh6GainValue:(float)value_;

- (NSString*)primitiveCh6Name;
- (void)setPrimitiveCh6Name:(NSString*)value;

- (NSString*)primitiveCh6SensorModel;
- (void)setPrimitiveCh6SensorModel:(NSString*)value;

- (NSString*)primitiveCh6SensorSerialNumber;
- (void)setPrimitiveCh6SensorSerialNumber:(NSString*)value;

- (NSNumber*)primitiveDasUnitId;
- (void)setPrimitiveDasUnitId:(NSNumber*)value;

- (int32_t)primitiveDasUnitIdValue;
- (void)setPrimitiveDasUnitIdValue:(int32_t)value_;

- (NSString*)primitiveExperimentComment;
- (void)setPrimitiveExperimentComment:(NSString*)value;

- (NSString*)primitiveExperimentName;
- (void)setPrimitiveExperimentName:(NSString*)value;

- (NSNumber*)primitiveExperimentNumber;
- (void)setPrimitiveExperimentNumber:(NSNumber*)value;

- (int32_t)primitiveExperimentNumberValue;
- (void)setPrimitiveExperimentNumberValue:(int32_t)value_;

- (NSString*)primitiveNodeID;
- (void)setPrimitiveNodeID:(NSString*)value;

- (NSNumber*)primitiveRevisionNum;
- (void)setPrimitiveRevisionNum:(NSNumber*)value;

- (int32_t)primitiveRevisionNumValue;
- (void)setPrimitiveRevisionNumValue:(int32_t)value_;

- (NSNumber*)primitiveSensor1AutoReCenterAttemptInterval;
- (void)setPrimitiveSensor1AutoReCenterAttemptInterval:(NSNumber*)value;

- (int32_t)primitiveSensor1AutoReCenterAttemptIntervalValue;
- (void)setPrimitiveSensor1AutoReCenterAttemptIntervalValue:(int32_t)value_;

- (NSNumber*)primitiveSensor1AutoReCenterAttempts;
- (void)setPrimitiveSensor1AutoReCenterAttempts:(NSNumber*)value;

- (int32_t)primitiveSensor1AutoReCenterAttemptsValue;
- (void)setPrimitiveSensor1AutoReCenterAttemptsValue:(int32_t)value_;

- (NSNumber*)primitiveSensor1AutoReCenterCycleInterval;
- (void)setPrimitiveSensor1AutoReCenterCycleInterval:(NSNumber*)value;

- (int32_t)primitiveSensor1AutoReCenterCycleIntervalValue;
- (void)setPrimitiveSensor1AutoReCenterCycleIntervalValue:(int32_t)value_;

- (NSNumber*)primitiveSensor1AutoReCenterEnable;
- (void)setPrimitiveSensor1AutoReCenterEnable:(NSNumber*)value;

- (BOOL)primitiveSensor1AutoReCenterEnableValue;
- (void)setPrimitiveSensor1AutoReCenterEnableValue:(BOOL)value_;

- (NSNumber*)primitiveSensor1AutoReCenterLevelHorizontal;
- (void)setPrimitiveSensor1AutoReCenterLevelHorizontal:(NSNumber*)value;

- (float)primitiveSensor1AutoReCenterLevelHorizontalValue;
- (void)setPrimitiveSensor1AutoReCenterLevelHorizontalValue:(float)value_;

- (NSNumber*)primitiveSensor1AutoReCenterLevelVertical;
- (void)setPrimitiveSensor1AutoReCenterLevelVertical:(NSNumber*)value;

- (float)primitiveSensor1AutoReCenterLevelVerticalValue;
- (void)setPrimitiveSensor1AutoReCenterLevelVerticalValue:(float)value_;

- (NSNumber*)primitiveSensor1AuxData1;
- (void)setPrimitiveSensor1AuxData1:(NSNumber*)value;

- (float)primitiveSensor1AuxData1Value;
- (void)setPrimitiveSensor1AuxData1Value:(float)value_;

- (NSNumber*)primitiveSensor1AuxData2;
- (void)setPrimitiveSensor1AuxData2:(NSNumber*)value;

- (float)primitiveSensor1AuxData2Value;
- (void)setPrimitiveSensor1AuxData2Value:(float)value_;

- (NSNumber*)primitiveSensor1AuxData3;
- (void)setPrimitiveSensor1AuxData3:(NSNumber*)value;

- (float)primitiveSensor1AuxData3Value;
- (void)setPrimitiveSensor1AuxData3Value:(float)value_;

- (NSNumber*)primitiveSensor2AutoReCenterAttemptInterval;
- (void)setPrimitiveSensor2AutoReCenterAttemptInterval:(NSNumber*)value;

- (int32_t)primitiveSensor2AutoReCenterAttemptIntervalValue;
- (void)setPrimitiveSensor2AutoReCenterAttemptIntervalValue:(int32_t)value_;

- (NSNumber*)primitiveSensor2AutoReCenterAttempts;
- (void)setPrimitiveSensor2AutoReCenterAttempts:(NSNumber*)value;

- (int32_t)primitiveSensor2AutoReCenterAttemptsValue;
- (void)setPrimitiveSensor2AutoReCenterAttemptsValue:(int32_t)value_;

- (NSNumber*)primitiveSensor2AutoReCenterCycleInterval;
- (void)setPrimitiveSensor2AutoReCenterCycleInterval:(NSNumber*)value;

- (int32_t)primitiveSensor2AutoReCenterCycleIntervalValue;
- (void)setPrimitiveSensor2AutoReCenterCycleIntervalValue:(int32_t)value_;

- (NSNumber*)primitiveSensor2AutoReCenterEnable;
- (void)setPrimitiveSensor2AutoReCenterEnable:(NSNumber*)value;

- (BOOL)primitiveSensor2AutoReCenterEnableValue;
- (void)setPrimitiveSensor2AutoReCenterEnableValue:(BOOL)value_;

- (NSNumber*)primitiveSensor2AutoReCenterLevelHorizontal;
- (void)setPrimitiveSensor2AutoReCenterLevelHorizontal:(NSNumber*)value;

- (float)primitiveSensor2AutoReCenterLevelHorizontalValue;
- (void)setPrimitiveSensor2AutoReCenterLevelHorizontalValue:(float)value_;

- (NSNumber*)primitiveSensor2AutoReCenterLevelVertical;
- (void)setPrimitiveSensor2AutoReCenterLevelVertical:(NSNumber*)value;

- (float)primitiveSensor2AutoReCenterLevelVerticalValue;
- (void)setPrimitiveSensor2AutoReCenterLevelVerticalValue:(float)value_;

- (NSNumber*)primitiveSensor2AuxData1;
- (void)setPrimitiveSensor2AuxData1:(NSNumber*)value;

- (float)primitiveSensor2AuxData1Value;
- (void)setPrimitiveSensor2AuxData1Value:(float)value_;

- (NSNumber*)primitiveSensor2AuxData2;
- (void)setPrimitiveSensor2AuxData2:(NSNumber*)value;

- (float)primitiveSensor2AuxData2Value;
- (void)setPrimitiveSensor2AuxData2Value:(float)value_;

- (NSNumber*)primitiveSensor2AuxData3;
- (void)setPrimitiveSensor2AuxData3:(NSNumber*)value;

- (float)primitiveSensor2AuxData3Value;
- (void)setPrimitiveSensor2AuxData3Value:(float)value_;

- (NSNumber*)primitiveSensor3AutoReCenterAttemptInterval;
- (void)setPrimitiveSensor3AutoReCenterAttemptInterval:(NSNumber*)value;

- (int32_t)primitiveSensor3AutoReCenterAttemptIntervalValue;
- (void)setPrimitiveSensor3AutoReCenterAttemptIntervalValue:(int32_t)value_;

- (NSNumber*)primitiveSensor3AutoReCenterAttempts;
- (void)setPrimitiveSensor3AutoReCenterAttempts:(NSNumber*)value;

- (int32_t)primitiveSensor3AutoReCenterAttemptsValue;
- (void)setPrimitiveSensor3AutoReCenterAttemptsValue:(int32_t)value_;

- (NSNumber*)primitiveSensor3AutoReCenterCycleInterval;
- (void)setPrimitiveSensor3AutoReCenterCycleInterval:(NSNumber*)value;

- (int32_t)primitiveSensor3AutoReCenterCycleIntervalValue;
- (void)setPrimitiveSensor3AutoReCenterCycleIntervalValue:(int32_t)value_;

- (NSNumber*)primitiveSensor3AutoReCenterEnable;
- (void)setPrimitiveSensor3AutoReCenterEnable:(NSNumber*)value;

- (BOOL)primitiveSensor3AutoReCenterEnableValue;
- (void)setPrimitiveSensor3AutoReCenterEnableValue:(BOOL)value_;

- (NSNumber*)primitiveSensor3AutoReCenterLevelHorizontal;
- (void)setPrimitiveSensor3AutoReCenterLevelHorizontal:(NSNumber*)value;

- (float)primitiveSensor3AutoReCenterLevelHorizontalValue;
- (void)setPrimitiveSensor3AutoReCenterLevelHorizontalValue:(float)value_;

- (NSNumber*)primitiveSensor3AutoReCenterLevelVertical;
- (void)setPrimitiveSensor3AutoReCenterLevelVertical:(NSNumber*)value;

- (float)primitiveSensor3AutoReCenterLevelVerticalValue;
- (void)setPrimitiveSensor3AutoReCenterLevelVerticalValue:(float)value_;

- (NSNumber*)primitiveSensor3AuxData1;
- (void)setPrimitiveSensor3AuxData1:(NSNumber*)value;

- (float)primitiveSensor3AuxData1Value;
- (void)setPrimitiveSensor3AuxData1Value:(float)value_;

- (NSNumber*)primitiveSensor3AuxData2;
- (void)setPrimitiveSensor3AuxData2:(NSNumber*)value;

- (float)primitiveSensor3AuxData2Value;
- (void)setPrimitiveSensor3AuxData2Value:(float)value_;

- (NSNumber*)primitiveSensor3AuxData3;
- (void)setPrimitiveSensor3AuxData3:(NSNumber*)value;

- (float)primitiveSensor3AuxData3Value;
- (void)setPrimitiveSensor3AuxData3Value:(float)value_;

- (NSNumber*)primitiveSensor4AutoReCenterAttemptInterval;
- (void)setPrimitiveSensor4AutoReCenterAttemptInterval:(NSNumber*)value;

- (int32_t)primitiveSensor4AutoReCenterAttemptIntervalValue;
- (void)setPrimitiveSensor4AutoReCenterAttemptIntervalValue:(int32_t)value_;

- (NSNumber*)primitiveSensor4AutoReCenterAttempts;
- (void)setPrimitiveSensor4AutoReCenterAttempts:(NSNumber*)value;

- (int32_t)primitiveSensor4AutoReCenterAttemptsValue;
- (void)setPrimitiveSensor4AutoReCenterAttemptsValue:(int32_t)value_;

- (NSNumber*)primitiveSensor4AutoReCenterCycleInterval;
- (void)setPrimitiveSensor4AutoReCenterCycleInterval:(NSNumber*)value;

- (int32_t)primitiveSensor4AutoReCenterCycleIntervalValue;
- (void)setPrimitiveSensor4AutoReCenterCycleIntervalValue:(int32_t)value_;

- (NSNumber*)primitiveSensor4AutoReCenterEnable;
- (void)setPrimitiveSensor4AutoReCenterEnable:(NSNumber*)value;

- (BOOL)primitiveSensor4AutoReCenterEnableValue;
- (void)setPrimitiveSensor4AutoReCenterEnableValue:(BOOL)value_;

- (NSNumber*)primitiveSensor4AutoReCenterLevelHorizontal;
- (void)setPrimitiveSensor4AutoReCenterLevelHorizontal:(NSNumber*)value;

- (float)primitiveSensor4AutoReCenterLevelHorizontalValue;
- (void)setPrimitiveSensor4AutoReCenterLevelHorizontalValue:(float)value_;

- (NSNumber*)primitiveSensor4AutoReCenterLevelVertical;
- (void)setPrimitiveSensor4AutoReCenterLevelVertical:(NSNumber*)value;

- (float)primitiveSensor4AutoReCenterLevelVerticalValue;
- (void)setPrimitiveSensor4AutoReCenterLevelVerticalValue:(float)value_;

- (NSNumber*)primitiveSensor4AuxData1;
- (void)setPrimitiveSensor4AuxData1:(NSNumber*)value;

- (float)primitiveSensor4AuxData1Value;
- (void)setPrimitiveSensor4AuxData1Value:(float)value_;

- (NSNumber*)primitiveSensor4AuxData2;
- (void)setPrimitiveSensor4AuxData2:(NSNumber*)value;

- (float)primitiveSensor4AuxData2Value;
- (void)setPrimitiveSensor4AuxData2Value:(float)value_;

- (NSNumber*)primitiveSensor4AuxData3;
- (void)setPrimitiveSensor4AuxData3:(NSNumber*)value;

- (float)primitiveSensor4AuxData3Value;
- (void)setPrimitiveSensor4AuxData3Value:(float)value_;

- (NSString*)primitiveStationComment;
- (void)setPrimitiveStationComment:(NSString*)value;

- (NSString*)primitiveStationName;
- (void)setPrimitiveStationName:(NSString*)value;

- (NSNumber*)primitiveStationNumber;
- (void)setPrimitiveStationNumber:(NSNumber*)value;

- (int32_t)primitiveStationNumberValue;
- (void)setPrimitiveStationNumberValue:(int32_t)value_;

- (NSString*)primitiveStream1Channels;
- (void)setPrimitiveStream1Channels:(NSString*)value;

- (NSNumber*)primitiveStream1SampleRate;
- (void)setPrimitiveStream1SampleRate:(NSNumber*)value;

- (int32_t)primitiveStream1SampleRateValue;
- (void)setPrimitiveStream1SampleRateValue:(int32_t)value_;

- (NSString*)primitiveStream2Channels;
- (void)setPrimitiveStream2Channels:(NSString*)value;

- (NSNumber*)primitiveStream2SampleRate;
- (void)setPrimitiveStream2SampleRate:(NSNumber*)value;

- (int32_t)primitiveStream2SampleRateValue;
- (void)setPrimitiveStream2SampleRateValue:(int32_t)value_;

- (NSString*)primitiveStream3Channels;
- (void)setPrimitiveStream3Channels:(NSString*)value;

- (NSNumber*)primitiveStream3SampleRate;
- (void)setPrimitiveStream3SampleRate:(NSNumber*)value;

- (int32_t)primitiveStream3SampleRateValue;
- (void)setPrimitiveStream3SampleRateValue:(int32_t)value_;

- (NSString*)primitiveStream4Channels;
- (void)setPrimitiveStream4Channels:(NSString*)value;

- (NSNumber*)primitiveStream4SampleRate;
- (void)setPrimitiveStream4SampleRate:(NSNumber*)value;

- (int32_t)primitiveStream4SampleRateValue;
- (void)setPrimitiveStream4SampleRateValue:(int32_t)value_;

- (NSString*)primitiveStream5Channels;
- (void)setPrimitiveStream5Channels:(NSString*)value;

- (NSNumber*)primitiveStream5SampleRate;
- (void)setPrimitiveStream5SampleRate:(NSNumber*)value;

- (int32_t)primitiveStream5SampleRateValue;
- (void)setPrimitiveStream5SampleRateValue:(int32_t)value_;

- (NSString*)primitiveStream6Channels;
- (void)setPrimitiveStream6Channels:(NSString*)value;

- (NSNumber*)primitiveStream6SampleRate;
- (void)setPrimitiveStream6SampleRate:(NSNumber*)value;

- (int32_t)primitiveStream6SampleRateValue;
- (void)setPrimitiveStream6SampleRateValue:(int32_t)value_;

- (NSString*)primitiveStream7Channels;
- (void)setPrimitiveStream7Channels:(NSString*)value;

- (NSNumber*)primitiveStream7SampleRate;
- (void)setPrimitiveStream7SampleRate:(NSNumber*)value;

- (int32_t)primitiveStream7SampleRateValue;
- (void)setPrimitiveStream7SampleRateValue:(int32_t)value_;

- (NSString*)primitiveStream8Channels;
- (void)setPrimitiveStream8Channels:(NSString*)value;

- (NSNumber*)primitiveStream8SampleRate;
- (void)setPrimitiveStream8SampleRate:(NSNumber*)value;

- (int32_t)primitiveStream8SampleRateValue;
- (void)setPrimitiveStream8SampleRateValue:(int32_t)value_;

- (NSString*)primitiveTreeID;
- (void)setPrimitiveTreeID:(NSString*)value;

- (NSMutableSet*)primitiveEntry;
- (void)setPrimitiveEntry:(NSMutableSet*)value;

@end
