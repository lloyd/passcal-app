#import "UserFields.h"

@interface UserFields ()

// Private interface goes here.

@end

@implementation UserFields

+(NSString *)modelName {
    return @"passcalApp_1_X";
}

+(id) initWithDictionary: (NSDictionary *) d {
    return [RevControlHelper initWithDictionary: [UserFields class] : d];
}

+(BOOL) doesNodeIdExist: (NSString*) nodeId {
    return [RevControlHelper doesNodeIdExist:[UserFields class] :nodeId];
}

-(id) createNewRevision {
    return [RevControlHelper createRevision:self];
}

@end
