//
//  main.m
//  passcalApp_1_X
//
//  Created by matt on 6/23/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
