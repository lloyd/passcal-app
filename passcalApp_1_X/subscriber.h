//
//  subscriber.h
//
//
// Interface which all subscrivers should inherent.  Note this is
// an active publisher pattern.
@protocol subscriber

// recvObject
// This method is called by the publisher to pass an object
- (void) recvObject : (NSObject*) obj;

@optional

// recvBytes
// Similar to above but for raw binary byte buffers
- (void) recvBytes : (uint8_t*) buffer : (uint16_t) len;
@end