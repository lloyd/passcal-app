//
//  myLocation.m
//  passcalApp_1_X
//
//  Created by field on 9/26/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "myLocation.h"
#import <UIKit/UIKit.h>

#import "CocoaLumberjack.h"
static const DDLogLevel ddLogLevel = DDLogLevelInfo;

const float minAccuracy = 5.0;

@implementation myLocation
-(id) init {
    if (!(self = [super init]))
        return nil;
        
    self.manager = [[CLLocationManager alloc] init];
    
    self.manager.desiredAccuracy = kCLLocationAccuracyBest;
    
    self.manager.delegate = self;
    
    return self;
}

-(void) start{
    [self.manager requestWhenInUseAuthorization];
    [self.manager startUpdatingLocation];
}
-(void) stop{
    [self.manager stopUpdatingLocation];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    DDLogError(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        self.lon = currentLocation.coordinate.longitude;
        self.lat = currentLocation.coordinate.latitude;
        self.altitude = currentLocation.altitude;
        self.hAccuracy = currentLocation.horizontalAccuracy;
        self.vAccuracy = currentLocation.verticalAccuracy;
        
        if (currentLocation.horizontalAccuracy <= minAccuracy &&
            currentLocation.verticalAccuracy   <= minAccuracy) {
            self.validGps = true;
        }
        else {
            self.validGps = false;
        }
        
    }
    else {
        self.validGps = false;
    }
}

@end
