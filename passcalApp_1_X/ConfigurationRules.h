//
//  ConfigurationRules.h
//  passcalApp_1_X
//
//  Created by field on 9/14/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConfigurationRules : NSObject

// Structured list
@property NSDictionary *reviewKeyLists;

// one to one
@property NSDictionary *displayKeyName; // <NSString, NSString>
@property NSDictionary *valueDisplayFormat; // <NSString, NSString>
@property NSSet *fieldEditable; // <NSString with keys>
@property NSSet *excludeFromMaster; // <NSString with keys>

@property NSDictionary *keyRules; // <NSString, NSArray>

@property NSArray *stationFieldsKeys;
@property NSArray *statusFieldsKeys;
@property NSArray *gpsFieldsKeys;
@property NSArray *sensorFieldsKeys;
@property NSArray *versionFieldsKeys;
@property NSArray *userFieldsKeys;

+ (id) sharedManager;
-(BOOL) evalRulesForKey : (NSString *) key : (NSDictionary *) deepEntryDict;

@end
