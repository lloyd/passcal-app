//
//  saveExport.m
//  passcalApp_1_X
//
//  Created by field on 7/14/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "saveExport.h"
#import "xmlDictionary.h"
#import "currentEntries.h"
#import "utils.h"
#import "RHManagedObjectContextManager.h"

#import "Entry.h"
#import "IosFields.h"
#import "Rf130StationFields.h"
#import "Rf130StatusFields.h"
#import "Rf130VersionFields.h"
#import "UserFields.h"

#import "CocoaLumberjack.h"
static const DDLogLevel ddLogLevel = DDLogLevelInfo;

@interface saveExport ()
{
    NSArray *pickerArray;
    UITextField *myTextField;
    
}
@property Entry *stationEntry;

- (void) addPickerView;
- (void) addTextView;
- (void) setStationFromUri: (NSURL*) url;
- (void) setDefaultFilename;

@end

@implementation saveExport

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setStationFromUri: self.stationURI];
    
    [self addTextView];
    [self addPickerView];
    
    [self setDefaultFilename];
}

- (void) setDefaultFilename {
    NSString *datePrefix = [utils date2Filename:self.stationEntry.entryTime];
    NSString *trimedStationName = [self.stationEntry.rf130Station.stationName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *defaultFilename = [NSString stringWithFormat:@"%@_%@.xml", datePrefix, trimedStationName];
    self.outFilename = defaultFilename;
    self.outFilenameTextField.text = defaultFilename;
}

-(void) setStationFromUri: (NSURL*) url {
    self.stationEntry = [Entry existingEntityWithUrl:url error:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)saveButton:(id)sender {
    UIAlertView *msgBox;
    switch (self.pickedRow) {
        case 0: // CompleteStation
            [self saveCompleteDasFields];
            msgBox = [[UIAlertView alloc]
                      initWithTitle:@"Saved" message: [NSString stringWithFormat:@"File saved as %@", self.outFilename]
                      delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [msgBox show];
            break;
   
        default:
            msgBox = [[UIAlertView alloc]
                       initWithTitle:@"Warning" message: @"Format not implemented"
                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [msgBox show];
            break;
    }
}

- (IBAction)resetFilenameButton:(id)sender {
    [self setDefaultFilename];
}

- (void)saveCompleteDasFields {
    NSMutableDictionary *stationDict = [[NSMutableDictionary alloc] initWithDictionary:[self.stationEntry deepSerialize:true]];
    stationDict[XMLDictionaryNodeNameKey] = @"dasFieldsComplete";
    NSString *xmlStr = [stationDict XMLString];
    
    NSError *error;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *exportDirName = @"export";
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *exportDir = [documentsDirectory stringByAppendingPathComponent:exportDirName];
    NSString *outFile = [exportDir stringByAppendingPathComponent:self.outFilename];
    
    if (![fileManager fileExistsAtPath:exportDir]){
        if (![fileManager createDirectoryAtPath:exportDir
                    withIntermediateDirectories:NO
                                     attributes:nil
                                          error:&error]) {
            DDLogError(@"Create directory error: %@", error);
        }
    }
    
    [xmlStr writeToFile:outFile atomically:YES encoding:NSStringEncodingConversionAllowLossy error:nil];
}

- (void) appendDictionaryToFile:(NSFileHandle *)fileHandle inDict:(NSDictionary *)inDict {
    if (fileHandle)
    {
        NSString *xmlStr = [[NSString alloc] initWithFormat:@"%@\n", [inDict XMLString]];
        [fileHandle seekToEndOfFile];
        [fileHandle writeData:[xmlStr dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

//// Start picker view ////

-(void) addPickerView {
    pickerArray = [[NSArray alloc]initWithObjects:@"DAS Fields Complete XML",
                   @"Station XML", nil];
    
    self.pickedRow = 0;
    self.outputFormatPicker.dataSource = self;
    self.outputFormatPicker.delegate = self;
    
}

#pragma mark - Picker View Data source
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component{
    return [pickerArray count];
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return pickerArray[row];
}

#pragma mark- Picker View Delegate

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:
(NSInteger)row inComponent:(NSInteger)component{
    self.pickedRow = row;
}

//// End picker view ////
//// Start tectField view ////

-(void) addTextView {
    self.outFilenameTextField.delegate = self;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.outFilenameTextField resignFirstResponder];
    
    self.outFilename = textField.text;
    
    return YES;
}

//// End tectField view ////

@end
