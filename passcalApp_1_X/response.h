//
//  response.h
//
// Base class for responses.  Oringally designed for RF130 messages but
// can be expanded for other message schemes.

#import <Foundation/Foundation.h>
#import "rf130Header.h"

const int responseSuffix;

@interface response : NSObject

@property rf130Header *header;
@property NSInteger   rawMessageSize;  // Size in bytes

@end
