//
//  RulesParser.m
//  passcalApp_1_X
//
//  Created by field on 9/14/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "rule.h"
#import "RulesParser.h"

#import "CocoaLumberjack.h"
static const DDLogLevel ddLogLevel = DDLogLevelInfo;

@implementation RulesParser

+ (NSDictionary *) parseRules: (NSDictionary*) keyConfig{
    NSMutableDictionary *rulesDict = [[NSMutableDictionary alloc] init];
    
    for (NSString *fieldKey in keyConfig) {
        NSMutableArray *newRulesArray = [[NSMutableArray alloc] init];
        if (![keyConfig[fieldKey] isKindOfClass:[NSDictionary class]])
        {
            DDLogError(@"Malformed key: \"%@\".  Possibly duplicate value", fieldKey);
            continue;
        }
        for (NSString *key in keyConfig[fieldKey]) {
            id<rule> newRule;
            // begin premade rules
            newRule = [RulesParser premadeRule:key:keyConfig[fieldKey][key]];
            if (newRule)
            {
                [newRulesArray addObject:newRule];
            }
            // begin normal rules
            if (!newRule && [key isEqualToString:@"rule"]) {
                // if multiple rules for same key
                if ([keyConfig[fieldKey][key] isKindOfClass:[NSArray class]]) {
                    NSArray *rulesArray = keyConfig[fieldKey][key];
                    for (NSString *item in rulesArray) {
                        newRule = [RulesParser makeRule:item];
                        if (newRule)
                        {
                            [newRulesArray addObject:newRule];
                        }
                    }
                }
                // if only one rule
                else if ([keyConfig[fieldKey][key] isKindOfClass:[NSString class]]) {
                    newRule = [RulesParser makeRule:keyConfig[fieldKey][key]];
                    if (newRule)
                    {
                        [newRulesArray addObject:newRule];
                    }
                }
            }
        }
        if ([newRulesArray count] > 0 )
        {
            rulesDict[fieldKey] = [[NSArray alloc] initWithArray:newRulesArray];
        }
    }
    return [[NSDictionary alloc] initWithDictionary:rulesDict];
}

+ (id<rule>) premadeRule : (NSString*) key : (id) value {
    if ([key isEqualToString:@"notEmptyRule"]) {
        return [[notEmptyRule alloc] init];
    }
    else if ([key isEqualToString:@"regexRule"]) {
        NSString *pattern = value;
        return [[regexRule alloc] initWithPattern:pattern];
    }
    return nil;
}

+ (id<rule>) makeRule : (NSString*) str {
    NSScanner *scanner = [NSScanner scannerWithString:str];

    id leftOperand, rightOperand;
    compoundRuleWithRefs *currentCompRule;
    simpleRuleWithRef *simpleRule;
    enum ruleExprOpsEnum expOp = invalidExprOp;
    enum ruleCompoundOpsEnum compOp = invalidCompOp;
    //BOOL startNewGroup = false;
    NSMutableArray *groupRuleStack = [[NSMutableArray alloc] init];
    
    while (!scanner.isAtEnd) {
        
        /*
        // Check for grouping
        if ([scanner scanString:@")" intoString:NULL]) {
            startNewGroup = true;
        }
        */

        
        // Check for grouping
        while ([scanner scanString:@"(" intoString:NULL]) { // push context
            if (currentCompRule) {
                [groupRuleStack addObject:currentCompRule];
                currentCompRule = nil;
            }
            else
                [groupRuleStack addObject:[NSNull null]];
        }
        
        leftOperand = [RulesParser scanIdentifier:scanner];
        if (!leftOperand) {
            leftOperand = [RulesParser scanLiteral:scanner];
        }
        if (!leftOperand) {
            DDLogError(@"Error parsing left operand of rule: \"%@\"", str);
            return nil;
        }
        
        expOp = [RulesParser scanExpOps:scanner];
        if (expOp == invalidExprOp) {
            DDLogError(@"Error parsing, no valid exp operator found in rule: \"%@\"", str);
            return nil;
        }
        
        rightOperand = [RulesParser scanIdentifier:scanner];
        if (!rightOperand) {
            rightOperand = [RulesParser scanLiteral:scanner];
        }
        if (!rightOperand) {
            DDLogError(@"Error parsing right operand of rule: \"%@\"", str);
            return nil;
        }

        simpleRule = [RulesParser createSimpleRule:leftOperand:rightOperand:expOp];
        
        while ([scanner scanString:@")" intoString:NULL]) { // pop context
            id outerContext = [groupRuleStack lastObject];
            [groupRuleStack removeLastObject];
            if (currentCompRule && [outerContext isKindOfClass:[compoundRuleWithRefs class]]) {
                [outerContext addRule:currentCompRule];
                currentCompRule = outerContext;
            }
            else if (!currentCompRule && [outerContext isKindOfClass:[compoundRuleWithRefs class]]) {
                currentCompRule = outerContext;
            }
            // else outerContext is null then leave currentCompRule alone
        }
        
        compOp = [RulesParser scanCompOps:scanner];
        if (compOp != invalidCompOp) {
            if (!currentCompRule) {
                currentCompRule = [[compoundRuleWithRefs alloc] init];
            }
            if (simpleRule) {
                [currentCompRule addRule:simpleRule];
                simpleRule = nil;
            }
            else {
                DDLogError(@"Error parsing unexpected compound state in rule: \"%@\"", str);
                return nil;
            }
            [currentCompRule addCompOp:compOp];
        }
    }
    // TODO check if stack is empty otherwise missing ")"
    if (currentCompRule) {
        if (simpleRule)
            [currentCompRule addRule:simpleRule];
        else {
            DDLogError(@"Missing right expression for compound in rule: \"%@\"", str);
            return nil;
        }
            
        return currentCompRule;
    }
    else
        return simpleRule;
    // TODO check that a valid rule was found
}
// TODO move to simple rule
+(simpleRuleWithRef*) createSimpleRule: (id) leftOperand : (id) rightOperand : (enum ruleExprOpsEnum) expOp {
    // Note can only check if something is a indentifer
    // This approch may be unsafe if string liberals exist
    if ([leftOperand isKindOfClass:[floatLiteral class]] &&
        [rightOperand isKindOfClass:[identifier class]]) {
        return [[simpleRuleWithRef alloc] initWithLeftLiteral: leftOperand : rightOperand : expOp];
    }
    else if ([leftOperand isKindOfClass:[identifier class]] &&
             [rightOperand isKindOfClass:[floatLiteral class]]) {
        return [[simpleRuleWithRef alloc] initWithRightLiteral: leftOperand : rightOperand : expOp];
    }
    else if ([leftOperand isKindOfClass:[identifier class]] &&
             [rightOperand isKindOfClass:[identifier class]]) {
        return [[simpleRuleWithRef alloc] initWithTwoIdentifiers: leftOperand : rightOperand : expOp];
    }
    
    return nil;
}

+(enum ruleExprOpsEnum) scanExpOps : (NSScanner*) scanner{
    // These arrays should have a one to one comparison
    enum ruleExprOpsEnum exprOpNames[] = {lessThan, greaterThan, lessThanEqual, greaterThanEqual, equal, notEqual};
    NSArray *exprOps = @[@"<", @">", @"<=", @">=", @"==", @"!="];
    for (int opIdx = 0; opIdx < [exprOps count]; opIdx++) {
        if ([scanner scanString:exprOps[opIdx] intoString:NULL]) {
            return exprOpNames[opIdx];
        }
    }
    
    return invalidExprOp;
}
+(enum ruleCompoundOpsEnum) scanCompOps : (NSScanner*) scanner{
    // These arrays should have a one to one comparison
    enum ruleCompoundOpsEnum compOpNames[] = {compoundAnd, compoundOr};
    NSArray *compOps = @[@"and", @"or"];
    for (int opIdx = 0; opIdx < [compOps count]; opIdx++) {
        if ([scanner scanString:compOps[opIdx] intoString:NULL]) {
            return compOpNames[opIdx];
        }
    }
    
    return invalidCompOp;
}

+(identifier*) scanIdentifier : (NSScanner*) scanner{
    NSMutableCharacterSet *idCharSet = [NSMutableCharacterSet alphanumericCharacterSet];
    //[idCharSet removeCharactersInString:@"$<>=!"]; not in above charset
    [idCharSet addCharactersInString:@"."];
    
    identifier *ident;
    [scanner scanString:@"$" intoString:NULL] &&
    [scanner scanCharactersFromSet:idCharSet intoString:&ident];
    
    return ident; // if identifier was not scanned it will return nil;
}

+(id) scanLiteral : (NSScanner*) scanner{
    NSNumber *numLiteral;
    NSDecimal decimalLiteral;
    if ([scanner scanDecimal:&decimalLiteral]) {
        numLiteral = [NSDecimalNumber decimalNumberWithDecimal:decimalLiteral];
        return numLiteral;
    }
    // FIXME check to try and reinterpret as int
    NSInteger integerLiteral;
    if ([scanner scanInteger:&integerLiteral]) {
        numLiteral = [NSNumber numberWithInteger:integerLiteral];
        return numLiteral;
    }
    
    return nil; // returns a number or a nil
}

@end
