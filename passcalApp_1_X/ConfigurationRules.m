//
//  ConfigurationRules.m
//  passcalApp_1_X
//
//  Created by field on 9/14/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "ConfigurationRules.h"
#import "xmlDictionary.h"
#import "RulesParser.h"
#import "SimpleParser.h"
#import "rule.h"

#import "CocoaLumberjack.h"
static const DDLogLevel ddLogLevel = DDLogLevelInfo;

@implementation ConfigurationRules
+ (id)sharedManager {
    static ConfigurationRules *sharedConfigurationRules = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{ // This inforces singleton
        sharedConfigurationRules = [[self alloc] init];
    });
    return sharedConfigurationRules;
}

- (id)init {
    if (self = [super init]) {
        // Do initialization
        [self loadXmlRulesFile];
    }
    return self;
}

- (BOOL) loadXmlRulesFile {
    NSString *configFilename = @"configRules.xml";
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *configFilePath = [documentsDirectory stringByAppendingPathComponent:configFilename];
    
    // Allow empty keys
    XMLDictionaryParser *parser = [XMLDictionaryParser sharedInstance];
    parser.stripEmptyNodes = false;
    
    NSDictionary *allDict = [NSDictionary dictionaryWithXMLFile:configFilePath];
    if (!allDict) {
        DDLogError(@"Error loading config file: %@\n", configFilePath);
        // FIXME Add some defaults
        return false;
    }
    
    NSDictionary *keyConfig = allDict[@"KeyConfiguation"];
    if (!keyConfig) {
        DDLogError(@"Error reading KeyConfiguation\n");
        // FIXME Add some defaults
        return false;
    }
    self.keyRules = [RulesParser parseRules:keyConfig];
    self.displayKeyName = [SimpleParser ParseStringValues:keyConfig :@"displayKey"];
    self.valueDisplayFormat = [SimpleParser ParseStringValues:keyConfig :@"valueDisplayFormat"];
    self.fieldEditable = [SimpleParser ParseEmptyValues:keyConfig :@"editable"];
    self.excludeFromMaster = [SimpleParser ParseEmptyValues:keyConfig :@"excludeFromMaster"];
    
    NSDictionary *reviewKeyLists = allDict[@"ReviewKeyLists"];
    if (!reviewKeyLists) {
        DDLogError(@"Error reading ReviewKeyLists\n");
        // FIXME Add some defaults
        return false;
    }
    self.stationFieldsKeys  = [self parseReviewKeyList:reviewKeyLists[@"stationFields"]];
    self.statusFieldsKeys   = [self parseReviewKeyList:reviewKeyLists[@"statusFields"]];
    self.gpsFieldsKeys      = [self parseReviewKeyList:reviewKeyLists[@"gpsFields"]];
    self.sensorFieldsKeys   = [self parseReviewKeyList:reviewKeyLists[@"sensorFields"]];
    self.versionFieldsKeys  = [self parseReviewKeyList:reviewKeyLists[@"versionFields"]];
    self.userFieldsKeys     = [self parseReviewKeyList:reviewKeyLists[@"userFields"]];
    
    return true;
}

// TODO change from BOOL to enum if it is not just true or false

-(BOOL) evalRulesForKey : (NSString *) key : (NSDictionary *) deepEntryDict {
    bool evalState = true;
    NSArray *rules = [self.keyRules objectForKey:key];
    if (rules) {
        for (id<rule> r in rules) { // Must follow all rules
            evalState = evalState && [r evalRule:key :deepEntryDict];
        }
        
    }
    // if no rules return true
    return evalState;
}

-(NSArray*) parseReviewKeyList: (NSString*) listComponent {
    if (![listComponent isKindOfClass:[NSString class]])
        return nil;
    NSArray *rawKeys = [listComponent componentsSeparatedByString:@"\n"];
    NSMutableArray *trimmedKeys = [[NSMutableArray alloc] init];
    
    for (NSString* rawString in rawKeys) {
        [trimmedKeys addObject:[rawString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
    }
    
    return [NSArray arrayWithArray:trimmedKeys];
}

@end
