//
//  saveFullDataStore.h
//  passcalApp_1_X
//
//  Created by field on 9/1/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface importDataStore : UITableViewController

@property NSArray *importUrls;
@property NSInteger selectionIdx;

//@property (strong, nonatomic) IBOutlet UITextField *outFilenameTextField;

- (IBAction)import:(id)sender;
- (IBAction)refreshDirectoryContents:(id)sender;

@end
