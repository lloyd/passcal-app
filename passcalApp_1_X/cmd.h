//
//  cmd.h
//
// Base class for commands.  Oringally designed for RF130 messages but
// can be expanded for other message schemes.

#import <Foundation/Foundation.h>

extern Byte const cmdAttnByte;
extern int const cmdSuffix;
extern NSString* const allUnitId;

@interface cmd : NSObject
{
    NSData *data; // protected member
}

@property NSString* id;

// getData
// Makes the inner data read only except for childern of this base class
- (NSData *) getData;

@end
