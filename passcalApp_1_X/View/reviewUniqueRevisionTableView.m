//
//  reviewUniqueRevisionTableView.m
//  passcalApp_1_X
//
//  Created by field on 9/1/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "reviewUniqueRevisionTableView.h"
#import "treeRevisionHistoryTableView.h"
#import "Entry.h"
#import "Rf130StationFields.h"

#define kCellIdentifier @"entryCell"

@interface reviewUniqueRevisionTableView ()

@property NSMutableArray *uniqEntries;

@end

@implementation reviewUniqueRevisionTableView

- (void)viewDidLoad {
    [super viewDidLoad];
    self.uniqEntries = [[NSMutableArray alloc] init];
    if (self.sortKey == nil)
        self.sortKey = @"entryTime";
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kCellIdentifier];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSString *predStr;
    NSPredicate *predicate;
    Entry *uniqEntry;
    NSString *updateTime = @"updateTime";
    NSSortDescriptor *sort1 = [[NSSortDescriptor alloc] initWithKey:updateTime ascending:NO]; // Newest entry first
    
    NSArray *allEntries = [Entry fetchAllWithError:nil];
    NSArray *uniqueTreeIDs = [allEntries valueForKeyPath:@"@distinctUnionOfObjects.treeID"];
    for (NSString *treeID in uniqueTreeIDs) {
        predStr = [[NSString alloc] initWithFormat:@"treeID = \"%@\"", treeID];
        predicate = [NSPredicate predicateWithFormat:predStr];
        uniqEntry = [Entry getWithPredicate:predicate sortDescriptor:sort1 error:nil];
        [self.uniqEntries addObject:uniqEntry];
    }
    // FIXME resort based off key
    
    [self.tableView reloadData]; // FIXME in main thread
}

-(void) reloadTableData {
    [self.tableView reloadData]; // FIXME in main thread
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {  
    return [self.uniqEntries count];
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger rowIdx = [indexPath row];
    Entry *entry = self.uniqEntries[rowIdx];
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    
    if ([self.sortKey isEqualToString:@"entryTime"])
        cell.textLabel.text = [NSString stringWithFormat:@"%@ => %04X", entry.entryTime,
                                    [entry.rf130Station.dasUnitId intValue]];
    else
    {
        // Using direct value coding rather than serialized dictionary
        NSString *firstTerm = [NSString stringWithFormat:@"%@", [entry valueForKeyPath:self.sortKey]];
        NSString *firstTermTrim = [firstTerm stringByTrimmingCharactersInSet:
                                   [NSCharacterSet whitespaceCharacterSet]];
        cell.textLabel.text = [NSString stringWithFormat:@"%@ => %@", firstTermTrim, entry.entryTime];
    }
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger rowIdx = [indexPath row];
    Entry *entry = self.uniqEntries[rowIdx];
    self.selectedEntry = entry;
    
    UIViewController *parentView = self.parentViewController;
    [parentView performSegueWithIdentifier:@"showHistorySegue" sender: self];
}

@end