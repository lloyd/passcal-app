//
//  userSerialNumViewController.m
//  passcalApp_1_X
//
//  Created by field on 7/20/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "userPowerNotesViewController.h"

#import "RHManagedObjectContextManager.h"
#import "Entry.h"
#import "Rf130StationFields.h"
#import "UserFields.h"
#import "CurrentEntries.h"

@interface userPowerNotesViewController ()

@property Entry *entry;

// Only Need local variables for non string attributes
@property float batteryVoltageLocal;
@property float powerBoxVsLocal;
@property float powerBoxApvLocal;
@property float powerBoxVpnLocal;
@property float solarPanelVoltageLocal;
@property float solarOrientationLocal;
@property float powerBoxVpLocal;

- (void) setStationFromUri: (NSURL*) url;

@end

@implementation userPowerNotesViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    if (self.stationURI == nil)
    {
        CurrentEntries *currentEntry = [CurrentEntries getInstance];
        self.stationURI = currentEntry.collectUrl;
    }
    
    if (self.stationURI != nil) // Only should happen when application is first installed
    {
        [self setStationFromUri: self.stationURI];
        NSString *str = [NSString stringWithFormat:@"%@",
                         self.entry.entryTime];
        [self setTitle:str];
        
        //self.stationDict = [self.entry getDict];
    }
    [self addTextViews];
    [self addRefLabels];

    [self reloadValuesFromStore];
}

-(void) reloadValuesFromStore
{
    if (self.entry == nil)
        return;
    
    [self addRefLabels]; // Just do this for good measure
    
    // Reload textViews
    self.batteryVoltageTextInput.text = [[NSString alloc] initWithFormat:@"%0.2f",
                                         self.entry.userFields.batteryVoltageValue];
    self.powerBoxVsTextInput.text = [[NSString alloc] initWithFormat:@"%0.2f",
                                     self.entry.userFields.powerBoxVsValue];
    self.powerBoxApvTextInput.text = [[NSString alloc] initWithFormat:@"%0.2f",
                                      self.entry.userFields.powerBoxApvValue];
    self.powerBoxVpnTextInput.text = [[NSString alloc] initWithFormat:@"%0.2f",
                                      self.entry.userFields.powerBoxVpnValue];
    self.solarPanelVoltageTextInput.text = [[NSString alloc] initWithFormat:@"%0.2f",
                                            self.entry.userFields.solarPanelVoltageValue];
    self.solarPanelSizeTextInput.text = self.entry.userFields.solarPanelSize;
    self.solarOrientationTextInput.text = [[NSString alloc] initWithFormat:@"%0.2f",
                                           self.entry.userFields.solarPanelOrientationValue];
    self.powerBoxVpTextInput.text = [[NSString alloc] initWithFormat:@"%0.2f",self.entry.userFields.powerBoxVpValue];
    
    // Reload Locals
    self.batteryVoltageLocal     = [self.batteryVoltageTextInput.text floatValue];
    self.powerBoxVsLocal         = [self.powerBoxVsTextInput.text floatValue];
    self.powerBoxApvLocal        = [self.powerBoxApvTextInput.text floatValue];
    self.powerBoxVpnLocal        = [self.powerBoxVpnTextInput.text floatValue];
    self.solarPanelVoltageLocal  = [self.solarPanelVoltageTextInput.text floatValue];
    self.solarOrientationLocal   = [self.solarOrientationTextInput.text floatValue];
    self.powerBoxVpLocal         = [self.powerBoxVpTextInput.text floatValue];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setStationFromUri: (NSURL*) url {
    self.entry = [Entry existingEntityWithUrl:url error:nil];
}

- (void) addTextViews {
    self.batteryVoltageTextInput.delegate = self;
    self.powerBoxVsTextInput.delegate = self;
    self.powerBoxApvTextInput.delegate = self;
    self.powerBoxVpnTextInput.delegate = self;
    self.solarPanelVoltageTextInput.delegate = self;
    self.solarPanelSizeTextInput.delegate = self;
    self.solarOrientationTextInput.delegate = self;
    self.powerBoxVpTextInput.delegate = self;
}

- (void) addRefLabels {
    self.entryTimeRef.text   = [[NSString alloc] initWithFormat:@"%@", self.entry.entryTime];
    self.dasUnitRef.text     = [[NSString alloc] initWithFormat:@"%04X", self.entry.rf130Station.dasUnitIdValue];
    self.expNameRef.text     = self.entry.rf130Station.experimentName;
    self.stationNameRef.text = self.entry.rf130Station.stationName;
}

- (IBAction) cancelButton:(id)sender {
    [self reloadValuesFromStore];
}

- (IBAction) commitButton:(id)sender {
    Entry *newEntry = [self.entry createNewRevision];
    UserFields *newUserFields = [self.entry.userFields createNewRevision];
    newEntry.userFields = newUserFields;
    self.entry = newEntry;
    
    self.entry.userFields.solarPanelSize = self.solarPanelSizeTextInput.text;

    // Use locals
    [self convert2Local]; // Do this incase the keyboard has not been resigned
    self.entry.userFields.batteryVoltageValue = self.batteryVoltageLocal;
    self.entry.userFields.powerBoxVsValue = self.powerBoxVsLocal;
    self.entry.userFields.powerBoxApvValue = self.powerBoxApvLocal;
    self.entry.userFields.powerBoxVpnValue = self.powerBoxVpnLocal;
    self.entry.userFields.solarPanelVoltageValue = self.solarPanelVoltageLocal;
    self.entry.userFields.solarPanelOrientationValue = self.solarOrientationLocal;
    self.entry.userFields.powerBoxVpValue = self.powerBoxVpLocal;
            
    [UserFields commit];
    [Entry commit];
    
    [CurrentEntries updateCommitEntry:self.entry];
    [CurrentEntries updateReviewEntryIfMatchingTreeID:self.entry];
    
    [self.masterDelegate updateMasterStatus];
    
    NSString *commitStr = [[NSString alloc]
                          initWithFormat:@"Entry Commited. (Rev %d)", self.entry.revisionNumValue];
    UIAlertView *messageAlert = [[UIAlertView alloc] initWithTitle:@"Committed"
                                                message:commitStr delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil, nil];
    [messageAlert show];
}

-(void) convert2Local {
    self.batteryVoltageLocal     = [self.batteryVoltageTextInput.text floatValue];
    self.powerBoxVsLocal         = [self.powerBoxVsTextInput.text floatValue];
    self.powerBoxApvLocal        = [self.powerBoxApvTextInput.text floatValue];
    self.powerBoxVpnLocal        = [self.powerBoxVpnTextInput.text floatValue];
    self.solarPanelVoltageLocal  = [self.solarPanelVoltageTextInput.text floatValue];
    self.solarOrientationLocal   = [self.solarOrientationTextInput.text floatValue];
    self.powerBoxVpLocal         = [self.powerBoxVpTextInput.text floatValue];

    // Apply locals to TextFields to provide user feedback
    self.batteryVoltageTextInput.text = [[NSString alloc] initWithFormat:@"%0.2f",
                                         self.batteryVoltageLocal];
    self.powerBoxVsTextInput.text = [[NSString alloc] initWithFormat:@"%0.2f",
                                     self.powerBoxVsLocal];
    self.powerBoxApvTextInput.text = [[NSString alloc] initWithFormat:@"%0.2f",
                                      self.powerBoxApvLocal];
    self.powerBoxVpnTextInput.text = [[NSString alloc] initWithFormat:@"%0.2f",
                                      self.powerBoxVpnLocal];
    self.solarPanelVoltageTextInput.text = [[NSString alloc] initWithFormat:@"%0.2f",
                                            self.solarPanelVoltageLocal];
    self.solarOrientationTextInput.text = [[NSString alloc] initWithFormat:@"%0.2f",
                                           self.solarOrientationLocal];
    self.powerBoxVpTextInput.text = [[NSString alloc] initWithFormat:@"%0.2f",self.powerBoxVpLocal];
}

- (IBAction)viewTapped:(id)sender {
    [self.batteryVoltageTextInput resignFirstResponder];
    [self.powerBoxVsTextInput resignFirstResponder];
    [self.powerBoxApvTextInput resignFirstResponder];
    [self.powerBoxVpnTextInput resignFirstResponder];
    [self.solarPanelVoltageTextInput resignFirstResponder];
    [self.solarPanelSizeTextInput resignFirstResponder];
    [self.solarOrientationTextInput resignFirstResponder];
    [self.powerBoxVpTextInput resignFirstResponder];
    
    [self convert2Local];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    [self convert2Local];
    
    return YES;
}

@end
