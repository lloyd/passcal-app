//
//  editEntryCellViewController.m
//  passcalApp_1_X
//
//  Created by field on 7/21/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "editEntryCellViewController.h"

@interface editEntryCellViewController ()

{
    NSString *editValue;
}

-(void) addRefLabels;

@end

@implementation editEntryCellViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.editedValue = nil;
    self.editTextField.delegate = self;
    [self addRefLabels];
}

-(void) addRefLabels {
    self.keyLabel.text         = self.key;
    self.origValueLabel.text   = [[NSString alloc] initWithFormat:@"Current Value: \"%@\"", self.origValue];
    self.editTextField.text    = [[NSString alloc] initWithFormat:@"%@", self.origValue];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
/*
    if (textField.text.length > 0)
    {
        editValue = textField.text;
    }
    */
    return YES;
}

-(IBAction)viewTapped:(id)sender {
    [self.editTextField resignFirstResponder];
    /*
    if (self.editTextField.text.length > 0)
    {
        editValue = self.editTextField.text;
    }
     */
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if (sender != self.saveButton)
        return;
    
    if (self.editTextField.text.length > 0)
    {
        editValue = self.editTextField.text;
    }
    

    if([self.origValue isKindOfClass:[NSNumber class]])
    {
        NSNumber *numValue = (NSNumber *)self.origValue;
        const char *ctype = [numValue objCType];
        
        if (ctype[0] == 'f') // float value
            self.editedValue = [NSNumber numberWithFloat:[editValue floatValue]];
        else
            self.editedValue = [NSNumber numberWithInteger:[editValue integerValue]];
    }
    else if ([self.origValue isKindOfClass:[NSDate class]])
    {
        UIAlertView *messageAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Editing dates not supported"
                                                              delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [messageAlert show];
    }
    else // For everything else assume text
    {
        self.editedValue = editValue;
    }
    
}


@end
