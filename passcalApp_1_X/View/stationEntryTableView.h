//
//  stationEntryTableView.h
//
// This class is segued into from mainmenu.  During segue typically both stationURI
// and keyArray are set from calling class.  This allows this class to pull information
// from persistant store and then only display certain keys from the retrieved object.

// Note this file also implements a helper class which predefines some sets of keys to be
// displayed.

#import <UIKit/UIKit.h>
#import "mainMenuTableViewController.h"

@interface stationEntryTableView : UITableViewController

@property id<masterStatusController> masterDelegate;
@property NSURL *stationURI;
@property NSArray *keyArray;

- (IBAction)unwindToStationEntryTableView:(UIStoryboardSegue *)segue;

@end