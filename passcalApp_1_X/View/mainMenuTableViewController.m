//
//  mainMenuTableViewController.m
//  passcalApp_1_X
//
//  Created by matt on 7/8/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "mainMenuTableViewController.h"
#import "ConfigurationRules.h"
#import "CurrentEntries.h"

#import "stationEntryTableView.h"
#import "reviewEntriesViewController.h"
#import "userSerialNumViewController.h"
#import "userPowerNotesViewController.h"
#import "userStationNotesViewController.h"

@interface mainMenuTableViewController ()

@end

@implementation mainMenuTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self updateMasterStatus];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) updateMasterStatus {
    ConfigurationRules *configRules = [ConfigurationRules sharedManager];
    CurrentEntries *currentEntry = [CurrentEntries getInstance];
    NSURL *entryURI;
    BOOL followsRules;
    
    if (currentEntry == nil)
        return;
    
    entryURI = currentEntry.reviewUrl;
    
    if (entryURI == nil) // Only should happen when application is first installed
        return;
        
    Entry *entry = [Entry existingEntityWithUrl:entryURI error:nil];
    NSDictionary *entryDict = [[NSDictionary alloc] initWithDictionary:[entry deepSerialize:true]];
    
    // stationFieldsKeys
    followsRules = true;
    for (NSString *key in configRules.stationFieldsKeys) {
        if (![configRules.excludeFromMaster containsObject:key])
            followsRules = followsRules && [configRules evalRulesForKey:key : entryDict];
    }
    if (!followsRules)
        self.stationFieldsCell.backgroundColor = [UIColor redColor];
    else
        self.stationFieldsCell.backgroundColor = [UIColor whiteColor];

    // statusFieldsKeys
    followsRules = true;
    for (NSString *key in configRules.statusFieldsKeys) {
        if (![configRules.excludeFromMaster containsObject:key])
            followsRules = followsRules && [configRules evalRulesForKey:key : entryDict];
    }
    if (!followsRules)
        self.statusFieldsCell.backgroundColor = [UIColor redColor];
    else
        self.statusFieldsCell.backgroundColor = [UIColor whiteColor];
    
    // gpsFieldsKeys
    followsRules = true;
    for (NSString *key in configRules.gpsFieldsKeys) {
        if (![configRules.excludeFromMaster containsObject:key])
            followsRules = followsRules && [configRules evalRulesForKey:key : entryDict];
    }
    if (!followsRules)
        self.gpsFieldsCell.backgroundColor = [UIColor redColor];
    else
        self.gpsFieldsCell.backgroundColor = [UIColor whiteColor];
    
    // sensorFieldsKeys
    followsRules = true;
    for (NSString *key in configRules.sensorFieldsKeys) {
        if (![configRules.excludeFromMaster containsObject:key])
            followsRules = followsRules && [configRules evalRulesForKey:key : entryDict];
    }
    if (!followsRules)
        self.sensorFieldsCell.backgroundColor = [UIColor redColor];
    else
        self.sensorFieldsCell.backgroundColor = [UIColor whiteColor];
    
    // versionFieldsKeys
    followsRules = true;
    for (NSString *key in configRules.versionFieldsKeys) {
        if (![configRules.excludeFromMaster containsObject:key])
            followsRules = followsRules && [configRules evalRulesForKey:key : entryDict];
    }
    if (!followsRules)
        self.versionFieldsCell.backgroundColor = [UIColor redColor];
    else
        self.versionFieldsCell.backgroundColor = [UIColor whiteColor];
    
    // userFieldsKeys
    followsRules = true;
    for (NSString *key in configRules.userFieldsKeys) {
        if (![configRules.excludeFromMaster containsObject:key])
            followsRules = followsRules && [configRules evalRulesForKey:key : entryDict];
    }
    if (!followsRules)
        self.userFieldsCell.backgroundColor = [UIColor redColor];
    else
        self.userFieldsCell.backgroundColor = [UIColor whiteColor];
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    ConfigurationRules *configRules = [ConfigurationRules sharedManager];
    stationEntryTableView *targetEntry;
    
    if ([[segue identifier] isEqualToString: @"stationFieldsSegue"])
    {
        UIViewController *destCtrl = [segue destinationViewController];
        if ([destCtrl isKindOfClass:[UINavigationController class]])
        {
            UINavigationController *navCtrl = (UINavigationController*) destCtrl;
            targetEntry = (stationEntryTableView*)[navCtrl topViewController];
        }
        else // Handle if iOS 6 since no nav controller
        {
            targetEntry = (stationEntryTableView*)destCtrl;
        }
        targetEntry.stationURI = nil; // Have the view lookup the URI from currentEntry
        targetEntry.keyArray = configRules.stationFieldsKeys;
        targetEntry.masterDelegate = self;
    }
    if ([[segue identifier] isEqualToString: @"statusFieldsSegue"])
    {
        UIViewController *destCtrl = [segue destinationViewController];
        if ([destCtrl isKindOfClass:[UINavigationController class]])
        {
            UINavigationController *navCtrl = (UINavigationController*) destCtrl;
            targetEntry = (stationEntryTableView*)[navCtrl topViewController];
        }
        else // Handle if iOS 6 since no nav controller
        {
            targetEntry = (stationEntryTableView*)destCtrl;
        }
        targetEntry.stationURI = nil; // Have the view lookup the URI from currentEntry
        targetEntry.keyArray = configRules.statusFieldsKeys;
        targetEntry.masterDelegate = self;
    }
    if ([[segue identifier] isEqualToString: @"gpsFieldsSegue"])
    {
        UIViewController *destCtrl = [segue destinationViewController];
        if ([destCtrl isKindOfClass:[UINavigationController class]])
        {
            UINavigationController *navCtrl = (UINavigationController*) destCtrl;
            targetEntry = (stationEntryTableView*)[navCtrl topViewController];
        }
        else // Handle if iOS 6 since no nav controller
        {
            targetEntry = (stationEntryTableView*)destCtrl;
        }
        targetEntry.stationURI = nil; // Have the view lookup the URI from currentEntry
        targetEntry.keyArray = configRules.gpsFieldsKeys;
        targetEntry.masterDelegate = self;
    }
    if ([[segue identifier] isEqualToString: @"sensorFieldsSegue"])
    {
        UIViewController *destCtrl = [segue destinationViewController];
        if ([destCtrl isKindOfClass:[UINavigationController class]])
        {
            UINavigationController *navCtrl = (UINavigationController*) destCtrl;
            targetEntry = (stationEntryTableView*)[navCtrl topViewController];
        }
        else // Handle if iOS 6 since no nav controller
        {
            targetEntry = (stationEntryTableView*)destCtrl;
        }
        targetEntry.stationURI = nil; // Have the view lookup the URI from currentEntry
        targetEntry.keyArray = configRules.sensorFieldsKeys;
        targetEntry.masterDelegate = self;
    }
    if ([[segue identifier] isEqualToString: @"versionFieldsSegue"])
    {
        UIViewController *destCtrl = [segue destinationViewController];
        if ([destCtrl isKindOfClass:[UINavigationController class]])
        {
            UINavigationController *navCtrl = (UINavigationController*) destCtrl;
            targetEntry = (stationEntryTableView*)[navCtrl topViewController];
        }
        else // Handle if iOS 6 since no nav controller
        {
            targetEntry = (stationEntryTableView*)destCtrl;
        }
        targetEntry.stationURI = nil; // Have the view lookup the URI from currentEntry
        targetEntry.keyArray = configRules.versionFieldsKeys;
        targetEntry.masterDelegate = self;
    }
    if ([[segue identifier] isEqualToString: @"userFieldsSegue"])
    {
        UIViewController *destCtrl = [segue destinationViewController];
        if ([destCtrl isKindOfClass:[UINavigationController class]])
        {
            UINavigationController *navCtrl = (UINavigationController*) destCtrl;
            targetEntry = (stationEntryTableView*)[navCtrl topViewController];
        }
        else // Handle if iOS 6 since no nav controller
        {
            targetEntry = (stationEntryTableView*)destCtrl;
        }
        targetEntry.stationURI = nil; // Have the view lookup the URI from currentEntry
        targetEntry.keyArray = configRules.userFieldsKeys;
        targetEntry.masterDelegate = self;
    }
    if ([[segue identifier] isEqualToString: @"userSerialNumSegue"])
    {
        userSerialNumViewController *userSerialNumView;
        UIViewController *destCtrl = [segue destinationViewController];
        if ([destCtrl isKindOfClass:[UINavigationController class]])
        {
            UINavigationController *navCtrl = (UINavigationController*) destCtrl;
            userSerialNumView = (userSerialNumViewController*)[navCtrl topViewController];
        }
        else // Handle if iOS 6 since no nav controller
        {
            userSerialNumView = (userSerialNumViewController*)destCtrl;
        }
        userSerialNumView.masterDelegate = self;
    }
    if ([[segue identifier] isEqualToString: @"powerNotesSegue"])
    {
        userPowerNotesViewController *powerNotesView;
        UIViewController *destCtrl = [segue destinationViewController];
        if ([destCtrl isKindOfClass:[UINavigationController class]])
        {
            UINavigationController *navCtrl = (UINavigationController*) destCtrl;
            powerNotesView = (userPowerNotesViewController*)[navCtrl topViewController];
        }
        else // Handle if iOS 6 since no nav controller
        {
            powerNotesView = (userPowerNotesViewController*)destCtrl;
        }
        powerNotesView.masterDelegate = self;
    }
    if ([[segue identifier] isEqualToString: @"stationNotesSegue"])
    {
        userStationNotesViewController *userStationNotes;
        UIViewController *destCtrl = [segue destinationViewController];
        if ([destCtrl isKindOfClass:[UINavigationController class]])
        {
            UINavigationController *navCtrl = (UINavigationController*) destCtrl;
            userStationNotes = (userStationNotesViewController*)[navCtrl topViewController];
        }
        else // Handle if iOS 6 since no nav controller
        {
            userStationNotes = (userStationNotesViewController*)destCtrl;
        }
        userStationNotes.masterDelegate = self;
    }
    if ([[segue identifier] isEqualToString: @"reviewEntriesSegue"])
    {
        reviewEntriesViewController *reviewEntries;
        UIViewController *destCtrl = [segue destinationViewController];
        if ([destCtrl isKindOfClass:[UINavigationController class]])
        {
            UINavigationController *navCtrl = (UINavigationController*) destCtrl;
            reviewEntries = (reviewEntriesViewController*)[navCtrl topViewController];
        }
        else // Handle if iOS 6 since no nav controller
        {
            reviewEntries = (reviewEntriesViewController*)destCtrl;
        }
        reviewEntries.masterDelegate = self;
    }
}

@end
