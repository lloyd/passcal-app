//
//  userStationNotesViewController.h
//
// This class implements the View logic for entering manual values
// into a station entry.

#import <UIKit/UIKit.h>
#import "mainMenuTableViewController.h"
#import "myLocation.h"

@interface userStationNotesViewController : UIViewController <UITextFieldDelegate,UITextViewDelegate>

@property NSURL *stationURI;
@property myLocation *loc;

-(void) reloadValuesFromStore;
- (IBAction) cancelButton:(id)sender;
- (IBAction) commitButton:(id)sender;
- (IBAction) setDepartureTime:(id)sender;
- (IBAction) setGPSLocation:(id)sender;
- (IBAction)viewTapped:(id)sender;

// Entry reference labels
@property (weak, nonatomic) IBOutlet UILabel *entryTimeRef;
@property (weak, nonatomic) IBOutlet UILabel *dasUnitRef;
@property (weak, nonatomic) IBOutlet UILabel *expNameRef;
@property (weak, nonatomic) IBOutlet UILabel *stationNameRef;

// Text fields
@property (weak, nonatomic) IBOutlet UITextView  *weatherNotesTextInput;
@property (weak, nonatomic) IBOutlet UITextView *userNotesTextInput;
@property (weak, nonatomic) IBOutlet UITextField *userStationNameTextInput;
@property (weak, nonatomic) IBOutlet UITextField *userExperimentNameTextInput;
@property (weak, nonatomic) IBOutlet UITextField *siteContactTextInput;
@property (weak, nonatomic) IBOutlet UITextField *fieldTeamTextInput;
@property (weak, nonatomic) IBOutlet UITextField *gpsLatTextInput;
@property (weak, nonatomic) IBOutlet UITextField *gpsLonTextInput;
@property (weak, nonatomic) IBOutlet UITextField *gpsElevationTextInput;
@property (weak, nonatomic) IBOutlet UITextField *magDeclinationTextInput;
@property (weak, nonatomic) IBOutlet UITextField *sensorOrientationTextInput;
@property (weak, nonatomic) IBOutlet UITextField *departureTimeTextInput;

@property id<masterStatusController> masterDelegate;
@end
