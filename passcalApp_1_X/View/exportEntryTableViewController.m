//
//  reviewEntriesTableViewController.m
//  passcalApp_1_X
//
//  Created by matt on 7/6/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "exportEntryTableViewController.h"
#import "stationEntryTableView.h"
#import "Rf130StationFields.h"

#import "CocoaLumberjack.h"
static const DDLogLevel ddLogLevel = DDLogLevelInfo;

#define kCellIdentifier @"entryCell"

@interface exportEntryTableViewController ()

@end

@implementation exportEntryTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setTitle:@"Export Entry"];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kCellIdentifier];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSFetchedResultsController *)fetchedResultsController {
    if (fetchedResultsController == nil) {
        
        NSSortDescriptor *sort1 = [[NSSortDescriptor alloc] initWithKey:@"entryTime" ascending:NO];
        
        NSPredicate *predicate;
        
        predicate = [NSPredicate predicateWithFormat:@"isHidden == NO"];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        [fetchRequest setEntity:[Entry entityDescriptionWithError:nil]];
        
        [fetchRequest setPredicate:predicate];
        [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sort1, nil]];
        
        self.fetchedResultsController = [[NSFetchedResultsController alloc]
                                         initWithFetchRequest:fetchRequest
                                         managedObjectContext:
                                         [Entry managedObjectContextForCurrentThreadWithError:nil]
                                         sectionNameKeyPath:nil
                                         cacheName:nil];
        
        fetchedResultsController.delegate = self;
        
        NSError *error = nil;
        if (![fetchedResultsController performFetch:&error]) {
            DDLogError(@"Unresolved error: %@", [error localizedDescription]);
        }
    }
    
    return fetchedResultsController;
}

-(void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    Entry *entry = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %04X", entry.entryTime, [entry.rf130Station.dasUnitId intValue]];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // We must dequeue from self.tableView and not tableView since this method is also used by the search controller
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Entry *entry = [self.fetchedResultsController objectAtIndexPath:indexPath];
    self.selectedEntry = entry;
    [self performSegueWithIdentifier:@"saveExportSegue" sender: self];
}
/*
 
 -(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 [[self.fetchedResultsController objectAtIndexPath:indexPath] delete];
 [rf130Station commit];
 }
 }
 */

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([[segue identifier] isEqualToString: @"saveExportSegue"])
    {
        stationEntryTableView *targetEntry = [segue destinationViewController];
        targetEntry.stationURI = [[self.selectedEntry objectID] URIRepresentation];
    }
    
    /*
    NSDictionary *dict = [self.selectedStation getDict];
    
    NSString *str = [dict XMLString];
    
    char *saves = "abcd";
    NSData *data = [[NSData alloc] initWithBytes:saves length:4];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:@"MyFile"];
    [str writeToFile:appFile atomically:NO encoding:NSStringEncodingConversionAllowLossy error:nil];
    */
}

@end