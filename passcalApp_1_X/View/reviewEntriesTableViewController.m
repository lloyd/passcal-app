//
//  reviewEntriesTableViewController.m
//  passcalApp_1_X
//
//  Created by matt on 7/6/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "reviewEntriesTableViewController.h"
#import "stationEntryTableView.h"
#import "currentEntries.h"
#import "Rf130StationFields.h"

#import "CocoaLumberjack.h"
static const DDLogLevel ddLogLevel = DDLogLevelInfo;

#define kCellIdentifier @"entryCell"

@interface reviewEntriesTableViewController ()

@end

@implementation reviewEntriesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.sortKey == nil)
        self.sortKey = @"entryTime";
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kCellIdentifier];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSFetchedResultsController *)fetchedResultsController {
    if (fetchedResultsController == nil) {
        
        NSSortDescriptor *sort1;
        if ([self.sortKey isEqualToString:@"entryTime"])
             sort1 = [[NSSortDescriptor alloc] initWithKey:self.sortKey ascending:NO];
        else
             sort1 = [[NSSortDescriptor alloc] initWithKey:self.sortKey ascending:YES];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isHidden == NO"];
                NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSError *tmpError = [[NSError alloc] init];
        [fetchRequest setEntity:[Entry entityDescriptionWithError:&tmpError]];
                [fetchRequest setPredicate:predicate];
        [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sort1, nil]];
        
        self.fetchedResultsController = [[NSFetchedResultsController alloc]
                                         initWithFetchRequest:fetchRequest
                                         managedObjectContext:
                                         [Entry managedObjectContextForCurrentThreadWithError:nil]
                                          sectionNameKeyPath:nil
                                          cacheName:nil];
        
        fetchedResultsController.delegate = self;
        
        NSError *error = nil;
        if (![fetchedResultsController performFetch:&error]) {
            DDLogError(@"Unresolved error: %@", [error localizedDescription]);
        }
    }
    
    return fetchedResultsController;
}

-(void) resetFetchedResultsController {
    self.fetchedResultsController = nil;
}

-(void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    Entry *entry = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    if ([self.sortKey isEqualToString:@"entryTime"])
        cell.textLabel.text = [NSString stringWithFormat:@"%@ => %04X", entry.entryTime, [entry.rf130Station.dasUnitId intValue]];
    else
    {
        // Using direct value coding rather than serialized dictionary
        NSString *firstTerm = [NSString stringWithFormat:@"%@", [entry valueForKeyPath:self.sortKey]];
        NSString *firstTermTrim = [firstTerm stringByTrimmingCharactersInSet:
                                    [NSCharacterSet whitespaceCharacterSet]];
        cell.textLabel.text = [NSString stringWithFormat:@"%@ => %@", firstTermTrim, entry.entryTime];
    }
    
    // Apply checkmark if the review object
    CurrentEntries *currentEntry = [CurrentEntries getInstance];
    if (currentEntry == nil) // Nothing ever slected
        return;
    if ([currentEntry.reviewUrl isEqual:[[entry objectID] URIRepresentation]])
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    else
        cell.accessoryType = UITableViewCellAccessoryNone;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // We must dequeue from self.tableView and not tableView since this method is also used by the search controller
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Entry *entry = [self.fetchedResultsController objectAtIndexPath:indexPath];
    self.selectedEntry = entry;
    
    // There should only be a single currentEntry
    CurrentEntries *currentEntry = [CurrentEntries getInstance];
    currentEntry.reviewUrl = [[self.selectedEntry objectID] URIRepresentation];
    [CurrentEntries commit];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
        [self.masterDelegate updateMasterStatus];
    });
}
/*

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [[self.fetchedResultsController objectAtIndexPath:indexPath] delete];
        [rf130Station commit];
    }
}
 */

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([[segue identifier] isEqualToString: @"selectEntrySegue"])
    {
        stationEntryTableView *targetEntry = [segue destinationViewController];
        targetEntry.stationURI = [[self.selectedEntry objectID] URIRepresentation];
    }
    
}

@end
