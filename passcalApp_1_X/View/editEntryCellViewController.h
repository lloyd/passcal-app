//
//  editEntryCellViewController.h
//
// This class implments the simple view of editing a single value at a time.
// The class is queried for the edited value when the return segue is called.
// Also during entry segue both key and origValue are set.

#import <UIKit/UIKit.h>

@interface editEntryCellViewController : UIViewController <UITextFieldDelegate>

@property NSString *key;
@property NSObject *origValue;
@property NSObject *editedValue;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;
@property (weak, nonatomic) IBOutlet UITextField *editTextField;
@property (weak, nonatomic) IBOutlet UILabel *origValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *keyLabel;


@end
