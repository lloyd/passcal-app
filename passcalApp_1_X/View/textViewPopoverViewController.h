//
//  textViewPopoverViewController.h
//  passcalApp_1_X
//
//  Created by field on 9/6/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface textViewPopoverViewController : UIViewController <UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITextView *mainTextView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneBarButton;

@property NSString *origValue;
@property NSString *editedValue;
@property UITextView *returnReference;

@end
