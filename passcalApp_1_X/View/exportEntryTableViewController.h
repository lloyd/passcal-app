//
//  exportEntryTableViewController.h
//
// This class is very similar to "reviewEntriesTableViewController" but allows
// for a similar view and does a push segue when the user selects a station.

#import "RHCoreDataTableViewController.h"
#import "Entry.h"

@interface exportEntryTableViewController : RHCoreDataTableViewController

@property Entry *selectedEntry;

@end
