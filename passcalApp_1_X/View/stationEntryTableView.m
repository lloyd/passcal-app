//
//  stationEntryTableView.m
//  passcalApp_1_X
//
//  Created by matt on 7/9/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "stationEntryTableView.h"
#import "editEntryCellViewController.h"
#import "RHManagedObjectContextManager.h"

#import "CurrentEntries.h"
#import "Entry.h"
#import "RevControlHelper.h"

#import "ConfigurationRules.h"

#define kCellIdentifier @"entryKeyValue"

@interface stationEntryTableView ()

@property Entry *entry;
@property NSMutableDictionary *entryDict;
@property NSMutableDictionary *changes;

@property NSString *editKey;

-(void) setStationFromUri: (NSURL*) url;

@end

@implementation stationEntryTableView
- (void)viewDidLoad {
    [super viewDidLoad];
    self.changes = [[NSMutableDictionary alloc] init];
    // Test URL in core data
    if (self.stationURI == nil)
    {
        CurrentEntries *currentEntry = [CurrentEntries getInstance];
        self.stationURI = currentEntry.reviewUrl;
    }
    
    if (self.stationURI == nil) // Only should happen when application is first installed
    {
        self.entryDict = [[NSMutableDictionary alloc]
                          initWithDictionary: @{@"Error" : @"No Entry Selected!"}];
        self.keyArray = [self.entryDict allKeys];
    }
    else
    {
        [self setStationFromUri: self.stationURI];
        
        NSString *str = [NSString stringWithFormat:@"%@",
                         self.entry.entryTime];
        [self setTitle:str];
        
        // self.entryDict = [self.stationEntry getDict];
        self.entryDict = [[NSMutableDictionary alloc]
                          initWithDictionary:[self.entry deepSerialize:true]];
        
        if (self.keyArray == nil)  // If not specified display all keys
        {
            self.keyArray = [self.entryDict allKeys];
        }
    }
    
    // GUI and table
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kCellIdentifier];
}

-(void) setStationFromUri: (NSURL*) url {   
    // TOOD check that an entry was passed
    self.entry = [Entry existingEntityWithUrl:url error:nil];
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.keyArray count];
}

-(void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    ConfigurationRules *configRules = [ConfigurationRules sharedManager];
    NSString *key = [self.keyArray objectAtIndex: indexPath.row];
    
    //NSObject *value = self.entryDict[key];
    
    NSString *displayKey, *valueFormat;
    if (configRules.displayKeyName[key])
        displayKey = configRules.displayKeyName[key];
    else
        displayKey = key;
    if (configRules.valueDisplayFormat[key]) {
        valueFormat = [NSString stringWithFormat: @"%%@ %@", configRules.valueDisplayFormat[key]];
        if ([self.entryDict[key] isKindOfClass:[NSNumber class]]) {
            const char *ctype = [self.entryDict[key] objCType];
            
            if (ctype[0] == 'f') { // float value
                float floatVal = [self.entryDict[key] floatValue];
                cell.textLabel.text = [NSString stringWithFormat:valueFormat, displayKey, floatVal];
            }
            else { // assume integer
                NSInteger intVal = [self.entryDict[key] integerValue];
                cell.textLabel.text = [NSString stringWithFormat:valueFormat, displayKey, intVal];
            }
        }
        else {
            cell.textLabel.text = [NSString stringWithFormat:valueFormat, displayKey,
                               self.entryDict[key]];
        }
    }
    else {
        cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", displayKey,
                               self.entryDict[key]];
    }
    
    // Perform rule checking here
    // If no rule should return true
    BOOL followsRules = [configRules evalRulesForKey:key :self.entryDict];
    if (!followsRules)
        cell.backgroundColor = [UIColor redColor];
    else
        cell.backgroundColor = [UIColor whiteColor];
    
    if ([configRules.fieldEditable containsObject:key]) // editable
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    else // not editab
        cell.accessoryType = UITableViewCellAccessoryNone;
    cell.accessoryView = nil;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // We must dequeue from self.tableView and not tableView since this method is also used by the search controller
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (NSIndexPath *)tableView:(UITableView *)tableView
  willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ConfigurationRules *configRules = [ConfigurationRules sharedManager];
    NSString *key = [self.keyArray objectAtIndex: indexPath.row];
    
    if ([configRules.fieldEditable containsObject:key]) { // editable
        self.editKey = key;
        [self performSegueWithIdentifier:@"editEntryCellSegue" sender: self];
    }
    
    return indexPath;
}


- (IBAction)unwindToStationEntryTableView:(UIStoryboardSegue *)segue {
    editEntryCellViewController *src = [segue sourceViewController];
    
    if (src.editedValue == nil) // No change
        return;
    
    self.entryDict[src.key] = src.editedValue;
    self.changes[src.key] = src.editedValue;
    
    //[self.stationEntry setValue:src.editedValue forKey:src.key];
    //[rf130Station commit];  TODO move commit to button so user can change anything on the page without issue
    
    // Reload the table with update dict
    // self.entryDict = [self.station getDict];
    //self.entryDict = [self.stationEntry deepSerialize:true];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

-(IBAction)commitChanges:(id)sender {
    if (self.changes.count == 0) {// No changes
        return;
    }
    Entry *newEntry = [self.entry createNewRevision:self.changes];
    self.changes = [[NSMutableDictionary alloc] init];
    self.entry = newEntry;
    [RevControlHelper commitAll];
    
    [CurrentEntries updateReviewEntry:self.entry];
    [CurrentEntries commit];
    
    self.entryDict = [[NSMutableDictionary alloc]
                      initWithDictionary:[self.entry deepSerialize:true]];
    
    NSString *commitStr = [[NSString alloc]
                           initWithFormat:@"Entry Commited. (Rev %d)", self.entry.revisionNumValue];
    UIAlertView *messageAlert = [[UIAlertView alloc] initWithTitle:@"Committed"
                                                           message:commitStr delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil, nil];
    [messageAlert show];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
        [self.masterDelegate updateMasterStatus];
    });
}

-(IBAction)cancelChanges:(id)sender {
    self.changes = [[NSMutableDictionary alloc] init];
    self.entryDict = [[NSMutableDictionary alloc]
                      initWithDictionary:[self.entry deepSerialize:true]];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString: @"editEntryCellSegue"])
    {
        editEntryCellViewController *target = (editEntryCellViewController*)[[segue destinationViewController] topViewController];
        target.key = self.editKey;
        target.origValue = self.entryDict[self.editKey];
    }
}

@end
