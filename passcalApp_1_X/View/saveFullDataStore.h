//
//  saveFullDataStore.h
//  passcalApp_1_X
//
//  Created by field on 9/1/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface saveFullDataStore : UIViewController <UITextFieldDelegate>

@property NSString *outFilename;

@property (strong, nonatomic) IBOutlet UITextField *outFilenameTextField;

- (IBAction)saveButton:(id)sender;
- (IBAction)resetFilenameButton:(id)sender;

@end
