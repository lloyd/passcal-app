//
//  reviewEntriesViewController.m
//  passcalApp_1_X
//
//  Created by field on 7/21/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "reviewEntriesViewController.h"
#import "reviewEntriesTableViewController.h"

@interface reviewEntriesViewController ()

@end

@implementation reviewEntriesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    reviewEntriesTableViewController *subView =
    (reviewEntriesTableViewController *)self.childViewControllers.lastObject; // Assume a single child
    subView.masterDelegate = self.masterDelegate;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)changeSort:(id)sender {
    reviewEntriesTableViewController *subView =
        (reviewEntriesTableViewController *)self.childViewControllers.lastObject; // Assume a single child
    
    if (sender == self.entryTimeSortButton)
    {
        subView.sortKey = @"entryTime";
        [subView resetFetchedResultsController]; // Call to update results with new key
        dispatch_async(dispatch_get_main_queue(), ^{
            [subView.tableView reloadData];
        });
    }
    else if (sender == self.expNameSortButton)
    {
        subView.sortKey = @"rf130Station.experimentName";
        [subView resetFetchedResultsController]; // Call to update results with new key
        dispatch_async(dispatch_get_main_queue(), ^{
            [subView.tableView reloadData];
        });
    }
    else if (sender ==  self.stationNameSortButton)
    {
        subView.sortKey = @"rf130Station.stationName";
        [subView resetFetchedResultsController]; // Call to update results with new key
        dispatch_async(dispatch_get_main_queue(), ^{
            [subView.tableView reloadData];
        });
    }
    
    else if (sender ==  self.lonSortButton)
    {
        subView.sortKey = @"rf130Status.gpsLongitude";
        [subView resetFetchedResultsController]; // Call to update results with new key
        dispatch_async(dispatch_get_main_queue(), ^{
            [subView.tableView reloadData];
        });
    }
}

@end
