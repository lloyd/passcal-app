//
//  loggingViewController.m
//  passcalApp_1_X
//
//  Created by field on 9/26/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "loggingViewController.h"
#import "AppDelegate.h"
#import "CocoaLumberjack.h"

NSString* const logFilename = @"masterLog.txt";
const int maxLines = 100;

@implementation loggingViewController

-(void) viewWillAppear:(BOOL)animated {
    [self loadLogFromFile:logFilename];
}

- (IBAction) refresh:(id)sender {
    [self loadLogFromFile:logFilename];
}

-(void) loadLogFromFile: (NSString*) filename {
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    DDFileLogger *fileLogger = appDelegate.fileLogger;
    NSString *logFile = [[fileLogger currentLogFileInfo] filePath];
    
    NSString* content = [NSString stringWithContentsOfFile:logFile
                                                  encoding:NSUTF8StringEncoding
                                                     error:NULL];
    NSArray* lines = [content componentsSeparatedByString: @"\n"];
    NSMutableString *logContent = [[NSMutableString alloc] init];
    for (int lineIdx = 1; lineIdx <= maxLines && lineIdx <= [lines count]; lineIdx++) {
        [logContent appendString:[NSString stringWithFormat:@"%@\n", lines[[lines count]-lineIdx]]];
    }
    
    self.logTextView.text = logContent;
}

@end
