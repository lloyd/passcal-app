//
//  saveFullDataStore.m
//  passcalApp_1_X
//
//  Created by field on 9/1/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "saveFullDataStore.h"
#import "xmlDictionary.h"
#import "currentEntries.h"
#import "utils.h"

#import "Entry.h"
#import "IosFields.h"
#import "Rf130StationFields.h"
#import "Rf130StatusFields.h"
#import "Rf130VersionFields.h"
#import "UserFields.h"

#import "CocoaLumberjack.h"
static const DDLogLevel ddLogLevel = DDLogLevelInfo;

@interface saveFullDataStore ()
{
    NSArray *pickerArray;
    UITextField *myTextField;
    
}
@property Entry *stationEntry;

- (void) setDefaultFilename;
- (void) appendDictionaryToFile:(NSFileHandle *)fileHandle
                         inDict:(NSDictionary *)inDict;

@end

NSString * const beginFullExportTag = @"<fullExport>\n";
NSString * const endFullExportTag =   @"</fullExport>\n";

@implementation saveFullDataStore

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addTextView];
    [self setDefaultFilename];
}

- (void) setDefaultFilename {
    NSString *datePrefix = [utils date2Filename:[[NSDate alloc] init]];
    NSString *defaultFilename = [NSString stringWithFormat:@"%@_fullDataStore.xml", datePrefix];
    self.outFilename = defaultFilename;
    self.outFilenameTextField.text = defaultFilename;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)saveButton:(id)sender {
    UIAlertView *msgBox;
    
    [self exportFullDatabase];
    
    msgBox = [[UIAlertView alloc]
              initWithTitle:@"Saved" message: [NSString stringWithFormat:@"File saved as %@", self.outFilename]
              delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [msgBox show];
}

- (IBAction)resetFilenameButton:(id)sender {
    [self setDefaultFilename];
}

- (void)exportFullDatabase {
    NSError *error;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *exportDirName = @"export";
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *exportDir = [documentsDirectory stringByAppendingPathComponent:exportDirName];
    
    if (![fileManager fileExistsAtPath:exportDir]){
        if (![fileManager createDirectoryAtPath:exportDir
                    withIntermediateDirectories:NO
                                     attributes:nil
                                          error:&error]) {
            DDLogError(@"Create directory error: %@", error);
        }
    }
    
    NSString *outFile = [exportDir stringByAppendingPathComponent:self.outFilename];
    // Clear file and add header
    // TOOD add some schema info or stuff like this
    NSString *header = @"";
    [header writeToFile:outFile atomically:YES encoding:NSStringEncodingConversionAllowLossy error:nil];
    NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:outFile];
    
    //// Start File ////
    [fileHandle writeData:[beginFullExportTag dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSArray *allEntries;
    //// Entry ////
    allEntries = [Entry fetchAllWithError:nil]; // TODO error handling
    for (Entry *anEntry in allEntries) {
        NSMutableDictionary *fieldsD = [NSMutableDictionary dictionaryWithDictionary:[anEntry serialize:true]];
        fieldsD[XMLDictionaryNodeNameKey] = [NSString stringWithFormat:@"%@_%@", [Entry entityName], anEntry.nodeID];
        [self appendDictionaryToFile:fileHandle inDict:fieldsD];
        
        NSMutableDictionary *relationsD = [NSMutableDictionary dictionaryWithDictionary:[RevControlHelper exportRelations:anEntry]];
        relationsD[XMLDictionaryNodeNameKey] = [NSString stringWithFormat:@"%@_relations_%@", [Entry entityName], anEntry.nodeID];
        [self appendDictionaryToFile:fileHandle inDict:relationsD];
    }
    
    /// IosFields ///
    allEntries = [IosFields fetchAllWithError:nil]; // TODO error handling
    for (IosFields *anEntry in allEntries) {
        NSMutableDictionary *fieldsD = [NSMutableDictionary dictionaryWithDictionary:[anEntry serialize:true]];
        fieldsD[XMLDictionaryNodeNameKey] = [NSString stringWithFormat:@"%@_%@", [IosFields entityName], anEntry.nodeID];
        [self appendDictionaryToFile:fileHandle inDict:fieldsD];
        /* For now only export relations for Entry.  When inverse relations are done correctly add others for double check.
         NSMutableDictionary *relationsD = [NSMutableDictionary dictionaryWithDictionary:[RevControlHelper exportRelations:anEntry]];
         relationsD[XMLDictionaryNodeNameKey] = [NSString stringWithFormat:@"%@_relations_%@", [IosFields entityName], anEntry.nodeID];
         [self appendDictionaryToFile:fileHandle inDict:relationsD];
         */
    }
    
    /// Rf130StationFields ///
    allEntries = [Rf130StationFields fetchAllWithError:nil]; // TODO error handling
    for (Rf130StationFields *anEntry in allEntries) {
        NSMutableDictionary *fieldsD = [NSMutableDictionary dictionaryWithDictionary:[anEntry serialize:true]];
        fieldsD[XMLDictionaryNodeNameKey] = [NSString stringWithFormat:@"%@_%@", [Rf130StationFields entityName], anEntry.nodeID];
        [self appendDictionaryToFile:fileHandle inDict:fieldsD];
        /* For now only export relations for Entry.  When inverse relations are done correctly add others for double check.
         NSMutableDictionary *relationsD = [NSMutableDictionary dictionaryWithDictionary:[RevControlHelper exportRelations:anEntry]];
         relationsD[XMLDictionaryNodeNameKey] = [NSString stringWithFormat:@"%@_relations_%@", [Rf130StationFields entityName], anEntry.nodeID];
         [self appendDictionaryToFile:fileHandle inDict:relationsD];
         */
    }
    
    /// Rf130StatusFields ///
    allEntries = [Rf130StatusFields fetchAllWithError:nil]; // TODO error handling
    for (Rf130StatusFields *anEntry in allEntries) {
        NSMutableDictionary *fieldsD = [NSMutableDictionary dictionaryWithDictionary:[anEntry serialize:true]];
        fieldsD[XMLDictionaryNodeNameKey] = [NSString stringWithFormat:@"%@_%@", [Rf130StatusFields entityName], anEntry.nodeID];
        [self appendDictionaryToFile:fileHandle inDict:fieldsD];
        /* For now only export relations for Entry.  When inverse relations are done correctly add others for double check.
         NSMutableDictionary *relationsD = [NSMutableDictionary dictionaryWithDictionary:[RevControlHelper exportRelations:anEntry]];
         relationsD[XMLDictionaryNodeNameKey] = [NSString stringWithFormat:@"%@_relations_%@", [Rf130StatusFields entityName], anEntry.nodeID];
         [self appendDictionaryToFile:fileHandle inDict:relationsD];
         */
    }
    
    /// Rf130VersionFields ///
    allEntries = [Rf130VersionFields fetchAllWithError:nil]; // TODO error handling
    for (Rf130VersionFields *anEntry in allEntries) {
        NSMutableDictionary *fieldsD = [NSMutableDictionary dictionaryWithDictionary:[anEntry serialize:true]];
        fieldsD[XMLDictionaryNodeNameKey] = [NSString stringWithFormat:@"%@_%@", [Rf130VersionFields entityName], anEntry.nodeID];
        [self appendDictionaryToFile:fileHandle inDict:fieldsD];
        /* For now only export relations for Entry.  When inverse relations are done correctly add others for double check.
         NSMutableDictionary *relationsD = [NSMutableDictionary dictionaryWithDictionary:[RevControlHelper exportRelations:anEntry]];
         relationsD[XMLDictionaryNodeNameKey] = [NSString stringWithFormat:@"%@_relations_%@", [Rf130VersionFields entityName], anEntry.nodeID];
         [self appendDictionaryToFile:fileHandle inDict:relationsD];
         */
    }
    
    /// UserFields ///
    allEntries = [UserFields fetchAllWithError:nil]; // TODO error handling
    for (UserFields *anEntry in allEntries) {
        NSMutableDictionary *fieldsD = [NSMutableDictionary dictionaryWithDictionary:[anEntry serialize:true]];
        fieldsD[XMLDictionaryNodeNameKey] = [NSString stringWithFormat:@"%@_%@", [UserFields entityName], anEntry.nodeID];
        [self appendDictionaryToFile:fileHandle inDict:fieldsD];
        /* For now only export relations for Entry.  When inverse relations are done correctly add others for double check.
         NSMutableDictionary *relationsD = [NSMutableDictionary dictionaryWithDictionary:[RevControlHelper exportRelations:anEntry]];
         relationsD[XMLDictionaryNodeNameKey] = [NSString stringWithFormat:@"%@_relations_%@", [UserFields entityName], anEntry.nodeID];
         [self appendDictionaryToFile:fileHandle inDict:relationsD];
         */
    }
    
    //// End File ////
    [fileHandle writeData:[endFullExportTag dataUsingEncoding:NSUTF8StringEncoding]];
    
    [fileHandle closeFile];
}

- (void) appendDictionaryToFile:(NSFileHandle *)fileHandle inDict:(NSDictionary *)inDict {
    if (fileHandle)
    {
        NSString *xmlStr = [[NSString alloc] initWithFormat:@"%@\n", [inDict XMLString]];
        [fileHandle seekToEndOfFile];
        [fileHandle writeData:[xmlStr dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

//// Start textField view ////

-(void) addTextView {
    self.outFilenameTextField.delegate = self;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.outFilenameTextField resignFirstResponder];
    
    self.outFilename = textField.text;
    
    return YES;
}

//// End textField view ////

@end