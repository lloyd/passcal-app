//
//  mainMenuTableViewController.h
//
// This class implements the tableView which is tpyically placed as a master
// element in a master detail application.  All of the data is set in the
// storyboard.  This class contains the preperations needed for many of the
// more complex segues.

#import <UIKit/UIKit.h>

@protocol masterStatusController <NSObject>
-(void) updateMasterStatus;
@end

@interface mainMenuTableViewController : UITableViewController <masterStatusController>

@property (weak, nonatomic) IBOutlet UITableViewCell *stationFieldsCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *statusFieldsCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *gpsFieldsCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *sensorFieldsCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *versionFieldsCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *userFieldsCell;

-(void) updateMasterStatus;

@end
