//
//  reviewUniqueRevisionTableView.h
//  passcalApp_1_X
//
//  Created by field on 9/1/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

//#import "RHCoreDataTableViewController.h"
#import <UIKit/UIKit.h>
#import "Entry.h"

@interface reviewUniqueRevisionTableView : UITableViewController

@property Entry *selectedEntry;
@property NSString *sortKey;

-(void) reloadTableData;

@end
