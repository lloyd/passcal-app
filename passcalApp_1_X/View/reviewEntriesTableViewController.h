//
//  reviewEntriesTableViewController.h
//
// This class implements a tableView which has been customized to work with the
// singlton interface to the data store.  The parent class sets the sortKey and
// the current selected station is retrieved from the currentEntry model in the
// store.

#import "RHCoreDataTableViewController.h"
#import "Entry.h"
#import "mainMenuTableViewController.h"

@interface reviewEntriesTableViewController : RHCoreDataTableViewController
@property id<masterStatusController> masterDelegate;

@property Entry *selectedEntry;
@property NSString *sortKey;

-(void) resetFetchedResultsController; // Should be called after changing sortKey

@end