//
//  loggingViewController.h
//  passcalApp_1_X
//
//  Created by field on 9/26/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface loggingViewController : UIViewController<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITextView *logTextView;
- (IBAction) refresh:(id)sender;

@end
