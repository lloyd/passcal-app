//
//  datePickerPopoverViewController.h
//  passcalApp_1_X
//
//  Created by field on 9/6/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface datePickerPopoverViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIDatePicker *centerDatePicker;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneBarButton;

@property NSDate *editedDate;

@end
