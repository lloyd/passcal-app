//
//  datePickerPopoverViewController.m
//  passcalApp_1_X
//
//  Created by field on 9/6/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "textViewPopoverViewController.h"

@implementation textViewPopoverViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.mainTextView.layer setBackgroundColor: [[UIColor whiteColor] CGColor]];
    [self.mainTextView.layer setBorderColor: [[UIColor grayColor] CGColor]];
    [self.mainTextView.layer setBorderWidth: 1.0];
    [self.mainTextView.layer setCornerRadius:8.0f];
    [self.mainTextView.layer setMasksToBounds:YES];
    
    // Might only be needed for iOS 7.0+
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.mainTextView.text = self.origValue;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if (sender == self.doneBarButton) {
        self.editedValue = self.mainTextView.text;
    }
}
@end
