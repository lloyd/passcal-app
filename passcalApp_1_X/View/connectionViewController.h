//
//  connectionViewController.h
//
// This class implments a GUI view which contains both the dasConnection and
// entryBuilder classes.  When user initiates a collect this classes sends
// a list of commands to send which will trigger responses from the DAS.  Both
// the status of the connection and the status of build are polled via timer.

#import <UIKit/UIKit.h>

@interface connectionViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

// GUI Elements
@property (weak, nonatomic) IBOutlet UITableView *dasList;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *dasSearchingIndicator;
@property (weak, nonatomic) IBOutlet UIProgressView *collectionProgressBar;

// Timer methods
- (void) stopGuiTimer;
- (void) guiTimerCallback:(NSTimer*) timer;

// Button callbacks
- (IBAction)refresh:(id)sender;
- (IBAction)collectButton:(id)sender;

@end
