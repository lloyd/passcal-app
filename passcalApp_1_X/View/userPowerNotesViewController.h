//
//  userPowerNotesViewController.h
//
// This class implements the View logic for entering manual values
// into a station entry.

#import <UIKit/UIKit.h>
#import "mainMenuTableViewController.h"

@interface userPowerNotesViewController : UIViewController <UITextFieldDelegate>

@property NSURL *stationURI;

-(void) reloadValuesFromStore;
- (IBAction) cancelButton:(id)sender;
- (IBAction) commitButton:(id)sender;
- (IBAction) viewTapped:(id)sender;

// Entry reference labels
@property (weak, nonatomic) IBOutlet UILabel *entryTimeRef;
@property (weak, nonatomic) IBOutlet UILabel *dasUnitRef;
@property (weak, nonatomic) IBOutlet UILabel *expNameRef;
@property (weak, nonatomic) IBOutlet UILabel *stationNameRef;

// Text fields
@property (weak, nonatomic) IBOutlet UITextField *batteryVoltageTextInput;
@property (weak, nonatomic) IBOutlet UITextField *powerBoxVsTextInput;
@property (weak, nonatomic) IBOutlet UITextField *powerBoxApvTextInput;
@property (weak, nonatomic) IBOutlet UITextField *powerBoxVpnTextInput;
@property (weak, nonatomic) IBOutlet UITextField *solarPanelVoltageTextInput;
@property (weak, nonatomic) IBOutlet UITextField *solarPanelSizeTextInput;
@property (weak, nonatomic) IBOutlet UITextField *solarOrientationTextInput;
@property (weak, nonatomic) IBOutlet UITextField *powerBoxVpTextInput;

@property id<masterStatusController> masterDelegate;
@end
