//
//  userSerialNumViewController.m
//  passcalApp_1_X
//
//  Created by field on 7/20/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "userSerialNumViewController.h"

#import "RHManagedObjectContextManager.h"
#import "Entry.h"
#import "Rf130StationFields.h"
#import "UserFields.h"
#import "CurrentEntries.h"

@interface userSerialNumViewController ()

@property Entry *entry;

// Only Need local variables for non string attributes

- (void) setStationFromUri: (NSURL*) url;

@end

@implementation userSerialNumViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    if (self.stationURI == nil)
    {
        CurrentEntries *currentEntry = [CurrentEntries getInstance];
        self.stationURI = currentEntry.collectUrl;
    }
    
    if (self.stationURI != nil) // Only should happen when application is first installed
    {
        [self setStationFromUri: self.stationURI];
        NSString *str = [NSString stringWithFormat:@"%@",
                         self.entry.entryTime];
        [self setTitle:str];
        
        //self.stationDict = [self.entry getDict];
    }
    [self addTextViews];
    [self addRefLabels];

    [self reloadValuesFromStore];
}

-(void) reloadValuesFromStore
{
    if (self.entry == nil)
        return;
    
    [self addRefLabels]; // Just do this for good measure
    
    // Reload textViews
    self.flashDrive1SerialNumTextInput.text = self.entry.userFields.flashDisk1SerialNum;
    self.flashDrive2SerialNumTextInput.text = self.entry.userFields.flashDisk2SerialNum;
    self.clockSerialNumberTextInput.text = self.entry.userFields.clockSerialNumber;
    self.enclosureSerialNumberTextInput.text = self.entry.userFields.enclosureSerialNumber;
    self.telemetrySerialNumberTextInput.text = self.entry.userFields.telemetrySerialNumber;
    self.powerBoxSerialNumberTextInput.text = self.entry.userFields.powerBoxSerialNumber;
    self.sensor1SerialNumberTextInput.text = self.entry.userFields.sensor1SerialNumber;
    self.sensor2SerialNumberTextInput.text = self.entry.userFields.sensor2SerialNumber;
    
    // Reload Locals

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setStationFromUri: (NSURL*) url {
    self.entry = [Entry existingEntityWithUrl:url error:nil];
}

- (void) addTextViews {
    self.flashDrive1SerialNumTextInput.delegate = self;
    self.flashDrive2SerialNumTextInput.delegate = self;
    self.clockSerialNumberTextInput.delegate = self;
    self.enclosureSerialNumberTextInput.delegate = self;
    self.telemetrySerialNumberTextInput.delegate = self;
    self.powerBoxSerialNumberTextInput.delegate = self;
    self.sensor1SerialNumberTextInput.delegate = self;
    self.sensor2SerialNumberTextInput.delegate = self;
}

- (void) addRefLabels {
    self.entryTimeRef.text   = [[NSString alloc] initWithFormat:@"%@", self.entry.entryTime];
    self.dasUnitRef.text     = [[NSString alloc] initWithFormat:@"%04X", self.entry.rf130Station.dasUnitIdValue];
    self.expNameRef.text     = self.entry.rf130Station.experimentName;
    self.stationNameRef.text = self.entry.rf130Station.stationName;
}

- (IBAction) cancelButton:(id)sender {
    [self reloadValuesFromStore];
}

- (IBAction) commitButton:(id)sender {
    Entry *newEntry = [self.entry createNewRevision];
    UserFields *newUserFields = [self.entry.userFields createNewRevision];
    newEntry.userFields = newUserFields;
    self.entry = newEntry;
    
    self.entry.userFields.flashDisk1SerialNum = self.flashDrive1SerialNumTextInput.text;
    self.entry.userFields.flashDisk2SerialNum = self.flashDrive2SerialNumTextInput.text;
    self.entry.userFields.clockSerialNumber = self.clockSerialNumberTextInput.text;
    self.entry.userFields.enclosureSerialNumber = self.enclosureSerialNumberTextInput.text;
    self.entry.userFields.telemetrySerialNumber =  self.telemetrySerialNumberTextInput.text;
    self.entry.userFields.powerBoxSerialNumber = self.powerBoxSerialNumberTextInput.text;
    self.entry.userFields.sensor1SerialNumber = self.sensor1SerialNumberTextInput.text;
    self.entry.userFields.sensor2SerialNumber = self.sensor2SerialNumberTextInput.text;
    
    [UserFields commit];
    [Entry commit];
    
    [CurrentEntries updateCommitEntry:self.entry];
    [CurrentEntries updateReviewEntryIfMatchingTreeID:self.entry];
    
    [self.masterDelegate updateMasterStatus];
    
    NSString *commitStr = [[NSString alloc]
                          initWithFormat:@"Entry Commited. (Rev %d)", self.entry.revisionNumValue];
    UIAlertView *messageAlert = [[UIAlertView alloc] initWithTitle:@"Committed"
                                                message:commitStr delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil, nil];
    [messageAlert show];
}

- (IBAction)viewTapped:(id)sender {
    [self.flashDrive1SerialNumTextInput resignFirstResponder];
    [self.flashDrive2SerialNumTextInput resignFirstResponder];
    [self.clockSerialNumberTextInput resignFirstResponder];
    [self.enclosureSerialNumberTextInput resignFirstResponder];
    [self.telemetrySerialNumberTextInput resignFirstResponder];
    [self.powerBoxSerialNumberTextInput resignFirstResponder];
    [self.sensor1SerialNumberTextInput resignFirstResponder];
    [self.sensor2SerialNumberTextInput resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    return YES;
}

@end
