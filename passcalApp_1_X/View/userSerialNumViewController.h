//
//  userSerialNumViewController.h
//
// This class implements the View logic for entering manual values
// into a station entry.

#import <UIKit/UIKit.h>
#import "mainMenuTableViewController.h"

@interface userSerialNumViewController : UIViewController <UITextFieldDelegate>

@property NSURL *stationURI;

-(void) reloadValuesFromStore;
- (IBAction) cancelButton:(id)sender;
- (IBAction) commitButton:(id)sender;
- (IBAction)viewTapped:(id)sender;

// Entry reference labels
@property (weak, nonatomic) IBOutlet UILabel *entryTimeRef;
@property (weak, nonatomic) IBOutlet UILabel *dasUnitRef;
@property (weak, nonatomic) IBOutlet UILabel *expNameRef;
@property (weak, nonatomic) IBOutlet UILabel *stationNameRef;

// Text fields
@property (weak, nonatomic) IBOutlet UITextField *flashDrive1SerialNumTextInput;
@property (weak, nonatomic) IBOutlet UITextField *flashDrive2SerialNumTextInput;
@property (weak, nonatomic) IBOutlet UITextField *clockSerialNumberTextInput;
@property (weak, nonatomic) IBOutlet UITextField *enclosureSerialNumberTextInput;
@property (weak, nonatomic) IBOutlet UITextField *telemetrySerialNumberTextInput;
@property (weak, nonatomic) IBOutlet UITextField *powerBoxSerialNumberTextInput;
@property (weak, nonatomic) IBOutlet UITextField *sensor1SerialNumberTextInput;
@property (weak, nonatomic) IBOutlet UITextField *sensor2SerialNumberTextInput;

@property id<masterStatusController> masterDelegate;
@end
