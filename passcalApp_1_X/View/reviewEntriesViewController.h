//
//  reviewEntriesViewController.h
//
// This class is the parent view which controls the display of entries in the store.
// Based off bar button hit the format of the desciption is changed as well as the
// sort.

#import <UIKit/UIKit.h>
#import "mainMenuTableViewController.h"

@interface reviewEntriesViewController : UIViewController

- (IBAction)changeSort:(id)sender;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *entryTimeSortButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *expNameSortButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *stationNameSortButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *lonSortButton;

@property (weak, nonatomic) IBOutlet UIView *subTableView;

@property id<masterStatusController> masterDelegate;

@end
