//
//  reviewRevisionHistoryViewController.m
//  passcalApp_1_X
//
//  Created by field on 9/1/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "reviewRevisionHistoryViewController.h"
#import "editEntryCellViewController.h"
#import "RHManagedObjectContextManager.h"

#import "CurrentEntries.h"
#import "Entry.h"
#import "RevControlHelper.h"

#import "reviewUniqueRevisionTableView.h"
#import "treeRevisionHistoryTableView.h"

#define kCellIdentifier @"entryKeyValue"

@interface reviewRevisionHistoryViewController ()

@property Entry *entry;
@property NSMutableDictionary *entryDict;
@property NSMutableDictionary *changes;

@property NSString *editKey;

-(void) setStationFromUri: (NSURL*) url;

@end

@implementation reviewRevisionHistoryViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.changes = [[NSMutableDictionary alloc] init];
    // Test URL in core data
    if (self.stationURI == nil)
    {
        CurrentEntries *currentEntry = [CurrentEntries getInstance];
        self.stationURI = currentEntry.reviewUrl;
    }
    
    if (self.stationURI == nil) // Only should happen when application is first installed
    {
        self.entryDict = [[NSMutableDictionary alloc]
                          initWithDictionary: @{@"Error" : @"No Entry Selected!"}];
        self.keyArray = [self.entryDict allKeys];
    }
    else
    {
        [self setStationFromUri: self.stationURI];
        
        /*
        NSString *str = [NSString stringWithFormat:@"%@",
                         self.entry.entryTime];
        [self setTitle:str];
         */
        
        // self.entryDict = [self.stationEntry getDict];
        self.entryDict = [[NSMutableDictionary alloc]
                          initWithDictionary:[self.entry deepSerialize:true]];
        
        if (self.keyArray == nil)  // If not specified display all keys
        {
            self.keyArray = [self.entryDict allKeys];
        }
    }

}

-(void) setStationFromUri: (NSURL*) url {
    // TOOD check that an entry was passed
    self.entry = [Entry existingEntityWithUrl:url error:nil];
}

-(IBAction)commitChanges:(id)sender {
    /*
    if (self.changes.count == 0) {// No changes
        return;
    }
    Entry *newEntry = [self.entry createNewRevision:self.changes];
    self.changes = [[NSMutableDictionary alloc] init];
    self.entry = newEntry;
    */
    [RevControlHelper commitAll];
    
    //[CurrentEntries updateReviewEntry:self.entry];
    //[CurrentEntries commit];
    
    self.entryDict = [[NSMutableDictionary alloc]
                      initWithDictionary:[self.entry serialize:true]];
    
    NSString *commitStr = [[NSString alloc]
                           initWithFormat:@"Entry Commited. (Rev %d)", self.entry.revisionNumValue];
    UIAlertView *messageAlert = [[UIAlertView alloc] initWithTitle:@"Committed"
                                                           message:commitStr delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil, nil];
    [messageAlert show];
    /*
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
     */
}

- (IBAction)changeSort:(id)sender {
    reviewUniqueRevisionTableView *subView =
        (reviewUniqueRevisionTableView *)self.childViewControllers.lastObject; // Assume a single child
    
    if (sender == self.entryTimeSortButton)
    {
        subView.sortKey = @"entryTime";
        [subView reloadTableData]; // Call to update results with new key
        dispatch_async(dispatch_get_main_queue(), ^{
            [subView.tableView reloadData];
        });
    }
    else if (sender == self.expNameSortButton)
    {
        subView.sortKey = @"rf130Station.experimentName";
        [subView reloadTableData]; // Call to update results with new key
        dispatch_async(dispatch_get_main_queue(), ^{
            [subView.tableView reloadData];
        });
    }
    else if (sender ==  self.stationNameSortButton)
    {
        subView.sortKey = @"rf130Station.stationName";
        [subView reloadTableData]; // Call to update results with new key
        dispatch_async(dispatch_get_main_queue(), ^{
            [subView.tableView reloadData];
        });
    }
    
    else if (sender ==  self.lonSortButton)
    {
        subView.sortKey = @"rf130Status.gpsLongitude";
        [subView reloadTableData]; // Call to update results with new key
        dispatch_async(dispatch_get_main_queue(), ^{
            [subView.tableView reloadData];
        });
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object's treeID to the new view controller.
    
    if ([[segue identifier] isEqualToString: @"showHistorySegue"])
    {
        
        
        reviewUniqueRevisionTableView *subView =
        (reviewUniqueRevisionTableView *)self.childViewControllers.lastObject; // Assume a single child
        
        UINavigationController *targetNavCtrl = [segue destinationViewController];
        treeRevisionHistoryTableView *targetEntry = (treeRevisionHistoryTableView *)targetNavCtrl.topViewController;
        targetEntry.treeID = subView.selectedEntry.treeID;
    }
    
}

@end