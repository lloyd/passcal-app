//
//  treeRevisionHistoryTableView.m
//  passcalApp_1_X
//
//  Created by field on 9/3/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "treeRevisionHistoryTableView.h"
#import "Entry.h"

#import "CocoaLumberjack.h"
static const DDLogLevel ddLogLevel = DDLogLevelInfo;

#define kCellIdentifier @"entryCell"

@implementation treeRevisionHistoryTableView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kCellIdentifier];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSFetchedResultsController *)fetchedResultsController {
    if (fetchedResultsController == nil) {
        
        NSSortDescriptor *sort1;
        sort1 = [[NSSortDescriptor alloc] initWithKey:@"updateTime" ascending:NO];
        
        NSString *predStr = [[NSString alloc] initWithFormat:@"treeID == \"%@\"", self.treeID];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predStr];
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        [fetchRequest setEntity:[Entry entityDescriptionWithError:nil]];
        [fetchRequest setPredicate:predicate];
        [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sort1, nil]];
        
        self.fetchedResultsController = [[NSFetchedResultsController alloc]
                                         initWithFetchRequest:fetchRequest
                                         managedObjectContext:
                                         [Entry managedObjectContextForCurrentThreadWithError:nil]
                                         sectionNameKeyPath:nil
                                         cacheName:nil];
        
        fetchedResultsController.delegate = self;
        
        NSError *error = nil;
        if (![fetchedResultsController performFetch:&error]) {
            DDLogError(@"Unresolved error: %@", [error localizedDescription]);
        }
    }
    
    return fetchedResultsController;
}

-(void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    Entry *entry = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ => %04X", entry.updateTime,
                                entry.revisionNumValue];
    
    if (entry.isHiddenValue)
        cell.accessoryType = UITableViewCellAccessoryNone;
    else
        cell.accessoryType = UITableViewCellAccessoryCheckmark; // Visable
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // We must dequeue from self.tableView and not tableView since this method is also used by the search controller
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Entry *entry = [self.fetchedResultsController objectAtIndexPath:indexPath];
    entry.isHiddenValue = !entry.isHiddenValue;
    
    [Entry commit];  // TODO find a better way then storing this
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

@end
