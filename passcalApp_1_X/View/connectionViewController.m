//
//  connectionViewController.m
//  passcalApp_1_X
//
//  Created by matt on 6/30/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "connectionViewController.h"
#import "rf130CmdFactory.h"
#import "dasConnection.h"
#import "stationEntryBuilder.h"

#import "rf130SimpleCmd.h"
#import "rf130StatusInfoCmd.h"

const NSString *q330PlaceHolder = @"Q330 not implemented yet";

@interface connectionViewController ()

@property dasConnection *das;
@property stationEntryBuilder *entryBuilder;
@property (weak) NSTimer *guiTimer; // Only need one timer since we cannot collect without a connection

// Table propoerties
@property NSArray *dasListSections;
@property NSMutableArray *rf130DasList;
@property NSArray *q330DasList;

-(void) setupUITable;

@end

@implementation connectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Configure view
    self.splitViewController.preferredDisplayMode = UISplitViewControllerDisplayModeAllVisible; // This may be for IOS 8 only
    
    // Setup table
    [self setupUITable];
    
    // Setup pub - sub pattern
    self.das = [[dasConnection alloc] init]; // Publisher of responses
    self.entryBuilder = [[stationEntryBuilder alloc] init];
    
    //self.dasSearchingIndicator = [[UIActivityIndicatorView alloc] init];
    [self updateCollectionStatus];
    
    [self.das regSubscriber:self.entryBuilder];
    
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self stopGuiTimer];
    // Attempt to make TCP connection
    [self.das open];
    
    // Start timer for connection
    self.guiTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                             target:self selector:@selector(guiTimerCallback:)
                             userInfo:nil repeats:YES];
}

- (void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [self stopGuiTimer];
    [self.entryBuilder resetCurrentEntry];
    
    // Close TCP connection
    [self.das close];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Timer Methods
- (void) stopGuiTimer {
    [self.guiTimer invalidate];
    self.guiTimer = nil;
}

- (void) guiTimerCallback:(NSTimer*) timer {
    if (self.das.connected)
    {
        [self.dasSearchingIndicator stopAnimating];
        [self updateRf130DasList:self.das.getUnitId];
    }
    else
    {
        [self.dasSearchingIndicator startAnimating];
        if (!self.das.busy)
        {
            [self.das retryConnection];
        }
    }
    
    // Update if doing collection
    [self updateCollectionStatus];
}

- (void)updateCollectionStatus {
    int count = [self.entryBuilder getMsgCount];
    float progress = count / ((float)totalRf130MsgCount);
    [self.collectionProgressBar setProgress:progress];
}



- (IBAction)refresh:(id)sender {
    
    [self stopGuiTimer];
    [self.entryBuilder resetMsgCount];
    [self.entryBuilder resetCurrentEntry];
    
    [self.das retryConnection];
    self.guiTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                             target:self selector:@selector(guiTimerCallback:)
                             userInfo:nil repeats:YES];
    
}

- (IBAction)collectButton:(id)sender {
    
    [self.entryBuilder resetMsgCount];
    [self.entryBuilder resetCurrentEntry];
    
    // Do a sequence of cmds
    cmd *cmd;
    
    // number of messages should be equal const totalMsgCount
    
    // SS -> Disk Status
    cmd = [rf130CmdFactory createCmd:@"SS" : @"DK"];
    [self.das sendCmd:cmd];
    
    // SS -> Acquisition
    cmd = [rf130CmdFactory createCmd:@"SS" : @"AQ"];
    [self.das sendCmd:cmd];
    
    // SS -> Unit Status
    cmd = [rf130CmdFactory createCmd:@"SS" : @"US"];
    [self.das sendCmd:cmd];
    
    // SS -> Version
    cmd = [rf130CmdFactory createCmd:@"SS" : @"VS"];
    [self.das sendCmd:cmd];
    
    // SS -> External Clock
    cmd = [rf130CmdFactory createCmd:@"SS" : @"XC"];
    [self.das sendCmd:cmd];
    
    // SS -> Sensor Information
    cmd = [rf130CmdFactory createCmd:@"SS" : @"SI"];
    [self.das sendCmd:cmd];
    
    // SS -> Aux Data
    cmd = [rf130CmdFactory createCmd:@"SS" : @"AD"];
    [self.das sendCmd:cmd];
    
    // SS -> Parameter
    cmd = [rf130CmdFactory createCmd:@"SS" : @"PR"];
    [self.das sendCmd:cmd];
    
    // PR -> Station Parameters
    cmd = [rf130CmdFactory createCmd:@"PR" : @"PS"];
    [self.das sendCmd:cmd];

    // PR -> Auto Re-Center Parameters
    for (int i=1; i <= 4; i++)
    {
        cmd = [rf130CmdFactory createCmd:@"PR" : @"PQ" : i];
        [self.das sendCmd:cmd];
    }
    
    // PR -> Channel Parameters
    for (int i=1; i <= 6; i++)
    {
        cmd = [rf130CmdFactory createCmd:@"PR" : @"PC" : i];
        [self.das sendCmd:cmd];
    }
    
    // PR -> DataStream Parameters
    for (int i=1; i <= 8; i++)
    {
        cmd = [rf130CmdFactory createCmd:@"PR" : @"PD" : i];
        [self.das sendCmd:cmd];
    }
    
    // PR -> Auxillary Data Parameters
    //cmd = [rf130CmdFactory createCmd:@"PR" : @"PA"];
    //[self.das sendCmd:cmd];
}

//// Begin tableView Methods ////

- (void) setupUITable {
    self.dasListSections = @[@"RefTek 130 DAS", @"Quanterra 330 DAS"];
    self.rf130DasList = [NSMutableArray arrayWithObjects: @"None Found", nil];
    self.q330DasList = @[q330PlaceHolder];
    
    // Load GUI
    /*
    UITableView *tableView = [[UITableView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame] style:UITableViewStylePlain];
    tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    tableView.delegate = self;
    tableView.dataSource = self;
    [tableView reloadData];
    self.dasList = tableView;
     */
    self.dasList.delegate = self;
    self.dasList.dataSource = self;
    //self.dasList.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    [self.dasList reloadData];
}

- (void) updateRf130DasList : (uint16_t)dasID {
    cmd *cmd;
    if (dasID == 0000){
        [self.rf130DasList replaceObjectAtIndex:0 withObject:@"None Found"];
        // Unit ID
        cmd = [rf130CmdFactory createCmd: @"ID"];
        [self.das sendCmd:cmd];
    }
    else {
        [self.rf130DasList replaceObjectAtIndex:0 withObject:
                               [[NSString alloc] initWithFormat:@"DAS Unit ID: %04X", dasID]];
    }
    [self.dasList reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.dasListSections count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0)
    {
        return [self.rf130DasList count];
    }
    else
    {
        return [self.q330DasList count];
    }
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return self.dasListSections[section];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *MyIdentifier = @"MyReuseIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:MyIdentifier];
    }
    
    NSString *cellText;
    if (indexPath.section == 0)
    {
        cellText = self.rf130DasList[indexPath.row];
    }
    else
    {
        cellText = self.q330DasList[indexPath.row];
    }
    
    cell.textLabel.text = cellText;
    return cell;
}

//// End tableView Methods ////

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
