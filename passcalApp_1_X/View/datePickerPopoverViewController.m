//
//  datePickerPopoverViewController.m
//  passcalApp_1_X
//
//  Created by field on 9/6/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "datePickerPopoverViewController.h"

@implementation datePickerPopoverViewController
- (void)viewDidLoad {
    [super viewDidLoad];

    NSTimeZone *utc = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    self.centerDatePicker.timeZone = utc;
    
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en-GB"];
    self.centerDatePicker.locale = locale;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if (sender == self.doneBarButton) {
        self.editedDate = self.centerDatePicker.date;
    }
}
@end
