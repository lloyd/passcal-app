//
//  userSerialNumViewController.m
//  passcalApp_1_X
//
//  Created by field on 7/20/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "userStationNotesViewController.h"
#import "datePickerPopoverViewController.h"
#import "textViewPopoverViewController.h"

#import "RHManagedObjectContextManager.h"
#import "Entry.h"
#import "Rf130StationFields.h"
#import "UserFields.h"
#import "CurrentEntries.h"

@interface userStationNotesViewController ()

@property Entry *entry;

// Only Need local variables for non string attributes
@property float gpsLatLocal;
@property float gpsLonLocal;
@property float gpsElevationLocal;
@property float magDeclinationLocal;
@property float sensorOrientationLocal;
@property NSDate *departureTimeLocal;

- (void) setStationFromUri: (NSURL*) url;

@end

@implementation userStationNotesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.loc = [[myLocation alloc] init];
    
    if (self.stationURI == nil)
    {
        CurrentEntries *currentEntry = [CurrentEntries getInstance];
        self.stationURI = currentEntry.collectUrl;
    }
    
    if (self.stationURI != nil) // Only should happen when application is first installed
    {
        [self setStationFromUri: self.stationURI];
        NSString *str = [NSString stringWithFormat:@"%@",
                         self.entry.entryTime];
        [self setTitle:str];
        
        //self.stationDict = [self.entry getDict];
    }
    [self addTextViews];
    [self addRefLabels];

    [self reloadValuesFromStore];
}

- (void) viewWillAppear:(BOOL)animated {
    [self.loc start];
}

-(void) viewWillDisappear:(BOOL)animated {
    [self.loc stop];
}

-(void) reloadValuesFromStore
{
    if (self.entry == nil)
        return;
    
    [self addRefLabels]; // Just do this for good measure
    
    // Reload textViews
    self.weatherNotesTextInput.text = self.entry.userFields.weatherNotes;
    self.userNotesTextInput.text = self.entry.userFields.userNotes;
    self.userStationNameTextInput.text = self.entry.userFields.stationName;
    self.userExperimentNameTextInput.text = self.entry.userFields.experimentName;
    self.siteContactTextInput.text = self.entry.userFields.siteContact;
    self.fieldTeamTextInput.text = self.entry.userFields.fieldTeam;
    
    self.gpsLatTextInput.text = [[NSString alloc] initWithFormat:@"%0.4f",
                                 self.entry.userFields.gpsLatitudeValue];
    self.gpsLonTextInput.text = [[NSString alloc] initWithFormat:@"%0.4f",
                                 self.entry.userFields.gpsLongitudeValue];
    self.gpsElevationTextInput.text = [[NSString alloc] initWithFormat:@"%0.2f",
                                       self.entry.userFields.gpsAltitudeValue];
    self.magDeclinationTextInput.text = [[NSString alloc] initWithFormat:@"%0.2f",
                                         self.entry.userFields.magneticDeclinationValue];
    self.sensorOrientationTextInput.text = [[NSString alloc] initWithFormat:@"%0.2f",
                                            self.entry.userFields.sensorOrientationValue];
    self.departureTimeTextInput.text = [[NSString alloc] initWithFormat:@"%@",
                                        self.entry.userFields.departureTime];

    // Load Locals
    self.departureTimeLocal = self.entry.userFields.departureTime;
    
    self.gpsLatLocal             = [self.gpsLatTextInput.text floatValue];
    self.gpsLonLocal             = [self.gpsLonTextInput.text floatValue];
    self.gpsElevationLocal       = [self.gpsElevationTextInput.text floatValue];
    self.magDeclinationLocal     = [self.magDeclinationTextInput.text floatValue];
    self.sensorOrientationLocal  = [self.sensorOrientationTextInput.text floatValue];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setStationFromUri: (NSURL*) url {
    self.entry = [Entry existingEntityWithUrl:url error:nil];
}

- (void) addTextViews {
    self.weatherNotesTextInput.delegate = self;
    self.userNotesTextInput.delegate = self;
    self.userStationNameTextInput.delegate = self;
    self.userExperimentNameTextInput.delegate = self;
    self.siteContactTextInput.delegate = self;
    self.fieldTeamTextInput.delegate = self;
    self.gpsLatTextInput.delegate = self;
    self.gpsLonTextInput.delegate = self;
    self.gpsElevationTextInput.delegate = self;
    self.magDeclinationTextInput.delegate = self;
    self.sensorOrientationTextInput.delegate = self;
    self.departureTimeTextInput.delegate = self;
    
    [self.weatherNotesTextInput.layer setBackgroundColor: [[UIColor whiteColor] CGColor]];
    [self.weatherNotesTextInput.layer setBorderColor: [[UIColor grayColor] CGColor]];
    [self.weatherNotesTextInput.layer setBorderWidth: 1.0];
    [self.weatherNotesTextInput.layer setCornerRadius:8.0f];
    [self.weatherNotesTextInput.layer setMasksToBounds:YES];
    
    [self.userNotesTextInput.layer setBackgroundColor: [[UIColor whiteColor] CGColor]];
    [self.userNotesTextInput.layer setBorderColor: [[UIColor grayColor] CGColor]];
    [self.userNotesTextInput.layer setBorderWidth: 1.0];
    [self.userNotesTextInput.layer setCornerRadius:8.0f];
    [self.userNotesTextInput.layer setMasksToBounds:YES];

}

- (void) addRefLabels {
    self.entryTimeRef.text   = [[NSString alloc] initWithFormat:@"%@", self.entry.entryTime];
    self.dasUnitRef.text     = [[NSString alloc] initWithFormat:@"%04X", self.entry.rf130Station.dasUnitIdValue];
    self.expNameRef.text     = self.entry.rf130Station.experimentName;
    self.stationNameRef.text = self.entry.rf130Station.stationName;
}

- (IBAction) cancelButton:(id)sender {
    [self reloadValuesFromStore];
}

- (IBAction) commitButton:(id)sender {
    Entry *newEntry = [self.entry createNewRevision];
    UserFields *newUserFields = [self.entry.userFields createNewRevision];
    newEntry.userFields = newUserFields;
    self.entry = newEntry;
    
    [self convert2Local]; // Just in case keyboard is not resigned
    
    self.entry.userFields.weatherNotes = self.weatherNotesTextInput.text;
    self.entry.userFields.userNotes = self.userNotesTextInput.text;
    self.entry.userFields.stationName = self.userStationNameTextInput.text;
    self.entry.userFields.experimentName = self.userExperimentNameTextInput.text;
    self.entry.userFields.siteContact = self.siteContactTextInput.text;
    self.entry.userFields.fieldTeam = self.fieldTeamTextInput.text;

    // Use locals
    self.entry.userFields.gpsLatitudeValue = self.gpsLatLocal;
    self.entry.userFields.gpsLongitudeValue = self.gpsLonLocal;
    self.entry.userFields.gpsAltitudeValue = self.gpsElevationLocal;
    self.entry.userFields.magneticDeclinationValue = self.magDeclinationLocal;
    self.entry.userFields.sensorOrientationValue = self.sensorOrientationLocal;
    self.entry.userFields.departureTime = self.departureTimeLocal;

    [UserFields commit];
    [Entry commit];
    
    [CurrentEntries updateCommitEntry:self.entry];
    [CurrentEntries updateReviewEntryIfMatchingTreeID:self.entry];
    
    [self.masterDelegate updateMasterStatus];
    
    NSString *commitStr = [[NSString alloc]
                          initWithFormat:@"Entry Commited. (Rev %d)", self.entry.revisionNumValue];
    UIAlertView *messageAlert = [[UIAlertView alloc] initWithTitle:@"Committed"
                                                message:commitStr delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil, nil];
    [messageAlert show];
}

-(void) convert2Local {
    self.gpsLatLocal             = [self.gpsLatTextInput.text floatValue];
    self.gpsLonLocal             = [self.gpsLonTextInput.text floatValue];
    self.gpsElevationLocal       = [self.gpsElevationTextInput.text floatValue];
    self.magDeclinationLocal     = [self.magDeclinationTextInput.text floatValue];
    self.sensorOrientationLocal  = [self.sensorOrientationTextInput.text floatValue];
    
    /*
    NSTimeZone *utc = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    //[dateFormat setDateFormat:@"yyyy:DDD:HH:mm:ss"];
    [dateFormat setTimeZone:utc];
    self.departureTimeLocal = [dateFormat dateFromString:self.departureTimeTextInput.text];
    */
    // Leave departureTime alone since it is managed by segue

    // Apply locals to TextFields to provide user feedback
    self.gpsLatTextInput.text = [[NSString alloc] initWithFormat:@"%0.4f",
                                    self.gpsLatLocal];
    self.gpsLonTextInput.text = [[NSString alloc] initWithFormat:@"%0.4f",
                                    self.gpsLonLocal];
    self.gpsElevationTextInput.text = [[NSString alloc] initWithFormat:@"%0.2f",
                                       self.gpsElevationLocal];
    self.magDeclinationTextInput.text = [[NSString alloc] initWithFormat:@"%0.2f",
                                         self.magDeclinationLocal];
    self.sensorOrientationTextInput.text = [[NSString alloc] initWithFormat:@"%0.2f",
                                            self.sensorOrientationLocal];
    self.departureTimeTextInput.text = [[NSString alloc] initWithFormat:@"%@",
                                        self.departureTimeLocal];
}

- (IBAction)viewTapped:(id)sender {
    [self.weatherNotesTextInput resignFirstResponder];
    [self.userNotesTextInput resignFirstResponder];
    [self.userStationNameTextInput resignFirstResponder];
    [self.userExperimentNameTextInput resignFirstResponder];
    [self.siteContactTextInput resignFirstResponder];
    [self.fieldTeamTextInput resignFirstResponder];
    [self.gpsLatTextInput resignFirstResponder];
    [self.gpsLonTextInput resignFirstResponder];
    [self.gpsElevationTextInput resignFirstResponder];
    [self.magDeclinationTextInput resignFirstResponder];
    [self.sensorOrientationTextInput resignFirstResponder];
    [self.departureTimeTextInput resignFirstResponder];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField == self.departureTimeTextInput)
    {
        [self performSegueWithIdentifier:@"datePickerSegue" sender:textField];
        return NO;
    }
    
    return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *) textView {

    if (textView == self.weatherNotesTextInput)
    {
        [self performSegueWithIdentifier:@"textViewPopoverSegue" sender:textView];
        return NO;
    }
    else if (textView == self.userNotesTextInput)
    {
        [self performSegueWithIdentifier:@"textViewPopoverSegue" sender:textView];
        return NO;
    }
    
    return YES;
}

- (IBAction) setDepartureTime:(id)sender {
    self.departureTimeLocal = [[NSDate alloc] init];
    self.departureTimeTextInput.text = [[NSString alloc] initWithFormat:@"%@",
                                        self.departureTimeLocal];
}
- (IBAction) setGPSLocation:(id)sender {
    self.gpsLatLocal = self.loc.lat;
    self.gpsLatTextInput.text = [NSString stringWithFormat:@"%0.10f", self.gpsLatLocal];
    self.gpsLonLocal = self.loc.lon;
    self.gpsLonTextInput.text = [NSString stringWithFormat:@"%0.10f", self.gpsLonLocal];
    self.gpsElevationLocal = self.loc.altitude;
    self.gpsElevationTextInput.text = [NSString stringWithFormat:@"%0.2f", self.gpsElevationLocal];
    
    if (!self.loc.validGps)
    {
        NSString *warnMsg = [NSString stringWithFormat:@"Low location accuracy. \n Horizontal:%0.2f m\n Vertical:%0.2f m.",
                             self.loc.hAccuracy, self.loc.vAccuracy];
        UIAlertView *messageAlert = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                               message:warnMsg
                                                              delegate:nil
                                                     cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil, nil];
        [messageAlert show];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    return YES;
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString: @"textViewPopoverSegue"])
    {
        UITextView *textFieldSender = sender;
        textViewPopoverViewController *target = (textViewPopoverViewController*)[[segue destinationViewController] topViewController];
        target.origValue = textFieldSender.text;
        target.editedValue = nil;
        target.returnReference = textFieldSender;
    }
}
- (IBAction)unwindToUserStationNotesFromDatePicker:(UIStoryboardSegue *)segue {
    datePickerPopoverViewController *src = [segue sourceViewController];
    
    if (src.editedDate == nil) // No change
        return;
    
    self.departureTimeLocal = src.editedDate;
    self.departureTimeTextInput.text = [[NSString alloc] initWithFormat:@"%@",
                                        self.departureTimeLocal];
}

- (IBAction)unwindToUserStationNotesFromTextViewPopover:(UIStoryboardSegue *)segue {
    textViewPopoverViewController *src = [segue sourceViewController];
    
    if (src.editedValue == nil) // No change
        return;
    
    src.returnReference.text = src.editedValue;
}

@end
