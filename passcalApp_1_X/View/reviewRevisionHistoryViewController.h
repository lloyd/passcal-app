//
//  reviewRevisionHistoryViewController.h
//  passcalApp_1_X
//
//  Created by field on 9/1/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface reviewRevisionHistoryViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIBarButtonItem *entryTimeSortButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *expNameSortButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *stationNameSortButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *lonSortButton;

- (IBAction)changeSort:(id)sender;

@property NSURL *stationURI;
@property NSArray *keyArray;

//-(IBAction)commitChanges:(id)sender;

@end
