//
//  RulesParser.h
//  passcalApp_1_X
//
//  Created by field on 9/14/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RulesParser : NSObject

// Returns a dict has the fieldKey as a key and a NSArray of rules
+ (NSDictionary *) parseRules: (NSDictionary*) keyConfig;

@end
