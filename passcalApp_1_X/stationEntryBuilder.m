//
//  stationEntryBuilder.m
//  passcalApp_1_X
//
//  Created by matt on 7/6/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//
//
// This classes job is to subscribe to dasConnection and build a station enitiy class

#import "stationEntryBuilder.h"
#import "currentEntries.h"
#import "Entry.h"
#import "IosFields.h"
#import "Rf130StationFields.h"
#import "Rf130StatusFields.h"
#import "Rf130VersionFields.h"
#import "UserFields.h"

#import "rf130IdResponse.h"
#import "rf130SSDiskStatusResponse.h"
#import "rf130SSAcquisitionStatusResponse.h"
#import "rf130SSUnitStatusResponse.h"
#import "rf130SSExtClockStatusResponse.h"
#import "rf130SSVersionStatusResponse.h"
#import "rf130SSParameterStatusResponse.h"
#import "rf130SSSensorInfoStatus.h"
#import "rf130PRStationParametersResponse.h"
#import "rf130PRDataStreamParamsResponse.h"
#import "rf130SSAuxDataResponse.h"
#import "rf130PRSensorAutoReCenter.h"
#import "rf130PRChannelParameters.h"

#import "CocoaLumberjack.h"
static const DDLogLevel ddLogLevel = DDLogLevelInfo;

const int totalRf130MsgCount = 9+6+8+4; // 9 single messages + 6 ch streams + 8 data streams + 4 recenter sensors

@interface stationEntryBuilder ()
{
    Entry *currentEntry;
    /*
    IosFields *iosFields;
    Rf130StationFields rf130StationFields;
    Rf130StatusFields rf130StatusFields;
    Rf130VersionFields rf130VersionFields;
    UserFields userFields;
    */
    int msgCount;
    int newMsgCount;
}
- (void) createCurrentEntry;
//- (void) commitCurrentEntry;
- (void) addAppFields;
@end

@implementation stationEntryBuilder

- (void) resetCurrentEntry {
    [currentEntry delete];
    currentEntry = nil;
    newMsgCount = 0;
}
- (void) resetMsgCount {
    msgCount = 0;
}

-(id) init {
    if (!(self = [super init]))
        return nil;
    
    [self resetCurrentEntry];
    return self;
}

- (int) getMsgCount {
    return msgCount;
}

// This is where nodeID originate and all revs start at 0
- (void) createCurrentEntry {
    if (currentEntry == nil) {
        currentEntry = [Entry newEntityWithError:nil];
        currentEntry.treeID = [[NSProcessInfo processInfo] globallyUniqueString];
        currentEntry.nodeID = [[NSProcessInfo processInfo] globallyUniqueString];
        currentEntry.revisionNumValue = 0;
    }
    if (currentEntry.iosFields == nil) {
        currentEntry.iosFields = [IosFields newEntityWithError:nil];
        currentEntry.iosFields.treeID = [[NSProcessInfo processInfo] globallyUniqueString];
        currentEntry.iosFields.nodeID = [[NSProcessInfo processInfo] globallyUniqueString];
        currentEntry.iosFields.revisionNumValue = 0;
    }
    if (currentEntry.rf130Station == nil) {
        currentEntry.rf130Station = [Rf130StationFields newEntityWithError:nil];
        currentEntry.rf130Station.treeID = [[NSProcessInfo processInfo] globallyUniqueString];
        currentEntry.rf130Station.nodeID = [[NSProcessInfo processInfo] globallyUniqueString];
        currentEntry.rf130Station.revisionNumValue = 0;
    }
    if (currentEntry.rf130Status == nil) {
        currentEntry.rf130Status = [Rf130StatusFields newEntityWithError:nil];
        currentEntry.rf130Status.treeID = [[NSProcessInfo processInfo] globallyUniqueString];
        currentEntry.rf130Status.nodeID = [[NSProcessInfo processInfo] globallyUniqueString];
        currentEntry.rf130Status.revisionNumValue = 0;
    }
    if (currentEntry.rf130Version == nil) {
        currentEntry.rf130Version = [Rf130VersionFields newEntityWithError:nil];
        currentEntry.rf130Version.treeID = [[NSProcessInfo processInfo] globallyUniqueString];
        currentEntry.rf130Version.nodeID = [[NSProcessInfo processInfo] globallyUniqueString];
        currentEntry.rf130Version.revisionNumValue = 0;
    }
    if (currentEntry.userFields == nil) {
        currentEntry.userFields = [UserFields newEntityWithError:nil];
        currentEntry.userFields.treeID = [[NSProcessInfo processInfo] globallyUniqueString];
        currentEntry.userFields.nodeID = [[NSProcessInfo processInfo] globallyUniqueString];
        currentEntry.userFields.revisionNumValue = 0;
    }
}

- (void) commitCurrentEntry {
    [IosFields commit];
    [Rf130StationFields commit];
    [Rf130StatusFields commit];
    [Rf130VersionFields commit];
    [UserFields commit];
    [Entry commit];
    // Once object is commited objectID / URI are automatically updated to persistant store
    NSManagedObjectID *stationId = [currentEntry objectID];
    NSURL *stationUrl = [stationId URIRepresentation];
    currentEntry = nil; // Do this so we do not delete the just commited entry
    [self resetCurrentEntry];
    
    // There should only be a single currentEntry
    CurrentEntries *cEntry = [CurrentEntries getInstance];
    cEntry.collectUrl = stationUrl;
    [CurrentEntries commit];
    DDLogError(@"Committed!!");
}

- (void) addAppFields {
    [self createCurrentEntry];

    currentEntry.iosFields.iosDeviceModel = [[UIDevice currentDevice] model];
    currentEntry.iosFields.iosDeviceUdid = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    currentEntry.iosFields.appVersion = version;
    NSString *build = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    currentEntry.iosFields.appBuildNum = [NSNumber numberWithInteger:[build integerValue]];
    
    currentEntry.entryTime = [[NSDate alloc] init]; // Time in UTC
    currentEntry.updateTime = currentEntry.entryTime;
    currentEntry.isHiddenValue = false;  // Show since this is a new entry
}

- (void) recvObject : (NSObject*) obj {
    [self createCurrentEntry];

    if([obj isKindOfClass:[rf130SSDiskStatusResponse class]])
    {
        rf130SSDiskStatusResponse *diskStatus = (rf130SSDiskStatusResponse*) obj;
        currentEntry.rf130Status.disk1Total = [NSNumber numberWithInteger:diskStatus.disk1Total];
        currentEntry.rf130Status.disk1Used = [NSNumber numberWithInteger:diskStatus.disk1Used];
        currentEntry.rf130Status.disk2Total = [NSNumber numberWithInteger:diskStatus.disk2Total];
        currentEntry.rf130Status.disk2Used = [NSNumber numberWithInteger:diskStatus.disk2Used];
        currentEntry.rf130Status.currentDisk = [NSNumber numberWithInteger:diskStatus.currentDisk];
        
        // Added derived fields
        currentEntry.rf130Status.disk1PercentUsedValue = 100.0*((float)diskStatus.disk1Used)/((float)diskStatus.disk1Total);
        currentEntry.rf130Status.disk2PercentUsedValue = 100.0*((float)diskStatus.disk2Used)/((float)diskStatus.disk2Total);
        
        msgCount++;
        newMsgCount++;
    }
    else if([obj isKindOfClass:[rf130SSAcquisitionStatusResponse class]])
    {
        rf130SSAcquisitionStatusResponse *aqStatus = (rf130SSAcquisitionStatusResponse*) obj;
        currentEntry.rf130Status.acquisitionActive = [NSNumber numberWithBool:aqStatus.acquisitionActive];
        currentEntry.rf130Status.acquisitionRequested = [NSNumber numberWithBool:aqStatus.acquisitionRequested];
        currentEntry.rf130Status.eventCount = [NSNumber numberWithInteger:aqStatus.eventCount];
        currentEntry.rf130Status.ramTotal = [NSNumber numberWithInteger:aqStatus.ramTotal];
        currentEntry.rf130Status.ramUsed = [NSNumber numberWithInteger:aqStatus.ramUsed];
        msgCount++;
        newMsgCount++;
    }
    else if([obj isKindOfClass:[rf130SSUnitStatusResponse class]])
    {
        rf130SSUnitStatusResponse *unitStatus = (rf130SSUnitStatusResponse*) obj;
        currentEntry.rf130Status.inputPower      =  unitStatus.inputPower;
        currentEntry.rf130Status.backupPower     =  unitStatus.backupPower;
        currentEntry.rf130Status.dasTemperature  =  unitStatus.temperature;
        msgCount++;
        newMsgCount++;
    }
    else if([obj isKindOfClass:[rf130SSExtClockStatusResponse class]])
    {
        rf130SSExtClockStatusResponse *clockStatus = (rf130SSExtClockStatusResponse*) obj;
        currentEntry.rf130Status.gpsLastLock          = [NSNumber numberWithInteger:clockStatus.lastLock];
        currentEntry.rf130Status.gpsLastLockPhase     = clockStatus.lastLockPhase;
        currentEntry.rf130Status.gpsSatsTracked       = [NSNumber numberWithInt: (int)clockStatus.satellitesTracked];
        currentEntry.rf130Status.gpsLatitude          = clockStatus.latitude;
        currentEntry.rf130Status.gpsLongitude         = clockStatus.longitude;
        currentEntry.rf130Status.gpsAltitude          = [NSNumber numberWithInt: (int)clockStatus.altitude];
        currentEntry.rf130Status.gpsMode              = clockStatus.gpsMode;
        currentEntry.rf130Status.gpsLocked            = [NSNumber numberWithBool:clockStatus.lockStatus];
        currentEntry.rf130Status.gpsOn                = [NSNumber numberWithBool:clockStatus.gpsAwake];
        msgCount++;
        newMsgCount++;
    }
    else if([obj isKindOfClass:[rf130SSVersionStatusResponse class]])
    {
        rf130SSVersionStatusResponse *verStatus = (rf130SSVersionStatusResponse*) obj;
        currentEntry.rf130Station.dasUnitId = [NSNumber numberWithInt: verStatus.header.unitID];
        currentEntry.rf130Status.dasTime = verStatus.dasTime;
        
        currentEntry.rf130Version.dasCpuVersion = verStatus.cpuVersion;
        
        unsigned long boardCount = [verStatus.boardInfos count];
        if (boardCount >= 1)
        {
            currentEntry.rf130Version.board0Revision    = ((rf130SSVersionStatusBoardInfo*) verStatus.boardInfos[0]).boardRevision;
            currentEntry.rf130Version.board0Acronym     = ((rf130SSVersionStatusBoardInfo*) verStatus.boardInfos[0]).boardAcronym;
            currentEntry.rf130Version.board0SerialNum   = ((rf130SSVersionStatusBoardInfo*) verStatus.boardInfos[0]).boardSerialNumber;
            currentEntry.rf130Version.board0FpgaNum     = ((rf130SSVersionStatusBoardInfo*) verStatus.boardInfos[0]).fpgaBoardNumber;
            currentEntry.rf130Version.board0FpgaVersion = ((rf130SSVersionStatusBoardInfo*) verStatus.boardInfos[0]).fpgaVersion;
        }
        if (boardCount >= 2)
        {
            currentEntry.rf130Version.board1Revision    = ((rf130SSVersionStatusBoardInfo*) verStatus.boardInfos[1]).boardRevision;
            currentEntry.rf130Version.board1Acronym     = ((rf130SSVersionStatusBoardInfo*) verStatus.boardInfos[1]).boardAcronym;
            currentEntry.rf130Version.board1SerialNum   = ((rf130SSVersionStatusBoardInfo*) verStatus.boardInfos[1]).boardSerialNumber;
            currentEntry.rf130Version.board1FpgaNum     = ((rf130SSVersionStatusBoardInfo*) verStatus.boardInfos[1]).fpgaBoardNumber;
            currentEntry.rf130Version.board1FpgaVersion = ((rf130SSVersionStatusBoardInfo*) verStatus.boardInfos[1]).fpgaVersion;
        }
        if (boardCount >= 3)
        {
            currentEntry.rf130Version.board2Revision    = ((rf130SSVersionStatusBoardInfo*) verStatus.boardInfos[2]).boardRevision;
            currentEntry.rf130Version.board2Acronym     = ((rf130SSVersionStatusBoardInfo*) verStatus.boardInfos[2]).boardAcronym;
            currentEntry.rf130Version.board2SerialNum   = ((rf130SSVersionStatusBoardInfo*) verStatus.boardInfos[2]).boardSerialNumber;
            currentEntry.rf130Version.board2FpgaNum     = ((rf130SSVersionStatusBoardInfo*) verStatus.boardInfos[2]).fpgaBoardNumber;
            currentEntry.rf130Version.board2FpgaVersion = ((rf130SSVersionStatusBoardInfo*) verStatus.boardInfos[2]).fpgaVersion;
        }
        if (boardCount >= 4)
        {
            currentEntry.rf130Version.board3Revision    = ((rf130SSVersionStatusBoardInfo*) verStatus.boardInfos[3]).boardRevision;
            currentEntry.rf130Version.board3Acronym     = ((rf130SSVersionStatusBoardInfo*) verStatus.boardInfos[3]).boardAcronym;
            currentEntry.rf130Version.board3SerialNum   = ((rf130SSVersionStatusBoardInfo*) verStatus.boardInfos[3]).boardSerialNumber;
            currentEntry.rf130Version.board3FpgaNum     = ((rf130SSVersionStatusBoardInfo*) verStatus.boardInfos[3]).fpgaBoardNumber;
            currentEntry.rf130Version.board3FpgaVersion = ((rf130SSVersionStatusBoardInfo*) verStatus.boardInfos[3]).fpgaVersion;
        }
        msgCount++;
        newMsgCount++;
    }
    else if([obj isKindOfClass:[rf130SSParameterStatusResponse class]])
    {
        rf130SSParameterStatusResponse *paramStatus = (rf130SSParameterStatusResponse*) obj;
        currentEntry.rf130Status.channelDasRecording = paramStatus.activeChannels;
        currentEntry.rf130Status.dasDataStreams      = paramStatus.activeDatastreams;
        
        msgCount++;
        newMsgCount++;
    }
    else if([obj isKindOfClass:[rf130SSSensorInfoStatus class]])
    {
        rf130SSSensorInfoStatus *sensorInfo = (rf130SSSensorInfoStatus*) obj;
        currentEntry.rf130Version.sensorManufacturer = sensorInfo.manufacturer;
        currentEntry.rf130Version.sensorModel        = sensorInfo.model;
        currentEntry.rf130Version.sensorSerialNumber = sensorInfo.serialNumber;
        
        msgCount++;
        newMsgCount++;
    }
    
    else if([obj isKindOfClass:[rf130PRStationParametersResponse class]])
    {
        rf130PRStationParametersResponse *stationParams = (rf130PRStationParametersResponse*) obj;
        currentEntry.rf130Station.stationNumber  = [NSNumber numberWithLong:stationParams.stationNumber];
        currentEntry.rf130Station.stationName    = stationParams.stationName;
        currentEntry.rf130Station.stationComment = stationParams.stationComment;
        
        currentEntry.rf130Station.experimentNumber  = [NSNumber numberWithLong:stationParams.experimentNumber];
        currentEntry.rf130Station.experimentName    = stationParams.experimentName;
        currentEntry.rf130Station.experimentComment = stationParams.experimentComment;
        
        msgCount++;
        newMsgCount++;
    }
    else if([obj isKindOfClass:[rf130PRChannelParameters class]])
    {
        rf130PRChannelParameters *chParams = (rf130PRChannelParameters*) obj;
        
        switch (chParams.chNum) {
            case 1:
                currentEntry.rf130Station.ch1Name               = chParams.chName;
                currentEntry.rf130Station.ch1Gain               = [NSNumber numberWithLong:chParams.gain];
                currentEntry.rf130Station.ch1SensorModel        = chParams.sensorModel;
                currentEntry.rf130Station.ch1SensorSerialNumber = chParams.sensorSerialNum;
                break;
            case 2:
                currentEntry.rf130Station.ch2Name               = chParams.chName;
                currentEntry.rf130Station.ch2Gain               = [NSNumber numberWithLong:chParams.gain];
                currentEntry.rf130Station.ch2SensorModel        = chParams.sensorModel;
                currentEntry.rf130Station.ch2SensorSerialNumber = chParams.sensorSerialNum;
                break;
            case 3:
                currentEntry.rf130Station.ch3Name               = chParams.chName;
                currentEntry.rf130Station.ch3Gain               = [NSNumber numberWithLong:chParams.gain];
                currentEntry.rf130Station.ch3SensorModel        = chParams.sensorModel;
                currentEntry.rf130Station.ch3SensorSerialNumber = chParams.sensorSerialNum;
                break;
            case 4:
                currentEntry.rf130Station.ch4Name               = chParams.chName;
                currentEntry.rf130Station.ch4Gain               = [NSNumber numberWithLong:chParams.gain];
                currentEntry.rf130Station.ch4SensorModel        = chParams.sensorModel;
                currentEntry.rf130Station.ch4SensorSerialNumber = chParams.sensorSerialNum;
                break;
            case 5:
                currentEntry.rf130Station.ch5Name               = chParams.chName;
                currentEntry.rf130Station.ch5Gain               = [NSNumber numberWithLong:chParams.gain];
                currentEntry.rf130Station.ch5SensorModel        = chParams.sensorModel;
                currentEntry.rf130Station.ch5SensorSerialNumber = chParams.sensorSerialNum;
                break;
            case 6:
                currentEntry.rf130Station.ch6Name               = chParams.chName;
                currentEntry.rf130Station.ch6Gain               = [NSNumber numberWithLong:chParams.gain];
                currentEntry.rf130Station.ch6SensorModel        = chParams.sensorModel;
                currentEntry.rf130Station.ch6SensorSerialNumber = chParams.sensorSerialNum;
                break;
                
            default:
                DDLogError(@"Error unhandled stream num");
                break;
        }

        msgCount++;
        newMsgCount++;
    }
    else if([obj isKindOfClass:[rf130PRDataStreamParamsResponse class]])
    {
        rf130PRDataStreamParamsResponse *dStreamParams = (rf130PRDataStreamParamsResponse*) obj;
        
        switch (dStreamParams.streamNumber) {
            case 1:
                currentEntry.rf130Station.stream1SampleRate = [NSNumber numberWithLong:dStreamParams.sampleRate];
                currentEntry.rf130Station.stream1Channels = dStreamParams.channels;
                break;
            case 2:
                currentEntry.rf130Station.stream2SampleRate = [NSNumber numberWithLong:dStreamParams.sampleRate];
                currentEntry.rf130Station.stream2Channels = dStreamParams.channels;
                break;
            case 3:
                currentEntry.rf130Station.stream3SampleRate = [NSNumber numberWithLong:dStreamParams.sampleRate];
                currentEntry.rf130Station.stream3Channels = dStreamParams.channels;
                break;
            case 4:
                currentEntry.rf130Station.stream4SampleRate = [NSNumber numberWithLong:dStreamParams.sampleRate];
                currentEntry.rf130Station.stream4Channels = dStreamParams.channels;
                break;
            case 5:
                currentEntry.rf130Station.stream5SampleRate = [NSNumber numberWithLong:dStreamParams.sampleRate];
                currentEntry.rf130Station.stream5Channels = dStreamParams.channels;
                break;
            case 6:
                currentEntry.rf130Station.stream6SampleRate = [NSNumber numberWithLong:dStreamParams.sampleRate];
                currentEntry.rf130Station.stream6Channels = dStreamParams.channels;
                break;
            case 7:
                currentEntry.rf130Station.stream7SampleRate = [NSNumber numberWithLong:dStreamParams.sampleRate];
                currentEntry.rf130Station.stream7Channels = dStreamParams.channels;
                break;
            case 8:
                currentEntry.rf130Station.stream8SampleRate = [NSNumber numberWithLong:dStreamParams.sampleRate];
                currentEntry.rf130Station.stream8Channels = dStreamParams.channels;
                break;
                
            default:
                DDLogError(@"Error unhandled stream num");
                break;
        }
        
        msgCount++;
        newMsgCount++;
    }
    else if([obj isKindOfClass:[rf130SSAuxDataResponse class]])
    {
        rf130SSAuxDataResponse *auxData = (rf130SSAuxDataResponse*) obj;
        
        unsigned long sensorCount = [auxData.auxSensorInfos count];
        if (sensorCount >= 1)
        {
            currentEntry.rf130Station.sensor1AuxData1 = ((rf130SSAuxSensorInfo*) auxData.auxSensorInfos[0]).aux1Data;
            currentEntry.rf130Station.sensor1AuxData2 = ((rf130SSAuxSensorInfo*) auxData.auxSensorInfos[0]).aux2Data;
            currentEntry.rf130Station.sensor1AuxData3 = ((rf130SSAuxSensorInfo*) auxData.auxSensorInfos[0]).aux3Data;
        }
        if (sensorCount >= 2)
        {
            currentEntry.rf130Station.sensor2AuxData1 = ((rf130SSAuxSensorInfo*) auxData.auxSensorInfos[1]).aux1Data;
            currentEntry.rf130Station.sensor2AuxData2 = ((rf130SSAuxSensorInfo*) auxData.auxSensorInfos[1]).aux2Data;
            currentEntry.rf130Station.sensor2AuxData3 = ((rf130SSAuxSensorInfo*) auxData.auxSensorInfos[1]).aux3Data;
        }
        if (sensorCount >= 3)
        {
            currentEntry.rf130Station.sensor3AuxData1 = ((rf130SSAuxSensorInfo*) auxData.auxSensorInfos[2]).aux1Data;
            currentEntry.rf130Station.sensor3AuxData2 = ((rf130SSAuxSensorInfo*) auxData.auxSensorInfos[2]).aux2Data;
            currentEntry.rf130Station.sensor3AuxData3 = ((rf130SSAuxSensorInfo*) auxData.auxSensorInfos[2]).aux3Data;
        }
        if (sensorCount >= 4)
        {
            currentEntry.rf130Station.sensor4AuxData1 = ((rf130SSAuxSensorInfo*) auxData.auxSensorInfos[3]).aux1Data;
            currentEntry.rf130Station.sensor4AuxData2 = ((rf130SSAuxSensorInfo*) auxData.auxSensorInfos[3]).aux2Data;
            currentEntry.rf130Station.sensor4AuxData3 = ((rf130SSAuxSensorInfo*) auxData.auxSensorInfos[3]).aux3Data;
        }
        msgCount++;
        newMsgCount++;
    }
    
    else if([obj isKindOfClass:[rf130PRSensorAutoReCenter class]])
    {
        rf130PRSensorAutoReCenter *autoReCenter = (rf130PRSensorAutoReCenter*) obj;
        
        switch (autoReCenter.sensor) {
            case 1:
                currentEntry.rf130Station.sensor1AutoReCenterEnableValue = autoReCenter.enable;
                currentEntry.rf130Station.sensor1AutoReCenterLevelVertical = autoReCenter.levelVertical;
                currentEntry.rf130Station.sensor1AutoReCenterCycleIntervalValue = (int)autoReCenter.cycleInterval;
                currentEntry.rf130Station.sensor1AutoReCenterLevelHorizontal = autoReCenter.levelHorizontal;
                currentEntry.rf130Station.sensor1AutoReCenterAttemptsValue = (int)autoReCenter.attempts;
                currentEntry.rf130Station.sensor1AutoReCenterAttemptIntervalValue = (int)autoReCenter.attemptInterval;
                break;
            case 2:
                currentEntry.rf130Station.sensor2AutoReCenterEnableValue = autoReCenter.enable;
                currentEntry.rf130Station.sensor2AutoReCenterLevelVertical = autoReCenter.levelVertical;
                currentEntry.rf130Station.sensor2AutoReCenterCycleIntervalValue = (int)autoReCenter.cycleInterval;
                currentEntry.rf130Station.sensor2AutoReCenterLevelHorizontal = autoReCenter.levelHorizontal;
                currentEntry.rf130Station.sensor2AutoReCenterAttemptsValue = (int)autoReCenter.attempts;
                currentEntry.rf130Station.sensor2AutoReCenterAttemptIntervalValue = (int)autoReCenter.attemptInterval;
                break;
            case 3:
                currentEntry.rf130Station.sensor3AutoReCenterEnableValue = autoReCenter.enable;
                currentEntry.rf130Station.sensor3AutoReCenterLevelVertical = autoReCenter.levelVertical;
                currentEntry.rf130Station.sensor3AutoReCenterCycleIntervalValue = (int)autoReCenter.cycleInterval;
                currentEntry.rf130Station.sensor3AutoReCenterLevelHorizontal = autoReCenter.levelHorizontal;
                currentEntry.rf130Station.sensor3AutoReCenterAttemptsValue = (int)autoReCenter.attempts;
                currentEntry.rf130Station.sensor3AutoReCenterAttemptIntervalValue = (int)autoReCenter.attemptInterval;
                break;
            case 4:
                currentEntry.rf130Station.sensor4AutoReCenterEnableValue = autoReCenter.enable;
                currentEntry.rf130Station.sensor4AutoReCenterLevelVertical = autoReCenter.levelVertical;
                currentEntry.rf130Station.sensor4AutoReCenterCycleIntervalValue =  (int)autoReCenter.cycleInterval;
                currentEntry.rf130Station.sensor4AutoReCenterLevelHorizontal = autoReCenter.levelHorizontal;
                currentEntry.rf130Station.sensor4AutoReCenterAttemptsValue = (int)autoReCenter.attempts;
                currentEntry.rf130Station.sensor4AutoReCenterAttemptIntervalValue = (int)autoReCenter.attemptInterval;
                break;
            default:
                DDLogError(@"Error unhandled sensor num");
                break;
        }
     
         msgCount++;
         newMsgCount++;
    }
    
    if (newMsgCount >= (totalRf130MsgCount)) // sub one since newMsgCount is 0 based
    {
        [self addAppFields]; // Grab entry time and iOS fields
        //[self addPlaceholders];
        [self commitCurrentEntry]; // commit and reset
        
        UIAlertView *msgBox = [[UIAlertView alloc]
                               initWithTitle:@"Information"
                               message: @"Automated parameters collected"
                               delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [msgBox show];
    }
}



@end
