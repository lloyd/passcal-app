//
//  cmd.m
//  passcalApp_1_X
//
//  Created by matt on 6/30/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "cmd.h"

const Byte cmdAttnByte = 0x84;
const int cmdSuffix = 4+1+1; // CCRC + <CR> + <LF>
NSString * const allUnitId = @"0000";

@implementation cmd

- (NSData *) getData{
    return data;
}

@end
