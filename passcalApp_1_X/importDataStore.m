//
//  saveFullDataStore.m
//  passcalApp_1_X
//
//  Created by field on 9/1/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "importDataStore.h"
#import "xmlDictionary.h"
#import "currentEntries.h"
#import "utils.h"

#import "Entry.h"
#import "IosFields.h"
#import "Rf130StationFields.h"
#import "Rf130StatusFields.h"
#import "Rf130VersionFields.h"
#import "UserFields.h"

#import "CocoaLumberjack.h"
static const DDLogLevel ddLogLevel = DDLogLevelInfo;

#define kCellIdentifier @"fileCell"

NSString * const importDirName = @"import";

@interface importDataStore ()
{
    NSFileManager *fileManager;
    NSString *importDir;
}
@property Entry *stationEntry;

@end

@implementation importDataStore

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.selectionIdx = -1;
    
    [self createImportDir];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kCellIdentifier];
}

- (void) createImportDir {
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    importDir = [documentsDirectory stringByAppendingPathComponent:importDirName];
    fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath:importDir]){
        if (![fileManager createDirectoryAtPath:importDir
                    withIntermediateDirectories:NO
                                     attributes:nil
                                          error:&error]) {
            DDLogError(@"Create directory error: %@", error);
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.importUrls = [fileManager
                        contentsOfDirectoryAtURL:[NSURL fileURLWithPath:importDir isDirectory:true]
                        includingPropertiesForKeys:@[] options:NSDirectoryEnumerationSkipsHiddenFiles error:nil];
    [self.tableView reloadData]; // FIXME in main thread
}

- (IBAction)import:(id)sender {
    UIAlertView *msgBox;
    
    NSString *selectedFilename = [self.importUrls[self.selectionIdx] lastPathComponent];
    NSString *fullFilename = [importDir stringByAppendingPathComponent:selectedFilename];
    NSDictionary *importDict = [NSDictionary dictionaryWithXMLFile: fullFilename];
    
    if ([@"fullExport" isEqualToString:importDict[XMLDictionaryNodeNameKey]])
        [self importFullDatabase: importDict];
    else if (@"dasFieldsComplete")
        [self importFieldsComplete:importDict];
    
    if (self.selectionIdx >= 0) {
        msgBox = [[UIAlertView alloc] initWithTitle:@"Import"
                  message: [NSString stringWithFormat:@"File import %@",
                            [self.importUrls[self.selectionIdx] lastPathComponent]]
                  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
    else {
        msgBox = [[UIAlertView alloc] initWithTitle:@"Error" message: @"Error importing"
                  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
    [msgBox show];
}

- (IBAction)refreshDirectoryContents:(id)sender {
    self.importUrls = [fileManager
                        contentsOfDirectoryAtURL:[NSURL fileURLWithPath:importDir isDirectory:true]
                        includingPropertiesForKeys:@[] options:NSDirectoryEnumerationSkipsHiddenFiles error:nil];
    [self.tableView reloadData]; // FIXME in main thread
}
- (BOOL)importFieldsComplete:(NSDictionary*) importDict {
    // Create indivdual dicts for each entity
    NSMutableDictionary *entryD = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *iosFieldsD = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *stationFieldsD = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *statusFieldsD = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *versionFieldsD = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *userFieldsD = [[NSMutableDictionary alloc] init];
    
    NSMutableDictionary *nodeID_dict = [[NSMutableDictionary alloc] init];
    
    NSArray *keyParts;
    for (NSString* importKey in importDict) {
        if ([@"__" isEqualToString:[importKey substringToIndex:2]]) // Any field with a prefix of "__" is internal xmlDict value
            continue;
        
        // if no period in key then must be a entry
        if ([importKey rangeOfString:@"."].location == NSNotFound) {
            [entryD setObject:importDict[importKey] forKey:importKey];
            continue;
        }
        keyParts = [importKey componentsSeparatedByString:@"."];
        if ([@"iosFields" isEqualToString:keyParts[0]]) {
            [iosFieldsD setObject:importDict[importKey] forKey:keyParts[1]];
        }
        else if ([@"rf130Station" isEqualToString:keyParts[0]]) {
            [stationFieldsD setObject:importDict[importKey] forKey:keyParts[1]];
        }
        else if ([@"rf130Status" isEqualToString:keyParts[0]]) {
            [statusFieldsD setObject:importDict[importKey] forKey:keyParts[1]];
        }
        else if ([@"rf130Version" isEqualToString:keyParts[0]]) {
            [versionFieldsD setObject:importDict[importKey] forKey:keyParts[1]];
        }
        else if ([@"userFields" isEqualToString:keyParts[0]]) {
            [userFieldsD setObject:importDict[importKey] forKey:keyParts[1]];
        }
        else
            DDLogError(@"Unknown entity found during import: %@", importKey);
        
        // Save off nodeIDs
        if ([@"nodeID" isEqualToString:keyParts[1]]) {
            [nodeID_dict setObject:importDict[importKey] forKey:importKey];
        }
    }
    
    if (![IosFields doesNodeIdExist:iosFieldsD[@"nodeID"]]) {
        [IosFields initWithDictionary:iosFieldsD];
    }
    if (![Rf130StationFields doesNodeIdExist:stationFieldsD[@"nodeID"]]) {
        [Rf130StationFields initWithDictionary:stationFieldsD];
    }
    if (![Rf130StatusFields doesNodeIdExist:statusFieldsD[@"nodeID"]]) {
        [Rf130StatusFields initWithDictionary:statusFieldsD];
    }
    if (![Rf130VersionFields doesNodeIdExist:versionFieldsD[@"nodeID"]]) {
        [Rf130VersionFields initWithDictionary:versionFieldsD];
    }
    if (![UserFields doesNodeIdExist:userFieldsD[@"nodeID"]]) {
        [UserFields initWithDictionary:userFieldsD];
    }
    Entry *newEntry;
    if (![Entry doesNodeIdExist:entryD[@"nodeID"]]) {
        newEntry = [Entry initWithDictionary:entryD];
    }
    if (!newEntry)
        return false;
    
    [Entry updateRelations:newEntry.nodeID :nodeID_dict];
    
    [IosFields commit];
    [Rf130StationFields commit];
    [Rf130StatusFields commit];
    [Rf130VersionFields commit];
    [UserFields commit];
    [Entry commit];
    
    
    return true;
}

- (BOOL)importFullDatabase:(NSDictionary*) importDict {
    NSArray *keyDescriptors;
    for (NSString *key in importDict) {
        
        if ([@"__" isEqualToString:[key substringToIndex:2]]) // Any field with a prefix of "__" is internal xmlDict value
             continue;
        
        keyDescriptors = [key componentsSeparatedByString:@"_"];
        if ([@"Entry" isEqualToString:keyDescriptors[0]]) {  // Handle these in a seperate loop
            if ([@"relations" isEqualToString:keyDescriptors[1]])
                 continue;
            if (![Entry doesNodeIdExist:importDict[key][@"nodeID"]]) {
                [Entry initWithDictionary:importDict[key]]; // Figure out if I need to save this or
            }
        }
        else if ([@"IosFields" isEqualToString:keyDescriptors[0]]) {
            if (![IosFields doesNodeIdExist:importDict[key][@"nodeID"]]) {
                [IosFields initWithDictionary:importDict[key]]; // Figure out if I need to save this or
            }
        }
        else if ([@"Rf130StationFields" isEqualToString:keyDescriptors[0]]) {
            if (![Rf130StationFields doesNodeIdExist:importDict[key][@"nodeID"]]) {
                [Rf130StationFields initWithDictionary:importDict[key]]; // Figure out if I need to save this or
            }
        }
        else if ([@"Rf130StatusFields" isEqualToString:keyDescriptors[0]]) {
            if (![Rf130StatusFields doesNodeIdExist:importDict[key][@"nodeID"]]) {
                [Rf130StatusFields initWithDictionary:importDict[key]]; // Figure out if I need to save this or
            }
        }
        else if ([@"Rf130VersionFields" isEqualToString:keyDescriptors[0]]) {
            if (![Rf130VersionFields doesNodeIdExist:importDict[key][@"nodeID"]]) {
                [Rf130VersionFields initWithDictionary:importDict[key]]; // Figure out if I need to save this or
            }
        }
        else if ([@"UserFields" isEqualToString:keyDescriptors[0]]) {
            if (![UserFields doesNodeIdExist:importDict[key][@"nodeID"]]) {
                [UserFields initWithDictionary:importDict[key]]; // Figure out if I need to save this or
            }
        }
        else {
            DDLogError(@"Unrecognized entity name: %@", keyDescriptors[0]);
        }
    }
    
    for (NSString *key in importDict) {
        if ([@"__" isEqualToString:[key substringToIndex:2]]) // Any field with a prefix of "__" is internal xmlDict value
            continue;
        
        keyDescriptors = [key componentsSeparatedByString:@"_"];
        if ([@"Entry" isEqualToString:keyDescriptors[0]]) {  // Handle these in a seperate loop
            if ([@"relations" isEqualToString:keyDescriptors[1]]){
                [Entry updateRelations:keyDescriptors[2] :importDict[key]];
            }
                
        }
    }
    
    [IosFields commit];
    [Rf130StationFields commit];
    [Rf130StatusFields commit];
    [Rf130VersionFields commit];
    [UserFields commit];
    [Entry commit];
    
    return true;
}

- (void) appendDictionaryToFile:(NSFileHandle *)fileHandle inDict:(NSDictionary *)inDict {
    if (fileHandle)
    {
        NSString *xmlStr = [[NSString alloc] initWithFormat:@"%@\n", [inDict XMLString]];
        [fileHandle seekToEndOfFile];
        [fileHandle writeData:[xmlStr dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.importUrls count];
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger rowIdx = [indexPath row];
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    
    cell.textLabel.text = [self.importUrls[rowIdx] lastPathComponent];
    if (rowIdx == self.selectionIdx)
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    else
        cell.accessoryType = UITableViewCellAccessoryNone;
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger rowIdx = [indexPath row];
    self.selectionIdx = rowIdx;
    
    [self.tableView reloadData]; // FIXME in main thread
}

@end