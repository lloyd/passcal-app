//
//  saveExport.h
//
// This class implements the GUI view which allows a user to export a entry
// as an XML file.  The station URI is passed by the calling segue.

#import <UIKit/UIKit.h>

@interface saveExport : UIViewController <UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate>

@property NSURL *stationURI;
@property NSString *outFilename;
@property NSInteger pickedRow;

@property (weak, nonatomic) IBOutlet UIPickerView *outputFormatPicker;
@property (strong, nonatomic) IBOutlet UITextField *outFilenameTextField;

- (IBAction)saveButton:(id)sender;
- (IBAction)resetFilenameButton:(id)sender;

@end