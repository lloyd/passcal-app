//
//  dasConnection.h
//
// This class in incharge of managing a TCP connection to the DAS.  When data is received
// it acts as a publisher.  Note currently code is very focused on RF130 responses but could
// be generalized for other network based DASes

#import <Foundation/Foundation.h>
#import "cmd.h"
#include "publisher.h"

@interface dasConnection : NSObject <NSStreamDelegate, publisher>

// General TCP Connection properties and methods
@property BOOL connected;
@property BOOL busy;
// open
// Attempts to open TCP socket to static address and port
- (void) open;

// close
// Closes TCP connection if open
- (void) close;

// retryConnection
// calls close and then open methods
- (void) retryConnection;

// send
// Sends give byte data over TCP connection
- (void) send:(NSData*)data;

// Sending of cmd abstract class
// If a RF130 ID message has been seen this will capture output and store for
// later querying
- (uint16_t) getUnitId;

// sendCmd
// Convienance method for sending cmds
- (void) sendCmd:(cmd*)cmd;

// Publisher interface
@property NSMutableArray *subscribers;
// synthesized - (void) regSubscriber: (id<subscriber>) sub;
@end
