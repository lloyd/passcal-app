//
//  dasConnection.m
//  passcalApp_1_X
//
//  Created by matt on 6/29/15.
//  Copyright (c) 2015 rev0Tech. All rights reserved.
//

#import "dasConnection.h"
#import "rf130ResponseFactory.h"
#import "rf130IdResponse.h"
#import "subscriber.h"
#import "utils.h"

#import "CocoaLumberjack.h"
static const DDLogLevel ddLogLevel = DDLogLevelInfo;

//const NSString* rf130Addr = @"192.168.4.1";
const UInt32 rf130Port = 2000;

@interface dasConnection ()
{
    NSInputStream *inStream;
    NSOutputStream *outStream;
    NSMutableData *outData;
    uint16_t connectedUnitId;
}

- (void) updateDasInfo : (uint16_t) unitID;
- (void) sendResp2Subs : (response*) resp;

@end

@implementation dasConnection

- (void) regSubscriber: (id<subscriber>) sub {
    if (self.subscribers == nil) {
        self.subscribers = [[NSMutableArray alloc] init];
    }
    [self.subscribers addObject:sub];
}

- (void) sendResp2Subs : (response*) resp {
    if (self.subscribers == nil)
        return;
    
    for (id<subscriber> sub in self.subscribers)
    {
        [sub recvObject: resp];
    }
}


- (void) open {
    self.busy = true;
    connectedUnitId = 0000;
    
    NSString *ipAddr = [utils getWifiIpv4];
    NSString *rf130Addr = [utils changeLastOctet:ipAddr : 1];
    
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    CFStreamCreatePairWithSocketToHost(NULL, (__bridge CFStringRef)rf130Addr, rf130Port,
                                              &readStream, &writeStream);
    
    inStream = (__bridge NSInputStream *)readStream;
    outStream = (__bridge NSOutputStream *)writeStream;
    [inStream setDelegate:self];
    [outStream setDelegate:self];
    
    [inStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [outStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    
    [inStream open];
    [outStream open];
    
}
- (void) close {
    
    connectedUnitId = 0000;
    self.connected = false;
    
    [inStream close];
    [outStream close];
    
    [inStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [outStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    
    inStream = nil;
    outStream = nil;
    outData = nil;
    
    DDLogInfo(@"dasConnection closed");
}
- (void) retryConnection {
    [self close];
    [self open];
}

- (void) send:(NSData*)data {
    
    // TODO check that all the data was sent
    NSInteger i = [outStream write:[data bytes] maxLength:[data length]];
    
    DDLogInfo(@"Num of bytes written %ld", (long)i);
    
}

- (void) updateDasInfo:(uint16_t) unitID {
    connectedUnitId = unitID;
}
- (uint16_t) getUnitId {
    return connectedUnitId;
}

- (void) sendCmd:(cmd*)cmd {
    
    NSData *data = [cmd getData];
    [self send:data];
}

- (void)stream:(NSStream *)theStream handleEvent:(NSStreamEvent)StreamEvent
{
    uint8_t buffer[4096];
    static uint8_t saveBuffer[4096];
    static size_t saveBufferLen=0;
    NSInteger len;
    uint16_t usedLen;
    response *resp = nil;
    switch (StreamEvent)
    {
        case NSStreamEventOpenCompleted:
            self.connected = true;
            self.busy = false;
            DDLogInfo(@"TCP Client - Stream opened");
            break;
            
        case NSStreamEventHasBytesAvailable:
            if (theStream == inStream)
            {
                while ([inStream hasBytesAvailable])
                {
                    len=0;
                    if (saveBufferLen > 0) // Copy existing bytes first
                    {
                        memcpy(buffer, saveBuffer, saveBufferLen);
                        len=saveBufferLen;
                    }
                    
                    len += [inStream read:&(buffer[saveBufferLen]) maxLength:sizeof(buffer)-saveBufferLen];
                    usedLen = 0;
                    saveBufferLen = 0;
                    NSString *output = [[NSString alloc] initWithBytes:buffer length:len encoding:NSASCIIStringEncoding];
                    if (output != nil)
                    {
                        DDLogInfo(@"TCP Client - Server sent %ld bytes: %@", (long)len, output);
                    }
                    while (len > 0 && usedLen < len)
                    {
                        resp = [rf130ResponseFactory createResponse:&(buffer[usedLen]):len-usedLen];
                        
                        if (resp == nil)
                            break; // If not recognized value bail
                        else if ([resp isKindOfClass:[NSNull class]]) // Happens when message not found
                                                                      // The rest of the message might come
                                                                      // in the next packet.
                        {
                            saveBufferLen = len-usedLen;
                            memcpy(saveBuffer, &(buffer[usedLen]), saveBufferLen);
                            break;
                        }
                       
                        DDLogInfo(@"Got response with code %@", resp.header.cmdCode);
                        
                        [self sendResp2Subs:resp];
                        usedLen += resp.rawMessageSize;
                        
                        if ([resp isKindOfClass:[rf130IdResponse class]]) // If ID Message
                        {
                            [self updateDasInfo : resp.header.unitID];
                        }
                    }
                }
            }
            break;
            
        case NSStreamEventErrorOccurred:
            DDLogWarn(@"TCP Client - Can't connect to the host");
            self.busy = false;
            self.connected = false;
            break;
            
        case NSStreamEventEndEncountered:
            DDLogInfo(@"TCP Client - End encountered");
            [theStream close];
            [theStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
            break;
            
        case NSStreamEventNone:
            DDLogInfo(@"TCP Client - None event");
            break;
            
        case NSStreamEventHasSpaceAvailable:
            DDLogInfo(@"TCP Client - Has space available event");
            if (outData != nil)
            {
                //Send rest of the packet
                NSInteger ActualOutputBytes = [outStream write:[outData bytes] maxLength:[outData length]];
                
                if (ActualOutputBytes >= [outData length])
                {
                    //It was all sent
                    outData = nil;
                }
                else
                {
                    //Only partially sent
                    [outData replaceBytesInRange:NSMakeRange(0, ActualOutputBytes)
                        withBytes:NULL length:0];		//Remove sent bytes from the start
                }
            }
            break;
            
        default:
            DDLogWarn(@"TCP Client - Unknown event");
    }
    
}

@end
